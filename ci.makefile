
all: clean pytest dredd

esa-base:
	docker build --tag esa-base .

rm-dist:
	rm -rf ./dist

create-dist:
	python setup.py sdist

x-build: rm-dist create-dist

build: esa-base x-build

esa-deploy:
	docker build --cache-from=esa-deploy --tag esa-deploy .

esa-pytest: esa-deploy
	docker build --tag esa-pytest .

pytest: esa-pytest
	docker run --name esa-pytest-cont esa-pytest make --file test.makefile pytest
	docker cp esa-pytest-cont:/app/report.xml .
	docker container rm esa-pytest-cont

pytest-mb: esa-deploy
	docker-compose \
	--file docker-compose.yml \
	--file docker-compose.test.yml \
	up --abort-on-container-exit --exit-code-from=pytest \
	esa pytest mc sc

shell-pytest: esa-pytest
	docker run --tty --interactive esa-pytest bash

dredd: esa-deploy
	touch ./dredd-hooks-output.txt
	docker-compose --file docker-compose.yml --file docker-compose.test.yml down
	docker-compose --file docker-compose.yml --file docker-compose.test.yml up \
	--build --abort-on-container-exit --exit-code-from=dredd  dredd

clean:
	rm -fr dist
	docker image rm --force esa-base esa-deploy esa-pytest

cypress-run:
	docker-compose --file docker-compose.yml --file docker-compose.test.yml down
	docker-compose --file docker-compose.yml --file docker-compose.test.yml up --detach esa gui nginx e2e-cypress
	docker exec e2e-cypress /usr/wait-for-it.sh --timeout=300 gui:8080
	docker exec e2e-cypress /usr/wait-for-it.sh --timeout=300 esa:5000
	docker exec e2e-cypress npx cypress run
	docker exec e2e-cypress npx nyc report --reporter=text-summary


## !
## Set the required module nickname
MODULE_SHORT_NAME=esa

## !
## Set the required docker image TAG
MODULE_TAG=v1.2.2

## !
# Set the required MODULE_DB_INIT value that defines a necessity to initialize module db or data files
#   1 - reinitialize and repopulate db or data files with initial data
#   0 - reuse the current database or data files created after the previous deployment
# ! make sure that MODULE_DB_INIT = 1 for initial deployment
MODULE_DB_INIT=1
#MODULE_DB_INIT=0

## !
## Update the values of CI_REGISTRY_IMAGE to your module registry
CI_REGISTRY_IMAGE?=registry.gitlab.com/dtoceanplus/dtop_esa

## !
## Set the value to cors urls
ALL_MODULES_NICKNAMES := si sg sc mc ec et ed sk lmo spey rams slc esa cm mm

## !
## Set DTOP Basic Authentication Header if necessary.
## That is Base64 encoded values of username: and password: that are requested for initial access to modules
## Basic Authentication Header Generator can be used, for example - https://www.blitter.se/utils/basic-authentication-header-generator/.
## For example: username: "admin@dtop.com", password: "j2zjf#afw21"  
## "Basic YWRtaW5AZHRvcC5jb206ajJ6amYjYWZ3MjE="
DTOP_BASIC_AUTH=Basic YWRtaW5AZHRvcC5jb206ajJ6amYjYWZ3MjE=

login:
	echo ${CI_REGISTRY_PASSWORD} | docker login --username ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY_IMAGE}

logout:
	docker logout ${CI_REGISTRY_IMAGE}

build-prod-be:
	docker pull ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} || true
	docker build --cache-from ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} \
	  --tag ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} \
	  --file ./${MODULE_SHORT_NAME}-prod.dockerfile \
          .
	docker push ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG}
	docker images

build-prod-fe:
	docker pull ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} || true
	docker build --build-arg DTOP_BASIC_AUTH="${DTOP_BASIC_AUTH}" --build-arg DTOP_MODULE_SHORT_NAME="${MODULE_SHORT_NAME}" \
		--cache-from ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} \
		--tag ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} \
		--file ./src/dtop_esa/GUI/frontend-prod.dockerfile \
		./src/dtop_esa
	docker push ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG}
	docker images