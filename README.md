# Overview

This repository is dedicated to the Environmental and Social Assessment tool of DTOceanPlus


## Installation of ESA module 

You will need to have the following tools installed on your machine:

Git (see for example https://git-scm.com/downloads)

Docker (see for example https://docs.docker.com/docker-for-windows/install/)

Start by cloning the ESA module gitlab repo:

```bash
git clone git@gitlab.com:fem-dtocean/dtop-esa.git
```
use the master branch

Build the module, launch the application and launch the GUI with the following command:
```bash
docker-compose up
```

Access the GUI in a web browser at the following address:
http://127.0.0.1:8080


# Documentation

Documentation for the full suite of tools is available at the temporary link; https://wave-energy-scotland.gitlab.io/dtoceanplus/dtop_documentation/deployment/esa/docs/index.html.
This documentation will be moved to a permanent address upon the final release of the suite of tools. 

