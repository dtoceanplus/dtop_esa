# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import atexit
import requests
import pytest
from pact import Consumer, Like, Provider, Term

pact = Consumer("esa").has_pact_with(Provider("lmo"), port=1238)
pact.start_service()
atexit.register(pact.stop_service)

LMO_PHASES = {
    "downtime": {
        "project_life": 20,
        "consuption": {
            "value": 740565.0
        }
    }
}

def test_lmo_results():
    pact.given(
        "lmo 1 exists and it has results"
    ).upon_receiving(

        
        "a request for lmo"
    ).with_request(
        "GET", Term(r'/lmo/\d+/phases', '/lmo/1/phases')
    ).will_respond_with(200, body=Like(LMO_PHASES))

    with pact:
        uri = 'http://localhost:1238/lmo/1/phases'
        result = requests.get(uri)
        assert result.status_code == 200
