# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import atexit
import requests
import pytest
from pact import Consumer, Like, Provider, Term

pact = Consumer("esa").has_pact_with(Provider("mc"), port=1237)
pact.start_service()
atexit.register(pact.stop_service)

MC_DIMENSIONS = {
    "width": 10,
    "height": 10,
    "wet_area": 1001
}

def test_mc_dimensions():
    pact.given(
        "mc 1 exists and it has dimensions"
    ).upon_receiving(
        "a request for dimensions"
    ).with_request(
        "GET", Term(r'/mc/\d+/dimensions', '/mc/1/dimensions')
    ).will_respond_with(200, body=Like(MC_DIMENSIONS))

    with pact:
        uri = 'http://localhost:1237/mc/1/dimensions'
        result = requests.get(uri)
        assert result.status_code == 200
