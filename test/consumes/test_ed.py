# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import atexit
import requests
import pytest
from pact import Consumer, Like, Provider, Term

pact = Consumer("esa").has_pact_with(Provider("ed"), port=1236)
pact.start_service()
atexit.register(pact.stop_service)

ED_RESULTS = {
    "export_cable_total_length": 1.5,
    "array_cable_total_length": 1.5,
    "umbilical_cable_total_length": 499.93
}

def test_ed_results():
    pact.given(
        "ed 1 exists at cpx 3"
    ).upon_receiving(
        "a request for outputs"
    ).with_request(
        "GET", Term(r'/api/energy-deliv-studies/\d+/results', '/api/energy-deliv-studies/1/results')
    ).will_respond_with(200, body=Like(ED_RESULTS))

    with pact:
        uri = 'http://localhost:1236/api/energy-deliv-studies/1/results'
        result = requests.get(uri)
        assert result.status_code == 200
