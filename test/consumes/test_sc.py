# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import atexit
import requests
import pytest
from pact import Consumer, Like, Provider, Term

pact = Consumer("esa").has_pact_with(Provider("sc"), port=1239)
pact.start_service()
atexit.register(pact.stop_service)

SC_FARM = {
    "direct_values": {
        "bathymetry": {
            "value": [
                50.0
            ]
        },
        "seabed_type": {
            "value": [
                'test'
            ]
        },
        "marine_species": {
            "species": [
                'test'
            ],
            "probability": [
                [0.0]
            ]
        }
    },
    "info": {
        "surface": 0.0
    }
}

def test_sc_farm():
    pact.given(
        "sc 1 exists and it has farm"
    ).upon_receiving(
        "a request for sc"
    ).with_request(
        "GET", Term(r'/sc/\d+/farm', '/sc/1/farm')
    ).will_respond_with(200, body=Like(SC_FARM))

    with pact:
        uri = 'http://localhost:1239/sc/1/farm'
        result = requests.get(uri)
        assert result.status_code == 200

SC_POINT = {
    "direct_values": {
        "bathymetry": {
            "longitude": 50.0,
            "latitude": 50.0
        }
    }
}

def test_sc_point():
    pact.given(
        "sc 1 exists and it has point"
    ).upon_receiving(
        "a request for sc"
    ).with_request(
        "GET", Term(r'/sc/\d+/point', '/sc/1/point')
    ).will_respond_with(200, body=Like(SC_POINT))

    with pact:
        uri = 'http://localhost:1239/sc/1/point'
        result = requests.get(uri)
        assert result.status_code == 200


SC_CURRENT = {
    "mag": {
        "values": [
            0.0
        ]
    },
    "theta": {
        "values": [
            0.0
        ]
    }
}

def test_sc_point():
    pact.given(
        "sc 1 exists and it has point"
    ).upon_receiving(
        "a request for sc"
    ).with_request(
        "GET", Term(r'/sc/\d+/point/time_series/currents', '/sc/1/point/time_series/currents')
    ).will_respond_with(200, body=Like(SC_CURRENT))

    with pact:
        uri = 'http://localhost:1239/sc/1/point/time_series/currents'
        result = requests.get(uri)
        assert result.status_code == 200
