# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import atexit
import requests
import pytest
from pact import Consumer, Like, Provider, Term

pact = Consumer("esa").has_pact_with(Provider("et"), port=1235)
pact.start_service()
atexit.register(pact.stop_service)

ET_MATERIALS = {
    "materials": {
        "value": [
            {
                "material_name": "steel",
                "material_qantity": 426000.0
            }
        ]

    }
}

def test_et_results():
    pact.given(
        "et 1 exists and has materials for pto"
    ).upon_receiving(

        
        "a request for array"
    ).with_request(
        "GET", Term(r'/energy_transf/\d+/array', '/energy_transf/1/array')
    ).will_respond_with(200, body=Like(ET_MATERIALS))

    with pact:
        uri = 'http://localhost:1235/energy_transf/1/array'
        result = requests.get(uri)
        assert result.status_code == 200
