# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import atexit
import requests
import pytest
from pact import Consumer, Like, Provider, Term

pact = Consumer("esa").has_pact_with(Provider("sk"), port=1240)
pact.start_service()
atexit.register(pact.stop_service)

SK_FARM = {
    "footprint": 215964.50557754177,
    "submerged_surface": 830.3191672712449,
    "material_quantity_list": [
        {
            "material_name": "steel",
            "material_quantity": 312588.64879567665
        }
    ]
}

def test_sk_farm():
    pact.given(
        "sk 1 exists and it has environmental impact"
    ).upon_receiving(

        
        "a request for sk"
    ).with_request(
        "GET", Term(r'/sk/\d+/environmental_impact', '/sk/1/environmental_impact')
    ).will_respond_with(200, body=Like(SK_FARM))

    with pact:
        uri = 'http://localhost:1240/sk/1/environmental_impact'
        result = requests.get(uri)
        assert result.status_code == 200
