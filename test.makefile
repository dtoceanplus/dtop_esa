SHELL:=bash

# clean:
# 	conda remove --name dtop_esa --all

# reset: clean
# 	conda env create --name dtop_esa --file dependency.yml --force
# 	conda env list

# -------------------------------------------
# Run Pytest tests locally

x-rm-dist:
	rm -rf ./dist

x-create-dist:
	python setup.py sdist

x-uninstall:
	pip uninstall --yes dtop_esa

x-install: x-uninstall x-rm-dist x-create-dist
	pip install dtop_esa --no-cache-dir --find-links dist

pytest: x-install x-pytest-prepare x-pytest

x-pytest-prepare:
	pip install pytest pytest-cov
	pip install dtop_esa --no-cache-dir --find-links dist

x-pytest:
	python -m pytest --cov=dtop_esa --junitxml=report.xml --ignore=test/integration --ignore=src/dtop_esa/GUI

# -------------------------------------------
# Run Dredd tests locally

dredd: x-install x-dredd-prepare x-dredd

x-dredd-prepare:
	pip install python-dotenv
	pip install dredd_hooks
	npm uninstall dredd --global
	npm install dredd@12.2.1 --global

x-dredd:
	bash -c "dredd --config dredd.yml"

# -------------------------------------------
# Run example (needs httpie, launch flask)

run:
	http 127.0.0.1:5000/esa/testname/run < ./src/dtop_esa/data_test/test_inputs_integrated.json
