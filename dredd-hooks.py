import json
import sys
import requests
import dredd_hooks as hooks
from dredd_utils import *


project_id = 1
project_id_404 = 2

@hooks.before("/esa/{ProjectId} > ESA project results > 404")
@hooks.before("/esa/{ProjectId}/endangered_species > endangered species > 404")
@hooks.before("/esa/{ProjectId}/environmental_impact_assessment > environmental impact assessment results > 404")
# @hooks.before("/esa/{ProjectId}/environmental_impact_assessment/eia_global > environmental impact assessment eia_global results > 404")
# @hooks.before("/esa/{ProjectId}/environmental_impact_assessment/eia_tech_group > environmental impact assessment eia_tech_group results > 404")
# @hooks.before("/esa/{ProjectId}/environmental_impact_assessment/eia_pressure > environmental impact assessment eia_pressure results > 404")
@hooks.before("/esa/{ProjectId}/carbon_footprint > carbon footprint results > 404")
# @hooks.before("/esa/{ProjectId}/carbon_footprint/CFP_global > carbon footprint CFP_global results > 404")
# @hooks.before("/esa/{ProjectId}/carbon_footprint/CFP_phase > carbon footprint CFP_phase results > 404")
@hooks.before("/esa/{ProjectId}/social_acceptance > social acceptance results > 404")
@hooks.before('/esa/{ProjectId}/inputs > post input data and save it to database > 201')
@hooks.before('/esa/{ProjectId}/run > post input data and save it to database, run analysis and save results to database > 201')
@hooks.before('/esa/{ProjectId}/inputs > post input data and save it to database > 400')
@hooks.before('/esa/{ProjectId}/run > post input data and save it to database, run analysis and save results to database > 400')
def skip_test(transaction):
    transaction['skip'] = True


@hooks.before("/esa/{ProjectId} > ESA project results > 200 > application/json")
@hooks.before("/esa/{ProjectId}/endangered_species > endangered species > 200 > application/json")
@hooks.before("/esa/{ProjectId}/environmental_impact_assessment > environmental impact assessment results > 200 > application/json")
# @hooks.before("/esa/{ProjectId}/environmental_impact_assessment/eia_global > 200 > application/json")
# @hooks.before("/esa/{ProjectId}/environmental_impact_assessment/eia_tech_group > 200 > application/json")
# @hooks.before("/esa/{ProjectId}/environmental_impact_assessment/eia_pressure > 200 > application/json")
@hooks.before("/esa/{ProjectId}/carbon_footprint > carbon footprint results > 200 > application/json")
# @hooks.before("/esa/{ProjectId}/carbon_footprint/CFP_global > 200 > application/json")
# @hooks.before("/esa/{ProjectId}/carbon_footprint/CFP_phase > 200 > application/json")
@hooks.before("/esa/{ProjectId}/social_acceptance > social acceptance results > 200 > application/json")
@if_not_skipped
def replace_project_id_by_existing_project(transaction):
    uri = transaction['request']['uri']
    full_path = transaction['fullPath']
    idx = uri.replace('/esa/', '') 
    if 'endangered_species' in uri:
        idx = idx.replace('/endangered_species', '')
    # if 'environmental_impact_assessment' in uri:
    #     idx = idx.replace('/environmental_impact_assessment', '')
    #     if 'eia_global' in uri:
    #         idx = idx.replace('/eia_global', '')
    #     if 'eia_tech_group' in uri:
    #         idx = idx.replace('/eia_tech_group', '')
    #     if '/eia_pressure' in uri:
    #         idx = idx.replace('/eia_pressure', '')
    # if 'carbon_footprint' in uri:
    #     idx = idx.replace('/carbon_footprint', '')
    #     if 'CFP_global' in uri:
    #         idx = idx.replace('/CFP_global', '')
    #     if 'CFP_phase' in uri:
    #         idx = idx.replace('/CFP_phase', '')
    if 'social_acceptance' in uri:
        idx = idx.replace('/social_acceptance', '')
    transaction['fullPath'] = full_path.replace(idx, str(project_id))


@hooks.before("/esa/{ProjectId}/endangered_species > endangered species > 500")
@hooks.before("/esa/{ProjectId}/environmental_impact_assessment > environmental impact assessment results > 500")
# @hooks.before("/esa/{ProjectId}/environmental_impact_assessment/eia_global > environmental impact assessment eia_global results > 500")
# @hooks.before("/esa/{ProjectId}/environmental_impact_assessment/eia_tech_group > environmental impact assessment eia_tech_group results > 500")
# @hooks.before("/esa/{ProjectId}/environmental_impact_assessment/eia_pressure > environmental impact assessment eia_pressure results > 500")
@hooks.before("/esa/{ProjectId}/carbon_footprint > carbon footprint results > 500")
# @hooks.before("/esa/{ProjectId}/carbon_footprint/CFP_global > carbon footprint CFP_global results > 500")
# @hooks.before("/esa/{ProjectId}/carbon_footprint/CFP_phase > carbon footprint CFP_phase results > 500")
@hooks.before("/esa/{ProjectId}/social_acceptance > social acceptance results > 500")
@hooks.before("/esa/{ProjectId} > ESA project results > 500")
@if_not_skipped
def replace_project_id_by_non_existing_project(transaction):
    uri = transaction['request']['uri']
    full_path = transaction['fullPath']
    if '1' in full_path:
        transaction['fullPath'] = full_path.replace('1', 'non_existing_project')
    idx = uri.replace('/esa/', '') 
    if 'endangered_species' in uri:
        idx = idx.replace('/endangered_species', '')
    # if 'environmental_impact_assessment' in uri:
    #     idx = idx.replace('/environmental_impact_assessment', '')
    #     if 'eia_global' in uri:
    #         idx = idx.replace('/eia_global', '')
    #     if 'eia_tech_group' in uri:
    #         idx = idx.replace('/eia_tech_group', '')
    #     if '/eia_pressure' in uri:
    #         idx = idx.replace('/eia_pressure', '')
    # if 'carbon_footprint' in uri:
    #     idx = idx.replace('/carbon_footprint', '')
    #     if 'CFP_global' in uri:
    #         idx = idx.replace('/CFP_global', '')
    #     if 'CFP_phase' in uri:
    #         idx = idx.replace('/CFP_phase', '')
    if 'social_acceptance' in uri:
        idx = idx.replace('/social_acceptance', '')
