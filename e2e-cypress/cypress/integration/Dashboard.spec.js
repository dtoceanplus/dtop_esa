context('Dashboard', () => {
    beforeEach(() => {
      cy.visit('/#/dashboard')
    })
  
    it('Have 2 images', () => {
      cy.url().should('include', 'dashboard')
      cy.get('#module_image')
      cy.get('#dtoceanplus_logo')
    })

    it('Have a sidebar', () => {
        cy.get('.hamburger').should('be.visible')
        cy.get('.scrollbar-wrapper').should('not.be.visible')
        cy.get('.hamburger').click()
        cy.get('.scrollbar-wrapper').should('be.visible')
    })

    it('Have an image to enter the module', () => {
        cy.get('#module_image').click()
        cy.wait(2000)
        cy.url().should('include', 'esahome')
    })
  
  })