FROM continuumio/miniconda3

WORKDIR /app

COPY dependency.yml .
RUN conda env create -f dependency.yml
RUN pip install python-dotenv

RUN echo "source activate dtop_esa" >> ~/.bashrc
ENV PATH /opt/conda/envs/dtop_esa/bin:$PATH

COPY src ./src
COPY setup.py .
RUN pip install -e .

EXPOSE 5000
COPY .flaskenv .

RUN conda install nodejs
RUN npm install dredd@12.2.1 --global
RUN pip install dredd_hooks requests