FROM continuumio/miniconda3

COPY dependency.yml .
RUN conda env create -f dependency.yml
RUN pip install python-dotenv

RUN echo "source activate dtop_site" >> ~/.bashrc
ENV PATH /opt/conda/envs/dtop_esa/bin:$PATH

COPY . /app
WORKDIR /app
RUN pip install -e .

EXPOSE 5000
COPY .flaskenv .
COPY test.makefile .
