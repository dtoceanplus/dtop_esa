# This file is for creating Python package.

# Packaging Python Projects: https://packaging.python.org/tutorials/packaging-projects/
# Setuptools documentation: https://setuptools.readthedocs.io/en/latest/index.html

import setuptools

setuptools.setup(
    name="dtop-esa",
    version="0.0.1",
    packages=setuptools.find_packages(where='src'),
    package_dir={'':'src'},
    install_requires=[
        "flask",
        "flask-babel",
        "flask_cors",
        "requests",
        "numpy",
        "pandas",
        "shapely",
        "scipy",
        "netCDF4",
        "pytest"
    ],
    include_package_data=True,
    zip_safe=False,
)
