# Makefile

.PHONY: build run dev compose verify

build:
	docker build --tag esa .

run:
	docker run --detach -p 5000:5000 esa

dev:
	docker run --tty --interactive esa bash

compose:
	docker-compose up esa


verify:
	make -f ci.makefile esa-deploy
	docker-compose --project-name esa -f verifiable.docker-compose -f verifier.docker-compose run verifier
	docker-compose --project-name esa -f verifiable.docker-compose stop esa
	docker cp `docker-compose --project-name esa -f verifiable.docker-compose ps -q`:/app/.coverage.make-verify

impose:
	make -f ci.makefile esa-deploy
	docker-compose run --rm contracts
