import sys

import requests

sys.stdout = sys.stderr = open("dredd-hooks-output.txt", "w")

def if_not_skipped(func):
    def wrapper(transaction):
        if not transaction['skip']:
            func(transaction)
    return wrapper 
