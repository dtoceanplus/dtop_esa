# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections 
import numpy as np 
import pandas as pd
import os
import pytest
import json 
from dtop_esa.Business.libraries.dtoesa.outputs.Project import Project
from dtop_esa.Business.libraries.dtoesa.inputs import Inputs
from dtop_esa.Business.main import process_inputs_EIA, run_main


dir_path = os.path.dirname(os.path.realpath(__file__))


#inputs_esa =  dir_path + '/../ressources/inputs.json'
dict_fuel_file          = dir_path + '/../ressources/dict_fuel.json'
dict_material_file      = dir_path + '/../ressources/dict_materials.json'
dict_gases_file         = dir_path + '/../ressources/dict_gases_impact_factor.json'
database_species_file   = dir_path + '/../ressources/database_species.json'
inputs_file   = dir_path + '/../data_test/test_inputs_standalone.json'

dict_fuel                   = json.load(open(dict_fuel_file))
dict_materials              = json.load(open(dict_material_file))
dict_gases_impact_factor    = json.load(open(dict_gases_file))
database_species            = json.load(open(database_species_file))
inputs  = json.load(open(inputs_file))



inputs_ESA = Inputs.Inputs()
inputs_ESA.loadFromJSONDict(inputs)
inputs_ESA2 = Inputs.Inputs()
inputs_ESA2.loadFromJSONDict(inputs)
inputs_ESA2.inputs_ESA.electrical_info.Fishery_Restriction_Surface_around_cables = 39090
inputs_ESA2.inputs_ESA.device_info.coordinates_devices_x = [29.3]
inputs_ESA2.inputs_ESA.farm_info.zone_type = "sea loch entrances"
inputs_ESA2.inputs_ESA.device_info.machine_type = "TEC"
inputs_ESA2.inputs_ESA.electrical_info.Burial == True

def test_process_inputs_EIA():
	# inputs = inputs_ESA.prop_rep()
	[my_inputs, receptors_table, protected_table, weighting_table] = process_inputs_EIA(inputs_ESA)


	assert my_inputs["inputs_EIA"]["input_maint"]["Number of Vessels"] == 5

def test_run_main():
    # inputs = inputs_ESA.prop_rep()
    [my_inputs, receptors_table, protected_table, weighting_table] = process_inputs_EIA(inputs_ESA)
    results = run_main(my_inputs, inputs_ESA, receptors_table, protected_table, weighting_table, complexity_level=3)
    results = results.prop_rep()
    [my_inputs, receptors_table, protected_table, weighting_table] = process_inputs_EIA(inputs_ESA2)
    results2 = run_main(my_inputs,  inputs_ESA,receptors_table, protected_table, weighting_table, complexity_level=3)
    results2 = results2.prop_rep()

    assert results["environmental_impact_assessment"]["eia_global"]["negative_impact"] == -54

