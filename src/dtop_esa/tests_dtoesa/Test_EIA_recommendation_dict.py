# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
import os 
import json
import pandas as pd
import numpy as np

from dtop_esa import Business

# Get current directory
dir_path = os.path.dirname(os.path.realpath(__file__))

def test_EIArecom_init_():

    EIArecom = Business.libraries.dtoesa.EIA.EIA_recommendation_dict.EIA_recommendation_dict(name = "test_season")
    EIArecom.init_from_files(function_name= 'Footprint', general_recommendation='blabl', generic_explanation='bleilbf', detailed_recommendation='bzih')

    assert EIArecom.function_name == 'Footprint'

def test_EIArecom_type_rep():
    EIArecom = Business.libraries.dtoesa.EIA.EIA_recommendation_dict.EIA_recommendation_dict(name = "test_recom")
    EIArecom.init_from_files(function_name= 'Footprint', general_recommendation='blabl', generic_explanation='bleilbf', detailed_recommendation='bzih')
    rep = EIArecom.type_rep()

    assert rep["name"] == 'test_recom'

def test_EIArecom_save_load():
    EIArecom = Business.libraries.dtoesa.EIA.EIA_recommendation_dict.EIA_recommendation_dict(name = "test_recom")
    EIArecom.init_from_files(function_name= 'Footprint', general_recommendation='blabl', generic_explanation='bleilbf', detailed_recommendation='bzih')    
    EIArecom.saveJSON()
    
    
    EIArecom2 = Business.libraries.dtoesa.EIA.EIA_recommendation_dict.EIA_recommendation_dict()
    EIArecom2.loadJSON(name= EIArecom.name)

    assert EIArecom2.function_name == 'Footprint'

def test_EIArecom_save_load_name():
    EIArecom = Business.libraries.dtoesa.EIA.EIA_recommendation_dict.EIA_recommendation_dict(name = "test_recom")
    EIArecom.init_from_files(function_name= 'Footprint', general_recommendation='blabl', generic_explanation='bleilbf', detailed_recommendation='bzih')    
    EIArecom.saveJSON()
    EIAconf = Business.libraries.dtoesa.EIA.EIA_confidence_dict.EIA_confidence_dict(name = "test_recom")
    EIAconf.init_from_files(function_name= 'Footprint', confidence_level=2)    
    EIAconf.saveJSON()

    EIArecom2 = Business.libraries.dtoesa.EIA.EIA_recommendation_dict.EIA_recommendation_dict()
    EIArecom2.loadJSON(filePath=EIArecom.name + ".json")
    EIAconf2 = Business.libraries.dtoesa.EIA.EIA_confidence_dict.EIA_confidence_dict()
    EIAconf2.loadJSON(filePath=EIAconf.name + ".json")
    assert EIArecom2.function_name == 'Footprint'

def test_EIArecom_loadFromJSONDict():
    EIArecom = Business.libraries.dtoesa.EIA.EIA_recommendation_dict.EIA_recommendation_dict(name = "test_recom")
    EIArecom.init_from_files(function_name= 'Footprint', general_recommendation='blabl', generic_explanation='bleilbf', detailed_recommendation='bzih')    
    EIArecom.saveJSON()
    
    EIArecom2 = Business.libraries.dtoesa.EIA.EIA_recommendation_dict.EIA_recommendation_dict()
    filePath='test_recom' + ".json"
    f = open(filePath,"r") 
    data = f.read()

    EIArecom2.loadFromJSONDict(data)
    dd = json.loads(data) 
    EIArecom2.loadFromJSONDict(dd) 
    
    assert EIArecom2.function_name == 'Footprint'

