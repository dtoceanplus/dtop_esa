# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
import os 
import json
import pandas as pd
import numpy as np

from dtop_esa import Business

# Get current directory
dir_path = os.path.dirname(os.path.realpath(__file__))

def test_EIAglobal_init_():

    EIAglobal = Business.libraries.dtoesa.EIA.EIA_global.EIA_global(name = "test_global")

    assert EIAglobal.name == "test_global"

def test_EIAglobal_type_rep():
    EIAglobal = Business.libraries.dtoesa.EIA.EIA_global.EIA_global(name = "test_global")
    rep = EIAglobal.type_rep()

    assert rep["name"] == 'test_global'

def test_EIAglobal_save_load():
    EIAglobal = Business.libraries.dtoesa.EIA.EIA_global.EIA_global(name = "test_global")
    EIAglobal.max_negative_impact = -100
    EIAglobal.saveJSON()
    
    EIAglobal2 = Business.libraries.dtoesa.EIA.EIA_global.EIA_global()
    EIAglobal2.loadJSON(name= EIAglobal.name)

    assert EIAglobal2.max_negative_impact == -100

def test_EIAglobal_save_load_name():
    EIAglobal = Business.libraries.dtoesa.EIA.EIA_global.EIA_global(name = "test_global")
    EIAglobal.saveJSON()
    
    EIAglobal2 = Business.libraries.dtoesa.EIA.EIA_global.EIA_global()
    EIAglobal2.loadJSON(filePath=EIAglobal.name + ".json")

    assert EIAglobal2.name == 'test_global'

def test_EIAglobal_loadFromJSONDict():
    EIAglobal = Business.libraries.dtoesa.EIA.EIA_global.EIA_global(name = "test_global")
    EIAglobal.max_negative_impact = -100
    EIAglobal.saveJSON()
    
    EIAglobal2 = Business.libraries.dtoesa.EIA.EIA_global.EIA_global()
    filePath='test_global' + ".json"
    f = open(filePath,"r") 
    data = f.read()

    EIAglobal2.loadFromJSONDict(data)
    dd = json.loads(data) 
    EIAglobal2.loadFromJSONDict(dd) 
    
    assert EIAglobal2.max_negative_impact == -100

