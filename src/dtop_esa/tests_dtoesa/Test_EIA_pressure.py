# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
import os 
import json
import pandas as pd
import numpy as np

from dtop_esa import Business

# Get current directory
dir_path = os.path.dirname(os.path.realpath(__file__))

def test_EIApressure_init_():

    EIApressure = Business.libraries.dtoesa.EIA.EIA_pressure.EIA_pressure(name = "test_pressure")

    assert EIApressure.name == "test_pressure"

def test_EIApressure_type_rep():
    EIApressure = Business.libraries.dtoesa.EIA.EIA_pressure.EIA_pressure(name = "test_pressure")
    rep = EIApressure.type_rep()

    assert rep["name"] == 'test_pressure'

def test_EIApressure_save_load():
    EIApressure = Business.libraries.dtoesa.EIA.EIA_pressure.EIA_pressure(name = "test_pressure")
    EIApressure.collision_risk.score = -100
    EIApressure.saveJSON()
    
    EIApressure2 = Business.libraries.dtoesa.EIA.EIA_pressure.EIA_pressure()
    EIApressure2.loadJSON(name= EIApressure.name)

    assert EIApressure2.collision_risk.score == -100

def test_EIApressure_save_load_name():
    EIApressure = Business.libraries.dtoesa.EIA.EIA_pressure.EIA_pressure(name = "test_pressure")
    EIApressure.saveJSON()
    
    EIApressure2 = Business.libraries.dtoesa.EIA.EIA_pressure.EIA_pressure()
    EIApressure2.loadJSON(filePath=EIApressure.name + ".json")

    assert EIApressure2.name == 'test_pressure'

def test_EIApressure_loadFromJSONDict():
    EIApressure = Business.libraries.dtoesa.EIA.EIA_pressure.EIA_pressure(name = "test_pressure")
    EIApressure.collision_risk.score = -100
    EIApressure.saveJSON()
    
    EIApressure2 = Business.libraries.dtoesa.EIA.EIA_pressure.EIA_pressure()
    filePath='test_pressure' + ".json"
    f = open(filePath,"r") 
    data = f.read()

    EIApressure2.loadFromJSONDict(data)
    dd = json.loads(data) 
    EIApressure2.loadFromJSONDict(dd) 
    
    assert EIApressure2.collision_risk.score == -100

