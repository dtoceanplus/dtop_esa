# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""py.test tests on main.py

.. moduleauthor:: Mathew Topper <mathew.topper@tecnalia.com>
"""

import os

import pytest
import pandas as pd

import numpy as np

from dtop_esa.Business.libraries.dtoesa.EIA.dtocean_environment.main import HydroStage, ElectricalStage, InstallationStage, MooringStage, OperationMaintenanceStage

mod_path = os.path.realpath(__file__)
mod_dir = os.path.dirname(mod_path)
test_data_dir = os.path.join(mod_dir, "test_data")

@pytest.fixture
def protected():
    
    # Input Dictionary
    protected_dict = {"species name": ["Balaenopteridae",
                                       "Balaenidae",
                                       "Phocidae",
                                       "Delphinidae",
                                       "Acipenseridae",
                                       "Anguillidae",
                                       "Scombridae",
                                       "Lamnidae",
                                       "Cetorhinidae",
                                       "Rajidae",
                                       "Squatinidae",
                                       "Anatidae",
                                       "Scolopacidae",
                                       "Procellariidae",
                                       "Cheloniidae",
                                       "Dermochelyidae",
                                       "Testunidae",
                                       "particular habitat",
                                       "fish"],
                      "observed": [False, False, False, False,False, False, False, False, False,False, False, False, False, False,False, False, False, False, False]}
                      
    protected_table = pd.DataFrame(protected_dict)
    protected_table = protected_table.set_index("species name")
    
    return protected_table
    
@pytest.fixture
def weighting():

    weighting_dict = {"Energy Modification": "Loose sand",
                      "Collision Risk": None,
                      "Collision Risk Vessel": None,
                      "Chemical Pollution": None,
                      "Turbidity": None,
                      "Underwater Noise": None,
                      "Reserve Effect": None,
                      "Reef Effect": None,
                      "Resting Place": None,
                      "Footprint": None,
                      "Electric Fields": None,
                      "Magnetic Fields": None,
                      "Temperature Modification": None}
    
    return weighting_dict

@pytest.fixture    
def receptors():
    
    table_path = os.path.join(test_data_dir, "species_receptors.csv")
    receptors_table = pd.read_csv(table_path, index_col=0)
    
    return receptors_table

def test_HydroStage(protected, weighting, receptors):
    
    test_hydro = HydroStage(protected,
                            receptors,
                            weighting)

    # Read devices positions for the test
    
    f = open(os.path.join(test_data_dir,"positions.txt"), 'r')
    data = np.genfromtxt(f)
    x=np.zeros(50)
    y=np.zeros(50)

    for ii in range(0, 50):
        datatmp=data[ii]
        x[ii]=datatmp[0]
        y[ii]=datatmp[1]
                            
    input_dict = {"Energy Modification"             : 0.3,
                  "Coordinates of the Devices"      : [x,y],
                  "Size of the Devices"             : 30.,
                  "Immersed Height of the Devices"  : 10.,
                  "Water Depth"                     : 15.,
                  "Current Direction"               : 45.,
                  "Initial Turbidity"               : 50.,
                  "Measured Turbidity"              : 70.,
                  "Initial Noise dB re 1muPa"       : 60.,
                  "Measured Noise dB re 1muPa"      : 150.,
                  "Fishery Restriction Surface"     : 1000.,
                  "Total Surface Area"              : 94501467.,
                  "Number of Objects"               : 50,
                  "Object Emerged Surface"          : 20.,
                  "Surface Area of Underwater Part" : 60.
                  }

    (confidence_dict, 
     eis_dict, 
     recommendation_dict,
     seasons,
     global_eis) = test_hydro(input_dict)


    assert 'Resting Place' in eis_dict.keys()
    assert len(seasons.columns) == 12
    assert "Energy Modification" in seasons.index
    assert test_hydro.get_module_name()== "Hydrodynamics" 

def test_elecstage(protected, weighting, receptors):
    test_elec = ElectricalStage(protected,
                            receptors,
                            weighting)

    f = open(os.path.join(test_data_dir,"positions.txt"), 'r')
    data = np.genfromtxt(f)
    x=np.zeros(50)
    y=np.zeros(50)

    for ii in range(0, 50):
        datatmp=data[ii]
        x[ii]=datatmp[0]
        y[ii]=datatmp[1]
                            
    input_dict = {"Coordinates of the Devices"      : [x,y],
                  "Size of the Devices"             : 30.,
                  "Immersed Height of the Devices"  : 10.,
                  "Water Depth"                     : 15.,
                  "Current Direction"               : 45.,
                  "Initial Noise dB re 1muPa"       : 60.,
                  "Measured Noise dB re 1muPa"      : 150.,
                  "Fishery Restriction Surface"     : 1000.,
                  "Total Surface Area"              : 94501467.,
                  "Number of Objects"               : 50,
                  "Object Emerged Surface"          : 20.,
                  "Surface Area of Underwater Part" : 60.,
                  "Surface Area Covered"            : 150.0,
                  "Initial Electric Field"          : 0.0,
                  "Measured Electric Field"         : 0.0,
                  "Initial Magnetic Field"          : 0.0,
                  "Measured Magnetic Field"         : 0.0,
                  "Initial Temperature"             : 0.0,
                  "Measured Temperature"            : 0.0
                  }

    (confidence_dict, 
     eis_dict, 
     recommendation_dict,
     seasons,
     global_eis) = test_elec(input_dict)

    assert test_elec.get_module_name()== "Electrical Subsystems" 
    assert 'Resting Place' in eis_dict.keys()

def test_MooringStage(protected, weighting, receptors):
    test_moor = MooringStage(protected,
                            receptors,
                            weighting)

    f = open(os.path.join(test_data_dir,"positions.txt"), 'r')
    data = np.genfromtxt(f)
    x=np.zeros(50)
    y=np.zeros(50)

    for ii in range(0, 50):
        datatmp=data[ii]
        x[ii]=datatmp[0]
        y[ii]=datatmp[1]
                            
    input_dict = {"Coordinates of the Devices"      : [x,y],
                  "Size of the Devices"             : 30.,
                  "Immersed Height of the Devices"  : 10.,
                  "Water Depth"                     : 15.,
                  "Current Direction"               : 45.,
                  "Initial Noise dB re 1muPa"       : 60.,
                  "Measured Noise dB re 1muPa"      : 150.,
                  "Total Surface Area"              : 94501467.,
                  "Number of Objects"               : 50,
                  "Surface Area of Underwater Part" : 60.,
                  "Surface Area Covered"            : 150.0
                  }

    (confidence_dict, 
     eis_dict, 
     recommendation_dict,
     seasons,
     global_eis) = test_moor(input_dict)

    assert test_moor.get_module_name()== "Moorings and Foundations" 
    assert 'Footprint' in eis_dict.keys()

def test_installationStage(protected, weighting, receptors):
    test_insta = InstallationStage(protected,
                            receptors,
                            weighting)

    f = open(os.path.join(test_data_dir,"positions.txt"), 'r')
    data = np.genfromtxt(f)
    x=np.zeros(50)
    y=np.zeros(50)

    for ii in range(0, 50):
        datatmp=data[ii]
        x[ii]=datatmp[0]
        y[ii]=datatmp[1]
                            
    input_dict = {"Initial Noise dB re 1muPa"       : 60.,
                  "Measured Noise dB re 1muPa"      : 150.,
                  "Total Surface Area"              : 94501467.,
                  "Surface Area Covered"            : 150.0,
                  "Number of Vessels"               : 3,
                  "Size of Vessels"                 : 12,
                  "Initial Turbidity"               : 50.,
                  "Measured Turbidity"              : 70.,
                  "Import of Chemical Polutant"     : 4
                  }

    (confidence_dict, 
     eis_dict, 
     recommendation_dict,
     seasons,
     global_eis) = test_insta(input_dict)

    assert test_insta.get_module_name()== "Installation" 
    assert 'Footprint' in eis_dict.keys()


def test_maintenanceStage(protected, weighting, receptors):
    test_maint = OperationMaintenanceStage(protected,
                            receptors,
                            weighting)

    f = open(os.path.join(test_data_dir,"positions.txt"), 'r')
    data = np.genfromtxt(f)
    x=np.zeros(50)
    y=np.zeros(50)

    for ii in range(0, 50):
        datatmp=data[ii]
        x[ii]=datatmp[0]
        y[ii]=datatmp[1]
                            
    input_dict = {"Initial Noise dB re 1muPa"       : 60.,
                  "Measured Noise dB re 1muPa"      : 150.,
                  "Total Surface Area"              : 94501467.,
                  "Surface Area Covered"            : 150.0,
                  "Number of Vessels"               : 3,
                  "Size of Vessels"                 : 12,
                  "Initial Turbidity"               : 50.,
                  "Measured Turbidity"              : 70.,
                  "Import of Chemical Polutant"     : 0
                  }

    (confidence_dict, 
     eis_dict, 
     recommendation_dict,
     seasons,
     global_eis) = test_maint(input_dict)
     
    assert test_maint.get_module_name()== "Operation and Maintenance" 
    assert 'Footprint' in eis_dict.keys()