# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
import os 
import json


# Get current directory
dir_path = os.path.dirname(os.path.realpath(__file__))
from dtop_esa import Business


def test_EIAresults_type_rep():

    EIAresults = Business.libraries.dtoesa.EIA.EIA_results.EIA_results(name = 'testEIAresults')
    rep = EIAresults.type_rep()

    assert rep["name"] == 'testEIAresults'

def test_EIAresults_save_load():
    EIAresults = Business.libraries.dtoesa.EIA.EIA_results.EIA_results(name = 'testEIAresults')    
    EIAresults.function_name = 'Footprint'
    EIAresults.score = -56
    EIAresults.saveJSON()
    
    EIAresults2 = Business.libraries.dtoesa.EIA.EIA_results.EIA_results()
    EIAresults2.loadJSON(name= EIAresults.name)

    assert EIAresults2.function_name == 'Footprint'

def test_EIAresults_save_load_name():
    EIAresults = Business.libraries.dtoesa.EIA.EIA_results.EIA_results(name = 'testEIAresults')    
    EIAresults.function_name = 'Footprint'
    EIAresults.score = -56
    EIAresults.saveJSON()
    
    EIAresults2 = Business.libraries.dtoesa.EIA.EIA_results.EIA_results()
    EIAresults2.loadJSON(filePath=EIAresults.name + ".json")

    assert EIAresults2.score == -56

def test_EIAresults_loadFromJSONDict():
    EIAresults = Business.libraries.dtoesa.EIA.EIA_results.EIA_results(name = 'testEIAresults')    
    EIAresults.function_name = 'Footprint'
    EIAresults.score = -56
    EIAresults.saveJSON()

    EIAresults2 = Business.libraries.dtoesa.EIA.EIA_results.EIA_results()
    filePath=EIAresults.name + ".json"
    f = open(filePath,"r") 
    data = f.read()

    EIAresults2.loadFromJSONDict(data)
    dd = json.loads(data) 
    EIAresults2.loadFromJSONDict(dd) 
    
    assert EIAresults2.function_name == 'Footprint'