# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections 
import numpy as np 
import pandas as pd
import os
import pytest
import json 
from dtop_esa.Business.libraries.dtoesa.outputs.Project import Project
from dtop_esa.Business.libraries.dtoesa.inputs.inputs_ESA_processed import inputs_ESA_processed


dir_path = os.path.dirname(os.path.realpath(__file__))

test_data_dir = os.path.join(dir_path, "test_data")
# Define inputs
inputs_file   = dir_path + '/../data_test/test_inputs_processed.json'
inputs = inputs_ESA_processed() 
data = json.load(open(inputs_file))
inputs.loadFromJSONDict(data)

inputs = inputs.prop_rep()

inputs["inputs_EIA"]["input_hydro"]["Coordinates of the Devices"] = [inputs['inputs_EIA']['input_hydro']['Coordinates_of_the_Devices_x'],inputs['inputs_EIA']['input_hydro']['Coordinates_of_the_Devices_y']]
inputs["inputs_EIA"]["input_elec"]["Coordinates of the Devices"] = [inputs['inputs_EIA']['input_elec']['Coordinates_of_the_Devices_x'],inputs['inputs_EIA']['input_elec']['Coordinates_of_the_Devices_y']]
inputs["inputs_EIA"]["input_moor"]["Coordinates of the Devices"] = [inputs['inputs_EIA']['input_moor']['Coordinates_of_the_Devices_x'],inputs['inputs_EIA']['input_moor']['Coordinates_of_the_Devices_y']]
[inputs["inputs_EIA"]["input_hydro"].pop(key) for key in ["__type__", "name","description","Coordinates_of_the_Devices_x","Coordinates_of_the_Devices_y"]]
[inputs["inputs_EIA"]["input_elec"].pop(key) for key in ["__type__", "name","description","Coordinates_of_the_Devices_x","Coordinates_of_the_Devices_y"]]
[inputs["inputs_EIA"]["input_moor"].pop(key) for key in ["__type__", "name","description","Coordinates_of_the_Devices_x","Coordinates_of_the_Devices_y"]]
[inputs["inputs_EIA"]["input_insta"].pop(key) for key in ["__type__", "name","description"]]
[inputs["inputs_EIA"]["input_maint"].pop(key) for key in ["__type__", "name","description"]]
[inputs["inputs_EIA"]["receptors_dict"].pop(key) for key in ["__type__", "name","description"]]
[inputs["inputs_EIA"]["weighting_dict"].pop(key) for key in ["__type__", "name","description"]]
[inputs["inputs_CFP"]["materials"].pop(key) for key in ["__type__", "name","description"]]
[inputs["inputs_CFP"]["materials_to_recycle"].pop(key) for key in ["__type__", "name","description"]]

  # Receptors table
receptors_dict = inputs["inputs_EIA"]["receptors_dict"]
receptors_dict = {k.replace("_"," "): v for k,v in receptors_dict.items()}
receptors_dict = {k.replace("Large odontocete Mysticete","Large odontocete_Mysticete"): v for k,v in receptors_dict.items()}
receptors_dict = {k.replace("Odontoncete dolphinds","Odontoncete_dolphinds"): v for k,v in receptors_dict.items()}
receptors_table = pd.DataFrame.from_dict(receptors_dict, orient = 'index')
receptors_table.columns = ["observed", 'observed january', 'observed february',
                        'observed march',
                        'observed april',
                        'observed may',
                        'observed june',
                        'observed july',
                        'observed august',
                        'observed september',
                        'observed october',
                        'observed november',
                        'observed december']
    # Protected table
protected_dict = inputs["inputs_EIA"]["protected_dict"]
protected_dict = {k.replace("_"," "): v for k,v in protected_dict.items()}
protected_table = pd.DataFrame(protected_dict)
protected_table = protected_table.set_index("species name")
    # Weighting table
weighting_dict = inputs["inputs_EIA"]["weighting_dict"]
for key in weighting_dict:
  weighting_dict[key] = {k.replace("_"," "): v for k,v in weighting_dict[key].items()}

    # my_inputs["inputs_EIA"]["input_hydro"] = {k.replace("Size_Devices","Size of the Devices"): v for k,v in inputs_hydro.items()}


inputs_hydro = inputs["inputs_EIA"]["input_hydro"]
inputs["inputs_EIA"]["input_hydro"] = {k.replace("_"," "): v for k,v in inputs_hydro.items()}

inputs_elec = inputs["inputs_EIA"]["input_elec"]
inputs["inputs_EIA"]["input_elec"] = {k.replace("_"," "): v for k,v in inputs_elec.items()}

inputs_moor = inputs["inputs_EIA"]["input_moor"]
inputs["inputs_EIA"]["input_moor"] = {k.replace("_"," "): v for k,v in inputs_moor.items()}

inputs_insta = inputs["inputs_EIA"]["input_insta"]
inputs["inputs_EIA"]["input_insta"] = {k.replace("_"," "): v for k,v in inputs_insta.items()}
inputs_maint = inputs["inputs_EIA"]["input_maint"]
inputs["inputs_EIA"]["input_maint"] = {k.replace("_"," "): v for k,v in inputs_maint.items()}

# f = open(os.path.join(test_data_dir,"positions.txt"), 'r')
# data = np.genfromtxt(f)
# x=np.zeros(50)
# y=np.zeros(50)

# for ii in range(0, 50):
#     datatmp=data[ii]
#     x[ii]=datatmp[0]
#     y[ii]=datatmp[1]


# input_hydro = {"Energy Modification"             : 0.3,
#                 "Coordinates of the Devices"      : [x,y],
#                 "Size of the Devices"             : 10.,
#                 "Immersed Height of the Devices"  : 10.,
#                 "Water Depth"                     : 15.,
#                 "Current Direction"               : 45.,
#                 "Initial Turbidity"               : 50.,
#                 "Measured Turbidity"              : 70.,
#                 "Initial Noise dB re 1muPa"       : 60.,
#                 "Measured Noise dB re 1muPa"      : 150.,
#                 "Fishery Restriction Surface"     : 1000.,
#                 "Total Surface Area"              : 94501467.,
#                 "Number of Objects"               : 50,
#                 "Object Emerged Surface"          : 0,
#                 "Surface Area of Underwater Part" : 60.
#                 }                                                                               
# input_elec = {"Coordinates of the Devices"      : [x,y],
#                 "Size of the Devices"             : 30.,
#                 "Immersed Height of the Devices"  : 10.,
#                 "Water Depth"                     : 15.,
#                 "Current Direction"               : 45.,
#                 "Initial Noise dB re 1muPa"       : 60.,
#                 "Measured Noise dB re 1muPa"      : 60.,
#                 "Fishery Restriction Surface"     : 1000.,
#                 "Total Surface Area"              : 94501467.,
#                 "Number of Objects"               : 50,
#                 "Object Emerged Surface"          : 20.,
#                 "Surface Area of Underwater Part" : 60.,
#                 "Surface Area Covered"            : 150.0,
#                 "Initial Electric Field"          : 0.0,
#                 "Measured Electric Field"         : 0.0,
#                 "Initial Magnetic Field"          : 0.0,
#                 "Measured Magnetic Field"         : 0.0,
#                 "Initial Temperature"             : 0.0,
#                 "Measured Temperature"            : 0.0
#                 }
# input_moor ={"Coordinates of the Devices"      : [x,y],
#                   "Size of the Devices"             : 10.,
#                   "Immersed Height of the Devices"  : 10.,
#                   "Water Depth"                     : 15.,
#                   "Current Direction"               : 45.,
#                   "Initial Noise dB re 1muPa"       : 60.,
#                   "Measured Noise dB re 1muPa"      : 60.,
#                   "Total Surface Area"              : 94501467.,
#                   "Number of Objects"               : 50,
#                   "Surface Area of Underwater Part" : 60.,
#                   "Surface Area Covered"            : 150.0
#                   }
# input_insta = {"Initial Noise dB re 1muPa"       : 60.,
#                   "Measured Noise dB re 1muPa"      : 150.,
#                   "Total Surface Area"              : 94501467.,
#                   "Surface Area Covered"            : 150.0,
#                   "Number of Vessels"               : 3,
#                   "Size of Vessels"                 : 12,
#                   "Initial Turbidity"               : 50.,
#                   "Measured Turbidity"              : 70.,
#                   "Import of Chemical Polutant"     : 0
#                   }
# input_maint = {"Initial Noise dB re 1muPa"       : 60.,
#                   "Measured Noise dB re 1muPa"      : 150.,
#                   "Total Surface Area"              : 94501467.,
#                   "Surface Area Covered"            : 150.0,
#                   "Number of Vessels"               : 3,
#                   "Size of Vessels"                 : 12,
#                   "Initial Turbidity"               : 50.,
#                   "Measured Turbidity"              : 70.,
#                   "Import of Chemical Polutant"     : 0
#                   }
                           


# protected_dict = {"species name": ["Balaenopteridae",
#                                        "Balaenidae",
#                                        "Phocidae",
#                                        "Delphinidae",
#                                        "Acipenseridae",
#                                        "Anguillidae",
#                                        "Scombridae",
#                                        "Lamnidae",
#                                        "Cetorhinidae",
#                                        "Rajidae",
#                                        "Squatinidae",
#                                        "Anatidae",
#                                        "Scolopacidae",
#                                        "Procellariidae",
#                                        "Cheloniidae",
#                                        "Dermochelyidae",
#                                        "Testunidae",
#                                        "particular habitat",
#                                        "fish"],
#                       "observed": [False, False, False, False,False, False, False, False, False,False, False, False, False, False,False, False, False, False, False]}
                      
# protected_table = pd.DataFrame(protected_dict)
# protected_table = protected_table.set_index("species name")

# weighting_dict = {"Energy Modification": None,
#                       "Collision Risk": None,
#                       "Collision Risk Vessel": None,
#                       "Chemical Pollution": None,
#                       "Turbidity": None,
#                       "Underwater Noise": None,
#                       "Reserve Effect": None,
#                       "Reef Effect": None,
#                       "Resting Place": None,
#                       "Footprint": None,
#                       "Electric Fields": None,
#                       "Magnetic Fields": None,
#                       "Temperature Modification": None}

# table_path = os.path.join(test_data_dir, "species_receptors.csv")
# receptors_table = pd.read_csv(table_path, index_col=0)


# Define inputs
inputs_cfp_file         = dir_path + '/../data_test/inputs_CFP.json'
inputs_es_user          = dir_path + '/../data_test/inputs_species.json'
dict_fuel_file          = dir_path + '/../ressources/dict_fuel.json'
dict_material_file      = dir_path + '/../ressources/dict_materials.json'
dict_gases_file         = dir_path + '/../ressources/dict_gases_impact_factor.json'
database_species_file   = dir_path + '/../ressources/database_species.json'

dict_fuel                   = json.load(open(dict_fuel_file))
dict_materials              = json.load(open(dict_material_file))
dict_gases_impact_factor    = json.load(open(dict_gases_file))
database_species            = json.load(open(database_species_file))
# outputs_sc_species          = json.load(open(inputs_es_user))
# inputs_CFP                  = json.load(open(inputs_cfp_file))

# inputs_SC = {}
# for spe in range(len(outputs_sc_species['species'])):
#     inputs_SC[outputs_sc_species['species'][spe]] = outputs_sc_species['probablity'][spe]


# inputs_SA = {"cost_of_consenting":465, "nb_vessel_crew": 65}
# inputs_EIA = {}
# inputs_EIA["input_hydro"] = input_hydro 
# inputs_EIA["input_elec"] = input_elec
# inputs_EIA["input_moor"] = input_moor
# inputs_EIA["input_insta"] = input_insta
# inputs_EIA["input_maint"] = input_maint
# inputs_ES ={
#         "__type__": "dtoesa:inputs_ES",
#         "name": "none",
#         "description": "",
#         "Coordinates_of_the_farm_x": 29.3,
#         "Coordinates_of_the_farm_y": 2.3,
#         "species_user": [{"name": "Physeter macrocephalus",
#                             "Class": "mammals"}
#         ],
#         "inputs_SC": [{"name": "Physeter macrocephalus",
#                             "probability_of_presence": 0.1}]
#     }


def test_project_init():

    ProjectAssess = Project(name ='test')
    ProjectAssess._init_from_files(inputs["inputs_CFP"], inputs["inputs_EIA"], inputs["inputs_ES"], inputs["inputs_SA"])

    assert ProjectAssess.inputs_ES["Coordinates_of_the_farm_x"] == 29.3
    assert ProjectAssess.projectId == "test"
    assert ProjectAssess.name == 'test'
    #assert ProjectAssess.description == 'None'

def test_project_cfp():
    ProjectAssess = Project(name ='test')
    ProjectAssess._init_from_files(inputs_CFP = inputs["inputs_CFP"])
    ProjectAssess.compute_cfp_assessment(dict_fuel, dict_materials, dict_gases_impact_factor)
    
    assert ProjectAssess.carbon_footprint.CFP_global.GWP == pytest.approx(5653550, 1)

def test_project_eia():
    ProjectAssess = Project(name ='test')
    ProjectAssess._init_from_files(inputs["inputs_CFP"], inputs["inputs_EIA"], inputs["inputs_ES"], inputs["inputs_SA"])
    ProjectAssess.compute_eia_assessment(inputs["inputs_EIA"]["protected_dict"], receptors_table, inputs['inputs_EIA']['weighting_dict'])
    
    assert ProjectAssess.environmental_impact_assessment.eia_tech_group.impact_installation.global_eis.max_negative_impact == 999

def test_project_es():
    ProjectAssess = Project(name ='test')
    ProjectAssess._init_from_files(inputs_ES=inputs["inputs_ES"])
    ProjectAssess.compute_es_assessment(database_species)
    
    assert ProjectAssess.endangered_species.mammals[0].Probability_of_presence == 999.0

def test_project_sa():
    ProjectAssess = Project(name ='test')
    ProjectAssess._init_from_files(inputs_SA= inputs["inputs_SA"])
    ProjectAssess.compute_sa_assessment()
    
    assert ProjectAssess.social_acceptance.cost_of_consenting == 465

def test_project_rep_type():
    ProjectAssess = Project(name ='test')
    ProjectAssess._init_from_files(inputs["inputs_CFP"], inputs["inputs_EIA"], inputs["inputs_ES"], inputs["inputs_SA"])
    rep = ProjectAssess.type_rep()

    assert rep["name"] == 'test'

def test_project_prop_rep():
    ProjectAssess = Project(name ='test')
    ProjectAssess._init_from_files(inputs["inputs_CFP"], inputs["inputs_EIA"], inputs["inputs_ES"], inputs["inputs_SA"])
    rep = ProjectAssess.prop_rep()

    assert rep["projectId"] == 'test'

def test_project_json_rep():
    ProjectAssess = Project(name ='test')
    ProjectAssess._init_from_files(inputs["inputs_CFP"], inputs["inputs_EIA"], inputs["inputs_ES"], inputs["inputs_SA"])
    ProjectAssess.compute_eia_assessment(inputs["inputs_EIA"]["protected_dict"], receptors_table, inputs['inputs_EIA']['weighting_dict'])
    ProjectAssess.compute_es_assessment(database_species)
    ProjectAssess.compute_sa_assessment()
    ProjectAssess.compute_cfp_assessment(dict_fuel, dict_materials, dict_gases_impact_factor)

    ProjectAssess.saveJSON()
    Project2 = Project()
    Project2.loadJSON(filePath=ProjectAssess.name + ".json")

    assert Project2.endangered_species.mammals[0].Order == "Cetacea"

def test_project_json_rep_name():
    ProjectAssess = Project(name ='test')
    ProjectAssess._init_from_files(inputs["inputs_CFP"], inputs["inputs_EIA"], inputs["inputs_ES"], inputs["inputs_SA"])
    ProjectAssess.compute_eia_assessment(inputs["inputs_EIA"]["protected_dict"], receptors_table, inputs['inputs_EIA']['weighting_dict'])
    ProjectAssess.compute_es_assessment(database_species)
    ProjectAssess.compute_sa_assessment()
    ProjectAssess.compute_cfp_assessment(dict_fuel, dict_materials, dict_gases_impact_factor)

    ProjectAssess.saveJSON()
    Project2 = Project()
    Project2.loadJSON(name = ProjectAssess.name)

    assert Project2.endangered_species.mammals[0].Order == "Cetacea"

def test_project_loadjson_is_file_raise():
    ProjectAssess = Project(name ='test')
    ProjectAssess._init_from_files(inputs["inputs_CFP"], inputs["inputs_EIA"], inputs["inputs_ES"], inputs["inputs_SA"])
    ProjectAssess.compute_eia_assessment(inputs["inputs_EIA"]["protected_dict"], receptors_table, inputs['inputs_EIA']['weighting_dict'])
    ProjectAssess.compute_es_assessment(database_species)
    ProjectAssess.compute_sa_assessment()
    ProjectAssess.compute_cfp_assessment(dict_fuel, dict_materials, dict_gases_impact_factor)

    ProjectAssess.saveJSON()
    Project2 = Project()
    
    with pytest.raises(Exception):
        Project2.loadJSON(filePath='projet' + ".json")


def test_project_loadFromJSONDict():
    ProjectAssess = Project(name ='test')
    ProjectAssess._init_from_files(inputs["inputs_CFP"], inputs["inputs_EIA"], inputs["inputs_ES"], inputs["inputs_SA"])
    ProjectAssess.compute_eia_assessment(inputs["inputs_EIA"]["protected_dict"], receptors_table, inputs['inputs_EIA']['weighting_dict'])
    ProjectAssess.compute_es_assessment(database_species)
    ProjectAssess.compute_sa_assessment()
    ProjectAssess.compute_cfp_assessment(dict_fuel, dict_materials, dict_gases_impact_factor)

    ProjectAssess.saveJSON()
    Project2 = Project()
    filePath='test' + ".json"
    f = open(filePath,"r") 
    data = f.read()
    dd = json.loads(data) 
    Project2.loadFromJSONDict(dd) 
    assert Project2.endangered_species.mammals[0].Order == "Cetacea"

def test_project_loadFromJSONDict_raise():
    Project2 = Project()
    print()
    filePath='test' + ".json"
    f = open(filePath,"r") 
    data = f.read()
    dd = json.loads(data) 
    Project2.loadFromJSONDict(dd) 

    assert Project2.endangered_species.mammals[0].Order == "Cetacea"

def test_project_is_set():
    Project2 = Project()
    isset = Project2.is_set('endangered_species')
    assert isset == True
