# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
import os 
import json 

# Get current directory
dir_path = os.path.dirname(os.path.realpath(__file__))
from dtop_esa.Business.libraries.dtoesa.SA.SocialList import SocialList

inputs_SA = {"cost_of_consenting":465, "nb_vessel_crew": 65}

def test_socialList_init(): 
    social = SocialList(name = 'testSocial')
    social.init_from_files(inputs_SA)
    assert social.name == 'testSocial'

def test_socialList_rep_type():
    social = SocialList(name = 'testSocial')
    social.cost_of_consenting = inputs_SA["cost_of_consenting"]
    social.nb_vessel_crew     = inputs_SA["nb_vessel_crew"]
    rep = social.type_rep()

    assert rep["name"] == 'testSocial'
    
def test_socialList_json_rep():
    social = SocialList(name = 'testSocial')
    social.cost_of_consenting = inputs_SA["cost_of_consenting"]
    social.nb_vessel_crew     = inputs_SA["nb_vessel_crew"]
    social.saveJSON()
    social2 = SocialList()
    social2.loadJSON(filePath=social.name + ".json")

    assert social2.nb_vessel_crew == 65

def test_socialList_loadjson_rep_name():
    social = SocialList(name = 'testSocial')
    social.cost_of_consenting = inputs_SA["cost_of_consenting"]
    social.nb_vessel_crew     = inputs_SA["nb_vessel_crew"]
    social.saveJSON()
    social2 = SocialList()
    social2.loadJSON(name = social.name)

    assert social2.nb_vessel_crew == 65

def test_socialList_loadjson_is_file_raise():
    social = SocialList(name = 'testSocial')
    social.cost_of_consenting = inputs_SA["cost_of_consenting"]
    social.nb_vessel_crew     = inputs_SA["nb_vessel_crew"]
    social.saveJSON()
    social2 = SocialList()
    
    with pytest.raises(Exception):
        social2.loadJSON(filePath='social' + ".json")


def test_speciesList_loadFromJSONDict():
    social = SocialList(name = 'testSocial')
    social.cost_of_consenting = inputs_SA["cost_of_consenting"]
    social.saveJSON()
    social2 = SocialList()
    filePath='testSocial' + ".json"
    f = open(filePath,"r") 
    data = f.read()

    social2.loadFromJSONDict(data)
    dd = json.loads(data) 
    social2.loadFromJSONDict(dd) 
    assert social2.nb_vessel_crew == 0

def test_socialList_is_set():
    social = SocialList(name = 'testSocial')
    social.cost_of_consenting = inputs_SA["cost_of_consenting"]
    isset = social.is_set("cost_of_consenting")
    assert isset == True
