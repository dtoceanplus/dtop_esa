# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
import os 
import json
import pandas as pd
import numpy as np

# Get current directory
dir_path = os.path.dirname(os.path.realpath(__file__))
from dtop_esa.Business.libraries.dtoesa.EIA import EIA_results_tech
from dtop_esa.Business.libraries.dtoesa.EIA.dtocean_environment import main

test_data_dir = os.path.join(dir_path, "test_data")
# Define inputs

f = open(os.path.join(test_data_dir,"positions.txt"), 'r')
data = np.genfromtxt(f)
x=np.zeros(50)
y=np.zeros(50)

for ii in range(0, 50):
    datatmp=data[ii]
    x[ii]=datatmp[0]
    y[ii]=datatmp[1]

input_hydro = {"Energy Modification"             : 0.3,
                "Coordinates of the Devices"      : [x,y],
                "Size of the Devices"             : 30.,
                "Immersed Height of the Devices"  : 10.,
                "Water Depth"                     : 15.,
                "Current Direction"               : 45.,
                "Initial Turbidity"               : 50.,
                "Measured Turbidity"              : 70.,
                "Initial Noise dB re 1muPa"       : 60.,
                "Measured Noise dB re 1muPa"      : 150.,
                "Fishery Restriction Surface"     : 1000.,
                "Total Surface Area"              : 94501467.,
                "Number of Objects"               : 50,
                "Object Emerged Surface"          : 20.,
                "Surface Area of Underwater Part" : 60.
                }                                                                               
input_elec = {"Coordinates of the Devices"      : [x,y],
                "Size of the Devices"             : 30.,
                "Immersed Height of the Devices"  : 10.,
                "Water Depth"                     : 15.,
                "Current Direction"               : 45.,
                "Initial Noise dB re 1muPa"       : 60.,
                "Measured Noise dB re 1muPa"      : 150.,
                "Fishery Restriction Surface"     : 1000.,
                "Total Surface Area"              : 94501467.,
                "Number of Objects"               : 50,
                "Object Emerged Surface"          : 20.,
                "Surface Area of Underwater Part" : 60.,
                "Surface Area Covered"            : 150.0,
                "Initial Electric Field"          : 0.0,
                "Measured Electric Field"         : 0.0,
                "Initial Magnetic Field"          : 0.0,
                "Measured Magnetic Field"         : 0.0,
                "Initial Temperature"             : 0.0,
                "Measured Temperature"            : 0.0
                }
input_moor ={"Coordinates of the Devices"      : [x,y],
                  "Size of the Devices"             : 30.,
                  "Immersed Height of the Devices"  : 10.,
                  "Water Depth"                     : 15.,
                  "Current Direction"               : 45.,
                  "Initial Noise dB re 1muPa"       : 60.,
                  "Measured Noise dB re 1muPa"      : 150.,
                  "Total Surface Area"              : 94501467.,
                  "Number of Objects"               : 50,
                  "Surface Area of Underwater Part" : 60.,
                  "Surface Area Covered"            : 150.0
                  }
input_insta = {"Initial Noise dB re 1muPa"       : 60.,
                  "Measured Noise dB re 1muPa"      : 150.,
                  "Total Surface Area"              : 94501467.,
                  "Surface Area Covered"            : 150.0,
                  "Number of Vessels"               : 3,
                  "Size of Vessels"                 : 12,
                  "Initial Turbidity"               : 50.,
                  "Measured Turbidity"              : 70.,
                  "Import of Chemical Polutant"     : 0
                  }
input_maint = {"Initial Noise dB re 1muPa"       : 60.,
                  "Measured Noise dB re 1muPa"      : 150.,
                  "Total Surface Area"              : 94501467.,
                  "Surface Area Covered"            : 150.0,
                  "Number of Vessels"               : 3,
                  "Size of Vessels"                 : 12,
                  "Initial Turbidity"               : 50.,
                  "Measured Turbidity"              : 70.,
                  "Import of Chemical Polutant"     : 0
                  }
                           
inputs_EIA = (input_hydro, input_elec, input_moor, input_insta, input_maint)

protected_dict = {"species name": ["Balaenopteridae",
                                       "Balaenidae",
                                       "Phocidae",
                                       "Delphinidae",
                                       "Acipenseridae",
                                       "Anguillidae",
                                       "Scombridae",
                                       "Lamnidae",
                                       "Cetorhinidae",
                                       "Rajidae",
                                       "Squatinidae",
                                       "Anatidae",
                                       "Scolopacidae",
                                       "Procellariidae",
                                       "Cheloniidae",
                                       "Dermochelyidae",
                                       "Testunidae",
                                       "particular habitat",
                                       "fish"],
                      "observed": [False, False, False, False,False, False, False, False, False,False, False, False, False, False,False, False, False, False, False]}
                      
protected_table = pd.DataFrame(protected_dict)
protected_table = protected_table.set_index("species name")

weighting_dict = {"Energy Modification": "Loose sand",
                      "Collision Risk": None,
                      "Collision Risk Vessel": None,
                      "Chemical Pollution": None,
                      "Turbidity": None,
                      "Underwater Noise": None,
                      "Reserve Effect": None,
                      "Reef Effect": None,
                      "Resting Place": None,
                      "Footprint": None,
                      "Electric Fields": None,
                      "Magnetic Fields": None,
                      "Temperature Modification": None}

table_path = os.path.join(test_data_dir, "species_receptors.csv")
receptors_table = pd.read_csv(table_path, index_col=0)
print(receptors_table)
def test_EIAtech_group_init():

    EIAtech = EIA_results_tech.EIA_results_tech(name = 'testEIAtech')

    assert EIAtech.name == 'testEIAtech'

def test_EIAtech_group_compute():

    EIAtech = EIA_results_tech.EIA_results_tech(name = 'testEIAtech')
    EIAtech.compute_impact_hydrodynamics(input_hydro, protected_table, receptors_table, weighting_dict)
    EIAtech.compute_impact_electrical(input_elec, protected_table, receptors_table, weighting_dict)
    EIAtech.compute_impact_station_keeping(input_moor, protected_table, receptors_table, weighting_dict)
    EIAtech.compute_impact_installation(input_insta, protected_table, receptors_table, weighting_dict)
    EIAtech.compute_impact_maintenance(input_maint, protected_table, receptors_table, weighting_dict)
    
    assert EIAtech.impact_hydrodynamics.global_eis.negative_impact == -69

def test_EIAtech_rep_type():
    EIAtech = EIA_results_tech.EIA_results_tech(name = 'testEIAtech')
    EIAtech.compute_impact_hydrodynamics(input_hydro, protected_table, receptors_table, weighting_dict)
    EIAtech.compute_impact_electrical(input_elec, protected_table, receptors_table, weighting_dict)
    EIAtech.compute_impact_station_keeping(input_moor, protected_table, receptors_table, weighting_dict)
    EIAtech.compute_impact_installation(input_insta, protected_table, receptors_table, weighting_dict)
    EIAtech.compute_impact_maintenance(input_maint, protected_table, receptors_table, weighting_dict)
    
    rep = EIAtech.type_rep()

    assert rep["name"] == 'testEIAtech'


def test_EIAtech_json_rep():
    EIAtech = EIA_results_tech.EIA_results_tech(name = 'testEIAtech')
    EIAtech.compute_impact_hydrodynamics(input_hydro, protected_table, receptors_table, weighting_dict)
    EIAtech.compute_impact_electrical(input_elec, protected_table, receptors_table, weighting_dict)
    EIAtech.compute_impact_station_keeping(input_moor, protected_table, receptors_table, weighting_dict)
    EIAtech.compute_impact_installation(input_insta, protected_table, receptors_table, weighting_dict)
    EIAtech.compute_impact_maintenance(input_maint, protected_table, receptors_table, weighting_dict)
    EIAtech.saveJSON()
    EIAtech2 = EIA_results_tech.EIA_results_tech()
    EIAtech2.loadJSON(filePath=EIAtech.name + ".json")

    assert EIAtech2.impact_electrical.global_eis.min_positive_impact == pytest.approx(10.00007)