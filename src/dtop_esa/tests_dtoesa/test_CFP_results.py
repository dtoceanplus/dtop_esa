# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
import os 
import json 

# Get current directory
dir_path = os.path.dirname(os.path.realpath(__file__))
from dtop_esa import Business

# Define inputs
inputs_cfp_file     = dir_path + '/../data_test/inputs_CFP.json'
dict_fuel_file      = dir_path + '/../ressources/dict_fuel.json'
dict_material_file  = dir_path + '/../ressources/dict_materials.json'
dict_gases_file     = dir_path + '/../ressources/dict_gases_impact_factor.json'

dict_fuel                 = json.load(open(dict_fuel_file))
dict_materials            = json.load(open(dict_material_file))
dict_gases_impact_factor  = json.load(open(dict_gases_file))
inputs_CFP   = json.load(open(inputs_cfp_file))
inputs_CFP2   = json.load(open(inputs_cfp_file))

inputs_CFP2["energy_produced"]["value"] = 0

def test_CFPresults_init():
    cfpresults = Business.libraries.dtoesa.CFP.CFP_results.CFP_results(name = 'testcfpresults')

    assert cfpresults.name == 'testcfpresults'

def test_CFPresults_get_phase():
    cfpresults = Business.libraries.dtoesa.CFP.CFP_results.CFP_results(name = "fabrication")
    cfpresults.get_phase()
 
    assert cfpresults.phase == "Production"

def test_CFPresults_get_GWP_CED():
    cfpresults = Business.libraries.dtoesa.CFP.CFP_results.CFP_results(name ="fabrication")
    cfpresults.get_phase()
    cfpresults.get_GWP(inputs_CFP, dict_fuel, dict_materials, dict_gases_impact_factor)
    cfpresults.get_CED(inputs_CFP, dict_fuel, dict_materials, dict_gases_impact_factor)
    cfpresults2 = Business.libraries.dtoesa.CFP.CFP_results.CFP_results(name ="installation")
    cfpresults2.get_GWP(inputs_CFP2, dict_fuel, dict_materials, dict_gases_impact_factor)
    cfpresults2.get_CED(inputs_CFP2, dict_fuel, dict_materials, dict_gases_impact_factor)
    assert cfpresults.CED == pytest.approx(0.071 , 0.05)
    assert cfpresults.GWP == pytest.approx(6.465, 0.1) 

def test_CFPresults_rep_type():
    CFPresults = Business.libraries.dtoesa.CFP.CFP_results.CFP_results(name = 'testcfpresults')
    rep = CFPresults.type_rep()

    assert rep["name"] == 'testcfpresults'

def test_CFPresults_json_rep():
    cfpresults = Business.libraries.dtoesa.CFP.CFP_results.CFP_results()
    cfpresults.get_phase()
    cfpresults.get_GWP(inputs_CFP, dict_fuel, dict_materials, dict_gases_impact_factor)
    cfpresults.get_GWP(inputs_CFP2, dict_fuel, dict_materials, dict_gases_impact_factor)
    cfpresults.get_CED(inputs_CFP, dict_fuel, dict_materials, dict_gases_impact_factor)

    cfpresults.get_CED(inputs_CFP2, dict_fuel, dict_materials, dict_gases_impact_factor)
    cfpresults.saveJSON()
    cfpresults2 = Business.libraries.dtoesa.CFP.CFP_results.CFP_results()
    cfpresults2.loadJSON(filePath=cfpresults.name + ".json")

    assert cfpresults2.GWP == 0

def test_CFPresults_json_rep_name():
    cfpresults = Business.libraries.dtoesa.CFP.CFP_results.CFP_results()
    cfpresults.get_phase()
    cfpresults.get_GWP(inputs_CFP, dict_fuel, dict_materials, dict_gases_impact_factor)
    cfpresults.get_CED(inputs_CFP, dict_fuel, dict_materials, dict_gases_impact_factor)
    cfpresults.saveJSON()
    cfpresults2 = Business.libraries.dtoesa.CFP.CFP_results.CFP_results()
    cfpresults2.loadJSON(name=cfpresults.name)
    failtry = Business.libraries.dtoesa.CFP.CFP_results.CFP_results()
    failtry.name=''
    try:
        failtry.loadJSON()
    except:
        pass

    assert cfpresults2.GWP == 0

def test_CFPresults_loadjson_raise():
    cfpresults2 = Business.libraries.dtoesa.CFP.CFP_results.CFP_results()

    with pytest.raises(Exception):
        cfpresults2.loadJSON(filePath='NFA' + ".json")

def test_CFPresults_loadFromJSONDict():
    cfpresults = Business.libraries.dtoesa.CFP.CFP_results.CFP_results(name = 'testcfpresults')
    cfpresults.get_phase()
    cfpresults.get_GWP(inputs_CFP, dict_fuel, dict_materials, dict_gases_impact_factor)
    cfpresults.get_CED(inputs_CFP, dict_fuel, dict_materials, dict_gases_impact_factor)
    cfpresults.saveJSON()
    cfpresults2 = Business.libraries.dtoesa.CFP.CFP_results.CFP_results()
    filePath='testcfpresults' + ".json"
    f = open(filePath,"r") 
    data = f.read()

    cfpresults2.loadFromJSONDict(data)
    dd = json.loads(data) 
    cfpresults2.loadFromJSONDict(dd) 
    
    assert cfpresults2.GWP == 0


def test_CFPresults_is_set():
    cfpresults = Business.libraries.dtoesa.CFP.CFP_results.CFP_results() 
    cfpresults.get_phase()
    isset = cfpresults.is_set('phase')
    assert isset == True
