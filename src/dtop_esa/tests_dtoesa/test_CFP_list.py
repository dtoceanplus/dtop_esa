# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
import os 
import json 

# Get current directory
dir_path = os.path.dirname(os.path.realpath(__file__))
from dtop_esa.Business.libraries.dtoesa.CFP.CFPList import CFPList

# Define inputs
inputs_cfp_file     = dir_path + '/../data_test/inputs_CFP.json'
dict_fuel_file      = dir_path + '/../ressources/dict_fuel.json'
dict_material_file  = dir_path + '/../ressources/dict_materials.json'
dict_gases_file     = dir_path + '/../ressources/dict_gases_impact_factor.json'

dict_fuel                 = json.load(open(dict_fuel_file))
dict_materials            = json.load(open(dict_material_file))
dict_gases_impact_factor  = json.load(open(dict_gases_file))
inputs_CFP                = json.load(open(inputs_cfp_file))
inputs_CFP2 = json.load(open(inputs_cfp_file))
inputs_CFP2["energy_produced"]["value"] = 0

def test_CFPList_init_from_files():
    cfplist = CFPList(name = 'testcfplist')
    cfplist.init_from_files(inputs_CFP, dict_fuel,dict_materials,dict_gases_impact_factor)
    
    assert cfplist.dict_fuel['LCV']['value'] == 42

def test_CFPList_compute_cfp_phase():
    cfplist = CFPList(name = 'testcfplist')
    cfplist.init_from_files(inputs_CFP, dict_fuel,dict_materials,dict_gases_impact_factor)
    cfplist.compute_cfp_phase()
    cfplist2 = CFPList(name = 'testcfplist2')
    cfplist2.init_from_files(inputs_CFP2, dict_fuel,dict_materials,dict_gases_impact_factor)
    cfplist2.compute_cfp_phase()

    assert cfplist.CFP_phase.cfp_dismantling.GWP == 0.33
    
def test_CFPList_compute_cfp_global():
    cfplist = CFPList(name = 'testcfplist')
    cfplist.init_from_files(inputs_CFP, dict_fuel,dict_materials,dict_gases_impact_factor)
    cfplist.compute_cfp_global()   

    assert cfplist.CFP_global.GWP == pytest.approx(7.244, 0.5)
    assert round(cfplist.CFP_global.GWP) == round(cfplist.CFP_phase.cfp_dismantling.GWP + cfplist.CFP_phase.cfp_exploitation.GWP + cfplist.CFP_phase.cfp_installation.GWP + cfplist.CFP_phase.cfp_production.GWP + cfplist.CFP_phase.cfp_treatment.GWP)

def test_CFPList_rep_type():
    cfplist = CFPList(name = 'testcfplist')
    cfplist.init_from_files(inputs_CFP, dict_fuel,dict_materials,dict_gases_impact_factor)  
    cfplist.compute_cfp_global()
    rep = cfplist.type_rep()

    assert rep["name"] == 'testcfplist'

def test_CFPList_json_rep():
    cfplist = CFPList(name = 'testcfplist')
    cfplist.init_from_files(inputs_CFP, dict_fuel,dict_materials,dict_gases_impact_factor)  
    cfplist.compute_cfp_global()
    cfplist.saveJSON()
    cfplist2 = CFPList()
    cfplist2.loadJSON(filePath=cfplist.name + ".json")

    assert cfplist2.CFP_global.GWP == pytest.approx(7.244, 0.5)

def test_CFPList_json_rep_name():
    cfplist = CFPList(name = 'testcfplist')
    cfplist.init_from_files(inputs_CFP, dict_fuel,dict_materials,dict_gases_impact_factor)  
    cfplist.compute_cfp_global()
    cfplist.saveJSON()
    cfplist2 = CFPList()
    cfplist2.loadJSON(name=cfplist.name)
    failtry = CFPList()
    failtry.name=''
    try:
        failtry.loadJSON()
    except:
        pass


    assert cfplist2.CFP_global.GWP == pytest.approx(7.244, 0.5)

def test_CFPList_loadjson_raise():
    cfplist2 = CFPList()

    with pytest.raises(Exception):
        cfplist2.loadJSON(filePath='NFA' + ".json")


def test_CFPList_loadFromJSONDict():
    cfplist = CFPList(name = 'testcfplist')
    cfplist.init_from_files(inputs_CFP, dict_fuel,dict_materials,dict_gases_impact_factor)  
    cfplist.compute_cfp_global()
    cfplist.saveJSON()
    cfplist2 = CFPList()
    filePath='testcfplist' + ".json"
    f = open(filePath,"r") 
    data = f.read()

    cfplist2.loadFromJSONDict(data)
    dd = json.loads(data) 
    cfplist2.loadFromJSONDict(dd) 

    assert cfplist2.CFP_global.GWP == pytest.approx(7.244, 0.5)

def test_CFPList_is_set():
    cfplist = CFPList(name = 'testcfplist')
    cfplist.init_from_files(inputs_CFP, dict_fuel,dict_materials,dict_gases_impact_factor)  
    cfplist.compute_cfp_global()
    isset = cfplist.is_set('CFP_global')
    assert isset == True