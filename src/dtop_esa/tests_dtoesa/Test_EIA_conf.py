# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
import os 
import json
import pandas as pd
import numpy as np

from dtop_esa import Business

# Get current directory
dir_path = os.path.dirname(os.path.realpath(__file__))

def test_EIAconf_init_():

    EIAconf = Business.libraries.dtoesa.EIA.EIA_confidence_dict.EIA_confidence_dict(name = "test_conf")
    EIAconf.init_from_files(function_name= 'Footprint', confidence_level= 3)

    assert EIAconf.function_name == 'Footprint'

def test_EIAconf_type_rep():
    EIAconf = Business.libraries.dtoesa.EIA.EIA_confidence_dict.EIA_confidence_dict(name = "test_conf")
    EIAconf.init_from_files(function_name= 'Footprint', confidence_level= 3)
    rep = EIAconf.type_rep()

    assert rep["name"] == 'test_conf'

def test_EIAconf_save_load():
    EIAconf = Business.libraries.dtoesa.EIA.EIA_confidence_dict.EIA_confidence_dict(name = "test_conf")
    EIAconf.init_from_files(function_name= 'Footprint', confidence_level= 3)
    EIAconf.saveJSON()
    
    EIAconf2 = Business.libraries.dtoesa.EIA.EIA_confidence_dict.EIA_confidence_dict()
    EIAconf2.loadJSON(name= EIAconf.name)

    assert EIAconf2.function_name == 'Footprint'

def test_EIAconf_save_load_name():
    EIAconf = Business.libraries.dtoesa.EIA.EIA_confidence_dict.EIA_confidence_dict(name = "test_conf")
    EIAconf.init_from_files(function_name= 'Footprint', confidence_level= 3)
    EIAconf.saveJSON()
    
    EIAconf2 = Business.libraries.dtoesa.EIA.EIA_confidence_dict.EIA_confidence_dict()
    EIAconf2.loadJSON(filePath=EIAconf.name + ".json")
    assert EIAconf2.function_name == 'Footprint'

def test_EIAconf_loadFromJSONDict():
    EIAconf = Business.libraries.dtoesa.EIA.EIA_confidence_dict.EIA_confidence_dict(name = "test_conf")
    EIAconf.init_from_files(function_name= 'Footprint', confidence_level= 3)
    EIAconf.saveJSON()
    
    EIAconf2 = Business.libraries.dtoesa.EIA.EIA_confidence_dict.EIA_confidence_dict()
    filePath='test_conf' + ".json"
    f = open(filePath,"r") 
    data = f.read()

    EIAconf2.loadFromJSONDict(data)
    dd = json.loads(data) 
    EIAconf2.loadFromJSONDict(dd) 
    
    assert EIAconf2.function_name == 'Footprint'

