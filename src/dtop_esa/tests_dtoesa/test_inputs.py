# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys
import os
import copy
import json
from dtop_esa.Business.libraries.dtoesa.inputs.Inputs import Inputs

dir_path = os.path.dirname(os.path.realpath(__file__))
dict_material_file      = dir_path + '/../ressources/dict_materials.json'
dict_materials              = json.load(open(dict_material_file))

def load_json_from_file(filePath):
    if not(os.path.isfile(filePath)):
        raise Exception("file %s not found."%filePath)
    f = open(filePath,"r")
    data_read = f.read()
    f.close()
    data = json.loads(data_read)
    return data

def test_inputs_dto():

    # Get path to example files
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "data_test")
    ec_farm_file = os.path.join(data_dir,'ec_farm.json')
    ed_get_results = os.path.join(data_dir,'ed_results.json')
    sc_farm_file = os.path.join(data_dir,'sc_farm.json') 
    sc_current_file = os.path.join(data_dir,'sc_current.json')
    sc_point_file = os.path.join(data_dir,'sc_point.json')
    sk_environmental_impact = os.path.join(data_dir,'sk_environmental_impact.json')
    mc_dimensions = os.path.join(data_dir,'mc_dimensions.json')   
    mc_general = os.path.join(data_dir,'mc_general.json')
    lmo_results = os.path.join(data_dir,'lmo_results.json')
    catalogue_vessels = os.path.join(data_dir,'catalogue_vessels.json')
    catalogue_dynamic = os.path.join(data_dir,'catalogue_dynamic_cable.json')
    catalogue_static = os.path.join(data_dir,'catalogue_static_cable.json')
    catalogue_collection = os.path.join(data_dir,'catalogue_collection_point.json')

    inp = Inputs()
    inp.run_mode='dto'

    # Load data from file instead of calling API (for testing purpose)
    inp.url_ec_get_farm=load_json_from_file(ec_farm_file)
    inp.url_sc_get_farm=load_json_from_file(sc_farm_file)
    inp.url_sc_get_current=load_json_from_file(sc_current_file)
    inp.url_sc_get_point=load_json_from_file(sc_point_file)
    inp.url_ed_get_results=load_json_from_file(ed_get_results)
    inp.url_sk_get_env_impact=load_json_from_file(sk_environmental_impact)
    inp.url_mc_general=load_json_from_file(mc_general)
    inp.url_mc_dimensions=load_json_from_file(mc_dimensions)
    inp.url_lmo_catalogue=load_json_from_file(lmo_results)

    inp.url_lmo_catalogue=load_json_from_file(catalogue_vessels)
    inp.url_ed_catalogue_static_cable=load_json_from_file(catalogue_static)
    inp.url_ed_catalogue_dynamic_cable=load_json_from_file(catalogue_dynamic)
    inp.url_ed_catalogue_collection_point=load_json_from_file(catalogue_collection)

    # Populate input structure
    inp.populate()

    # print('results1')
    # print(inp.inputs_ESA.inputs_EIA.input_hydro.Coordinates_of_the_Devices_x[1])

    # print('result2')
    # print(inp.inputs_ESA.inputs_EIA.input_hydro.Number_of_Objects)

    # print('lmo')
    # print(inp.inputs_ESA.inputs_CFP.vessel_consumption.Exploitation)

    assert (inp.inputs_ESA.farm_info.Coordinates_of_the_farm_x == 29.3 and inp.inputs_ESA.device_info.number_of_device == pytest.approx(2, 0.001))
    assert inp.inputs_ESA.device_info.foundation.materials.unalloyed_steel.value == pytest.approx(321065.3, 0.1)
    assert inp.inputs_ESA.logistics_info.exploitation.fuel_consumption == 0.0
    assert inp.inputs_ESA.logistics_info.installation.mean_Size_of_Vessels == pytest.approx(68, 1)
    assert inp.inputs_ESA.device_info.resource_reduction == 0.97
    assert inp.inputs_ESA.electrical_info.footprint == pytest.approx(15351672.636369277, 1)