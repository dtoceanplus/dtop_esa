# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
import os 
import json
import pandas as pd
import numpy as np

from dtop_esa import Business

# Get current directory
dir_path = os.path.dirname(os.path.realpath(__file__))

saison = {"january": 10.000076189293443,
                    "february": 10.000076189293443,
                    "march": 10.000076189293443,
                    "april": 10.000076189293443,
                    "may": 10.000076189293443,
                    "june": 10.000076189293443,
                    "july": 10.000076189293443,
                    "august": 10.000076189293443,
                    "september": 10.000076189293443,
                    "october": 10.000076189293443,
                    "november": 10.000076189293443,
                    "december": 10.000076189293443}


def test_EIAseasonscore_init():
    EIAseasonscore = Business.libraries.dtoesa.EIA.EIA_season_score.EIA_season_score(name = "test_season_score")
    EIAseasonscore.June = 12.0
    
    assert EIAseasonscore.name == "test_season_score"

def test_EIAseasonscore_type_rep():
    EIAseasonscore = Business.libraries.dtoesa.EIA.EIA_season_score.EIA_season_score(name = "test_season_score")
    EIAseasonscore.June = 12.0
    rep = EIAseasonscore.type_rep()

    assert rep["name"] == "test_season_score"

def test_EIAseasonscore_savejson():
    EIAseasonscore = Business.libraries.dtoesa.EIA.EIA_season_score.EIA_season_score(name = "test_season_score")
    EIAseasonscore.June = 12.2
    EIAseasonscore.saveJSON()
    EIAseasonscore2 = Business.libraries.dtoesa.EIA.EIA_season_score.EIA_season_score(name = "test_season_score2")
    EIAseasonscore2.loadJSON(name= EIAseasonscore.name)

    assert EIAseasonscore2.June == 12.2

def test_EIAseasonscore_save_load_name():
    EIAseasonscore = Business.libraries.dtoesa.EIA.EIA_season_score.EIA_season_score(name = "test_season_score")
    EIAseasonscore.June = 12.2
    EIAseasonscore.saveJSON()
    EIAseasonscore2 = Business.libraries.dtoesa.EIA.EIA_season_score.EIA_season_score()
 
    EIAseasonscore2.loadJSON(filePath=EIAseasonscore.name + ".json")
    assert EIAseasonscore2.June == 12.2

def test_EIAseasonscore_loadFromJSONDict():
    EIAseasonscore = Business.libraries.dtoesa.EIA.EIA_season_score.EIA_season_score(name = "test_season_score")
    EIAseasonscore.June = 12.2
    EIAseasonscore.saveJSON()
    EIAseasonscore2 = Business.libraries.dtoesa.EIA.EIA_season_score.EIA_season_score(name = "test_season_score2")
 
    filePath='test_season_score' + ".json"
    f = open(filePath,"r") 
    data = f.read()

    EIAseasonscore2.loadFromJSONDict(data)
    dd = json.loads(data) 
    EIAseasonscore2.loadFromJSONDict(dd) 
    
    assert EIAseasonscore2.June == 12.2