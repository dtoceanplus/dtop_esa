# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""py.test tests on main.py

.. moduleauthor:: Mathew Topper <mathew.topper@tecnalia.com>
"""
from dtop_esa import Business
import numpy as np


#Tests positive impact assessment functions

def test_reef_eff():
    
    '''Test res_effect_elect'''

    out = Business.libraries.dtoesa.EIA.dtocean_environment.functions.reef_eff(100,1,50)
    
    assert out == 0.5
    assert isinstance(out, float)

def test_reserve_eff():
    
    '''Test res_effect_found'''

    out = Business.libraries.dtoesa.EIA.dtocean_environment.functions.reserve_eff(70,100)
    
    assert out == 0.7
    assert isinstance(out, float)
    
def test_restplace():
    
    '''Test restplace_dev'''

    out = Business.libraries.dtoesa.EIA.dtocean_environment.functions.restplace(1,50,100)
    
    assert out == 0.5
    assert isinstance(out, float)
    
# Tests for adverse impact assessment functions

def test_energy_mod():
    
    '''Test energy_mod'''

    out = Business.libraries.dtoesa.EIA.dtocean_environment.functions.energy_mod(0.3)

    assert out == 0.3
    assert isinstance(out, float)
    
def test_coll_risM():
    
    '''Test coll_riskM'''

    x = [100, 200, 300]
    y = [300,  50, 100]

    out = Business.libraries.dtoesa.EIA.dtocean_environment.functions.coll_risk([x,y],30,50,100,50)
    
    assert np.isclose(out, 0.1666, rtol=1e-03)
    assert isinstance(out, float)
    
def test_turbidity():
    
    '''Test turbid'''

    out = Business.libraries.dtoesa.EIA.dtocean_environment.functions.turbidity(10,40)
    
    assert out == 1.
    assert isinstance(out, float)
    
def test_undwater_noise():

    out = Business.libraries.dtoesa.EIA.dtocean_environment.functions.undwater_noise(100,50)
    
    assert out==0.
    assert isinstance(out, float)

def test_chempoll_risk():
    
    '''Test chem_poll_I'''

    out = Business.libraries.dtoesa.EIA.dtocean_environment.functions.chempoll_risk('True')
    
    assert out == 1.
    assert isinstance(out, float)
    
def test_footprint():
    
    '''Test footprint()'''
    
    out = Business.libraries.dtoesa.EIA.dtocean_environment.functions.footprint(40,100)
    
    assert out==0.4
    assert isinstance(out,float)

def test_electric_imp():
    
    '''Test electric_imp'''

    out = Business.libraries.dtoesa.EIA.dtocean_environment.functions.electric_imp(60,100)
    
    assert out == 1.0
    assert isinstance(out, float)

def test_temperature_mod():
    
    '''Test temperature_mod'''

    out = Business.libraries.dtoesa.EIA.dtocean_environment.functions.temperature_mod(40,200)
    
    assert out == 1.0
    assert isinstance(out, float)

