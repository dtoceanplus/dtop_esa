# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
import os 
import json 
from pathlib import Path, PurePosixPath
from dtop_esa.Business.libraries.dtoesa.ES.SpeciesList import SpeciesList

# Get current directory
dir_path = os.path.dirname(os.path.realpath(__file__))

database_species_file   = dir_path + '/../ressources/database_species.json'
database_species        = json.load(open(database_species_file))

inputs_ES ={
        "__type__": "dtoesa:inputs_ES",
        "name": "none",
        "description": "",
        "Coordinates_of_the_farm_x": 29.3,
        "Coordinates_of_the_farm_y": 2.3,
        "species_user": [{"name": "Physeter macrocephalus",
                            "Class": "mammals"}
        ],
        "inputs_SC": [{"name": "Physeter macrocephalus",
                            "probability_of_presence": 0.1}]
    }

def test_speciesList_init_from_files(): 

    species = SpeciesList(name = 'testSpecies')
    species.init_from_files(database_species, inputs_ES)

    assert species.inputs['inputs_SC'][0]["probability_of_presence"]== 0.1

def test_speciesList_compute_all(): 

    species = SpeciesList()
    species.init_from_files(database_species, inputs_ES)  
    species.compute_all_species()

    assert species.mammals[0].Probability_of_presence == 0.1



def test_speciesList_rep_type():

    species = SpeciesList()
    species.init_from_files(database_species, inputs_ES)  
    species.compute_all_species()
    rep = species.type_rep()

    assert rep["name"] == 'none'

def test_speciesList_prop_rep():

    species = SpeciesList()
    species.init_from_files(database_species, inputs_ES)  
    species.compute_all_species()
    prop = species.prop_rep()

    assert prop["mammals"][0]['name'] == 'Physeter macrocephalus'

def test_speciesList_json_rep():

    species = SpeciesList()
    species.init_from_files(database_species, inputs_ES)  
    species.compute_all_species()
    species.saveJSON()
    species2 = SpeciesList()
    species2.loadJSON(filePath=species.name + ".json")

    assert species2.mammals[0].Order == "Cetacea"

def test_speciesList_loadjson_rep_name():

    species = SpeciesList()
    species.init_from_files(database_species, inputs_ES)  
    species.compute_all_species()
    species.saveJSON()
    species2 = SpeciesList()
    species2.loadJSON(name = species.name)

    assert species2.mammals[0].Order == "Cetacea"

def test_speciesList_loadjson_is_file_raise():

    species = SpeciesList()
    species.init_from_files(database_species, inputs_ES)  
    species.compute_all_species()
    species.saveJSON()
    species2 = SpeciesList()
    
    with pytest.raises(Exception):
        species2.loadJSON(filePath='species' + ".json")


def test_speciesList_loadFromJSONDict():

    species = SpeciesList()
    species.init_from_files(database_species, inputs_ES)  
    species.compute_all_species()
    species.saveJSON()
    species2 = SpeciesList()
    filePath='none' + ".json"
    f = open(filePath,"r") 
    data = f.read()

    species2.loadFromJSONDict(data)
    dd = json.loads(data) 
    species2.loadFromJSONDict(dd) 
    assert species2.mammals[0].Order == "Cetacea"

def test_speciesList_is_set():

    species = SpeciesList()
    species.init_from_files(database_species, inputs_ES)  
    species.compute_all_species()
    isset = species.is_set('mammals')
    assert isset == True
