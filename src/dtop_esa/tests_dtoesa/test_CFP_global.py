# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
import os 
import json 

# Get current directory
dir_path = os.path.dirname(os.path.realpath(__file__))
from dtop_esa import Business

# Define inputs
inputs_cfp_file     = dir_path + '/../data_test/inputs_CFP.json'
dict_fuel_file      = dir_path + '/../ressources/dict_fuel.json'
dict_material_file  = dir_path + '/../ressources/dict_materials.json'
dict_gases_file     = dir_path + '/../ressources/dict_gases_impact_factor.json'

dict_fuel                 = json.load(open(dict_fuel_file))
dict_materials            = json.load(open(dict_material_file))
dict_gases_impact_factor  = json.load(open(dict_gases_file))
inputs_CFP                = json.load(open(inputs_cfp_file))
def test_CFPglobal_init():
    cfpglobal = Business.libraries.dtoesa.CFP.CFP_global.CFP_global(name = 'testcfpglobal')

    assert cfpglobal.name == 'testcfpglobal'

def test_CFPglobal_rep_type():
    cfpglobal = Business.libraries.dtoesa.CFP.CFP_global.CFP_global(name = 'testcfpglobal')
    rep = cfpglobal.type_rep()

    assert rep["name"] == 'testcfpglobal'

def test_CFPglobal_json_rep():
    cfpglobal = Business.libraries.dtoesa.CFP.CFP_global.CFP_global(name = 'testcfpglobal')
    cfpglobal.GWP = 2620816
    cfpglobal.saveJSON()
    cfpglobal2 = Business.libraries.dtoesa.CFP.CFP_global.CFP_global()
    cfpglobal2.loadJSON(filePath=cfpglobal.name + ".json")

    assert cfpglobal2.GWP == pytest.approx(2620816.1, 0.5)

def test_CFPglobal_json_rep_name():
    cfpglobal = Business.libraries.dtoesa.CFP.CFP_global.CFP_global(name = 'testcfpglobal')
    cfpglobal.GWP = 2620816
    cfpglobal.saveJSON()
    cfpglobal2 = Business.libraries.dtoesa.CFP.CFP_global.CFP_global()
    cfpglobal2.loadJSON(name=cfpglobal.name)

    assert cfpglobal2.GWP == pytest.approx(2620816.1, 0.5)

def test_CFPglobal_loadjson_raise():
    cfpglobal2 = Business.libraries.dtoesa.CFP.CFP_global.CFP_global()

    with pytest.raises(Exception):
        cfpglobal2.loadJSON(filePath='NFA' + ".json")

def test_CFPglobal_loadFromJSONDict():
    cfpglobal = Business.libraries.dtoesa.CFP.CFP_global.CFP_global(name = 'testcfpglobal')
    cfpglobal.GWP = 2620816
    cfpglobal.saveJSON()
    cfpglobal2 = Business.libraries.dtoesa.CFP.CFP_global.CFP_global()
    filePath='testcfpglobal' + ".json"
    f = open(filePath,"r") 
    data = f.read()

    cfpglobal2.loadFromJSONDict(data)
    dd = json.loads(data) 
    cfpglobal2.loadFromJSONDict(dd) 
    assert cfpglobal2.GWP == 2620816

def test_CFPglobal_is_set():
    cfpglobal = Business.libraries.dtoesa.CFP.CFP_global.CFP_global(name = 'testcfpglobal') 
    cfpglobal.GWP = 2620816
    isset = cfpglobal.is_set('GWP')
    assert isset == True