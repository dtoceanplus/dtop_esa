# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
import os 
import json 
from dtop_esa import Business


# Get current directory
dir_path = os.path.dirname(os.path.realpath(__file__))
# Define inputs
database= dir_path + '/../ressources/database_species.json'
database = json.load(open(database))

def test_species_init_from_file():
    species = Business.libraries.dtoesa.ES.Species.Species(name='Balaenoptera musculus')
    species.init_from_file(database, 0.2)

    assert species.Probability_of_presence == 0.2
    assert species.Class == 'Mammals'
    
def test_species_rep_type():
    species = Business.libraries.dtoesa.ES.Species.Species(name='Balaenoptera musculus')
    species.init_from_file(database, 0.2)
    rep = species.type_rep()

    assert rep["name"] == 'Balaenoptera musculus'

def test_species_prop_rep():
    species = Business.libraries.dtoesa.ES.Species.Species(name='Balaenoptera musculus')
    species.init_from_file(database, 0.2)
    prop = species.prop_rep()

    assert prop["Probability_of_presence"] == 0.2

def test_species_json_rep():
    species = Business.libraries.dtoesa.ES.Species.Species(name='Balaenoptera musculus')
    species.init_from_file(database, 0.2)
    species.saveJSON()
    species2 = Business.libraries.dtoesa.ES.Species.Species()
    species2.loadJSON(filePath=species.name + ".json")

    assert species2.Order == "Cetacea"

def test_species_json_rep_name():
    species = Business.libraries.dtoesa.ES.Species.Species(name='Balaenoptera musculus')
    species.init_from_file(database, 0.2)
    species.saveJSON()
    species2 = Business.libraries.dtoesa.ES.Species.Species()
    species2.loadJSON(name = species.name)

    assert species2.Order == "Cetacea"

def test_species_loadjson_is_file_raise():
    species = Business.libraries.dtoesa.ES.Species.Species(name='Balaenoptera musculus')
    species.init_from_file(database, 0.2)
    species.saveJSON()
    species2 = Business.libraries.dtoesa.ES.Species.Species()
    
    with pytest.raises(Exception):
        species2.loadJSON(filePath='species' + ".json")


def test_species_loadFromJSONDict():
    species = Business.libraries.dtoesa.ES.Species.Species(name='Balaenoptera musculus')
    species.init_from_file(database, 0.2)
    species.saveJSON()
    species2 = Business.libraries.dtoesa.ES.Species.Species()
    filePath='Balaenoptera musculus' + ".json"
    f = open(filePath,"r") 
    data = f.read()

    species2.loadFromJSONDict(data)
    dd = json.loads(data) 
    species2.loadFromJSONDict(dd) 
    print(species2)
    assert species2.Order == "Cetacea"

def test_species_is_set():
    species = Business.libraries.dtoesa.ES.Species.Species(name='Balaenoptera musculus')
    species.init_from_file(database, 0.2)
    isset = species.is_set('Class')
    assert isset == True
