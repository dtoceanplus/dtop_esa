# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
import os 
import json
import pandas as pd
import numpy as np

from dtop_esa import Business

# Get current directory
dir_path = os.path.dirname(os.path.realpath(__file__))

saison = {"january": 10.000076189293443,
                    "february": 10.000076189293443,
                    "march": 10.000076189293443,
                    "april": 10.000076189293443,
                    "may": 10.000076189293443,
                    "june": 10.000076189293443,
                    "july": 10.000076189293443,
                    "august": 10.000076189293443,
                    "september": 10.000076189293443,
                    "october": 10.000076189293443,
                    "november": 10.000076189293443,
                    "december": 10.000076189293443}



def test_EIAseason_init_():

    EIAseason = Business.libraries.dtoesa.EIA.EIA_season.EIA_season(name = "test_season")
    EIAseason.init_from_files(function_name = 'function_test', season_info = saison)

    assert EIAseason.season_score.January == pytest.approx(10.00007)

def test_EIAseason_type_rep():
    EIAseason = Business.libraries.dtoesa.EIA.EIA_season.EIA_season(name = "test_season")
    EIAseason.init_from_files(function_name = 'function_test', season_info = saison)
    rep = EIAseason.type_rep()

    assert rep["name"] == 'test_season'

def test_EIAseason_save_load():
    EIAseason = Business.libraries.dtoesa.EIA.EIA_season.EIA_season(name = "test_season")
    EIAseason.init_from_files(function_name = 'function_test', season_info = saison)
    EIAseason.saveJSON()
    
    EIAseason2 = Business.libraries.dtoesa.EIA.EIA_season.EIA_season()
    EIAseason2.loadJSON(name= EIAseason.name)

    assert EIAseason2.function_name == 'function_test'

def test_EIAseason_save_load_name():
    EIAseason = Business.libraries.dtoesa.EIA.EIA_season.EIA_season(name = "test_season")
    EIAseason.init_from_files(function_name = 'function_test', season_info = saison)
    EIAseason.saveJSON()
    
    EIAseason2 = Business.libraries.dtoesa.EIA.EIA_season.EIA_season()
    EIAseason2.loadJSON(filePath=EIAseason.name + ".json")
    assert EIAseason2.function_name == 'function_test'

def test_EIAseason_loadFromJSONDict():
    EIAseason = Business.libraries.dtoesa.EIA.EIA_season.EIA_season(name = "test_season")
    EIAseason.init_from_files(function_name = 'function_test', season_info = saison)
    EIAseason.saveJSON()
    EIAseason2 = Business.libraries.dtoesa.EIA.EIA_season.EIA_season()
    filePath='test_season' + ".json"
    f = open(filePath,"r") 
    data = f.read()

    EIAseason2.loadFromJSONDict(data)
    dd = json.loads(data) 
    EIAseason2.loadFromJSONDict(dd) 
    
    assert EIAseason2.function_name == 'function_test'

