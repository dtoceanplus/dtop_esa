# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
import os 
import json 

from dtop_esa import Business
# Get current directory
dir_path = os.path.dirname(os.path.realpath(__file__))


# Define inputs
inputs_cfp_file     = dir_path + '/../data_test/inputs_CFP.json'
dict_fuel_file      = dir_path + '/../ressources/dict_fuel.json'
dict_material_file  = dir_path + '/../ressources/dict_materials.json'
dict_gases_file     = dir_path + '/../ressources/dict_gases_impact_factor.json'

dict_fuel                 = json.load(open(dict_fuel_file))
dict_materials            = json.load(open(dict_material_file))
dict_gases_impact_factor  = json.load(open(dict_gases_file))
inputs_CFP                = json.load(open(inputs_cfp_file))

def test_CFPphase_init():
    cfpphase = Business.libraries.dtoesa.CFP.CFP_phase.CFP_phase(name = 'testcfpphase')

    assert cfpphase.name == 'testcfpphase'

def test_CFPphase_get_cfp_phase():
    cfpphase = Business.libraries.dtoesa.CFP.CFP_phase.CFP_phase()
    cfpphase.get_cfp_phase(phase = 'Installation', inputs =inputs_CFP, dict_fuel= dict_fuel, dict_materials = dict_materials, dict_gases_impact_factor = dict_gases_impact_factor )
    cfpphase.get_cfp_phase(phase = 'Production', inputs =inputs_CFP, dict_fuel= dict_fuel, dict_materials = dict_materials, dict_gases_impact_factor = dict_gases_impact_factor )
    assert cfpphase.cfp_installation.GWP == pytest.approx(0.85, 0.1)
    assert cfpphase.cfp_production.GWP == pytest.approx(6.465, 0.1)

def test_CFPphase_rep_type():
    cfpphase = Business.libraries.dtoesa.CFP.CFP_phase.CFP_phase(name = 'testcfpphase')
    rep = cfpphase.type_rep()

    assert rep["name"] == 'testcfpphase'

def test_CFPphase_json_rep():
    cfpphase = Business.libraries.dtoesa.CFP.CFP_phase.CFP_phase(name = 'testcfpphase')
    cfpphase.get_cfp_phase(phase = 'Installation', inputs =inputs_CFP, dict_fuel= dict_fuel, dict_materials = dict_materials, dict_gases_impact_factor = dict_gases_impact_factor )
    cfpphase.get_cfp_phase(phase = 'Production', inputs =inputs_CFP, dict_fuel= dict_fuel, dict_materials = dict_materials, dict_gases_impact_factor = dict_gases_impact_factor )
    cfpphase.saveJSON()
    cfpphase2 = Business.libraries.dtoesa.CFP.CFP_phase.CFP_phase()
    cfpphase2.loadJSON(filePath=cfpphase.name + ".json")
    print(cfpphase2)
    assert cfpphase2.cfp_installation.GWP == pytest.approx(0.85, 0.1)

def test_CFPphase_json_rep_name():
    cfpphase = Business.libraries.dtoesa.CFP.CFP_phase.CFP_phase(name = 'testcfpphase')
    cfpphase.get_cfp_phase(phase = 'Installation', inputs =inputs_CFP, dict_fuel= dict_fuel, dict_materials = dict_materials, dict_gases_impact_factor = dict_gases_impact_factor )
    cfpphase.saveJSON()
    cfpphase2 = Business.libraries.dtoesa.CFP.CFP_phase.CFP_phase()
    cfpphase2.loadJSON(name=cfpphase.name)

    assert cfpphase2.cfp_installation.GWP == pytest.approx(0.85, 0.1)

def test_CFPphase_loadjson_raise():
    cfpphase2 = Business.libraries.dtoesa.CFP.CFP_phase.CFP_phase()

    with pytest.raises(Exception):
        cfpphase2.loadJSON(filePath='NFA' + ".json")

def test_CFPphase_loadFromJSONDict():
    cfpphase = Business.libraries.dtoesa.CFP.CFP_phase.CFP_phase(name = 'testcfpphase')
    cfpphase.get_cfp_phase(phase = 'Installation', inputs =inputs_CFP, dict_fuel= dict_fuel, dict_materials = dict_materials, dict_gases_impact_factor = dict_gases_impact_factor )
    cfpphase.get_cfp_phase(phase = 'Production', inputs =inputs_CFP, dict_fuel= dict_fuel, dict_materials = dict_materials, dict_gases_impact_factor = dict_gases_impact_factor )
    cfpphase.saveJSON()
    cfpphase2 = Business.libraries.dtoesa.CFP.CFP_phase.CFP_phase()
    filePath='testcfpphase' + ".json"
    f = open(filePath,"r") 
    data = f.read()

    cfpphase2.loadFromJSONDict(data)
    dd = json.loads(data) 
    cfpphase2.loadFromJSONDict(dd) 
    
    assert cfpphase2.cfp_installation.GWP == pytest.approx(0.85, 0.1)

def test_CFPphase_is_set():
    cfpphase = Business.libraries.dtoesa.CFP.CFP_phase.CFP_phase(name = 'testcfpphase') 
    cfpphase.GWP = 2620816
    isset = cfpphase.is_set('GWP')
    assert isset == True
