# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
import os 
import json

from dtop_esa.Business.libraries.dtoesa.EIA.EIAList import EIAList
from dtop_esa.Business.libraries.dtoesa.inputs.inputs_EIA import inputs_EIA
# Get current directory
dir_path = os.path.dirname(os.path.realpath(__file__))

import numpy as np
import pandas as pd


test_data_dir = os.path.join(dir_path, "test_data")
# Define inputs

f = open(os.path.join(test_data_dir,"positions.txt"), 'r')
# data = np.genfromtxt(f)
# x=np.zeros(50)
# y=np.zeros(50)

# for ii in range(0, 50):
# datatmp=data[ii]
# x[ii]=datatmp[0]
# y[ii]=datatmp[1]


# input_hydro = {"Energy Modification" : 0.3,
# "Coordinates of the Devices"  : [x,y],
# "Size of the Devices" : 10.,
# "Immersed Height of the Devices"  : 10.,
# "Water Depth" : 15.,
# "Current Direction"   : 45.,
# "Initial Turbidity"   : 50.,
# "Measured Turbidity"  : 70.,
# "Initial Noise dB re 1muPa"   : 60.,
# "Measured Noise dB re 1muPa"  : 150.,
# "Fishery Restriction Surface" : 1000.,
# "Total Surface Area"  : 94501467.,
# "Number of Objects"   : 50,
# "Object Emerged Surface"  : 0,
# "Surface Area of Underwater Part" : 60.
# }   
# input_elec = {"Coordinates of the Devices"  : [x,y],
# "Size of the Devices" : 30.,
# "Immersed Height of the Devices"  : 10.,
# "Water Depth" : 15.,
# "Current Direction"   : 45.,
# "Initial Noise dB re 1muPa"   : 60.,
# "Measured Noise dB re 1muPa"  : 150.,
# "Fishery Restriction Surface" : 1000.,
# "Total Surface Area"  : 94501467.,
# "Number of Objects"   : 50,
# "Object Emerged Surface"  : 20.,
# "Surface Area of Underwater Part" : 60.,
# "Surface Area Covered": 150.0,
# "Initial Electric Field"  : 0.0,
# "Measured Electric Field" : 0.0,
# "Initial Magnetic Field"  : 0.0,
# "Measured Magnetic Field" : 0.0,
# "Initial Temperature" : 0.0,
# "Measured Temperature": 0.0
# }
# input_moor ={"Coordinates of the Devices"  : [x,y],
#   "Size of the Devices" : 10.,
#   "Immersed Height of the Devices"  : 10.,
#   "Water Depth" : 15.,
#   "Current Direction"   : 45.,
#   "Initial Noise dB re 1muPa"   : 60.,
#   "Measured Noise dB re 1muPa"  : 150.,
#   "Total Surface Area"  : 94501467.,
#   "Number of Objects"   : 50,
#   "Surface Area of Underwater Part" : 60.,
#   "Surface Area Covered": 150.0
#   }
# input_insta = {"Initial Noise dB re 1muPa"   : 60.,
#   "Measured Noise dB re 1muPa"  : 150.,
#   "Total Surface Area"  : 94501467.,
#   "Surface Area Covered": 150.0,
#   "Number of Vessels"   : 3,
#   "Size of Vessels" : 12,
#   "Initial Turbidity"   : 50.,
#   "Measured Turbidity"  : 70.,
#   "Import of Chemical Polutant" : 0
#   }
# input_maint = {"Initial Noise dB re 1muPa"   : 60.,
#   "Measured Noise dB re 1muPa"  : 150.,
#   "Total Surface Area"  : 94501467.,
#   "Surface Area Covered": 150.0,
#   "Number of Vessels"   : 3,
#   "Size of Vessels" : 12,
#   "Initial Turbidity"   : 50.,
#   "Measured Turbidity"  : 70.,
#   "Import of Chemical Polutant" : 0
#   }
# # input_hydro = input_hydro.input_hydro()
# # input_hydro.Energy_Modification = 0.3
# # input_hydro.Coordinates_of_the_Devices  = [x,y]
# # input_hydro.Size_of_the_Devices = 30
# # input_hydro.Immersed_Height_of_the_Devices  = 10
# # input_hydro.Water_Depth = 15.
# # input_hydro.Current_Direction   = 45.
# # input_hydro.Initial_Turbidity   = 50.
# # input_hydro.Measured_Turbidity  = 70.
# # input_hydro.Initial_Noise_dB_re_1muPa   = 60.
# # input_hydro.Measured_Noise_dB_re_1muPa  = 150.
# # input_hydro.Fishery_Restriction_Surface = 1000.
# # input_hydro.Total_Surface_Area  = 94501467.
# # input_hydro.Number_of_Objects   = 50
# # input_hydro.Object_Emerged_Surface  = 20.
# # input_hydro.Surface_Area_of_Underwater_Part = 60.

# # input_elec = input_elec.input_elec()
# # input_elec.Coordinates_of_the_Devices  = [x,y]
# # input_elec.Size_of_the_Devices = 30.
# # input_elec.Immersed_Height_of_the_Devices  = 10.
# # input_elec.Water_Depth = 15.
# # input_elec.Current_Direction   = 45.
# # input_elec.Initial_Noise_dB_re_1muPa   = 60.
# # input_elec.Measured_Noise_dB_re_1muPa  = 150.
# # input_elec.Fishery_Restriction_Surface = 1000.
# # input_elec.Total_Surface_Area  = 94501467.
# # input_elec.Number_of_Objects   = 50
# # input_elec.Object_Emerged_Surface  = 20.
# # input_elec.Surface_Area_of_Underwater_Part = 60.
# # input_elec.Surface_Area_Covered= 150.0
# # input_elec.Initial_Electric_Field  = 0.0
# # input_elec.Measured_Electric_Field = 0.0
# # input_elec.Initial_Magnetic_Field  = 0.0
# # input_elec.Measured_Magnetic_Field = 0.0
# # input_elec.Initial_Temperature = 0.0
# # input_elec.Measured_Temperature= 0.0

# # input_moor = input_moor.input_moor() 
# # input_moor.Coordinates_of_the_Devices  = [x,y]
# # input_moor.Size_of_the_Devices = 30.
# # input_moor.Immersed_Height_of_the_Devices  = 10.
# # input_moor.Water_Depth = 15.
# # input_moor.Current_Direction   = 45.
# # input_moor.Initial_Noise_dB_re_1muPa   = 60.
# # input_moor.Measured_Noise_dB_re_1muPa  = 150.
# # input_moor.Total_Surface_Area  = 94501467.
# # input_moor.Number_of_Objects   = 50
# # input_moor.Surface_Area_of_Underwater_Part = 60.
# # input_moor.Surface_Area_Covered= 150.0

# # input_insta = input_insta.input_insta() 
# # input_insta.Initial_Noise_dB_re_1muPa   = 60.
# # input_insta.Measured_Noise_dB_re_1muPa  = 150.
# # input_insta.Total_Surface_Area  = 94501467.
# # input_insta.Surface_Area_Covered= 150.0
# # input_insta.Number_of_Vessels   = 6
# # input_insta.Size_of_Vessels = 12
# # input_insta.Initial_Turbidity   = 50.
# # input_insta.Measured_Turbidity  = 60.
# # input_insta.Import_of_Chemical_Polutant = 3.0

# # input_maint = input_maint.input_maint()
# # input_maint.Initial_Noise_dB_re_1muPa   = 60.
# # input_maint.Measured_Noise_dB_re_1muPa  = 120.
# # input_maint.Total_Surface_Area  = 94501467.
# # input_maint.Surface_Area_Covered= 112.0
# # input_maint.Number_of_Vessels   = 3
# # input_maint.Size_of_Vessels = 12
# # input_maint.Initial_Turbidity   = 50.
# # input_maint.Measured_Turbidity  = 70.
# # input_maint.Import_of_Chemical_Polutant = 15.0
   


protected_dict = {"species name": ["Balaenopteridae",
   "Balaenidae",
   "Phocidae",
   "Delphinidae",
   "Acipenseridae",
   "Anguillidae",
   "Scombridae",
   "Lamnidae",
   "Cetorhinidae",
   "Rajidae",
   "Squatinidae",
   "Anatidae",
   "Scolopacidae",
   "Procellariidae",
   "Cheloniidae",
   "Dermochelyidae",
   "Testunidae",
   "particular habitat",
   "fish"],
  "observed": [False, False, False, False,False, False, False, False, False,False, False, False, False, False,False, False, False, False, False]}
  
protected_table = pd.DataFrame(protected_dict)
protected_table = protected_table.set_index("species name")


table_path = os.path.join(test_data_dir, "species_receptors.csv")
receptors_table = pd.read_csv(table_path, index_col=0)
# inputs_EIA = inputs_EIA()
# inputs_EIA.input_hydro = input_hydro 
# inputs_EIA.input_elec = input_elec
# inputs_EIA.input_moor = input_moor
# inputs_EIA.input_insta = input_insta
# inputs_EIA.input_maint = input_maint
x= [634280.1105]
y= [6654718.566]
inputs_EIA=  {

"input_hydro": {
"Energy_Modification": 0.3,
"Coordinates of the Devices":[x,y],
"Size_of_the_Devices": 10.0,
"Immersed_Height_of_the_Devices": 10.0,
"Water_Depth": 15.0,
"Current_Direction": 45.0,
"Initial_Turbidity": 50.0,
"Measured_Turbidity": 70.0,
"Initial_Noise_dB_re_1muPa": 60.0,
"Measured_Noise_dB_re_1muPa": 150.0,
"Fishery_Restriction_Surface": 1000.0,
"Total_Surface_Area": 94501467.0,
"Number_of_Objects": 1.0,
"Object_Emerged_Surface": 0.0,
"Surface_Area_of_Underwater_Part": 60.0
},
"input_elec": {

"Coordinates of the Devices":[x,y],
"Size_of_the_Devices": 30.0,
"Immersed_Height_of_the_Devices": 10.0,
"Water_Depth": 15.0,
"Current_Direction": 45.0,
"Initial_Noise_dB_re_1muPa": 60.0,
"Measured_Noise_dB_re_1muPa": 150.0,
"Fishery_Restriction_Surface": 1000.0,
"Total_Surface_Area": 94501467.0,
"Number_of_Objects": 50.0,
"Object_Emerged_Surface": 20.0,
"Surface_Area_of_Underwater_Part": 60.0,
"Surface_Area_Covered": 150.0,
"Initial_Electric_Field": 0.0,
"Measured_Electric_Field": 0.0,
"Initial_Magnetic_Field": 0.0,
"Measured_Magnetic_Field": 0.0,
"Initial_Temperature": 0.0,
"Measured_Temperature": 0.0
},
"input_moor": {
"Coordinates of the Devices":[x,y],
"Size_of_the_Devices": 10.0,
"Immersed_Height_of_the_Devices": 10.0,
"Water_Depth": 15.0,
"Current_Direction": 45.0,
"Initial_Noise_dB_re_1muPa": 60.0,
"Measured_Noise_dB_re_1muPa": 150.0,
"Total_Surface_Area": 94501467,
"Number_of_Objects": 50.0,
"Surface_Area_of_Underwater_Part": 60.0,
"Surface_Area_Covered": 150.0
},
"input_insta": {
"Initial_Noise_dB_re_1muPa": 60.0,
"Measured_Noise_dB_re_1muPa": 150.0,
"Total_Surface_Area": 94501467,
"Surface_Area_Covered": 150.0,
"Number_of_Vessels": 3.0,
"Size_of_Vessels": 12.0,
"Initial_Turbidity": 50.0,
"Measured_Turbidity": 70.0,
"Import_of_Chemical_Polutant": 0.0
},
"input_maint": {
"Initial_Noise_dB_re_1muPa": 60.0,
"Measured_Noise_dB_re_1muPa": 150.0,
"Total_Surface_Area": 94501467.0,
"Surface_Area_Covered": 150.0,
"Number_of_Vessels": 3.0,
"Size_of_Vessels": 12.0,
"Initial_Turbidity": 50.0,
"Measured_Turbidity": 70.0,
"Import_of_Chemical_Polutant": 0.0
}}
weighting_dict= {
            "__type__": "dtoesa:inputs:weighting_dict",
            "name": "none",
            "description": "",
            "weighting_hydro": {
                "__type__": "dtoesa:inputs:weighting_list",
                "name": "none",
                "description": "",
                "Energy Modification": "Sand",
                "Collision Risk": "open water/ devices in serie",
                "Collision Risk Vessel": "",
                "Chemical Pollution": "",
                "Turbidity": "",
                "Underwater Noise": "noise device 90 - 100 dB re 1muPa",
                "Reserve Effect": "no restriction",
                "Reef Effect": "Wave design horizontal",
                "Resting Place": "No dangerous part of devices",
                "Footprint": "",
                "Electric Fields": "",
                "Magnetic Fields": "",
                "Temperature Modification": ""
            },
            "weighting_elec": {
                "__type__": "dtoesa:inputs:weighting_list",
                "name": "none",
                "description": "",
                "Energy Modification": "",
                "Collision Risk": "cable buried",
                "Collision Risk Vessel": "",
                "Chemical Pollution": "",
                "Turbidity": "",
                "Underwater Noise": "noise electrical components 0 dB re 1muPa",
                "Reserve Effect": "no restriction",
                "Reef Effect": "substation",
                "Resting Place": "",
                "Footprint": "cable buried",
                "Electric Fields": "cable buried",
                "Magnetic Fields": "cable buried",
                "Temperature Modification": "cable buried"
            },
            "weighting_moor": {
                "__type__": "dtoesa:inputs:weighting_list",
                "name": "none",
                "description": "",
                "Energy Modification": "",
                "Collision Risk": "taut",
                "Collision Risk Vessel": "",
                "Chemical Pollution": "",
                "Turbidity": "",
                "Underwater Noise": "chafing 90 dB re 1muPa",
                "Reserve Effect": "",
                "Reef Effect": "No Data",
                "Resting Place": "",
                "Footprint": "No Data",
                "Electric Fields": "",
                "Magnetic Fields": "",
                "Temperature Modification": ""
            },
            "weighting_insta": {
                "__type__": "dtoesa:inputs:weighting_list",
                "name": "none",
                "description": "",
                "Energy Modification": "",
                "Collision Risk": "",
                "Collision Risk Vessel": "No Data",
                "Chemical Pollution": "highly toxic antifouling",
                "Turbidity": "No Data",
                "Underwater Noise": "noise vessels or tools 90 - 100 dB re 1muPa",
                "Reserve Effect": "",
                "Reef Effect": "",
                "Resting Place": "",
                "Footprint": "No Data",
                "Electric Fields": "",
                "Magnetic Fields": "",
                "Temperature Modification": ""
            },
            "weighting_maint": {
                "__type__": "dtoesa:inputs:weighting_list",
                "name": "none",
                "description": "",
                "Energy Modification": "",
                "Collision Risk": "",
                "Collision Risk Vessel": "No Data",
                "Chemical Pollution": "natural antifouling",
                "Turbidity": "No Data",
                "Underwater Noise": "noise vessels or tools 90 - 100 dB re 1muPa",
                "Reserve Effect": "",
                "Reef Effect": "",
                "Resting Place": "",
                "Footprint": "No Data",
                "Electric Fields": "",
                "Magnetic Fields": "",
                "Temperature Modification": ""
            }
        }
inputs_hydro = inputs_EIA["input_hydro"]
inputs_EIA["input_hydro"] = {k.replace("_"," "): v for k,v in inputs_hydro.items()}

inputs_elec = inputs_EIA["input_elec"]
inputs_EIA["input_elec"] = {k.replace("_"," "): v for k,v in inputs_elec.items()}

inputs_moor = inputs_EIA["input_moor"]
inputs_EIA["input_moor"] = {k.replace("_"," "): v for k,v in inputs_moor.items()}

inputs_insta = inputs_EIA["input_insta"]
inputs_EIA["input_insta"] = {k.replace("_"," "): v for k,v in inputs_insta.items()}
inputs_maint = inputs_EIA["input_maint"]
inputs_EIA["input_maint"] = {k.replace("_"," "): v for k,v in inputs_maint.items()}

def test_EIAList_init_from_files():
  EIAlist = EIAList(name = 'testEIAlist')
  EIAlist.init_from_files(inputs_EIA, protected_table, receptors_table, weighting_dict)

  assert EIAlist.inputs["input_elec"]['Size of the Devices'] == 30

def test_EIAList_compute_EIA_tech_group():

  EIAlist = EIAList(name = 'testEIAlist')
  EIAlist.init_from_files(inputs_EIA, protected_table, receptors_table, weighting_dict)
  EIAlist.compute_eia_tech_group()
  EIAlist.compute_eia_pressure()

  assert EIAlist.eia_tech_group.impact_hydrodynamics.global_eis.min_negative_impact == -58
  assert EIAlist.eia_pressure.energy_modification.score== -74
def test_EIAList_compute_EIA_global():

  EIAlist = EIAList(name = 'testEIAlist')
  EIAlist.init_from_files(inputs_EIA, protected_table, receptors_table, weighting_dict)
  EIAlist.compute_eia_global()   

  assert EIAlist.eia_global.positive_impact == 10

def test_EIAList_rep_type():
  EIAlist = EIAList(name = 'testEIAlist')
  EIAlist.init_from_files(inputs_EIA, protected_table, receptors_table, weighting_dict)  
  EIAlist.compute_eia_global()
  rep = EIAlist.type_rep()

  assert rep["name"] == 'testEIAlist'

def test_EIAList_json_rep():
  EIAlist = EIAList(name = 'testEIAlist')
  EIAlist.init_from_files(inputs_EIA, protected_table, receptors_table, weighting_dict)  
  EIAlist.compute_eia_global()
  EIAlist.compute_eia_tech_group()
  EIAlist.compute_eia_pressure()

  EIAlist.saveJSON()
  EIAlist2 = EIAList()
  EIAlist2.loadJSON(filePath=EIAlist.name + ".json")

  assert EIAlist2.eia_tech_group.impact_hydrodynamics.global_eis.min_negative_impact == -58

def test_EIAList_json_rep_name():
  EIAlist = EIAList(name = 'testEIAlist')
  EIAlist.init_from_files(inputs_EIA, protected_table, receptors_table, weighting_dict)  
  EIAlist.compute_eia_global()
  EIAlist.compute_eia_tech_group()
  EIAlist.compute_eia_pressure()

  EIAlist.saveJSON()
  EIAlist2 = EIAList()
  EIAlist2.loadJSON(name=EIAlist.name)

  assert EIAlist2.eia_global.max_negative_impact == -42.0

def test_EIAList_loadjson_raise():
  EIAlist2 = EIAList()

  with pytest.raises(Exception):
    EIAlist2.loadJSON(filePath='NFA' + ".json")


def test_EIAList_loadFromJSONDict():
  EIAlist = EIAList(name = 'testEIAlist')
  EIAlist.init_from_files(inputs_EIA, protected_table, receptors_table, weighting_dict)  
  EIAlist.compute_eia_global()
  EIAlist.compute_eia_tech_group()
  EIAlist.saveJSON()
  EIAlist2 = EIAList()
  filePath='testEIAlist' + ".json"
  f = open(filePath,"r") 
  data = f.read()

  EIAlist2.loadFromJSONDict(data)
  dd = json.loads(data) 
  EIAlist2.loadFromJSONDict(dd) 

  assert EIAlist2.eia_global.max_negative_impact == -42.0

def test_EIAList_is_set():
  EIAlist = EIAList(name = 'testEIAlist')
  EIAlist.init_from_files(inputs_EIA, protected_table, receptors_table, weighting_dict)  
  EIAlist.compute_eia_global()
  EIAlist.compute_eia_tech_group()
  isset = EIAlist.is_set('eia_global')
  assert isset == True