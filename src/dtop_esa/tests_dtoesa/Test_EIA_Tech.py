# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest
import os 
import json
import pandas as pd
import numpy as np

from dtop_esa import Business

# Get current directory
dir_path = os.path.dirname(os.path.realpath(__file__))

def test_EIATech_init_():

    EIATech = Business.libraries.dtoesa.EIA.EIA_tech.EIA_tech(name = "test_tech")

    assert EIATech.name == "test_tech"

def test_EIATech_type_rep():
    EIATech = Business.libraries.dtoesa.EIA.EIA_tech.EIA_tech(name = "test_tech")
    rep = EIATech.type_rep()

    assert rep["name"] == 'test_tech'

def test_EIATech_save_load():
    EIATech = Business.libraries.dtoesa.EIA.EIA_tech.EIA_tech(name = "test_tech")
    EIATech.saveJSON()
    
    EIATech2 = Business.libraries.dtoesa.EIA.EIA_tech.EIA_tech()
    EIATech2.loadJSON(name= EIATech.name)

    assert EIATech2.name == 'test_tech'

def test_EIATech_save_load_name():
    EIATech = Business.libraries.dtoesa.EIA.EIA_tech.EIA_tech(name = "test_tech")
    EIATech.saveJSON()
    
    EIATech2 = Business.libraries.dtoesa.EIA.EIA_tech.EIA_tech()
    EIATech2.loadJSON(filePath=EIATech.name + ".json")

    assert EIATech2.name == 'test_tech'

def test_EIATech_loadFromJSONDict():
    EIATech = Business.libraries.dtoesa.EIA.EIA_tech.EIA_tech(name = "test_tech")
    EIATech.saveJSON()
    
    EIATech2 = Business.libraries.dtoesa.EIA.EIA_tech.EIA_tech()
    filePath='test_tech' + ".json"
    f = open(filePath,"r") 
    data = f.read()

    EIATech2.loadFromJSONDict(data)
    dd = json.loads(data) 
    EIATech2.loadFromJSONDict(dd) 
    
    assert EIATech2.name == 'test_tech'

