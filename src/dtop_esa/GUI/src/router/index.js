// This is the Environmental and Social Acceptance module of the DTOceanPlus suite
// Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

export const constantRoutes = [

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

//   {
//     path: '/',
//     component: Layout,
//     redirect: '/dashboard',
//     children: [{
//       path: 'dashboard',
//       name: 'Dashboard',
//       component: () => import('@/views/dashboard/index'),
//       meta: { title: 'Dashboard', icon: 'dashboard' }
//     }]
//   },
// 
//   {
//     path: '/',
//     component: Layout,
//     redirect: '/esahome',
//     children: [{
//       path: 'esahome',
//       name: 'ESAHome',
//       component: () => import('@/views/esa_home/index'),
//       meta: { title: 'ESA Home', icon: 'home' }
//     }]
//   },

  {
    path: '/esainputs',
    component: Layout,
    redirect: '/esainputs',
    name: 'ESAInputs',
    meta: { title: 'Inputs', icon: 'nested' },
    children: [
      {
        path: 'farm_info',
        name: 'Farm_Info',
        component: () => import('@/views/inputs_page/Farm_Info/index'),
        meta: { title: 'Farm Info' }
      },
      {
        path: 'device_info',
        name: 'Device_Info',
        component: () => import('@/views/inputs_page/Device_Info/index'),
        meta: { title: 'Device Info' }
      },
      {
        path: 'electrical_info',
        name: 'Electrical_Info',
        component: () => import('@/views/inputs_page/Electrical_Info/index'),
        meta: { title: 'Electrical Info' }
      },
      {
        path: 'logistics_info',
        name: 'Logistics_Info',
        component: () => import('@/views/inputs_page/Logistics_Info/index'),
        meta: { title: 'Logistics Info' }
      }
    ]
  },

  {
    path: '/',
    component: Layout,
    redirect: '/run_module',
    children: [{
      path: 'run_module',
      name: 'Run_Module',
      component: () => import('@/views/run_module/index'),
      meta: { title: 'Run Module' }
    }]
  },

  {
    path: '/esaoutputs',
    component: Layout,
    redirect: '/esaoutputs',
    name: 'ESAOutputs',
    meta: { title: 'Outputs', icon: 'nested' },
    children: [
      {
        path: 'endangered_species',
        name: 'Endangered_Species',
        component: () => import('@/views/outputs_page/Endangered_Species/index'),
        meta: { title: 'Endangered Species' }
      },
      {
        path: 'environmental_impact_assessment',
        name: 'Environmental_Impact_Assessment',
        component: () => import('@/views/outputs_page/Environmental_Impact_Assessment/index'),
        meta: { title: 'Environmental Impact Assessment' }
      },
      {
        path: 'carbon_footprint',
        name: 'Carbon_Footprint',
        component: () => import('@/views/outputs_page/Carbon_Footprint/index'),
        meta: { title: 'Carbon Footprint' }
      },
      {
        path: 'social_acceptance',
        name: 'Social_Acceptance',
        component: () => import('@/views/outputs_page/Social_Acceptance/index'),
        meta: { title: 'Social Acceptance' }
      }
    ]
  },

  {
    path: '/pdf',
    name: 'PDF',
    component: () => import('@/views/pdf/download'),
    hidden: true
  },

  {
    path: 'external-link',
    component: Layout,
    meta: { title: 'Links', icon: 'link' },
    children: [
      {
        path: 'https://dtoceanplus.eu',
        meta: { title: 'DTOcean+ website'}
      },
      {
        path: 'https://dtoceanplus.gitlab.io/documentation/index.html',
        meta: { title: 'DTOcean+ documentation'}
      },
      {
        path: 'https://dtoceanplus.gitlab.io/documentation/deployment/esa/docs/index.html',
        meta: { title: 'Environmental and Social Acceptance documentation'}
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
