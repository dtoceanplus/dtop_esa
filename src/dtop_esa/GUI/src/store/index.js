// This is the Environmental and Social Acceptance module of the DTOceanPlus suite
// Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
import axios from 'axios'
import { DTOP_DOMAIN, DTOP_PROTOCOL } from "@/../dtop/dtopConsts"

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    Project: [],
    ProjectStatus: [],
    test_begin: false,
    switch_farm_info_page: [false, false, false, false, false, false, false, false, false, false, false, false, false, false],
    used_materials_DI: [],
    used_materials_foundation_DI: [],
    used_materials_EI: [],
    Results: [],
    GoToRunModule: false,
    project_running: false,
    GWP: [],
    CED: [],
    EPP: [],
    names: [],
    Name: '',
    Description: '',
    OMTest: false,
    entitylist: ''
  },
  modules: {
    app,
    settings,
    user
  },
  mutations: {
    // The mutations define the actions to apply to all variables
    set_Project: (state, Project) => {
      // if Project == 1 call the method Initiate_Project_ProjectStatus
      // Else state.Project = Project
      if (Project === 1) {
        Initiate_Project_ProjectStatus()
      } else {
        state.Project = Project
      }
    },
    changeRunMode: (state, RunMode) => {
      // state.Project.run_mode = RunMode
      state.Project.run_mode = RunMode
    },
    changeComplexity: (state, Complexity) => {
      // state.ProjectStatus.complexity_level = Complexity
      state.ProjectStatus.complexity_level = Complexity
    },
    set_ProjectStatus: (state, ProjectStatus) => {
      // state.ProjectStatus = ProjectStatus
      state.ProjectStatus = ProjectStatus
    },
    set_TestBegin: (state, test_begin) => {
      // state.test_begin = test_begin
      state.test_begin = test_begin
    },
    set_SwitchFarmInfoPage: (state, switch_farm_info_page) => {
      // if switch_farm_info_page == 1 set state.switch_farm_info_page to vectore filled with false
      // Else state.switch_farm_info_page = switch_farm_info_page
      if (switch_farm_info_page === 1) {
        state.switch_farm_info_page = [false, false, false, false, false, false, false, false, false, false, false, false, false, false]
      } else {
        state.switch_farm_info_page = switch_farm_info_page
      }
    },
    set_um_DI: (state, used_materials_DI) => {
      // if used_materials_DI == 1 set state.used_materials_DI = []
      // Else state.used_materials_DI = used_materials_DI
      if (used_materials_DI === 1) {
        state.used_materials_DI = []
      } else {
        state.used_materials_DI = used_materials_DI
      }
    },
    set_umf_DI: (state, used_materials_foundation_DI) => {
      // if used_materials_foundation_DI == 1 set state.used_materials_foundation_DI = []
      // Else state.used_materials_foundation_DI = used_materials_foundation_DI
      if (used_materials_foundation_DI === 1) {
        state.used_materials_foundation_DI = []
      } else {
        state.used_materials_foundation_DI = used_materials_foundation_DI
      }
    },
    set_um_EI: (state, used_materials_EI) => {
      // if used_materials_EI == 1 set state.used_materials_EI = []
      // Else state.used_materials_EI = used_materials_EI
      if (used_materials_EI === 1) {
        state.used_materials_EI = []
      } else {
        state.used_materials_EI = used_materials_EI
      }
    },
    set_Results: (state, Results) => {
      // state.Results = Results
      state.Results = Results
    },
    set_GoToRunModule: (state, GoToRunModule) => {
      // state.GoToRunModule = GoToRunModule
      state.GoToRunModule = GoToRunModule
    },
    set_project_running: (state, project_running) => {
      state.project_running = project_running
    },
    set_GWP: (state, GWP) => {
      state.GWP = GWP
    },
    set_CED: (state, CED) => {
      state.CED = CED
    },
    set_EPP: (state, EPP) => {
      state.EPP = EPP
    },
    set_names: (state, names) => {
      state.names = names
    },
    set_Name: (state, Name) => {
      state.Name = Name
    },
    set_Description: (state, Description) => {
      state.Description = Description
    },
    set_OMTest: (state, OMTest) => {
      state.OMTest = OMTest
    },
    set_entitylist: (state, entitylist) => {
      state.entitylist = entitylist
    }
  },
  actions: {
    // set the action called by the setter define into the index.vue pages
    set_ProjectAction: ({commit, state}, newValue) => {
      // Set the action for Project variable
      commit('set_Project', newValue)
      return state.Project
    },
    set_ProjectStatusAction: ({commit, state}, newValue) => {
      // Set the action for ProjectStatus variable
      commit('set_ProjectStatus', newValue)
      return state.ProjectStatus
    },
    set_testbeginAction: ({commit, state}, newValue) => {
      // Set the action for test_begin variable
      commit('set_TestBegin', newValue)
      return state.test_begin
    },
    set_UMDI: ({commit, state}, newValue) => {
      // Set the action for used_materials_DI variable
      commit('set_um_DI', newValue)
      return state.used_materials_DI
    },
    set_UMFDI: ({commit, state}, newValue) => {
      // Set the action for used_materials_foundation_DI variable
      commit('set_umf_DI', newValue)
      return state.used_materials_foundation_DI
    },
    set_UMEI: ({commit, state}, newValue) => {
      // Set the action for used_materials_EI variable
      commit('set_um_EI', newValue)
      return state.used_materials_EI
    },
    set_ResultsAction: ({commit, state}, newValue) => {
      // Set the action for Results variable
      commit('set_Results', newValue)
      return state.Results
    },
    set_GoToRunModuleAction: ({commit, state}, newValue) => {
      // Set the action for GoToRunModule variable
      commit('set_GoToRunModule', newValue)
      return state.GoToRunModule
    },
    set_project_runningAction: ({commit, state}, newValue) => {
      commit('set_project_running', newValue)
      return state.project_running
    },
    set_GWPAction: ({commit, state}, newValue) => {
      commit('set_GWP', newValue)
      return state.GWP
    },
    set_CEDAction: ({commit, state}, newValue) => {
      commit('set_CED', newValue)
      return state.CED
    },
    set_EPPAction: ({commit, state}, newValue) => {
      commit('set_EPP', newValue)
      return state.EPP
    },
    set_namesAction: ({commit, state}, newValue) => {
      commit('set_names', newValue)
      return state.names
    },
    set_NameAction: ({commit, state}, newValue) => {
      commit('set_Name', newValue)
      return state.Name
    },
    set_DescriptionAction: ({commit, state}, newValue) => {
      commit('set_Description', newValue)
      return state.Description
    },
    set_OMTestAction: ({commit, state}, newValue) => {
      commit('set_OMTest', newValue)
      return state.OMTest
    },
    set_entitylistAction: ({commit, state}, newValue) => {
      commit('set_entitylist', newValue)
      return state.entitylist
    }
  },
  getters
})

const Initiate_Project_ProjectStatus = () => {
  // Initialise the Project and ProjectStatus structure
  // Call the API GET method initialise_Project_ProjectStatus
  // The API method is described in */service/api/postInputs/_init_.py
  const path = `${DTOP_PROTOCOL}//esa.${DTOP_DOMAIN}/esa/initialise_Project_ProjectStatus`
  axios
    .get(path)
    .then((res) => {
      store.commit('set_Project', res.data[0]);
      store.commit('set_ProjectStatus', res.data[1]);
    })
}

Initiate_Project_ProjectStatus();

export default store