import Vue from 'vue';
import Vuex from 'vuex';

import Project from '@/../tests/unit/json/Project.json'
import ElementUI from 'element-ui';

Vue.use(ElementUI);

  export const state = {
    Results: Project
  };

  export function __createMocks(custom = { state: {} }) {
    const mockState = Object.assign({}, state, custom.state);
  
    return {
      state: mockState,
      store: new Store({
        state: mockState,
      }),
    };
  }
  
  export const store = __createMocks().store;