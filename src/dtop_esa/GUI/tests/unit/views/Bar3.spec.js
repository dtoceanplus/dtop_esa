import { shallowMount, createLocalVue } from '@vue/test-utils'

import Bar3 from '@/views/outputs_page/Carbon_Footprint/components/Bar3'
import ElementUI from 'element-ui'

import Vuex from 'vuex'
import axios from 'axios'

import ProjectInputs from '../json/Project_inputs.json'
import ProjectStatusInputs from '../json/Project_status.json'
import Project_Results from '../json/Project.json'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(ElementUI)

jest.mock('@/store')
jest.mock('axios')

describe('Bar3', () => {
    const localVue = createLocalVue()
    localVue.use(Vuex)
    localVue.use(ElementUI)
    let Bar3Id = 1;

    const $router = {
        push: jest.fn(),
    }
    let store = new Vuex.Store({
        state: {
            Project: ProjectInputs,
            ProjectStatus: ProjectStatusInputs
        }
    })
    const wrapper = shallowMount(Bar3, {
        data() {
            return {
                Results: Project_Results,
                ProjectName: 'test_run'
            }
        },
        localVue,
        store,
        mocks: {
            $router,
            $route: {
                params: {
                    Bar3Id: Bar3Id,
                },
                query: { ProjectName: 'test_run' }
            }
        }
    })

    it('Test ProjectName data', () => {
        expect(wrapper.vm.ProjectName).toEqual('test_run')
    })

    it("initChart", async () => {
        expect(wrapper.vm.chart).toBeDefined
    })

    it("should test lifecycle when audio tag is destroyed", () => {
        expect(wrapper.vm.chart).toBeDefined;
        // when
        // wrapper.destroy()
        // then
        expect(wrapper.vm.chart).toBeUndefined;
    });

})