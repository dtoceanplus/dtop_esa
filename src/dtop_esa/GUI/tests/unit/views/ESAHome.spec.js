import { shallowMount, createLocalVue } from '@vue/test-utils'

import ESAHome from '@/views/esa_home/index'
import ElementUI from 'element-ui'

import ProjectInputs from '../json/Project_inputs.json'
import ProjectStatusInputs from '../json/Project_status.json'
import Project_Results from '../json/Project.json'

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

import { __createMocks as createStoreMocks } from '@/store';

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(ElementUI)

jest.mock('@/store')
jest.mock('axios')

describe('ESAHome', () => {
  const localVue = createLocalVue()
  localVue.use(Vuex)
  localVue.use(ElementUI)
  let ESAHomeId = 1;

  const $router = {
      push: jest.fn(),
  }
  let store = new Vuex.Store({
      state: {
          Project: ProjectInputs,
          ProjectStatus: ProjectStatusInputs,
          Results: Project_Results,
          Name: ProjectInputs.name
      }
  })
  const wrapper = shallowMount(ESAHome, {
      data() {
          return {
              Results: Project_Results,
              ProjectName: 'test_run'
          }
      },
      localVue,
      store,
      mocks: {
          $router,
          $route: {
              params: {
                ESAHomeId: ESAHomeId,
              },
              query: { ProjectName: 'test_run' }
          }
      }
  })

  it('Test ProjectName data', () => {
    expect(wrapper.vm.ProjectName).toEqual('test_run')
  })

  it('LoadOutputs', async () => {
    const path = `${process.env.VUE_APP_API_URL}/esa/${wrapper.vm.ProjectName}/loadoutputs`
    axios.resolveWith(path)
    // await wrapper.vm.loadOutputs()
    // await wrapper.vm.$nextTick()
    expect(wrapper.vm.Results).toBeDefined
  })

  it('DeleteProject', async() => {
    axios.resolveWith({data: ['test', 'test2', 'test3']})
    await wrapper.vm.DeleteProject()
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.esa_projects).toEqual(['test', 'test2', 'test3'])
  })

  it('onCommit', async () => {  
    const storeCommit = jest.spyOn(store, 'commit');
    await wrapper.vm.onCommit()
    await wrapper.vm.$nextTick()
    expect(storeCommit).toHaveBeenCalledTimes(5)
    expect(store.commit).toHaveBeenCalledWith('set_Project', 1)
    expect(store.commit).toHaveBeenCalledWith('set_TestBegin', true)
    expect(store.commit).toHaveBeenCalledWith('set_SwitchFarmInfoPage', 1)
    expect(store.commit).toHaveBeenCalledWith('set_um_DI', 1)
    expect(store.commit).toHaveBeenCalledWith('set_umf_DI', 1)
    store.commit.mockRestore()
    expect(wrapper.vm.$store.state.test_begin).toBeDefined
    expect(wrapper.vm.$store.state.swtich_farm_info_page).toBeDefined
    expect(wrapper.vm.$store.used_materials_DI).toBeDefined
    expect(wrapper.vm.$store.used_materials_foundation_DI).toBeDefined
  })

  it('Test_Fill_1', () => {
    let result = wrapper.vm.Test_Fill(0)
    expect(result).toBe('false')
  })

  it('Test_Fill_2', () => {
    let result = wrapper.vm.Test_Fill(1)
    expect(result).toBe('true')
  })

  it('onCreate', async() => {
    await wrapper.vm.onCreate()
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.run).toBeDefined
    expect(wrapper.vm.complex).toBeDefined
    setTimeout(() => {
      expect($router.push).toHaveBeenCalledWith({name: 'Farm_Info'});
    }, 2100);
  })

  it('GetMyProjectList', async() => {
    axios.resolveWith({data: ['test', 'test2', 'test3']})
    await wrapper.vm.GetMyProjectList()
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.esa_projects).toEqual(['test', 'test2', 'test3'])
  })

  it('GoToSelectedPage', async() => {
    // axios.resolveWith([ProjectInputs, ProjectStatusInputs])
    await wrapper.vm.GoToSelectedPage('test', 1)
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.Project).toEqual(ProjectInputs)
    expect(wrapper.vm.ProjectStatus).toEqual(ProjectStatusInputs)
    expect($router.push).toHaveBeenCalledWith({
      "name": 'Farm_Info'
    });
   })

  it('changePage', async() => {
    await wrapper.vm.changePage('Farm_Info')
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "Farm_Info"});
  })

  it('computed_properties', async () => {
    const storeDispatch = jest.spyOn(store, 'dispatch');
    wrapper.vm.test_begin = "value"
    wrapper.vm.Description = "value"
    wrapper.vm.Name= "value"
    wrapper.vm.Project = "value"
    wrapper.vm.ProjectStatus = "value"
    wrapper.vm.GoToRunModule = "value"
    wrapper.vm.Results = "value"
    expect(storeDispatch).toBeCalledTimes(6);  
    expect(store.dispatch).toHaveBeenCalledWith("set_testbeginAction", "value")
    expect(store.dispatch).toHaveBeenCalledWith("set_DescriptionAction", "value")
    expect(store.dispatch).toHaveBeenCalledWith("set_NameAction", "value")
    expect(store.dispatch).toHaveBeenCalledWith("set_ProjectAction", "value")
    expect(store.dispatch).toHaveBeenCalledWith("set_ProjectStatusAction", "value")
    expect(store.dispatch).toHaveBeenCalledWith("set_GoToRunModuleAction", "value")
    // expect(store.dispatch).toHaveBeenCalledWith("set_ResultsAction", "value")
    store.dispatch.mockRestore()
  })

});
