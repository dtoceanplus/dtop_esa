import { shallowMount, createLocalVue } from '@vue/test-utils'

import Farm_Info from '@/views/inputs_page/Farm_Info/index'
import ElementUI from 'element-ui'

import ProjectInputs from '../json/Project_inputs.json'
import ProjectStatusInputs from '../json/Project_status.json'
import Project_Results from '../json/Project.json'

import Vue from 'vue'
import Vuex from 'vuex'

import { __createMocks as createStoreMocks } from '@/store';

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(ElementUI)

jest.mock('@/store')
jest.mock('axios')

describe('Farm_Info', () => {
    const localVue = createLocalVue()
    localVue.use(Vuex)
    localVue.use(ElementUI)
    let Farm_InfoId = 1;

    const $router = {
        push: jest.fn(),
    }
    const $notify = {
        info: jest.fn(),
    }
    let store = new Vuex.Store({
        state: {
            Project: ProjectInputs,
            ProjectStatus: ProjectStatusInputs,
            Name: ProjectInputs.name,
            switch_farm_info_page: [false, false, true, false, false, false, false, false, false, false, false, false, false, false],
            entitylist: ''
        }
    })
    const wrapper = shallowMount(Farm_Info, {
        data() {
            return {
                Results: Project_Results,
                ProjectName: 'test_run'
            }
        },
        localVue,
        store,
        mocks: {
            $router,
            $route: {
                params: {
                    Farm_InfoId: Farm_InfoId,
                },
                query: { ProjectName: 'test_run' }
            }
        }
    })

    it('Test ProjectName data', () => {
        expect(wrapper.vm.ProjectName).toEqual('test_run')
    })

    it('commitProjectfromSwitch', async () => {
        let testIndex = 0
        const storeCommit = jest.spyOn(store, 'commit');
        await wrapper.vm.commitProjectfromSwitch(testIndex)
        await wrapper.vm.$nextTick()
        for (let i=0;i<14;i++) {
            if (wrapper.vm.Project.inputs_ESA.farm_info.receptors[wrapper.vm.names[testIndex]][i] === false) {
                expect(wrapper.vm.Project.inputs_ESA.farm_info.receptors[wrapper.vm.names[testIndex]][testIndex]).toBeTruthy()
            } else {
                expect(wrapper.vm.Project.inputs_ESA.farm_info.receptors[wrapper.vm.names[testIndex]][testIndex]).toBe("")
            }
        }
        expect(storeCommit).toHaveBeenCalledTimes(1)
        expect(store.commit).toHaveBeenCalledWith('set_SwitchFarmInfoPage', wrapper.vm.switch_farm_info_page)
        store.commit.mockRestore()
    })

    it('commitProjectfromSwitch_2', async () => {
        let testIndex = 2
        const storeCommit = jest.spyOn(store, 'commit');
        await wrapper.vm.commitProjectfromSwitch(testIndex)
        await wrapper.vm.$nextTick()
        for (let i=0;i<14;i++) {
            if (wrapper.vm.Project.inputs_ESA.farm_info.receptors[wrapper.vm.names[testIndex]][i] === false) {
                expect(wrapper.vm.Project.inputs_ESA.farm_info.receptors[wrapper.vm.names[testIndex]][testIndex]).toBeTruthy()
            } else {
                expect(wrapper.vm.Project.inputs_ESA.farm_info.receptors[wrapper.vm.names[testIndex]][testIndex]).toBe("")
            }
        }
        expect(storeCommit).toHaveBeenCalledTimes(1)
        expect(store.commit).toHaveBeenCalledWith('set_SwitchFarmInfoPage', wrapper.vm.switch_farm_info_page)
        store.commit.mockRestore()
    })

    it('CommitProject', async () => {
        let testIndex = 0
        await wrapper.vm.CommitProject(testIndex)
        await wrapper.vm.$nextTick()
        for (let i=0;i<14;i++) {
            if (wrapper.vm.Project.inputs_ESA.farm_info.receptors[wrapper.vm.names[testIndex]][i] === false) {
                expect(wrapper.vm.Project.inputs_ESA.farm_info.receptors[wrapper.vm.names[testIndex]][testIndex]).toBe("")
            }
        }
    })

    it('addNewRow', async () => {
        await wrapper.vm.addNewRow()
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.PS).toEqual([
            {
             "Class": "Aves",
             "__type__": "dtoesa:inputs:input_species",
             "description": "",
             "name": "Puffinus mauretanicus",
             "probability_of_presence": 0,
            },
            {
             "Class": "Reptilia",
             "__type__": "dtoesa:inputs:input_species",
             "description": "",
             "name": "Chelonia mydas",
             "probability_of_presence": 0,
            },
            {
              "Class": "",
              "name": "",
              "probability_of_presence": 0,
            },
        ])
    })

    it('deleteRow', async () => {
        let PS = [{
            name: '',
            probability_of_presence: 0,
            Class: ''
        }]
        await wrapper.vm.deleteRow(0, 0, PS)
        await wrapper.vm.$nextTick()
        expect(PS).not.toContain({
            name: '',
            probability_of_presence: 0,
            Class: ''
        })
    })

    it('changePage', async () => {
        let pageNameTest = 'Dashboard'
        await wrapper.vm.changePage(pageNameTest)
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.ProjectStatus.farm_info_page).toBeTruthy()
        expect($router.push).toHaveBeenCalledWith({ "name": pageNameTest });
    })

    it('openSidebar1', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar1();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Farm General Info',
            message: '<h3> Coordinate of the Farm </h3> Longitude and latitude in degre decimal. (Needed for the endangered species assessment)' +
              '<h3> Project Lifetime </h3> Duration of the total lifecycle of the project in years. (Needed for the Carbon Footprint Assessment)' +
              '<h3> Levelized Cost of Energy </h3> Cost of a unit of energy in €/kWh. (Needed for Social Acceptance assessment)',
            duration: 0,
            position: 'top-right'
          })
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Farm General Info',
            message: '<h3> Coordinate of the Farm </h3> Longitude and latitude in degre decimal. (Needed for the endangered species assessment)' +
              '<h3> Project Lifetime </h3> Duration of the total lifecycle of the project in years. (Needed for the Carbon Footprint Assessment)' +
              '<h3> Levelized Cost of Energy </h3> Cost of a unit of energy in €/kWh. (Needed for Social Acceptance assessment)',
            duration: 0,
            position: 'top-right'
          });
        $notify.info.mockRestore()
    })

    it('openSidebar2', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar2();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Area Description',
            message: '<h3> Zone Type </h3> Description' +
              '<h3> Water Depth </h3> Description' +
              '<h3> Current Direction </h3> Description' +
              '<h3> Total Surface Area </h3> Description' +
              '<h3> Soil Type </h3> Description',
            duration: 0,
            position: 'top-right'
          })
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Area Description',
            message: '<h3> Zone Type </h3> Description' +
              '<h3> Water Depth </h3> Description' +
              '<h3> Current Direction </h3> Description' +
              '<h3> Total Surface Area </h3> Description' +
              '<h3> Soil Type </h3> Description',
            duration: 0,
            position: 'top-right'
          });
        $notify.info.mockRestore()
    })

    it('openSidebar3', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar3();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Initial State',
            message: '<h3> Turbidity </h3> Description' +
              '<h3> Noise </h3> Description' +
              '<h3> Electrical Field </h3> Description' +
              '<h3> Magnetic Field </h3> Description' +
              '<h3> Temperature Field </h3> Description',
            duration: 0,
            position: 'top-right'
          })
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Initial State',
            message: '<h3> Turbidity </h3> Description' +
              '<h3> Noise </h3> Description' +
              '<h3> Electrical Field </h3> Description' +
              '<h3> Magnetic Field </h3> Description' +
              '<h3> Temperature Field </h3> Description',
            duration: 0,
            position: 'top-right'
          });
        $notify.info.mockRestore()
    })

    it('openSidebar4', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar4();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Fishing Regulation',
            message: '<h3> Fishing Regulation </h3> Description',
            duration: 0,
            position: 'top-right'
          })
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Fishing Regulation',
            message: '<h3> Fishing Regulation </h3> Description',
            duration: 0,
            position: 'top-right'
          });
        $notify.info.mockRestore()
    })

    it('openSidebar5', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar5();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Protected Species',
            message: '<h3> Information about the protected species </h3> Description',
            duration: 0,
            position: 'top-right'
          })
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Protected Species',
            message: '<h3> Information about the protected species </h3> Description',
            duration: 0,
            position: 'top-right'
          });
        $notify.info.mockRestore()
    })

    it('openSidebar6', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar6();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Receptors',
            message: '<h3> Hard Substrate Benthic Habitat </h3> Description' +
              '<h3> Soft Substrate Benthic Habitat </h3> Description' +
              '<h3> Particular Habitat </h3> Description' +
              '<h3> Shallow Diving Birds </h3> Description' +
              '<h3> Medium Diving Birds </h3> Description' +
              '<h3> Deep Diving Birds </h3> Description' +
              '<h3> Large Odontocete Mysticete </h3> Description' +
              '<h3> Odontoncete Dolphinds </h3> Description' +
              '<h3> Seals </h3> Description' +
              '<h3> Fishes </h3> Description' +
              '<h3> Bony Fishes </h3> Description' +
              '<h3> Magnetosensitive Species </h3> Description' +
              '<h3> Electrosensitive Species </h3> Description' +
              '<h3> Elasmobranchs </h3> Description',
            duration: 0,
            position: 'top-right'
          })
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Receptors',
            message: '<h3> Hard Substrate Benthic Habitat </h3> Description' +
              '<h3> Soft Substrate Benthic Habitat </h3> Description' +
              '<h3> Particular Habitat </h3> Description' +
              '<h3> Shallow Diving Birds </h3> Description' +
              '<h3> Medium Diving Birds </h3> Description' +
              '<h3> Deep Diving Birds </h3> Description' +
              '<h3> Large Odontocete Mysticete </h3> Description' +
              '<h3> Odontoncete Dolphinds </h3> Description' +
              '<h3> Seals </h3> Description' +
              '<h3> Fishes </h3> Description' +
              '<h3> Bony Fishes </h3> Description' +
              '<h3> Magnetosensitive Species </h3> Description' +
              '<h3> Electrosensitive Species </h3> Description' +
              '<h3> Elasmobranchs </h3> Description',
            duration: 0,
            position: 'top-right'
          });
        $notify.info.mockRestore()
    })

    it('computed_properties', async () => {
        const storeDispatch = jest.spyOn(store, 'dispatch');
        wrapper.vm.Project = "value"
        wrapper.vm.ProjectStatus = "value"
        expect(storeDispatch).toBeCalledTimes(2);  
        expect(store.dispatch).toHaveBeenCalledWith("set_ProjectAction", "value")
        expect(store.dispatch).toHaveBeenCalledWith("set_ProjectStatusAction", "value")
        store.dispatch.mockRestore()
    })
});
