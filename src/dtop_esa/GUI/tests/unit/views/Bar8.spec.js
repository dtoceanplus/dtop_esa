import { shallowMount, createLocalVue } from '@vue/test-utils'

import Bar8 from '@/views/outputs_page/Carbon_Footprint/components/Bar8'
import ElementUI from 'element-ui'

import Vuex from 'vuex'
import axios from 'axios'

import ProjectInputs from '../json/Project_inputs.json'
import ProjectStatusInputs from '../json/Project_status.json'
import Project_Results from '../json/Project.json'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(ElementUI)

jest.mock('@/store')
jest.mock('axios')

describe('Bar8', () => {
    const localVue = createLocalVue()
    localVue.use(Vuex)
    localVue.use(ElementUI)
    let Bar8Id = 1;

    const $router = {
        push: jest.fn(),
    }
    let store = new Vuex.Store({
        state: {
            Project: ProjectInputs,
            ProjectStatus: ProjectStatusInputs,
            names: ['test', 'test2','test3'],
            GWP: [ [ 18.692, 13.268, 16.536, 11.307, -12.374, 7.429 ], [ 8.692, 3.268, 6.536, 1.307, -2.374, 17.429 ] ]
        }
    })
    const wrapper = shallowMount(Bar8, {
        data() {
            return {
                Results: Project_Results,
                ProjectName: 'test_run'
            }
        },
        localVue,
        store,
        mocks: {
            $router,
            $route: {
                params: {
                    Bar8Id: Bar8Id,
                },
                query: { ProjectName: 'test_run' }
            }
        }
    })

    it('Test ProjectName data', () => {
        expect(wrapper.vm.ProjectName).toEqual('test_run')
    })

    it("initChart", async () => {
        expect(wrapper.vm.chart).toBeDefined
    })

    it("should test lifecycle when audio tag is destroyed", () => {
        expect(wrapper.vm.chart).toBeDefined;
        // when
        // wrapper.destroy()
        // then
        expect(wrapper.vm.chart).toBeUndefined;
    });

})