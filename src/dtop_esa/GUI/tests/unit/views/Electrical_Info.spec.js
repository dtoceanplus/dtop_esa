import { shallowMount, createLocalVue } from '@vue/test-utils'

import Electrical_Info from '@/views/inputs_page/Electrical_Info/index'
import ElementUI from 'element-ui'

import ProjectInputs from '../json/Project_inputs.json'
import ProjectStatusInputs from '../json/Project_status.json'
import Project_Results from '../json/Project.json'

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

import { __createMocks as createStoreMocks } from '@/store';

jest.mock('@/store')
jest.mock('axios')

describe('Electrical_Info', () => {
    const localVue = createLocalVue()
    localVue.use(Vuex)
    localVue.use(ElementUI)
    let Electrical_InfoId = 1;

    const $router = {
        push: jest.fn(),
    }
    const $notify = {
        info: jest.fn(),
    }
    let store = new Vuex.Store({
        state: {
            Project: ProjectInputs,
            ProjectStatus: ProjectStatusInputs,
            Name: ProjectInputs.name,
            used_materials_EI: [],
            entitylist: ''
        }
    })
    const wrapper = shallowMount(Electrical_Info, {
        data() {
            return {
                Results: Project_Results,
                ProjectName: 'test_run'
            }
        },
        localVue,
        store,
        mocks: {
            $router,
            $route: {
                params: {
                    Electrical_InfoId: Electrical_InfoId,
                },
                query: { ProjectName: 'test_run' }
            }
        }
    })

    it('Test ProjectName data', () => {
        expect(wrapper.vm.ProjectName).toEqual('test_run')
    })

    it('addNewElement', async () => {
        let lon = wrapper.vm.Project.inputs_ESA.electrical_info.collection_points_coordinates_x
        await wrapper.vm.addNewElement(lon)
        await wrapper.vm.$nextTick()
        expect(lon).toEqual([0, 0])
    })

    it('deleteElement', async () => {
        let lon = wrapper.vm.Project.inputs_ESA.electrical_info.collection_points_coordinates_x
        await wrapper.vm.deleteElement(lon)
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.LON).not.toContain(lon)
    })

    it('changePage', async () => {
        let pageNameTest = 'Dashboard'
        await wrapper.vm.changePage(pageNameTest)
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.ProjectStatus.electrical_info_page).toBeTruthy()
        expect($router.push).toHaveBeenCalledWith({ "name": pageNameTest });
    })

    it('openSidebar1', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar1();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Electrical general info',
            message: '<h3> Annual Energy Produced </h3> Description' +
              '<h3> Colonisable surface area of the electrical components </h3> Description' +
              '<h3> Footprint of the electrical installation </h3> Description',
            duration: 0,
            position: 'top-right'
          })
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Electrical general info',
            message: '<h3> Annual Energy Produced </h3> Description' +
              '<h3> Colonisable surface area of the electrical components </h3> Description' +
              '<h3> Footprint of the electrical installation </h3> Description',
            duration: 0,
            position: 'top-right'
          });
        $notify.info.mockRestore()
    })

    it('openSidebar2', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar2();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Installation Info',
            message: '<h3> Collection point(s) location(s) </h3> Description' +
              '<h3> Height </h3> Description' +
              '<h3> Width </h3> Description' +
              '<h3> Length </h3> Description' +
              '<h3> Wet Area </h3> Description' +
              '<h3> Dry Area </h3> Description',
            duration: 0,
            position: 'top-right'
          })
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Installation Info',
            message: '<h3> Collection point(s) location(s) </h3> Description' +
              '<h3> Height </h3> Description' +
              '<h3> Width </h3> Description' +
              '<h3> Length </h3> Description' +
              '<h3> Wet Area </h3> Description' +
              '<h3> Dry Area </h3> Description',
            duration: 0,
            position: 'top-right'
          });
        $notify.info.mockRestore()
    })

    it('openSidebar3', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar3();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Fishing Restriction',
            message: '<h3> Fishery restriction surface around cables </h3> Description',
            duration: 0,
            position: 'top-right'
          })
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Fishing Restriction',
            message: '<h3> Fishery restriction surface around cables </h3> Description',
            duration: 0,
            position: 'top-right'
          });
        $notify.info.mockRestore()
    })

    it('openSidebar4', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar4();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Resources',
            message: '<h3> Used materials for electrical installation </h3> Description' + 
            '<h3> Quantity of used materials for the project </h3> Description' +
            '<h3> Quantity of materials to recycle </h3> Description',
            duration: 0,
            position: 'top-right'
          })
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Resources',
            message: '<h3> Used materials for electrical installation </h3> Description' + 
            '<h3> Quantity of used materials for the project </h3> Description' +
            '<h3> Quantity of materials to recycle </h3> Description',
            duration: 0,
            position: 'top-right'
          });
        $notify.info.mockRestore()
    })

    it('openSidebar5', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar5();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Environmental measurements',
            message: '<h3> Measured Noise </h3> Description' +
            '<h3> Measured Electric Field </h3> Description' +
            '<h3> Measured Magnetic Field </h3> Description' +
            '<h3> Measured Temperature </h3> Description',
            duration: 0,
            position: 'top-right'
          })
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Environmental measurements',
            message: '<h3> Measured Noise </h3> Description' +
            '<h3> Measured Electric Field </h3> Description' +
            '<h3> Measured Magnetic Field </h3> Description' +
            '<h3> Measured Temperature </h3> Description',
            duration: 0,
            position: 'top-right'
          });
        $notify.info.mockRestore()
    })

    it('SetValue', async () => {
        await wrapper.vm.SetValue()
        for (var i=0; i<wrapper.vm.materials_properties.length; i++) {
            if (!(wrapper.vm.used_materials_EI.includes(wrapper.vm.materials_properties[i]))) {
              expect(wrapper.vm.Project.inputs_ESA.device_info.materials[wrapper.vm.materials_properties[i]].value).toBe(0)
              expect(wrapper.vm.Project.inputs_ESA.device_info.materials_to_recycle[wrapper.vm.materials_properties[i]].value).toBe(0)
            }
        }
    })

    it('computed_properties', async () => {
        const storeDispatch = jest.spyOn(store, 'dispatch');
        wrapper.vm.used_materials_EI = "value"
        wrapper.vm.ProjectStatus = "value"
        wrapper.vm.Project = "value"
        expect(storeDispatch).toBeCalledTimes(3);  
        expect(store.dispatch).toHaveBeenCalledWith("set_UMEI", "value")
        expect(store.dispatch).toHaveBeenCalledWith("set_ProjectStatusAction", "value")
        expect(store.dispatch).toHaveBeenCalledWith("set_ProjectAction", "value")
        store.dispatch.mockRestore()
    })
});
