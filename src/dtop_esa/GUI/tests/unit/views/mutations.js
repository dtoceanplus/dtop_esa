import axios from 'axios'
export default {
    set_Project: (state, Project) => {
      if (Project === 1) {
        Initiate_Project_ProjectStatus()
      } else {
        state.Project = Project
      }
    },
    changeRunMode: (state, RunMode) => {
      state.Project.run_mode = RunMode
    },
    changeComplexity: (state, Complexity) => {
      state.ProjectStatus.complexity_level = Complexity
    },
    set_ProjectStatus: (state, ProjectStatus) => {
      state.ProjectStatus = ProjectStatus
    },
    set_TestBegin: (state, test_begin) => {
      state.test_begin = test_begin
    },
    set_SwitchFarmInfoPage: (state, switch_farm_info_page) => {
      if (switch_farm_info_page === 1) {
        state.switch_farm_info_page = [false, false, false, false, false, false, false, false, false, false, false, false, false, false]
      } else {
        state.switch_farm_info_page = switch_farm_info_page
      }
    },
    set_um_DI: (state, used_materials_DI) => {
      if (used_materials_DI === 1) {
        state.used_materials_DI = []
      } else {
        state.used_materials_DI = used_materials_DI
      }
    },
    set_umf_DI: (state, used_materials_foundation_DI) => {
      if (used_materials_foundation_DI === 1) {
        state.used_materials_foundation_DI = []
      } else {
        state.used_materials_foundation_DI = used_materials_foundation_DI
      }
    },
    set_um_EI: (state, used_materials_EI) => {
      if (used_materials_EI === 1) {
        state.used_materials_EI = []
      } else {
        state.used_materials_EI = used_materials_EI
      }
    },
    Initiate_Project_ProjectStatus: () => {
      const path = 'http://localhost:5000/esa/initialise_Project_ProjectStatus'
      axios
        .get(path)
        .then((res) => {
          Project = res.data[0]
          ProjectStatus = res.data[1]
        })
    }
  }
