import { shallowMount, createLocalVue } from '@vue/test-utils'

import Carbon_Footprint from '@/views/outputs_page/Carbon_Footprint/index'
import ElementUI from 'element-ui'

import Vuex from 'vuex'
import axios from 'axios'

import ProjectInputs from '../json/Project_inputs.json'
import ProjectStatusInputs from '../json/Project_status.json'
import Project_Results from '../json/Project.json'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(ElementUI)

jest.mock('@/store')
jest.mock('axios')

describe('Carbon_Footprint', () => {
    const localVue = createLocalVue()
    localVue.use(Vuex)
    localVue.use(ElementUI)
    let Carbon_FootprintId = 1;

    const $router = {
        push: jest.fn(),
    }
    let store = new Vuex.Store({
        state: {
            Project: ProjectInputs,
            ProjectStatus: ProjectStatusInputs,
            Name: ProjectInputs.name
        }
    })
    const wrapper = shallowMount(Carbon_Footprint, {
        data() {
            return {
                Results: Project_Results,
                ProjectName: 'test_run'
            }
        },
        localVue,
        store,
        mocks: {
            $router,
            $route: {
                params: {
                    Carbon_FootprintId: Carbon_FootprintId,
                },
                query: { ProjectName: 'test_run' }
            }
        }
    })

    it('Test ProjectName data', () => {
        expect(wrapper.vm.ProjectName).toEqual('test_run')
    })

    it('LoadOutputs', async () => {
        const path = `http://localhost:5000/esa/test_run/loadoutputs`
        axios.resolveWith(path)
        // await wrapper.vm.loadOutputs()
        // await wrapper.vm.$nextTick()
        expect(wrapper.vm.Results).toBeDefined
    })

    it('GoToES', async () => {
        await wrapper.vm.GoToES()
        await wrapper.vm.$nextTick()
        expect($router.push).toHaveBeenCalledWith({"name": "Endangered_Species"});
    })

    it('GoToEIA', async () => {
        await wrapper.vm.GoToEIA()
        await wrapper.vm.$nextTick()
        expect($router.push).toHaveBeenCalledWith({"name": "Environmental_Impact_Assessment"});
    })

    it('GoToSA', async () => {
        await wrapper.vm.GoToSA()
        await wrapper.vm.$nextTick()
        expect($router.push).toHaveBeenCalledWith({"name": "Social_Acceptance"});
    })

    it('GetMyProjectList', async() => {
        axios.resolveWith({data: [{'name': 'test_run2'}]})
        await wrapper.vm.GetMyProjectList()
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.temp).toEqual([{"name": "test_run2"}])
        const path2 = `${process.env.VUE_APP_API_URL}/esa/CheckCFP`;
        for (let i=0; i<wrapper.vm.temp.length; i++) {
            if(wrapper.vm.temp[i].name != wrapper.vm.Project.name){
                const payload = {name: wrapper.vm.temp[i].name};
                axios.resolveWith({
                    method: 'get',
                    url: path2,
                    params: payload
                })
            }
        }
        expect(wrapper.vm.esa_projects).toBeDefined
        expect(wrapper.vm.names).toBeDefined
    })

    it('LoadCFP', async() => {  
        for (let i=0; i<wrapper.vm.CheckedProject.length; i++) {
            const payload = {name: wrapper.vm.CheckedProject[i]}
            axios.resolveWith({method: 'get',
                url: path,
                params: payload})
            await wrapper.vm.LoadCFP()
            await wrapper.vm.$nextTick()
            expect(wrapper.vm.GWP).toEqual('test')
            expect(wrapper.vm.CED).toEqual('test2')
            expect(wrapper.vm.EPP).toEqual('test3')
            expect(wrapper.vm.CompareProject).toBeTruthy()
        }
    })
    
    it('computed_properties', async () => {
        const storeDispatch = jest.spyOn(store, 'dispatch');
        wrapper.vm.GWP = "value"
        wrapper.vm.CED = "value"
        wrapper.vm.EPP = "value"
        wrapper.vm.names = "value"
        expect(storeDispatch).toBeCalledTimes(4);  
        expect(store.dispatch).toHaveBeenCalledWith("set_GWPAction", "value")
        expect(store.dispatch).toHaveBeenCalledWith("set_CEDAction", "value")
        expect(store.dispatch).toHaveBeenCalledWith("set_EPPAction", "value")
        expect(store.dispatch).toHaveBeenCalledWith("set_namesAction", "value")
        store.dispatch.mockRestore()
    })
  
});