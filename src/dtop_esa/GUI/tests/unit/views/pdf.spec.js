import { shallowMount, createLocalVue } from '@vue/test-utils'

import PDF from '@/views/pdf/download'
import ElementUI from 'element-ui'
import ProjectInputs from '../json/Project_inputs.json'
import ProjectStatusInputs from '../json/Project_status.json'
import Project_Results from '../json/Project.json'

import Vue from 'vue'
import axios from 'axios'
import Vuex from 'vuex'
import { data } from 'autoprefixer'

jest.mock('axios')

describe('PDF', () => {
    const localVue = createLocalVue()
    localVue.use(Vuex)
    localVue.use(ElementUI)
    let PDFId = 1

    const $router = {
        push: jest.fn(),
    }
    let store = new Vuex.Store({
        state: {
            Project: ProjectInputs,
            ProjectStatus: ProjectStatusInputs,
            surveys_1: [],
            surveys_2: [],
            surveys_3: [],
            surveys_4: [],
            surveys_5: [],
            risks_1: [],
            risks_2: [],
            risks_3: [],
            risks_4: [],
            risks_5: [],
            mitigation_1: [],
            mitigation_2: [],
            mitigation_3: [],
            mitigation_4: [],
            mitigation_5: []
        }
    })
    const wrapper = shallowMount(PDF, {
        data() {
            return {
                Results: Project_Results,
                ProjectName: 'test_run'
            }
        },
        localVue,
        store,
        mocks: {
            $router,
            $route: {
                params: {
                    PDFId: PDFId,
                },
                query: { ProjectName: 'test_run' }
            }
        }
    })

    it('Test ProjectName data', () => {
        expect(wrapper.vm.ProjectName).toEqual('test_run')
        expect(wrapper.vm.Results).toBeDefined
    })

    it('fetchData', async () => {
        await wrapper.vm.fetchData()
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.fullscreenLoading).toBeTruthy()
    })

    // it('InitialiseLocalVariables', async () => {
    //     for (const i in wrapper.vm.Results.endangered_species.aves) {
    //         if (!(wrapper.vm.surveys_1.includes(wrapper.vm.Results.endangered_species.aves[i].Surveys))) {
    //             expect(wrapper.vm.surveys_1.push).toHaveBeenCalledWith(wrapper.vm.Results.endangered_species.aves[i].Surveys)
    //         }
    //         for (const j in wrapper.vm.Results.endangered_species.aves[i].Risks_mitigation) {
    //             if (!(wrapper.vm.risks_1.includes(wrapper.vm.Results.endangered_species.aves[i].Risks_mitigation[j].Risk))) {
    //               expect(wrapper.vm.risks_1.push).toHaveBeenCalledWith(wrapper.vm.Results.endangered_species.aves[i].Risks_mitigation[j].Risk)
    //             }
    //             if (!(wrapper.vm.mitigation_1.includes(wrapper.vm.Results.endangered_species.aves[i].Risks_mitigation[j].Mitigation))) {
    //               expect(wrapper.vm.mitigation_1.push).toHaveBeenCalledWith(wrapper.vm.Results.endangered_species.aves[i].Risks_mitigation[j].Mitigation)
    //             }
    //         }
    //     }
    //     for (const i in wrapper.vm.Results.endangered_species.actinopterygii) {
    //         if (!(wrapper.vm.surveys_2.includes(wrapper.vm.Results.endangered_species.actinopterygii[i].Surveys))) {
    //             expect(wrapper.vm.surveys_2.push).toHaveBeenCalledWith(wrapper.vm.Results.endangered_species.actinopterygii[i].Surveys)
    //         }
    //         for (const j in wrapper.vm.Results.endangered_species.actinopterygii[i].Risks_mitigation) {
    //             if (!(wrapper.vm.risks_2.includes(wrapper.vm.Results.endangered_species.actinopterygii[i].Risks_mitigation[j].Risk))) {
    //             expect(wrapper.vm.risks_2.push).toHaveBeenCalled(wrapper.vm.Results.endangered_species.actinopterygii[i].Risks_mitigation[j].Risk)
    //             }
    //             if (!(wrapper.vm.mitigation_2.includes(wrapper.vm.Results.endangered_species.actinopterygii[i].Risks_mitigation[j].Mitigation))) {
    //             expect(wrapper.vm.mitigation_2.push).toHaveBeenCalled(wrapper.vm.Results.endangered_species.actinopterygii[i].Risks_mitigation[j].Mitigation)
    //             }
    //         }
    //     }
    //     for (const i in wrapper.vm.Results.endangered_species.chondrichtyes) {
    //         if (!(wrapper.vm.surveys_3.includes(wrapper.vm.Results.endangered_species.chondrichtyes[i].Surveys))) {
    //           expect(wrapper.vm.surveys_3.push).toHaveBeenCalled(wrapper.vm.Results.endangered_species.chondrichtyes[i].Surveys)
    //         }
    //         for (const j in wrapper.vm.Results.endangered_species.chondrichtyes[i].Risks_mitigation) {
    //           if (!(wrapper.vm.risks_3.includes(wrapper.vm.Results.endangered_species.chondrichtyes[i].Risks_mitigation[j].Risk))) {
    //             expect(wrapper.vm.risks_3.push).toHaveBeenCalled(wrapper.vm.Results.endangered_species.chondrichtyes[i].Risks_mitigation[j].Risk)
    //           }
    //           if (!(wrapper.vm.mitigation_3.includes(wrapper.vm.Results.endangered_species.chondrichtyes[i].Risks_mitigation[j].Mitigation))) {
    //             expect(wrapper.vm.mitigation_3.push).toHaveBeenCalled(wrapper.vm.Results.endangered_species.chondrichtyes[i].Risks_mitigation[j].Mitigation)
    //           }
    //         }
    //     }
    //     for (const i in wrapper.vm.Results.endangered_species.mammals) {
    //         if (!(wrapper.vm.surveys_4.includes(wrapper.vm.Results.endangered_species.mammals[i].Surveys))) {
    //           expect(wrapper.vm.surveys_4.push).toHaveBeenCalled(wrapper.vm.Results.endangered_species.mammals[i].Surveys)
    //         }
    //         for (const j in wrapper.vm.Results.endangered_species.mammals[i].Risks_mitigation) {
    //           if (!(wrapper.vm.risks_4.includes(wrapper.vm.Results.endangered_species.mammals[i].Risks_mitigation[j].Risk))) {
    //             expect(wrapper.vm.risks_4.push).toHaveBeenCalled(wrapper.vm.Results.endangered_species.mammals[i].Risks_mitigation[j].Risk)
    //           }
    //           if (!(wrapper.vm.mitigation_4.includes(wrapper.vm.Results.endangered_species.mammals[i].Risks_mitigation[j].Mitigation))) {
    //             expect(wrapper.vm.mitigation_4.push).toHaveBeenCalled(wrapper.vm.Results.endangered_species.mammals[i].Risks_mitigation[j].Mitigation)
    //           }
    //         }
    //     }
    //     for (const i in wrapper.vm.Results.endangered_species.reptilia) {
    //         if (!(wrapper.vm.surveys_5.includes(wrapper.vm.Results.endangered_species.reptilia[i].Surveys))) {
    //           expect(wrapper.vm.surveys_5.push).toHaveBeenCalled(wrapper.vm.Results.endangered_species.reptilia[i].Surveys)
    //         }
    //         for (const j in wrapper.vm.Results.endangered_species.reptilia[i].Risks_mitigation) {
    //           if (!(wrapper.vm.risks_5.includes(wrapper.vm.Results.endangered_species.reptilia[i].Risks_mitigation[j].Risk))) {
    //             expect(wrapper.vm.risks_5.push).toHaveBeenCalled(wrapper.vm.Results.endangered_species.reptilia[i].Risks_mitigation[j].Risk)
    //           }
    //           if (!(wrapper.vm.mitigation_5.includes(wrapper.vm.Results.endangered_species.reptilia[i].Risks_mitigation[j].Mitigation))) {
    //             expect(wrapper.vm.mitigation_5.push).toHaveBeenCalled(wrapper.vm.Results.endangered_species.reptilia[i].Risks_mitigation[j].Mitigation)
    //           }
    //         }
    //     }
    // })

    it('computed_properties', async () => {
      const storeDispatch = jest.spyOn(store, 'dispatch');
      wrapper.vm.Results = "value"
      expect(storeDispatch).toBeCalledTimes(0);  
      // expect(store.dispatch).toHaveBeenCalledWith("set_ResultsAction", "value")
      store.dispatch.mockRestore()
    })

    /*
    it('loadOutputs', async () => {
        const path = `http://localhost:5000/esa/${wrapper.vm.Project.name}/loadoutputs`
        axios.resolveWith(path)
        //await wrapper.vm.loadOutputs()
        //await wrapper.vm.$nextTick()
        expect(wrapper.vm.Results).toBeDefined
    })
    */
})
