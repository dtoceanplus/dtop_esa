import { shallowMount, createLocalVue } from '@vue/test-utils'

import Run_Module from '@/views/run_module/index'
import ElementUI from 'element-ui'

import ProjectInputs from '../json/Project_inputs.json'
import ProjectStatusInputs from '../json/Project_status.json'
import Project_Results from '../json/Project.json'

import axios from 'axios'
import Vuex from 'vuex'

jest.mock('axios')

describe('Run_Module', () => {
    const localVue = createLocalVue()
    localVue.use(Vuex)
    localVue.use(ElementUI)
    let Run_ModuleId = 1

    const $router = {
        push: jest.fn(),
    }
    let store = new Vuex.Store({
        state: {
            Project: ProjectInputs,
            ProjectStatus: ProjectStatusInputs
        }
    })
    const wrapper = shallowMount(Run_Module, {
        data() {
            return {
                Results: Project_Results,
                ProjectName: 'test_run'
            }
        },
        localVue,
        store,
        mocks: {
            $router,
            $route: {
                params: {
                    Run_ModuleId: Run_ModuleId,
                },
                query: { ProjectName: 'test_run' }
            }
        }
    })

    it('Test ProjectName data', () => {
        expect(wrapper.vm.ProjectName).toBeDefined
    })

    it('LoadOutputs', async () => {
        const path = `http://localhost:5000/esa/test_run/loadoutputs`
        axios.resolveWith(path)
        await wrapper.vm.LoadOutputs('test_run')
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.Results).toBeDefined
    })

    it('RunProject', async () => {
        const path = `http://localhost:5000/esa/${wrapper.vm.Project.name}/run`
        const payload = {
            inputs_ESA: wrapper.vm.Project.inputs_ESA,
            run_mode: wrapper.vm.Project.run_mode,
            url_sc_get_farm: wrapper.vm.Project.url_sc_get_farm,
            url_sc_get_current: wrapper.vm.Project.url_sc_get_current,
            url_ec_get_farm: wrapper.vm.Project.url_ec_get_farm,
            url_ec_get_project: wrapper.vm.Project.url_ec_get_project,
            url_mc_dimensions: wrapper.vm.Project.url_mc_dimensions,
            url_mc_general: wrapper.vm.Project.url_mc_general,
            url_ed_get_results: wrapper.vm.Project.url_ed_get_results,
            url_sk_get_env_impact: wrapper.vm.Project.url_sk_get_env_impact,
            url_lmo_results: wrapper.vm.Project.url_lmo_results,
            url_lmo_catalogue: wrapper.vm.Project.url_lmo_catalogue,
            url_ed_catalogue_static_cable: wrapper.vm.Project.url_ed_catalogue_static_cable,
            url_ed_catalogue_dynamic_cable: wrapper.vm.Project.url_ed_catalogue_dynamic_cable,
            url_ed_catalogue_collection_point: wrapper.vm.Project.url_ed_catalogue_collection_point
        }
        axios.resolveWith(path, payload)
        // await wrapper.vm.RunProject()
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.Log).toBeDefined
        expect(wrapper.vm.is_saved).toBeDefined
        expect(wrapper.vm.project_running).toBeDefined
    })

    it('onRunModule', async () => {
        const payload = {
            inputs_ESA: wrapper.vm.Project.inputs_ESA,
            run_mode: wrapper.vm.Project.run_mode,
            url_sc_get_farm: wrapper.vm.Project.url_sc_get_farm,
            url_sc_get_current: wrapper.vm.Project.url_sc_get_current,
            url_ec_get_farm: wrapper.vm.Project.url_ec_get_farm,
            url_ec_get_project: wrapper.vm.Project.url_ec_get_project,
            url_mc_dimensions: wrapper.vm.Project.url_mc_dimensions,
            url_mc_general: wrapper.vm.Project.url_mc_general,
            url_ed_get_results: wrapper.vm.Project.url_ed_get_results,
            url_sk_get_env_impact: wrapper.vm.Project.url_sk_get_env_impact,
            url_lmo_results: wrapper.vm.Project.url_lmo_results,
            url_lmo_catalogue: wrapper.vm.Project.url_lmo_catalogue,
            url_ed_catalogue_static_cable: wrapper.vm.Project.url_ed_catalogue_static_cable,
            url_ed_catalogue_dynamic_cable: wrapper.vm.Project.url_ed_catalogue_dynamic_cable,
            url_ed_catalogue_collection_point: wrapper.vm.Project.url_ed_catalogue_collection_point
        }
        // let mockRun = jest.mock(wrapper.vm.RunProject)
        await wrapper.vm.onRunModule()
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.payload).toBeDefined
        expect(wrapper.vm.Log).toBeDefined
        // expect(mockRun).toHaveBeenCalledTimes(1)
        // expect(mockRun).toHaveBeenCalledWith(payload)
    })

    it('RedirectToOutput', async() => {
        await wrapper.vm.RedirectToOutput()
        await wrapper.vm.$nextTick()
        expect($router.push).toHaveBeenCalledWith({"name": 'Endangered_Species' });
    })

    it('SaveAs', async () => {
        const payload = {
            inputs_ESA: wrapper.vm.Project.inputs_ESA,
            run_mode: wrapper.vm.Project.run_mode,
            url_sc_get_farm: wrapper.vm.Project.url_sc_get_farm,
            url_sc_get_current: wrapper.vm.Project.url_sc_get_current,
            url_ec_get_farm: wrapper.vm.Project.url_ec_get_farm,
            url_ec_get_project: wrapper.vm.Project.url_ec_get_project,
            url_mc_dimensions: wrapper.vm.Project.url_mc_dimensions,
            url_mc_general: wrapper.vm.Project.url_mc_general,
            url_ed_get_results: wrapper.vm.Project.url_ed_get_results,
            url_sk_get_env_impact: wrapper.vm.Project.url_sk_get_env_impact,
            url_lmo_results: wrapper.vm.Project.url_lmo_results,
            url_lmo_catalogue: wrapper.vm.Project.url_lmo_catalogue,
            url_ed_catalogue_static_cable: wrapper.vm.Project.url_ed_catalogue_static_cable,
            url_ed_catalogue_dynamic_cable: wrapper.vm.Project.url_ed_catalogue_dynamic_cable,
            url_ed_catalogue_collection_point: wrapper.vm.Project.url_ed_catalogue_collection_point
        }
        // let mockSaveProject = jest.mock('saveProject')
        // let mockSaveStatus = jest.mock('saveStatus')
        await wrapper.vm.SaveAs()
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.payload).toBeDefined
        expect(wrapper.vm.Log).toBeDefined
        expect(wrapper.vm.payload0).toBeDefined
        /*
        expect(mockSaveProject).toHaveBeenCalledTimes(1)
        expect(mockSaveStatus).toHaveBeenCalledTimes(1)
        expect(mockSaveProject).toHaveBeenCalledWith(payload, wrapper.vm.Project.name)
        expect(mockSaveStatus).toHaveBeenCalledWith(payload, wrapper.vm.Project.name)
        */
    })

    it('saveProject', async () => {
        const payload = {
            inputs_ESA: wrapper.vm.Project.inputs_ESA,
            run_mode: wrapper.vm.Project.run_mode,
            url_sc_get_farm: wrapper.vm.Project.url_sc_get_farm,
            url_sc_get_current: wrapper.vm.Project.url_sc_get_current,
            url_ec_get_farm: wrapper.vm.Project.url_ec_get_farm,
            url_ec_get_project: wrapper.vm.Project.url_ec_get_project,
            url_mc_dimensions: wrapper.vm.Project.url_mc_dimensions,
            url_mc_general: wrapper.vm.Project.url_mc_general,
            url_ed_get_results: wrapper.vm.Project.url_ed_get_results,
            url_sk_get_env_impact: wrapper.vm.Project.url_sk_get_env_impact,
            url_lmo_results: wrapper.vm.Project.url_lmo_results,
            url_lmo_catalogue: wrapper.vm.Project.url_lmo_catalogue,
            url_ed_catalogue_static_cable: wrapper.vm.Project.url_ed_catalogue_static_cable,
            url_ed_catalogue_dynamic_cable: wrapper.vm.Project.url_ed_catalogue_dynamic_cable,
            url_ed_catalogue_collection_point: wrapper.vm.Project.url_ed_catalogue_collection_point
        }
        // const mockedFunc = jest.spyOn(Run_Module, 'saveProject');
        let value = wrapper.vm.Project.name
        await wrapper.vm.saveProject(payload, value)
        await wrapper.vm.$nextTick()
        /*
        expect(wrapper.vm.saveProject).toBeCalledTimes(1);  
        expect(wrapper.vm.saveProject).toHaveBeenCalledWith("payload", "value")
        store.dispatch.mockRestore()
        */
    })

    it('saveStatus', async () => {
        const payload = {
            inputs_ESA: wrapper.vm.Project.inputs_ESA,
            run_mode: wrapper.vm.Project.run_mode,
            url_sc_get_farm: wrapper.vm.Project.url_sc_get_farm,
            url_sc_get_current: wrapper.vm.Project.url_sc_get_current,
            url_ec_get_farm: wrapper.vm.Project.url_ec_get_farm,
            url_ec_get_project: wrapper.vm.Project.url_ec_get_project,
            url_mc_dimensions: wrapper.vm.Project.url_mc_dimensions,
            url_mc_general: wrapper.vm.Project.url_mc_general,
            url_ed_get_results: wrapper.vm.Project.url_ed_get_results,
            url_sk_get_env_impact: wrapper.vm.Project.url_sk_get_env_impact,
            url_lmo_results: wrapper.vm.Project.url_lmo_results,
            url_lmo_catalogue: wrapper.vm.Project.url_lmo_catalogue,
            url_ed_catalogue_static_cable: wrapper.vm.Project.url_ed_catalogue_static_cable,
            url_ed_catalogue_dynamic_cable: wrapper.vm.Project.url_ed_catalogue_dynamic_cable,
            url_ed_catalogue_collection_point: wrapper.vm.Project.url_ed_catalogue_collection_point
        }
        // const mockedFunc = jest.spyOn(Run_Module, 'saveStatus');
        let value = wrapper.vm.Project.name
        await wrapper.vm.saveStatus(payload, value)
        await wrapper.vm.$nextTick()
        /*
        expect(wrapper.vm.saveStatus).toBeCalledTimes(1);  
        expect(wrapper.vm.saveStatus).toHaveBeenCalledWith("payload", "value")
        store.dispatch.mockRestore()
        */
    });

    it('changePage', async() => {
        await wrapper.vm.changePage('Farm_Info')
        await wrapper.vm.$nextTick()
        expect($router.push).toHaveBeenCalledWith({"name": "Farm_Info"});
    });

    it('computed_properties', async () => {
        const storeDispatch = jest.spyOn(store, 'dispatch');
        wrapper.vm.Results = "value"
        wrapper.vm.Description = "value"
        wrapper.vm.Name= "value"
        wrapper.vm.project_running = "value"
        wrapper.vm.is_saved = "value"
        wrapper.vm.GoToRunModule = "value"
        expect(storeDispatch).toBeCalledTimes(4);  
        // expect(store.dispatch).toHaveBeenCalledWith("set_ResultsAction", "value")
        expect(store.dispatch).toHaveBeenCalledWith("set_DescriptionAction", "value")
        expect(store.dispatch).toHaveBeenCalledWith("set_NameAction", "value")
        expect(store.dispatch).toHaveBeenCalledWith("set_project_runningAction", "value")
        expect(store.dispatch).toHaveBeenCalledWith("set_GoToRunModuleAction", "value")
        store.dispatch.mockRestore()
    });

})