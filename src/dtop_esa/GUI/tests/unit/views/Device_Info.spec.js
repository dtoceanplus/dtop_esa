import { shallowMount, createLocalVue } from '@vue/test-utils'

import Device_Info from '@/views/inputs_page/Device_Info/index'
import ElementUI from 'element-ui'

import ProjectInputs from '../json/Project_inputs.json'
import ProjectInputs_2 from '../json/Project_inputs_2.json'
import ProjectStatusInputs from '../json/Project_status.json'
import Project_Results from '../json/Project.json'

import Vue from 'vue'
import Vuex from 'vuex'

import { __createMocks as createStoreMocks } from '@/store';

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(ElementUI)

jest.mock('@/store')
jest.mock('axios')

describe('Device_Info', () => {
    const localVue = createLocalVue()
    localVue.use(Vuex)
    localVue.use(ElementUI)
    let Device_InfoId = 1;

    const $router = {
        push: jest.fn(),
    };
    const $notify = {
        info: jest.fn(),
    };
    let store = new Vuex.Store({
        state: {
            Project: ProjectInputs,
            ProjectStatus: ProjectStatusInputs,
            Name: ProjectInputs.name,
            used_materials_DI: [],
            used_materials_foundation_DI: [],
            entitylist: ''
        }
    });
    const wrapper = shallowMount(Device_Info, {
        data() {
            return {
                Results: Project_Results,
                ProjectName: 'test_run'
            }
        },
        localVue,
        store,
        mocks: {
            $router,
            $route: {
                params: {
                    Device_InfoId: Device_InfoId,
                },
                query: { ProjectName: 'test_run' }
            }
        }
    });

    it('Test ProjectName data', () => {
        expect(wrapper.vm.ProjectName).toEqual('test_run')
    })

    it('addNewElement', async () => {
        let lon = wrapper.vm.Project.inputs_ESA.device_info.coordinates_devices_x
        await wrapper.vm.addNewElement(lon)
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.LON).toEqual(lon)
    })

    it('deleteElement', async () => {
        let lon = wrapper.vm.Project.inputs_ESA.device_info.coordinates_devices_x;
        await wrapper.vm.deleteElement(lon);
        await wrapper.vm.$nextTick();
        expect(wrapper.vm.LON).not.toContain(lon);
    })

    it('changePage', async () => {
        let pageNameTest = 'Dashboard';
        await wrapper.vm.changePage(pageNameTest);
        await wrapper.vm.$nextTick();
        expect(wrapper.vm.ProjectStatus.device_info_page).toBeTruthy();
        expect($router.push).toHaveBeenCalledWith({ "name": pageNameTest });
    })

    it('openSidebar1', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar1();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Device General Info',
            message: '<h3> Device Type </h3> Description' +
              '<h3> Devices locations </h3> Description',
            duration: 0,
            position: 'top-right'
        })
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Device General Info',
            message: '<h3> Device Type </h3> Description' +
              '<h3> Devices locations </h3> Description',
            duration: 0,
            position: 'top-right'
        });
        $notify.info.mockRestore()
    })

    it('openSidebar2', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar2();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Device Dimensions',
            message: '<h3> Height </h3> Description' +
              '<h3> Width </h3> Description' +
              '<h3> Length </h3> Description' +
              '<h3> Wet Area </h3> Description' +
              '<h3> Dry Area </h3> Description',
            duration: 0,
            position: 'top-right'
        })
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Device Dimensions',
            message: '<h3> Height </h3> Description' +
              '<h3> Width </h3> Description' +
              '<h3> Length </h3> Description' +
              '<h3> Wet Area </h3> Description' +
              '<h3> Dry Area </h3> Description',
            duration: 0,
            position: 'top-right'
        });
        $notify.info.mockRestore()
    })

    it('openSidebar3', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar3();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Ressources',
            message: '<h3> Ressource Reduction </h3> Description' +
              '<h3> Used materials for the project </h3> Description' +
              '<h3> Quantity of used materials for the project </h3> Description' +
              '<h3> Quantity of used materials to recycle </h3> Description',
            duration: 0,
            position: 'top-right'
        })
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Ressources',
            message: '<h3> Ressource Reduction </h3> Description' +
              '<h3> Used materials for the project </h3> Description' +
              '<h3> Quantity of used materials for the project </h3> Description' +
              '<h3> Quantity of used materials to recycle </h3> Description',
            duration: 0,
            position: 'top-right'
        });
        $notify.info.mockRestore()
    })

    it('openSidebar4', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar4();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Environmental measurements',
            message: '<h3> Measured noise of the device </h3> Description' + 
            '<h3> Measured turbidity due to device installation </h3> Description',
            duration: 0,
            position: 'top-right'
          })
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Environmental measurements',
            message: '<h3> Measured noise of the device </h3> Description' + 
            '<h3> Measured turbidity due to device installation </h3> Description',
            duration: 0,
            position: 'top-right'
          });
        $notify.info.mockRestore()
    })

    it('openSidebar5', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar5();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Fishing restriction',
            message: '<h3> Fishery restriction surface around devices </h3> Description',
            duration: 0,
            position: 'top-right'
          })
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Fishing restriction',
            message: '<h3> Fishery restriction surface around devices </h3> Description',
            duration: 0,
            position: 'top-right'
          });
        $notify.info.mockRestore()
    })

    it('openSidebar6', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar6();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Foundation',
            message: '<h3> Used materials for the foundation </h3> Description' +
              '<h3> Quantity of used materials for the foundation </h3> Description' +
              '<h3> Quantity materials used for foundation to recycle </h3> Description' +
              '<h3> Footprint  </h3> Description' +
              '<h3> Surface of colonisable part </h3> Description' +
              '<h3> Measured Noise </h3> Description',
            duration: 0,
            position: 'top-right'
        })
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Foundation',
            message: '<h3> Used materials for the foundation </h3> Description' +
              '<h3> Quantity of used materials for the foundation </h3> Description' +
              '<h3> Quantity materials used for foundation to recycle </h3> Description' +
              '<h3> Footprint  </h3> Description' +
              '<h3> Surface of colonisable part </h3> Description' +
              '<h3> Measured Noise </h3> Description',
            duration: 0,
            position: 'top-right'
        });
        $notify.info.mockRestore()
    })

    // it('SetValue1', async () => {
    //     await wrapper.vm.SetValue1()
    //     for (var i=0; i<wrapper.vm.materials_properties.length; i++) {
    //         if (!(wrapper.vm.used_materials_DI.includes(wrapper.vm.materials_properties[i]))) {
    //           expect(wrapper.vm.Project.inputs_ESA.device_info.materials[wrapper.vm.materials_properties[i]].value).toBe(0)
    //           expect(wrapper.vm.Project.inputs_ESA.device_info.materials_to_recycle[wrapper.vm.materials_properties[i]].value).toBe(0)
    //         }
    //     }
    // })

    // it('SetValue2', async () => {
    //     await wrapper.vm.SetValue2()
    //     for (var i=0; i<wrapper.vm.materials_properties.length; i++) {
    //         if (!(wrapper.vm.used_materials_foundation_DI.includes(wrapper.vm.materials_properties[i]))) {
    //           expect(wrapper.vm.Project.inputs_ESA.device_info.foundation.materials[wrapper.vm.materials_properties[i]].value).toBe(0)
    //           expect(wrapper.vm.Project.inputs_ESA.device_info.foundation.materials_to_recycle[wrapper.vm.materials_properties[i]].value).toBe(0)
    //         }
    //     }
    // })

    it('computed_properties', async () => {
        const storeDispatch = jest.spyOn(store, 'dispatch');
        wrapper.vm.used_materials_DI = "value"
        wrapper.vm.used_materials_foundation_DI = "value"
        wrapper.vm.ProjectStatus = "value"
        wrapper.vm.Project = "value"
        expect(storeDispatch).toBeCalledTimes(4);  
        expect(store.dispatch).toHaveBeenCalledWith("set_UMDI", "value")
        expect(store.dispatch).toHaveBeenCalledWith("set_UMFDI", "value")
        expect(store.dispatch).toHaveBeenCalledWith("set_ProjectStatusAction", "value")
        expect(store.dispatch).toHaveBeenCalledWith("set_ProjectAction", "value")
        store.dispatch.mockRestore()
    })
})