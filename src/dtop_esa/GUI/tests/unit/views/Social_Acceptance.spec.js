import { shallowMount, createLocalVue } from '@vue/test-utils'

import Social_Acceptance from '@/views/outputs_page/Social_Acceptance/index'
import ElementUI from 'element-ui'

import Vuex from 'vuex'
import axios from 'axios'

import ProjectInputs from '../json/Project_inputs.json'
import ProjectStatusInputs from '../json/Project_status.json'
import Project_Results from '../json/Project.json'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(ElementUI)

jest.mock('@/store')
jest.mock('axios')

describe('Social_Acceptance', () => {
    const localVue = createLocalVue()
    localVue.use(Vuex)
    localVue.use(ElementUI)
    let Social_AcceptanceId = 1;

    const $router = {
        push: jest.fn(),
    };
    const $notify = {
        info: jest.fn(),
    };
    let store = new Vuex.Store({
        state: {
            Project: ProjectInputs,
            ProjectStatus: ProjectStatusInputs,
            Name: ProjectInputs.name
        }
    });
    const wrapper = shallowMount(Social_Acceptance, {
        data() {
            return {
                Results: Project_Results,
                ProjectName: 'test_run'
            }
        },
        localVue,
        store,
        mocks: {
            $router,
            $route: {
                params: {
                    Social_AcceptanceId: Social_AcceptanceId,
                },
                query: { ProjectName: 'test_run' }
            }
        }
    });

    it('Test ProjectName data', () => {
        expect(wrapper.vm.ProjectName).toEqual('test_run')
    });

    it('LoadOutputs', async () => {
        const path = `http://localhost:5000/esa/test_run/loadoutputs`
        axios.resolveWith(path)
        // await wrapper.vm.LoadOutputs()
        // await wrapper.vm.$nextTick()
        expect(wrapper.vm.Results).toBeDefined
    });

    it('GoToES', async () => {
        await wrapper.vm.GoToES()
        await wrapper.vm.$nextTick()
        expect($router.push).toHaveBeenCalledWith({"name": "Endangered_Species"});
    });

    it('GoToEIA', async () => {
        await wrapper.vm.GoToEIA()
        await wrapper.vm.$nextTick()
        expect($router.push).toHaveBeenCalledWith({"name": "Environmental_Impact_Assessment"});
    });

    it('GoToCF', async () => {
        await wrapper.vm.GoToCF()
        await wrapper.vm.$nextTick()
        expect($router.push).toHaveBeenCalledWith({"name": "Carbon_Footprint"});
    });

    it('openSidebar', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Global results',
            message: '<h3> Title </h3> Description' +
              '<h3> Title </h3> Description',
            duration: 0,
            position: 'top-right'
          });
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Global results',
            message: '<h3> Title </h3> Description' +
              '<h3> Title </h3> Description',
            duration: 0,
            position: 'top-right'
          });
        $notify.info.mockRestore()
    });

});