import mutations from './mutations'
import ProjectInputs_file from '../json/Project_inputs.json'
import ProjectStatus_file from '../json/Project_status.json'
import axios from 'axios'
jest.mock("axios")

test('changeRunMode', () => {
    const state = {
      Project: []
    }
    mutations.changeRunMode(state, 'dto')
    expect(state.Project.run_mode).toBe ('dto')
  })
  
test('changeComplexity', () => {
  const state = {
    ProjectStatus: []
  }
  mutations.changeComplexity(state, 1)
  expect(state.ProjectStatus.complexity_level).toBe (1)
  mutations.changeComplexity(state, 2)
  expect(state.ProjectStatus.complexity_level).toBe (2)
  mutations.changeComplexity(state, 3)
  expect(state.ProjectStatus.complexity_level).toBe (3)
})

test('set_ProjectStatus', () => {
  const state = {
    ProjectStatus: []
  }
  mutations.set_ProjectStatus(state, ProjectStatus_file)
  expect(state.ProjectStatus).toBe (ProjectStatus_file)
})

test('set_TestBegin', () => {
  const state = {
    test_begin: []
  }
  mutations.set_TestBegin(state, true)
  expect(state.test_begin).toBe (true)
  mutations.set_TestBegin(state, false)
  expect(state.test_begin).toBe (false)
})

test('set_SwitchFarmInfoPage_1', () => {
  const state = {
    switch_farm_info_page: []
  }
  mutations.set_SwitchFarmInfoPage(state, 1)
  expect(state.switch_farm_info_page).toStrictEqual ([false, false, false, false, false, false, false, false, false, false, false, false, false, false])
})

test('set_SwitchFarmInfoPage_2', () => {
  const state = {
    switch_farm_info_page: []
  }
  mutations.set_SwitchFarmInfoPage(state, [false, true, false, true, false, true, false, true, false, true, false, true, false, true])
  expect(state.switch_farm_info_page).toStrictEqual ([false, true, false, true, false, true, false, true, false, true, false, true, false, true])
})

test('set_um_DI', () => {
  const state = {
    used_materials_DI: []
  }
  mutations.set_um_DI(state, 1)
  expect(state.used_materials_DI).toStrictEqual ([])
  mutations.set_um_DI(state, ['test','test1','test2'])
  expect(state.used_materials_DI).toStrictEqual (['test','test1','test2'])
})

test('set_umf_DI', () => {
  const state = {
    used_materials_foundation_DI: []
  }
  mutations.set_umf_DI(state, 1)
  expect(state.used_materials_foundation_DI).toStrictEqual ([])
  mutations.set_umf_DI(state, ['test','test1','test2'])
  expect(state.used_materials_foundation_DI).toStrictEqual (['test','test1','test2'])
})

test('set_um_EI', () => {
  const state = {
    used_materials_foundation_DI: []
  }
  mutations.set_um_EI(state, 1)
  expect(state.used_materials_EI).toStrictEqual ([])
  mutations.set_um_EI(state, ['test','test1','test2'])
  expect(state.used_materials_EI).toStrictEqual (['test','test1','test2'])
})