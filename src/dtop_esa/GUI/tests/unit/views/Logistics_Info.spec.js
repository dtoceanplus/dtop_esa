import { shallowMount, createLocalVue } from '@vue/test-utils'

import Logistics_Info from '@/views/inputs_page/Logistics_Info/index'
import ElementUI from 'element-ui'

import ProjectInputs from '../json/Project_inputs.json'
import ProjectStatusInputs from '../json/Project_status.json'
import Project_Results from '../json/Project.json'

import Vue from 'vue'
import Vuex from 'vuex'

import { __createMocks as createStoreMocks } from '@/store';

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(ElementUI)

jest.mock('@/store')
jest.mock('axios')

describe('Logistics_Info', () => {
    const localVue = createLocalVue()
    localVue.use(Vuex)
    localVue.use(ElementUI)
    let Logistics_InfoId = 1;

    const $router = {
        push: jest.fn(),
    };
    const $notify = {
        info: jest.fn(),
    };
    let store = new Vuex.Store({
        state: {
            Project: ProjectInputs,
            ProjectStatus: ProjectStatusInputs,
            Name: ProjectInputs.name,
            entitylist: ''
        }
    });
    const wrapper = shallowMount(Logistics_Info, {
        data() {
            return {
                Results: Project_Results,
                ProjectName: 'test_run'
            }
        },
        localVue,
        store,
        mocks: {
            $router,
            $route: {
                params: {
                    Logistics_InfoId: Logistics_InfoId,
                },
                query: { ProjectName: 'test_run' }
            }
        }
    });

    it('Test ProjectName data', () => {
        expect(wrapper.vm.ProjectName).toEqual('test_run')
    })

    it('changePage', async () => {
        let pageNameTest = 'Dashboard'
        await wrapper.vm.changePage(pageNameTest)
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.ProjectStatus.logistics_info_page).toBeTruthy()
        expect($router.push).toHaveBeenCalledWith({ "name": pageNameTest });
    })

    it('openSidebar1', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar1();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Installation',
            message: '<h3> Number of Vessels </h3> Description' +
            '<h3> Vessels Mean Size </h3> Description' +
            '<h3> Number of Passengers </h3> Description' +
            '<h3> Measured Noise of the vessel </h3> Description' +
            '<h3> Measured Turbidity of the device </h3> Description' +
            '<h3> Chemical Polutant </h3> Description' +
            '<h3> Fuel Comsumption </h3> Description',
            duration: 0,
            position: 'top-right'
          })
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Installation',
            message: '<h3> Number of Vessels </h3> Description' +
            '<h3> Vessels Mean Size </h3> Description' +
            '<h3> Number of Passengers </h3> Description' +
            '<h3> Measured Noise of the vessel </h3> Description' +
            '<h3> Measured Turbidity of the device </h3> Description' +
            '<h3> Chemical Polutant </h3> Description' +
            '<h3> Fuel Comsumption </h3> Description',
            duration: 0,
            position: 'top-right'
          });
        $notify.info.mockRestore()
    })

    it('computed_properties', async () => {
        const storeDispatch = jest.spyOn(store, 'dispatch');
        wrapper.vm.Project = "value"
        wrapper.vm.ProjectStatus = "value"
        expect(storeDispatch).toBeCalledTimes(2);  
        expect(store.dispatch).toHaveBeenCalledWith("set_ProjectAction", "value")
        expect(store.dispatch).toHaveBeenCalledWith("set_ProjectStatusAction", "value")
        store.dispatch.mockRestore()
    })
})
