import { shallowMount, createLocalVue } from '@vue/test-utils'

import Environmental_Impact_Assessment from '@/views/outputs_page/Environmental_Impact_Assessment/index'
import ElementUI from 'element-ui'

import ProjectInputs from '../json/Project_inputs.json'
import ProjectStatusInputs from '../json/Project_status.json'
import Project_Results from '../json/Project.json'

import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(ElementUI)

jest.mock('@/store')
jest.mock('axios')

describe('Environmental_Impact_Assessment', () => {
    const localVue = createLocalVue()
    localVue.use(Vuex)
    localVue.use(ElementUI)

    const $router = {
        push: jest.fn(),
    };
    const $notify = {
        info: jest.fn(),
    };
    let store = new Vuex.Store({
        state: {
            Results: Project_Results,
            Project: ProjectInputs,
            ProjectStatus: ProjectStatusInputs,
            Name: ProjectInputs.name
        }
    });
    const wrapper = shallowMount(Environmental_Impact_Assessment, {
        data() {
            return {
                ProjectName: 'test_run'
            }
        },
        localVue,
        store,
        mocks: {
            $router
        }
    });

    it('Test ProjectName data', () => {
        expect(wrapper.vm.ProjectName).toEqual('test_run')
    });

    it('GoToES', async () => {
        await wrapper.vm.GoToES()
        await wrapper.vm.$nextTick()
        expect($router.push).toHaveBeenCalledWith({"name": "Endangered_Species"});
    });

    it('GoToCF', async () => {
        await wrapper.vm.GoToCF()
        await wrapper.vm.$nextTick()
        expect($router.push).toHaveBeenCalledWith({"name": "Carbon_Footprint"});
    });

    it('GoToSA', async () => {
        await wrapper.vm.GoToSA()
        await wrapper.vm.$nextTick()
        expect($router.push).toHaveBeenCalledWith({"name": "Social_Acceptance"});
    });

    it('openSidebar1', async () => {
        const notifyInfo = jest.spyOn($notify, 'info');
        await wrapper.vm.openSidebar1();
        await wrapper.vm.$nextTick();
        $notify.info({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Global Environmental Impact Assessment',
            message: '<h3> Title </h3> Description' +
              '<h3> Title </h3> Description',
            duration: 0,
            position: 'top-right'
          })
        expect(notifyInfo).toBeCalledTimes(1);  
        expect($notify.info).toHaveBeenCalledWith({
            customClass: 'my_notify',
            dangerouslyUseHTMLString: true,
            title: 'Global Environmental Impact Assessment',
            message: '<h3> Title </h3> Description' +
              '<h3> Title </h3> Description',
            duration: 0,
            position: 'top-right'
          });
        $notify.info.mockRestore()
    })
});