import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'

import Dashboard from '@/views/dashboard/index'
import ElementUI from 'element-ui'

import Vue from 'vue'
import axios from 'axios'

jest.mock('axios')

describe('Dashboard', () => {
    const localVue = createLocalVue()  
    let DashboardId = 2
    const $router = {
        push: jest.fn()
    }

    const wrapper = shallowMount(Dashboard, {
        mocks: {
            $route: {
                params: {
                    DashboardId: DashboardId,
                }
            },
            $router
        }
    })

    it('Name', () => {
        expect(Dashboard.name).toEqual("Dashboard")
    })
})
  
