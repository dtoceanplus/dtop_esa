import { shallowMount, createLocalVue } from '@vue/test-utils'

import Bar1 from '@/views/outputs_page/Carbon_Footprint/components/Bar1'
import ElementUI from 'element-ui'

import Vuex from 'vuex'
import axios from 'axios'

import ProjectInputs from '../json/Project_inputs.json'
import ProjectStatusInputs from '../json/Project_status.json'
import Project_Results from '../json/Project.json'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(ElementUI)

jest.mock('@/store')
jest.mock('axios')

describe('Bar1', () => {
    const localVue = createLocalVue()
    localVue.use(Vuex)
    localVue.use(ElementUI)
    let Bar1Id = 1;

    const $router = {
        push: jest.fn(),
    }
    let store = new Vuex.Store({
        state: {
            Project: ProjectInputs,
            ProjectStatus: ProjectStatusInputs, 
            Results: Project_Results
        }
    })
    const wrapper = shallowMount(Bar1, {
        data() {
            return {
                Results: Project_Results,
                ProjectName: 'test_run'
            }
        },
        localVue,
        store,
        mocks: {
            $router,
            $route: {
                params: {
                    Bar1Id: Bar1Id,
                },
                query: { ProjectName: 'test_run' }
            }
        }
    })

    it('Test ProjectName data', () => {
        expect(wrapper.vm.ProjectName).toEqual('test_run')
    })

    it("initChart", async () => {
        expect(wrapper.vm.chart).toBeDefined
    })

    
    it("beforeDestroy => chart component", () => {
        wrapper.vm.chart = true;
        //expect().toReturn()
    })
    

    it("should test lifecycle when audio tag is destroyed", () => {
        wrapper.vm.chart = false;
        expect(wrapper.vm.chart).toBeDefined;
        // when
        //wrapper.destroy()
        // then
        expect(wrapper.vm.chart).toBeUndefined;
    });

})