.. _ocean-energy_risks_opportunities:


Description of ocean energies and identification of potential risks and opportunities 
*************************************************************************************

The oceans are the world’s largest source of energy. Ocean energy technologies exploit the power of tides and waves, as well as differences in temperature and salinity, to produce energy. Many of the animal populations that reside in the energy-rich areas of the ocean are already under considerable stress from other human activities including shipping, fishing, waste disposal, and shoreline development (Crain et al. 2009). To achieve sustainable development, it is important that the MRE industry not cause additional environmental stress and related damage (Copping and Hemery, 2020). 

The installation of ocean energies in European waters is subject to a series of regulatory drivers. The focus here is made on environmental drivers that will regulate the development and installation of these new structures in the European Waters. As the ocean are already subject to numerous anthropogenic pressures, the populations of many species are decreasing at an unsustainable rate, and the number of species listed as Endangered from marine life such as whales, dolphins, seabirds and turtles, are on rise. The loss in species biodiversity is a well-recognized reality nowadays and this loss not only affects the endangered species, but it also impacts the functioning of marine ecosystems which threats their stability and resilience capacity and therefore, could impact the ecosystem services that they provide for human kind. The Marine Strategy Framework Directive (MSFD), the Habitat Directive and the Birds Directive are among the main European Directive that has been set in order to set a framework for European Waters and species conservation. These Directives are the cornerstone of the European environmental protection strategy and these Directives will directly frame the installation of any new structure at sea, including pre-installation and post-installation phases of Marine Renewable Energies (MRE) (Table xx1). Two additional regulatory drivers are important to highlight as important for MRE development. The Paris Agreement which goal is to keep the increase in global average temperature to well below 2 °C by mitigating the greenhouse-gas-emissions. The MRE sector has a major role to play in this worldwide agreement. And the Maritime Spatial Planning Directive that fosters an integrated ecosystem approach management, including the development of socio-economic opportunities by identifying and giving the suitable room to new and changing spatial uses. 


.. csv-table:: Pre-installation environmental and social acceptance concerns or opportunities
   :header: "Environmental or social", "Elements of concern/reason for the study", "Main UE regulatory drivers"
   :widths: 10 15 5

     "Species under special protection", "Marine animals under threat of extinction ", "MSFD"
     "Marine mammals", "Concern and speical societal value afforded to specific groups of animals", "MSFD"
     "Migratory birds", "Birds that migrate across regions and continents and are considered to be at risk", "Birds directives"
     "Important fish and shellfish populations", "Fish populations of commercial, recreational, or cultural importance", "MSFD"
     "Habitats", "Need to assess quantity and quality of habitat, due to important role in supporting marine species", "Habitat Directive"
     "Water quality", "Cumulative degradation of water quality, changes in sediment transport", "MSFD and WFD"
     "Global warming", "Need of low-carbon renewable energy and to adress the need for climate change mitigation", "Paris Agreement"

.. csv-table:: Post-installation environmental and social acceptance concerns or opportunities
   :header: "Environmental or social", "Elements of concern/reason for the study", "Main UE regulatory drivers"
   :widths: 10 15 5

     "Marines mammals", "Entrapment", "Nearfields monitoring"
     "...", "...", "..."

In this global regulatory context, deployment of marine renewable energy devices leads to interactions between these technologies and the surrounding environment. Certain aspects of the technology can be seen as potential stressors affecting environmental receptors like marine fauna and habitats (Copping et al. 2013, Aquatera Limited 2012). But at the same time, as a relevant technology to reduce climate change and create new socio-economic opportunities. Tidal and Wave energy devices may pose, for instance, a risk of collision to marine mammals, fish, and seabirds. The collision risk relates to the moving components of devices (blades and rotors), as well as vessels traffic from and to deployed ocean energy sites. Other factors could influence the collision risk such as animal behavior, and the number of animals in the vicinity of the moving parts of devices (Copping and Hemery, 2020). The list of potential stressors includes also acoustic impacts. Marine animals use sound in the ocean like terrestrial animals and humans use sight on land—to communicate, navigate, find food, socialize, and evade predators. Anthropogenic noise in the marine environment has the potential to interfere with these activities. At high levels, underwater sound can cause physical harm (Table xx1) (Copping and Hemery, 2020).
In order to integrate the environmental risk assessment and the socio-economic opportunities in the DTOceanPlus suite of tools, the following section will detail the way that the Environmental and Social Acceptance (ESA) module was structured to consider the risks and opportunities of ocean energy conceptual development while considering the regulatory framework for the installation of these new structures at sea.
