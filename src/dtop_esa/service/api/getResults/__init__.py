# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from flask import Blueprint, render_template, url_for, jsonify, request, current_app, send_file, abort
from os import listdir
import os
import os.path
import sys
from dtop_esa.Business.libraries.dtoesa.outputs.Project import Project
#------------------------------------
# @ USER DEFINED IMPORTS START
from pathlib import Path, PurePosixPath
working= os.getcwd()
data_path = Path('src/dtop_esa/ressources/')
PATH_ressources = os.path.join(working, data_path)
PATH_STORAGE = os.path.join(working,Path('storage/'))
import json

PATH_GUI = os.path.join(working, "src/dtop_esa/GUI/src/assets/Sites_images")
# @ USER DEFINED IMPORTS END
#------------------------------------


bp = Blueprint('getResults', __name__)

#------------------------------------
# @ USER DEFINED FUNCTIONS START
@bp.route('/MyProjects', methods=['GET'], strict_slashes=False)
def MyProjects():
    MyTable = list_files4(PATH_STORAGE,'json')
    return MyTable

def list_files4(directory, extension):
    import time
    fl=listdir(directory)
    MyTable = []
    for i, fli in enumerate(fl):
        if fli.endswith('.'+extension):
            path_file = os.path.join(directory, fl[i])
            path_file = os.path.abspath(path_file)
            dict = {}
            dict['path_file'] = path_file
            tim = os.path.getctime(path_file)
            dict['time'] = time.ctime(tim)
            dict['name'] = fli[:-5]
            MyTable.append(dict)
    return jsonify(MyTable)
# @ USER DEFINED FUNCTIONS END
#------------------------------------

##############################
# Functions for project load #
##############################

# List all available projects files
@bp.route('/Projects', methods=['GET'])
def get_all_available_projects():
    [folder,folder_name,files]=list_files('./storage','json')
    return jsonify(files)

# Load project file
def load_Project(ProjectId):

    Entity_name = 'error404'    
    if (os.path.exists('./storage/' + ProjectId + '.json')):
        project_name = ProjectId
    else:
        with open('./storage/Entity_table.json') as json_file:
            List = json.load(json_file)
            for entity in List['List_of_entity']:
                if int(ProjectId) == entity['EntityId']:
                    Entity_name = entity['EntityName']
                
        project_name=Entity_name + '_' + ProjectId
    filename=os.path.join('./storage',project_name+'.json')
    if os.path.exists(filename):
        project=Project()
        project.loadJSON(filePath = filename)
    else:
        abort(404)
    return project

# Find object index in array from its name
def find_index_from_name(name,object_array):
    index=-1
    for idx in range(0,len(object_array)):
        if name==object_array[idx].name:
            index=idx
    if index==-1:
        abort(404)
    return index

###########
# GET API #
###########

@bp.route('/<ProjectId>', methods=['GET'])
def get_Project(ProjectId):
    project=load_Project(ProjectId)
    return jsonify(project.prop_rep())


@bp.route('/<ProjectId>/endangered_species', methods=['GET'])
def get_project_endangered_species(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.endangered_species.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/carbon_footprint', methods=['GET'])
def get_project_carbon_footprint(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.carbon_footprint.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/carbon_footprint/CFP_global', methods=['GET'])
def get_project_carbon_footprint_CFP_global(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.carbon_footprint.CFP_global.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/carbon_footprint/CFP_phase', methods=['GET'])
def get_project_carbon_footprint_CFP_phase(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.carbon_footprint.CFP_phase.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/environmental_impact_assessment', methods=['GET'])
def get_project_environmental_impact_assessment(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.environmental_impact_assessment.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/environmental_impact_assessment/eia_global', methods=['GET'])
def get_project_environmental_impact_assessment_eia_global(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.environmental_impact_assessment.eia_global.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/environmental_impact_assessment/eia_tech_group', methods=['GET'])
def get_project_environmental_impact_assessment_eia_tech_group(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.environmental_impact_assessment.eia_tech_group.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/environmental_impact_assessment/eia_pressure', methods=['GET'])
def get_project_environmental_impact_assessment_eia_pressure(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.environmental_impact_assessment.eia_pressure.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/social_acceptance', methods=['GET'])
def get_project_social_acceptance(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.social_acceptance.prop_rep()
    except:
        abort(404)
    return jsonify(res)

#####################
# Utility functions #
#####################

def list_files(directory, extension):
    fl=listdir(directory)
    folder=[]
    folder_name=[]
    files=[]
    for i in range(0,len(fl)):
        path=os.path.join(directory, fl[i])
        path=os.path.abspath(path)
        if os.path.isdir(path):
            folder.append(path)
            folder_name.append(fl[i])
        elif path.endswith('.'+extension):
            files.append(path)
    return folder,folder_name,files
