# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from flask import Blueprint, render_template, url_for, jsonify, request, current_app, send_file, abort
from os import listdir
import os
import os.path
import sys
from dtop_esa.Business.libraries.dtoesa.outputs.Project import Project
#------------------------------------
# @ USER DEFINED IMPORTS START
from pathlib import Path, PurePosixPath
working= os.getcwd()
data_path = Path('src/dtop_esa/ressources/')
PATH_ressources = os.path.join(working, data_path)
PATH_STORAGE = os.path.join(working,Path('storage/'))
import json

PATH_GUI = os.path.join(working, "src/dtop_esa/GUI/src/assets/Sites_images")

# @ USER DEFINED IMPORTS END
#------------------------------------


bp = Blueprint('representation', __name__)

#------------------------------------
# @ USER DEFINED FUNCTIONS START
# @ USER DEFINED FUNCTIONS END
#------------------------------------

#####################################################
# Functions for project digital representation load #
#####################################################

# Load project file

# Load project file
def load_Project(ProjectId):

    Entity_name = 'error404' 
    
    if (os.path.exists('./storage/' + ProjectId + '.json')):
        project_name = ProjectId
    else:
        with open('./storage/Entity_table.json') as json_file:
            List = json.load(json_file)
            for entity in List['List_of_entity']:
                if int(ProjectId) == entity['EntityId']:
                    Entity_name = entity['EntityName']
                
        project_name=Entity_name + '_' + ProjectId
    filename=os.path.join('./storage',project_name+'.json')
    if os.path.exists(filename):
        project=Project()
        project.loadJSON(filePath = filename)
    else:
        abort(404)
    return project

###########
# GET API #
###########

@bp.route('/representation/<ProjectId>', methods=['GET'])
def get_project_dr(ProjectId):
    """Flask blueprint route for getting the digital representation of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns an error message. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of the digital representation or an error message.

    """
    project=load_Project(ProjectId)
    try:
        res=project.digital_representation.prop_rep()
    except:
        abort(404)
    return jsonify(res)