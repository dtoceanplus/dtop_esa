# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from flask import Blueprint, render_template, url_for, jsonify, request, current_app, send_file, abort
from os import listdir
import os
import os.path
import sys
import collections
from dtop_esa.Business.libraries.dtoesa.inputs.Inputs import Inputs
#------------------------------------
# @ USER DEFINED IMPORTS START
# from dtop_esa.Business.inputs.Inputs import Inputs
from dtop_esa.Business.main import run_main, process_inputs_EIA
from dtop_esa.Business.libraries.dtoesa.inputs.inputs_ESA_status import inputs_ESA_status
from dtop_esa.Business.libraries.dtoesa.outputs.Project import Project
import json
# @ USER DEFINED IMPORTS END
#------------------------------------


bp = Blueprint('postInputs', __name__)

#------------------------------------
# @ USER DEFINED FUNCTIONS START

def save_inputstatus_to_storage(my_inputstatus,ProjectId):

    # Save the project input
    storagePath = './storage'
    fileName=os.path.join(storagePath,ProjectId + '_status.json')
    my_inputstatus.saveJSON(fileName)

# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/initialise_Project_ProjectStatus', methods=['GET'], strict_slashes=False)
def initiate():

    if not(os.path.exists('./storage/Entity_table.json')):
        f = open("./storage/Entity_table.json",'w')
        f.write('{"List_of_entity": []}')
        f.close()

    my_inputs = Inputs()
    my_inputs.name = ''
    my_inputstatus = inputs_ESA_status()

    return jsonify(my_inputs.prop_rep(), my_inputstatus.prop_rep())


# API that save the status structure
@bp.route('/<ProjectId>/saveStatus', methods=['POST'], strict_slashes=False)
def save_inputstatus(ProjectId):

    # Create status structure from request
    my_inputstatus = inputs_ESA_status()

    # Load status from request
    my_inputstatus_rep = request.get_json()
    my_inputstatus.loadFromJSONDict(my_inputstatus_rep)
    my_inputstatus.name = ProjectId

    if (my_inputstatus.entity_id == ''):
        cpt = 1
        testlist = []

        # Get the ID
        with open('./storage/Entity_table.json') as json_file:
            List = json.load(json_file)
            for entity in List['List_of_entity']:
                testlist.append(entity['EntityId'])

        while cpt in testlist:
            cpt = cpt+1

        List['List_of_entity'].append({"EntityName": ProjectId, "EntityId": cpt})
        my_inputstatus.entity_id = cpt

        ListtoWrite = json.dumps(List)
        f=open('./storage/Entity_table.json', "w")
        f.write(ListtoWrite)
        f.close()

        my_filename = ProjectId + '_' + str(cpt)
        my_inputstatus.entity_id = cpt
    else:

        # Get the ID
        with open('./storage/Entity_table.json') as json_file:
            List = json.load(json_file)
            for entity in List['List_of_entity']:
                if (int(my_inputstatus.entity_id) == entity['EntityId'] and my_inputstatus.name == entity['EntityName']):
                    my_filename = entity['EntityName'] + '_' + str(entity['EntityId'])
        my_inputstatus.entity_id = entity['EntityId']

    # Save the input status structure
    save_inputstatus_to_storage(my_inputstatus,my_filename)

    return jsonify(my_inputstatus.prop_rep())

# API that load the input structure from a saved json file (the project name is defined by the GUI)
@bp.route('/<eid>/<ProjectId>/loadinputs', methods=['GET'], strict_slashes=False)
def load_inputs_structure(eid,ProjectId):

    # Load inputs from stored file
    if eid == 'none':
        my_filename = ProjectId
    else:
        my_filename = ProjectId + '_' + eid

    # Load inputs from stored file
    filename=os.path.join('./storage',my_filename+'_inputs.json')
    my_inputs=Inputs()
    my_inputs.loadJSON(filePath = filename)

    # Load inputstatus from stored file
    filename=os.path.join('./storage',my_filename+'_status.json')
    my_inputstatus=inputs_ESA_status()
    my_inputstatus.loadJSON(filePath = filename)

    return jsonify(my_inputs.prop_rep(), my_inputstatus.prop_rep())

@bp.route('/GetProjectsList', methods=['GET'], strict_slashes=False)
def MyProjects():
    MyTable = list_files('./storage','json')
    return jsonify(MyTable)

def list_files(directory, extension):
    import time
    fl=listdir(directory)
    MyTable = []
    for i, fli in enumerate(fl):
        if(fli[:-12] != 'Temp'):
            if (fli.endswith('_inputs.'+extension)and(os.path.exists(os.path.join('./storage',fli[:-12] + '_status.json')))):
                path_file = os.path.join(directory, fl[i])
                path_file = os.path.abspath(path_file)
                dict = {}
                dict['path_file'] = path_file
                tim = os.path.getctime(path_file)
                dict['time'] = time.ctime(tim)
                dict['name'] = fli[:-12]
                MyTable.append(dict)
    return MyTable

# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/<ProjectId>/DeleteProject', methods=['POST'], strict_slashes=False)
def delete(ProjectId):

    testlist = []

    # Get the Name
    with open('./storage/Entity_table.json') as json_file:
        List = json.load(json_file)
        for entity in List['List_of_entity']:
            if not(ProjectId == entity['EntityName'] + '_' + str(entity['EntityId'])):
                testlist.append(entity)
    List['List_of_entity'] = testlist
    ListtoWrite = json.dumps(List)
    f=open('./storage/Entity_table.json', "w")
    f.write(ListtoWrite)
    f.close()

    try:
        os.remove('./storage/'+ProjectId+'_inputs.json')
    except:
        print('')
    try:
        os.remove('./storage/'+ProjectId+'_status.json')
    except:
        print('')
    try:
        os.remove('./storage/'+ProjectId+'.json')
    except:
        print('')

    return "ok"

# API that load the output structure from a saved json file (the project name is defined by the GUI)
@bp.route('/<eid>/<ProjectId>/loadoutputs', methods=['POST'], strict_slashes=False)
def load_outputs_structure(eid,ProjectId):

    # Load inputs from stored file
    if eid == 'none':
        my_filename = ProjectId
    else:
        my_filename = ProjectId + '_' + eid

    if os.path.exists(os.path.join('./storage',my_filename+'.json')):

        # Load inputs from stored file
        filename=os.path.join('./storage',my_filename+'.json')
        my_outputs=Project()
        my_outputs.loadJSON(filePath = filename)

        res0 = my_outputs.endangered_species

        res1 = my_outputs.environmental_impact_assessment

        res2 = my_outputs.carbon_footprint

        res3 = my_outputs.social_acceptance

        return jsonify(res0.prop_rep(),res1.prop_rep(),res2.prop_rep(),res3.prop_rep())

    else:

        return jsonify(False)

# API that load the output structure from a saved json file (the project name is defined by the GUI)
@bp.route('/CheckCFP', methods=['GET'], strict_slashes=False)
def Check_CFP():

    name = request.args['name']

    if os.path.exists(os.path.join('./storage',name+'.json')):

        # Load inputs from stored file
        filename=os.path.join('./storage',name+'.json')
        my_outputs=Project()
        my_outputs.loadJSON(filePath = filename)

        GWP = [my_outputs.carbon_footprint.CFP_phase.cfp_production.GWP, my_outputs.carbon_footprint.CFP_phase.cfp_installation.GWP, my_outputs.carbon_footprint.CFP_phase.cfp_exploitation.GWP, my_outputs.carbon_footprint.CFP_phase.cfp_dismantling.GWP, my_outputs.carbon_footprint.CFP_phase.cfp_treatment.GWP]
        CED = [my_outputs.carbon_footprint.CFP_phase.cfp_production.CED, my_outputs.carbon_footprint.CFP_phase.cfp_installation.CED, my_outputs.carbon_footprint.CFP_phase.cfp_exploitation.CED, my_outputs.carbon_footprint.CFP_phase.cfp_dismantling.CED, my_outputs.carbon_footprint.CFP_phase.cfp_treatment.CED]
        if (GWP[0] == 0 and GWP[1] == 0 and GWP[2] == 0 and GWP[3] == 0 and GWP[4] == 0 and CED[0] == 0 and CED[1] == 0 and CED[2] == 0 and CED[3] == 0 and CED[4] == 0):
            return jsonify(False)
        else:
            return jsonify(True)

    else:
        return jsonify(False)

# API that load the output structure from a saved json file (the project name is defined by the GUI)
@bp.route('/loadCFP', methods=['GET'], strict_slashes=False)
def load_CFP_structure():

    name = request.args['name']

    if os.path.exists(os.path.join('./storage',name+'.json')):

        # Load inputs from stored file
        filename=os.path.join('./storage',name+'.json')
        my_outputs=Project()
        my_outputs.loadJSON(filePath = filename)

        GWP = [my_outputs.carbon_footprint.CFP_phase.cfp_production.GWP, my_outputs.carbon_footprint.CFP_phase.cfp_installation.GWP, my_outputs.carbon_footprint.CFP_phase.cfp_exploitation.GWP, my_outputs.carbon_footprint.CFP_phase.cfp_dismantling.GWP, my_outputs.carbon_footprint.CFP_phase.cfp_treatment.GWP, my_outputs.carbon_footprint.CFP_global.GWP]
        CED = [my_outputs.carbon_footprint.CFP_phase.cfp_production.CED, my_outputs.carbon_footprint.CFP_phase.cfp_installation.CED, my_outputs.carbon_footprint.CFP_phase.cfp_exploitation.CED, my_outputs.carbon_footprint.CFP_phase.cfp_dismantling.CED, my_outputs.carbon_footprint.CFP_phase.cfp_treatment.CED, my_outputs.carbon_footprint.CFP_global.CED]
        EPP = [my_outputs.carbon_footprint.CFP_global.EPP]
        return jsonify(GWP, CED, EPP)

    else:
        return jsonify(False, False, False)

# @ USER DEFINED FUNCTIONS END
#------------------------------------


def load_inputs_from_request():

    # Create input main structure
    my_inputs = Inputs()

    # Populate input data
    my_inputs_rep = request.get_json()
    my_inputs.loadFromJSONDict(my_inputs_rep)

#------------------------------------
# @ USER DEFINED PREPROCESS START
    my_inputs.populate() # If my_inputs.run_mode='dto', this command calls other modules' API and populate the input structure

# @ USER DEFINED PREPROCESS END
#------------------------------------

    return my_inputs

def save_inputs_to_storage(my_inputs,ProjectId):

    # Save the project input
    storagePath = './storage'
    fileName=os.path.join(storagePath,ProjectId + '_inputs.json')
    my_inputs.saveJSON(fileName)

    # Delete existing outputs
    filename=os.path.join(storagePath,ProjectId+'.json')
    if os.path.exists(filename):
        os.remove(filename)

# API that saves all inputs from request structure (request is to be created by the GUI frontend)
@bp.route('/<ProjectId>/inputs', methods=['POST'])
def save_inputs(ProjectId):

    # Load inputs from request
    my_inputs = load_inputs_from_request()

    # Save the project input
    save_inputs_to_storage(my_inputs,ProjectId)

    return jsonify(my_inputs.prop_rep(), 201)

# API that reads all inputs from request, run SK analysis and save results in <projectId>.json file
@bp.route('/<ProjectId>/run', methods=['POST'])
def run(ProjectId):

    # Load inputs from request
    my_inputs = load_inputs_from_request()

    # Run analysis
    log = collections.OrderedDict()

    # Save the project input
    save_inputs_to_storage(my_inputs,ProjectId)

    try:

#------------------------------------
# @ USER DEFINED RUN START
        # inputs = my_inputs.prop_rep()
        filename=os.path.join('./storage',ProjectId+'_status.json')
        my_inputstatus=inputs_ESA_status()
        my_inputstatus.loadJSON(filePath = filename)
        [my_inputs_processed, receptors_table, protected_table, weighting_table] = process_inputs_EIA(my_inputs)  #inputs["inputs_ESA"])

                                    # Run analysis
        project = run_main(my_inputs_processed, my_inputs,receptors_table, protected_table, weighting_table, my_inputstatus.complexity_level)

                                    # Save the project output
        storagePath = './storage'
        fileName=os.path.join(storagePath,ProjectId + '.json')
        project.saveJSON(fileName=fileName)
# @ USER DEFINED RUN END
#------------------------------------


        # Report success
        log["analysis_status"] = "success"

    except Exception as e:

        # Report fail and error message
        log["analysis_status"] = "failed"
        if hasattr(e, 'message'):
            log["error_message"] =str(e.message)
        else:
            log["error_message"] =str(e)

    return jsonify(log),201
