# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from flask import Blueprint, request

import shutil
import requests
import os

bp = Blueprint("provider_states", __name__)

@bp.route("/provider_states/setup", methods=["POST"])
def provider_states_setup():
    """
    Should be available only with `FLASK_ENV=development`.
    """
    consumer = request.json["consumer"]
    state = request.json["state"]

    if state.startswith("esa 1 exists"):
        shutil.copyfile("./src/dtop_esa/service/api/integration/project_example.json", "./storage/1.json")

    return ""
