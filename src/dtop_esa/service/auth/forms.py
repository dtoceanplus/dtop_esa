"""WTForms forms for Login"""

from wtforms import StringField, PasswordField
from wtforms import validators as v
from flask_wtf import FlaskForm
from flask_babel import lazy_gettext as _


class LoginForm(FlaskForm):
    username = StringField(_(u"Username"), [v.InputRequired()])
    password = PasswordField(_(u"Password"), [v.InputRequired()])
