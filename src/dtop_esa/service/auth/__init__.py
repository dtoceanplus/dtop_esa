"""Package for Login"""

from .views import login_required

__all__ = ["login_required"]
