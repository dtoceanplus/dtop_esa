import os
from flask import Flask, request, make_response, jsonify
from flask_babel import Babel
from flask_cors import CORS

#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------


babel = Babel()

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__)
    CORS(app, resources={r"/*": {"origins": "*"}})
    babel.init_app(app)
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)
    # Registering Blueprints
    from .api import getResults as res
    app.register_blueprint(res.bp, url_prefix='/esa')
    from .api import postInputs as inp
    app.register_blueprint(inp.bp, url_prefix='/esa')

    from .api import representation as dr
    app.register_blueprint(dr.bp)

#------------------------------------
# @ USER DEFINED BLUEPRINT REGISTRATION START
    from .api import mm_integration as mmi
    app.register_blueprint(mmi.bp, url_prefix='/esa')

    from .gui import main
    app.register_blueprint(main.bp)

    if os.environ.get("FLASK_ENV") == "development":
        from .api.integration import provider_states
        app.register_blueprint(provider_states.bp)


# @ USER DEFINED BLUEPRINT REGISTRATION END
#------------------------------------



#------------------------------------
# @ USER DEFINED FUNCTIONS START
# @ USER DEFINED FUNCTIONS END
#------------------------------------

    return app

@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(['en', 'fr'])   
