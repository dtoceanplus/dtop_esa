import numpy as np 
import os
import json
from pathlib import Path, PurePosixPath
import pandas as pd
import numpy as np
from flask import jsonify
import re
from .libraries.dtoesa.outputs.Project import Project
from .libraries.dtoesa.inputs import *
from .libraries.dtoesa.inputs.inputs_ESA import inputs_ESA
from .libraries.dtoesa.inputs.inputs_ESA import inputs_ESA
from .libraries.dtoesa.inputs.inputs_ESA_processed import inputs_ESA_processed
from .libraries.dtoesa.outputs.Project import Project
from collections import Counter
# Paths
working= os.getcwd()
data_path = Path('src/dtop_esa/ressources/')
PATH_ressources = os.path.join(working, data_path)
PATH_STORAGE = os.path.join(working,Path('storage/'))

data_species_file  = os.path.join(PATH_ressources, "database_species.json")
dict_fuel_file     = os.path.join(PATH_ressources, "dict_fuel.json")
dict_material_file = os.path.join(PATH_ressources, "dict_materials.json")
dict_gases_file    = os.path.join(PATH_ressources, "dict_gases_impact_factor.json")
database_species         = json.load(open(data_species_file))
dict_fuel                = json.load(open(dict_fuel_file))
dict_materials           = json.load(open(dict_material_file))
dict_gases_impact_factor = json.load(open(dict_gases_file))

def process_inputs_EIA(my_inputs):
    '''
    Process all inputs 
    '''
    my_inputs_processed= inputs_ESA_processed()
    # first step 
    my_inputs_processed.name = my_inputs.name
    my_inputs_processed.description = my_inputs.description
    my_inputs_processed.projectId = my_inputs.name

    # EIA
    depth = min(my_inputs.inputs_ESA.farm_info.water_depth)
    my_inputs_processed.inputs_EIA.input_hydro.Energy_Modification = my_inputs.inputs_ESA.device_info.resource_reduction
    my_inputs_processed.inputs_EIA.input_hydro.Size_of_the_Devices = my_inputs.inputs_ESA.device_info.dimensions.length
    my_inputs_processed.inputs_EIA.input_hydro.Immersed_Height_of_the_Devices = my_inputs.inputs_ESA.device_info.dimensions.height
    my_inputs_processed.inputs_EIA.input_hydro.Water_Depth = min(my_inputs.inputs_ESA.farm_info.water_depth)
    my_inputs_processed.inputs_EIA.input_hydro.Current_Direction = my_inputs.inputs_ESA.farm_info.current_direction
    my_inputs_processed.inputs_EIA.input_hydro.Inial_Turbidity = my_inputs.inputs_ESA.farm_info.Initial_Turbidity
    my_inputs_processed.inputs_EIA.input_hydro.Measured_Turbidity = my_inputs.inputs_ESA.device_info.measured_turbidity
    my_inputs_processed.inputs_EIA.input_hydro.Initial_Noise_dB_re_1muPa = my_inputs.inputs_ESA.farm_info.Initial_Noise_dB_re_1muPa
    my_inputs_processed.inputs_EIA.input_hydro.Measured_Noise_dB_re_1muPa = my_inputs.inputs_ESA.device_info.measured_noise

    if my_inputs.inputs_ESA.farm_info.Fisheries == "Complete prohibition":
        my_inputs_processed.inputs_EIA.input_hydro.Fishery_Restriction_Surface = my_inputs.inputs_ESA.farm_info.total_surface_area
        my_inputs_processed.inputs_EIA.input_elec.Fishery_Restriction_Surface = my_inputs.inputs_ESA.farm_info.total_surface_area
    else:
        my_inputs_processed.inputs_EIA.input_hydro.Fishery_Restriction_Surface = my_inputs.inputs_ESA.device_info.Fishery_Restriction_Surface_around_devices
        my_inputs_processed.inputs_EIA.input_elec.Fishery_Restriction_Surface = my_inputs.inputs_ESA.electrical_info.Fishery_Restriction_Surface_around_cables

    my_inputs_processed.inputs_EIA.input_hydro.Total_Surface_Area = my_inputs.inputs_ESA.farm_info.total_surface_area
    my_inputs_processed.inputs_EIA.input_hydro.Number_of_Objects = my_inputs.inputs_ESA.device_info.number_of_device
    my_inputs_processed.inputs_EIA.input_hydro.Object_Emerged_Surface = my_inputs.inputs_ESA.device_info.dimensions.dry_area
    my_inputs_processed.inputs_EIA.input_hydro.Surface_Area_of_Underwater_Part = my_inputs.inputs_ESA.device_info.dimensions.wet_area
    
    my_inputs_processed.inputs_EIA.input_elec.Size_of_the_Devices = my_inputs.inputs_ESA.electrical_info.collection_points_dimensions.width
    my_inputs_processed.inputs_EIA.input_elec.Current_Direction = my_inputs.inputs_ESA.farm_info.current_direction
    my_inputs_processed.inputs_EIA.input_elec.Immersed_Height_of_the_Devices = my_inputs.inputs_ESA.electrical_info.collection_points_dimensions.height
    my_inputs_processed.inputs_EIA.input_elec._Initial_Temperature = my_inputs.inputs_ESA.farm_info._Initial_Temperature
    my_inputs_processed.inputs_EIA.input_elec._Measured_Temperature = my_inputs.inputs_ESA.electrical_info._Measured_Temperature
    my_inputs_processed.inputs_EIA.input_elec.Initial_Noise_dB_re_1muPa = my_inputs.inputs_ESA.farm_info.Initial_Noise_dB_re_1muPa
    my_inputs_processed.inputs_EIA.input_elec.Measured_Noise_dB_re_1muPa = my_inputs.inputs_ESA.electrical_info.measured_noise
    my_inputs_processed.inputs_EIA.input_elec.Initial_Electric_Field = my_inputs.inputs_ESA.farm_info.Initial_Electric_Field
    my_inputs_processed.inputs_EIA.input_elec.Measured_Electric_Field = my_inputs.inputs_ESA.electrical_info.Measured_Electric_Field
    my_inputs_processed.inputs_EIA.input_elec.Initial_Magnetic_Field = my_inputs.inputs_ESA.farm_info.Initial_Magnetic_Field
    my_inputs_processed.inputs_EIA.input_elec.Measured_Magnetic_Field = my_inputs.inputs_ESA.electrical_info.Measured_Magnetic_Field        
    my_inputs_processed.inputs_EIA.input_elec.Total_Surface_Area = my_inputs.inputs_ESA.farm_info.total_surface_area
    my_inputs_processed.inputs_EIA.input_elec.Water_Depth = min(my_inputs.inputs_ESA.farm_info.water_depth)
    my_inputs_processed.inputs_EIA.input_elec.Surface_Area_Covered = my_inputs.inputs_ESA.electrical_info.footprint
    my_inputs_processed.inputs_EIA.input_elec.Number_of_Objects = len(my_inputs.inputs_ESA.electrical_info.collection_points_coordinates_x)
    my_inputs_processed.inputs_EIA.input_elec.Surface_Area_of_Underwater_Part = my_inputs.inputs_ESA.electrical_info.Surface_Area_of_electrical_Part

    my_inputs_processed.inputs_EIA.input_moor.Size_of_the_Devices = my_inputs.inputs_ESA.device_info.dimensions.width
    my_inputs_processed.inputs_EIA.input_moor.Immersed_Height_of_the_Devices = my_inputs.inputs_ESA.device_info.dimensions.height
    my_inputs_processed.inputs_EIA.input_moor.Current_Direction = my_inputs.inputs_ESA.farm_info.current_direction
    my_inputs_processed.inputs_EIA.input_moor.Initial_Noise_dB_re_1muPa = my_inputs.inputs_ESA.farm_info.Initial_Noise_dB_re_1muPa
    my_inputs_processed.inputs_EIA.input_moor.Measured_Noise_dB_re_1muPa = my_inputs.inputs_ESA.device_info.measured_noise
    my_inputs_processed.inputs_EIA.input_moor.Number_of_Objects = my_inputs.inputs_ESA.device_info.number_of_device
    my_inputs_processed.inputs_EIA.input_moor.Total_Surface_Area = my_inputs.inputs_ESA.farm_info.total_surface_area
    my_inputs_processed.inputs_EIA.input_moor.Water_Depth = min(my_inputs.inputs_ESA.farm_info.water_depth)
    my_inputs_processed.inputs_EIA.input_moor.Surface_Area_Covered = my_inputs.inputs_ESA.device_info.foundation.footprint
    my_inputs_processed.inputs_EIA.input_moor.Surface_Area_of_Underwater_Part = my_inputs.inputs_ESA.device_info.foundation.submerged_surface
    
    my_inputs_processed.inputs_EIA.input_insta.Total_Surface_Area = my_inputs.inputs_ESA.farm_info.total_surface_area
    my_inputs_processed.inputs_EIA.input_insta.Initial_Noise_dB_re_1muPa = my_inputs.inputs_ESA.farm_info.Initial_Noise_dB_re_1muPa
    my_inputs_processed.inputs_EIA.input_insta.Measured_Noise_dB_re_1muPa = my_inputs.inputs_ESA.logistics_info.installation.measure_noise
    my_inputs_processed.inputs_EIA.input_insta.Surface_Area_Covered = 0
    my_inputs_processed.inputs_EIA.input_insta.Number_of_Vessels = my_inputs.inputs_ESA.logistics_info.installation.Number_of_Vessels
    my_inputs_processed.inputs_EIA.input_insta.Size_of_Vessels = my_inputs.inputs_ESA.logistics_info.installation.mean_Size_of_Vessels
    my_inputs_processed.inputs_EIA.input_insta.Initial_Turbidity = my_inputs.inputs_ESA.farm_info.Initial_Turbidity
    my_inputs_processed.inputs_EIA.input_insta.Measured_Turbidity = my_inputs.inputs_ESA.logistics_info.installation.measured_turbidity
    if my_inputs.inputs_ESA.logistics_info.installation.chemical_polutant:
        my_inputs_processed.inputs_EIA.input_insta.Import_of_Chemical_Polutant = 1

    my_inputs_processed.inputs_EIA.input_maint.Surface_Area_Covered = 0
    my_inputs_processed.inputs_EIA.input_maint.Total_Surface_Area = my_inputs.inputs_ESA.farm_info.total_surface_area
    my_inputs_processed.inputs_EIA.input_maint.Initial_Noise_dB_re_1muPa = my_inputs.inputs_ESA.farm_info.Initial_Noise_dB_re_1muPa
    my_inputs_processed.inputs_EIA.input_maint.Measured_Noise_dB_re_1muPa = my_inputs.inputs_ESA.logistics_info.exploitation.measure_noise
    my_inputs_processed.inputs_EIA.input_maint.Number_of_Vessels = my_inputs.inputs_ESA.logistics_info.exploitation.Number_of_Vessels
    my_inputs_processed.inputs_EIA.input_maint.Size_of_Vessels = my_inputs.inputs_ESA.logistics_info.exploitation.mean_Size_of_Vessels
    my_inputs_processed.inputs_EIA.input_maint.Inial_Turbidity = my_inputs.inputs_ESA.farm_info.Initial_Turbidity
    my_inputs_processed.inputs_EIA.input_maint.Measured_Turbidity = my_inputs.inputs_ESA.logistics_info.exploitation.measured_turbidity
    if my_inputs.inputs_ESA.logistics_info.exploitation.chemical_polutant:
        my_inputs_processed.inputs_EIA.input_maint.Import_of_Chemical_Polutant = 1

    # Weighting 
        # hydro
    
    if type(my_inputs.inputs_ESA.farm_info.soil_type)== np.ndarray or type(my_inputs.inputs_ESA.farm_info.soil_type) == list:
        try:
            seabed_type = Counter(my_inputs.inputs_ESA.farm_info.soil_type).most_common(1)
            my_inputs_processed.inputs_EIA.weighting_dict.weighting_hydro.Energy_Modification = seabed_type[0][0]
        except:
            pass
    else:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_hydro.Energy_Modification = my_inputs.inputs_ESA.farm_info.soil_type

    if type(my_inputs.inputs_ESA.device_info.coordinates_devices_x)==np.ndarray:
        my_inputs.inputs_ESA.device_info.coordinates_devices_x= my_inputs.inputs_ESA.device_info.coordinates_devices_x.tolist()
        my_inputs.inputs_ESA.device_info.coordinates_devices_y= my_inputs.inputs_ESA.device_info.coordinates_devices_y.tolist()
    if my_inputs.inputs_ESA.device_info.coordinates_devices_x.count(my_inputs.inputs_ESA.device_info.coordinates_devices_x[0])==len(my_inputs.inputs_ESA.device_info.coordinates_devices_x) or my_inputs.inputs_ESA.device_info.coordinates_devices_y.count(my_inputs.inputs_ESA.device_info.coordinates_devices_y[0])==len(my_inputs.inputs_ESA.device_info.coordinates_devices_y):
        if  my_inputs.inputs_ESA.farm_info.zone_type == "open water":
            my_inputs_processed.inputs_EIA.weighting_dict.weighting_hydro.Collision_Risk = "open water/ devices in serie"
        elif  my_inputs.inputs_ESA.farm_info.zone_type == "sea loch entrance":
            my_inputs_processed.inputs_EIA.weighting_dict.weighting_hydro.Collision_Risk = "sea loch entrance/ devices in serie"
        elif  my_inputs.inputs_ESA.farm_info.zone_type == "sounds":
            my_inputs_processed.inputs_EIA.weighting_dict.weighting_hydro.Collision_Risk = "sounds/ devices in serie"
    else:
        if  my_inputs.inputs_ESA.farm_info.zone_type == "open water":
            my_inputs_processed.inputs_EIA.weighting_dict.weighting_hydro.Collision_Risk = "open water/ devices in parallel"
        elif  my_inputs.inputs_ESA.farm_info.zone_type == "sea loch entrances":
            my_inputs_processed.inputs_EIA.weighting_dict.weighting_hydro.Collision_Risk = "sea loch entrance/ devices in parallel"
        elif  my_inputs.inputs_ESA.farm_info.zone_type == "sounds":
            my_inputs_processed.inputs_EIA.weighting_dict.weighting_hydro.Collision_Risk = "sounds/ devices in parallel"

    if  my_inputs.inputs_ESA.device_info.machine_type == "WEC":
        if  my_inputs.inputs_ESA.device_info.dimensions.height > my_inputs.inputs_ESA.device_info.dimensions.width:
            my_inputs_processed.inputs_EIA.weighting_dict.weighting_hydro.Reef_Effect = "Wave design vertical"
        else:
            my_inputs_processed.inputs_EIA.weighting_dict.weighting_hydro.Reef_Effect = "Wave design horizontal"
    elif my_inputs.inputs_ESA.device_info.machine_type == "TEC": 
        if  my_inputs.inputs_ESA.device_info.dimensions.height > my_inputs.inputs_ESA.device_info.dimensions.width:
            my_inputs_processed.inputs_EIA.weighting_dict.weighting_hydro.Reef_Effect = "Tidal design vertical"
        else:
            my_inputs_processed.inputs_EIA.weighting_dict.weighting_hydro.Reef_Effect = "Tidal design horizontal"
    my_inputs_processed.inputs_EIA.weighting_dict.weighting_hydro.Reserve_Effect = my_inputs.inputs_ESA.farm_info.Fisheries
    my_inputs_processed.inputs_EIA.weighting_dict.weighting_hydro.Resting_Place = "No dangerous part of devices"
    noise = my_inputs.inputs_ESA.device_info.measured_noise
    if noise >= 0 and noise < 90:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_hydro.Underwater_Noise = "noise device 0 - 90 dB re 1muPa"
    elif noise > 90 and noise <100:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_hydro.Underwater_Noise = "noise device 90 - 100 dB re 1muPa"
    elif noise > 100 and noise <150:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_hydro.Underwater_Noise = "noise device 100 - 150 dB re 1muPa"
    elif noise > 150 and noise <200:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_hydro.Underwater_Noise = "noise device 150 - 200  dB re 1muPa"
    elif noise > 200:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_hydro.Underwater_Noise = "noise device > 200 dB re 1muPa"

        #elec
    if my_inputs.inputs_ESA.electrical_info.Burial == True:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_elec.Collision_Risk = "cable buried"
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_elec.Electric_Fields = "cable buried"
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_elec.Footprint = "cable buried"
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_elec.Magnetic_Fields = "cable buried"
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_elec.Temperature_Modification = "cable buried"
        
    else:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_elec.Collision_Risk = "cable not buried"
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_elec.Electric_Fields = "cable not buried"
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_elec.Footprint = "cable not buried"
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_elec.Magnetic_Fields = "cable not buried"
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_elec.Temperature_Modification = "cable not buried"
    if len(my_inputs.inputs_ESA.electrical_info.collection_points_coordinates_x)>0:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_elec.Reef_Effect = "substation"
    elif my_inputs.inputs_ESA.electrical_info.Burial == false:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_elec.Reef_Effect = "cable not buried"
    else:
        pass

    my_inputs_processed.inputs_EIA.weighting_dict.weighting_elec.Reserve_Effect = my_inputs.inputs_ESA.farm_info.Fisheries

    noise = my_inputs.inputs_ESA.electrical_info.measured_noise
    if noise >= 0 and noise < 90:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_elec.Underwater_Noise = "noise electrical components 0 dB re 1muPa"
    elif noise > 90 and noise <100:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_elec.Underwater_Noise = "noise electrical components 90 dB re 1muPa"
    elif noise > 100 and noise <150:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_elec.Underwater_Noise = "noise electrical components 100 dB re 1muPa"
    elif noise > 150 and noise <200:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_elec.Underwater_Noise = "noise electrical components 150 dB re 1muPa"
    elif noise > 200:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_elec.Underwater_Noise = "noise electrical components 200 dB re 1muPa"

            #moor

    # voir collision risk avec rocio en fonction profondeur
    if my_inputs_processed.inputs_EIA.input_moor.Water_Depth < 20:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_moor.Collision_Risk = "taut"
    my_inputs_processed.inputs_EIA.weighting_dict.weighting_moor.Footprint = ''
    my_inputs_processed.inputs_EIA.weighting_dict.weighting_moor.Reef_Effect = ''
    noise = my_inputs.inputs_ESA.device_info.foundation.measured_noise
    if noise >= 0 and noise < 90:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_moor.Underwater_Noise = "chafing 0 dB re 1muPa"
    elif noise > 90 and noise <100:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_moor.Underwater_Noise = "chafing 90 dB re 1muPa"
    elif noise > 100 and noise <150:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_moor.Underwater_Noise = "chafing 100 dB re 1muPa"
    elif noise > 150 and noise <200:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_moor.Underwater_Noise = "chafing 150 dB re 1muPa"
    elif noise > 200:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_moor.Underwater_Noise = "chafing 200 dB re 1muPa"

        #insta/maintenance
    my_inputs_processed.inputs_EIA.weighting_dict.weighting_insta.Chemical_Pollution = my_inputs.inputs_ESA.logistics_info.installation.chemical_polutant
    my_inputs_processed.inputs_EIA.weighting_dict.weighting_maint.Chemical_Pollution = my_inputs.inputs_ESA.logistics_info.exploitation.chemical_polutant
    my_inputs_processed.inputs_EIA.weighting_dict.weighting_insta.Collision_Risk_Vessel = ''
    my_inputs_processed.inputs_EIA.weighting_dict.weighting_maint.Collision_Risk_Vessel = ''
    my_inputs_processed.inputs_EIA.weighting_dict.weighting_insta.Footprint = ''
    my_inputs_processed.inputs_EIA.weighting_dict.weighting_maint.Footprint = ''
    my_inputs_processed.inputs_EIA.weighting_dict.weighting_insta.Turbidity = ''
    my_inputs_processed.inputs_EIA.weighting_dict.weighting_maint.Turbidity = ''
    noise = my_inputs.inputs_ESA.logistics_info.installation.measure_noise

    if noise > 0 and noise < 90:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_insta.Underwater_Noise = "noise vessels or tools 0 - 90 dB re 1muPa"
    elif noise >= 90 and noise <100:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_insta.Underwater_Noise = "noise vessels or tools 90 - 100 dB re 1muPa"
    elif noise >= 100 and noise <150:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_insta.Underwater_Noise = "noise vessels or tools 100 - 150 dB re 1muPa"
    elif noise >= 150 and noise <200:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_insta.Underwater_Noise = "noise vessels or tools 150 - 200 dB re 1muPa"
    elif noise >= 200:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_insta.Underwater_Noise = "noise vessels or tools > 200 dB re 1muPa"

    noise = my_inputs.inputs_ESA.logistics_info.exploitation.measure_noise
    if noise > 0 and noise < 90:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_maint.Underwater_Noise = "noise vessels or tools 0 - 90 dB re 1muPa"
    elif noise > 90 and noise <100:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_maint.Underwater_Noise = "noise vessels or tools 90 - 100 dB re 1muPa"
    elif noise > 100 and noise <150:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_maint.Underwater_Noise = "noise vessels or tools 100 - 150 dB re 1muPa"
    elif noise > 150 and noise <200:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_maint.Underwater_Noise = "noise vessels or tools 150 - 200 dB re 1muPa"
    elif noise > 200:
        my_inputs_processed.inputs_EIA.weighting_dict.weighting_maint.Underwater_Noise = "noise vessels or tools > 200 dB re 1muPa"
    my_inputs_processed.inputs_EIA.receptors_dict = my_inputs.inputs_ESA.farm_info.receptors
    my_inputs_processed.inputs_ES.species_user = my_inputs.inputs_ESA.farm_info.protected_species
    my_inputs_processed.inputs_ES.Coordinates_of_the_farm_x = np.median(my_inputs.inputs_ESA.farm_info.Coordinates_of_the_farm_x)
    my_inputs_processed.inputs_ES.Coordinates_of_the_farm_y = np.median(my_inputs.inputs_ESA.farm_info.Coordinates_of_the_farm_y)

    my_inputs_processed.inputs_SA.nb_vessel_crew = my_inputs.inputs_ESA.logistics_info.decommisioning.Number_of_passengers+my_inputs.inputs_ESA.logistics_info.installation.Number_of_passengers+my_inputs.inputs_ESA.logistics_info.exploitation.Number_of_passengers
    my_inputs_processed.inputs_SA.cost_of_consenting = my_inputs.inputs_ESA.farm_info.lcoe
    # CFP
    
    device_materials = my_inputs.inputs_ESA.device_info.materials.prop_rep()
    foundations_materials = my_inputs.inputs_ESA.device_info.foundation.materials.prop_rep()
    electrical_materials = my_inputs.inputs_ESA.electrical_info.materials.prop_rep()
    device_materials_to_recycle = my_inputs.inputs_ESA.device_info.materials_to_recycle.prop_rep()
    foundations_materials_to_recycle = my_inputs.inputs_ESA.device_info.foundation.materials_to_recycle.prop_rep()
    electrical_materials_to_recycle = my_inputs.inputs_ESA.electrical_info.materials_to_recycle.prop_rep()
    # materials = {**device_materials,**foundations_materials,**electrical_materials}
    # for liste in range(len(materials_sk)):
    #         mat = materials_sk[liste]["material_name"]
    #         qty = materials_sk[liste]["material_quantity"]

    #         if mat in my_inputs.inputs_ESA.device_info.materials.prop_rep():
    #             getattr(self.inputs_ESA.device_info.foundation.materials,mat).value += qty

    [device_materials.pop(key) for key in ["__type__", "name","description"]]
    [foundations_materials.pop(key) for key in ["__type__", "name","description"]]
    [electrical_materials.pop(key) for key in ["__type__", "name","description"]]
    [device_materials_to_recycle.pop(key) for key in ["__type__", "name","description"]]
    [foundations_materials_to_recycle.pop(key) for key in ["__type__", "name","description"]]
    [electrical_materials_to_recycle.pop(key) for key in ["__type__", "name","description"]]
    for key, value in device_materials.items():
        if key in my_inputs_processed.inputs_CFP.materials.prop_rep():
            getattr(my_inputs_processed.inputs_CFP.materials,key).value += value["value"]
        else:
            pass

    for key, value in foundations_materials.items():
        if key in my_inputs_processed.inputs_CFP.materials.prop_rep():
            getattr(my_inputs_processed.inputs_CFP.materials,key).value += value["value"]
        else:
            pass
    for key, value in electrical_materials.items():
        if key in my_inputs_processed.inputs_CFP.materials.prop_rep():
            getattr(my_inputs_processed.inputs_CFP.materials,key).value += value["value"]
        else:
            pass
    for key, value in device_materials_to_recycle.items():
        if key in my_inputs_processed.inputs_CFP.materials_to_recycle.prop_rep():
            getattr(my_inputs_processed.inputs_CFP.materials_to_recycle,key).value += value["value"]
        else:
            pass

    for key, value in foundations_materials_to_recycle.items():
        if key in my_inputs_processed.inputs_CFP.materials_to_recycle.prop_rep():
            getattr(my_inputs_processed.inputs_CFP.materials_to_recycle,key).value += value["value"]
        else:
            pass
    for key, value in electrical_materials_to_recycle.items():
        if key in my_inputs_processed.inputs_CFP.materials_to_recycle.prop_rep():
            getattr(my_inputs_processed.inputs_CFP.materials_to_recycle,key).value += value["value"]
        else:
            pass
    my_inputs_processed.inputs_CFP.vessel_consumption.Installation.value = my_inputs.inputs_ESA.logistics_info.installation.fuel_consumption
    my_inputs_processed.inputs_CFP.vessel_consumption.Exploitation.value = my_inputs.inputs_ESA.logistics_info.exploitation.fuel_consumption
    my_inputs_processed.inputs_CFP.vessel_consumption.Dismantling.value = my_inputs.inputs_ESA.logistics_info.decommisioning.fuel_consumption
    my_inputs_processed.inputs_CFP.energy_produced.value = my_inputs.inputs_ESA.electrical_info.energy_produced
    my_inputs_processed.inputs_CFP.project_lifetime.value = my_inputs.inputs_ESA.farm_info.project_lifetime
    my_inputs_processed.inputs_SA.nb_vessel_crew = my_inputs.inputs_ESA.logistics_info.installation.Number_of_passengers+my_inputs.inputs_ESA.logistics_info.exploitation.Number_of_passengers+my_inputs.inputs_ESA.logistics_info.decommisioning.Number_of_passengers
    print("Process inputs for Environmental Impact Assessment")
# Process my inputs for EIA 
    my_inputs_processed = my_inputs_processed.prop_rep()
    my_inputs_processed["inputs_EIA"]["input_hydro"]["Coordinates of the Devices"] = [my_inputs.inputs_ESA.device_info.coordinates_devices_x,my_inputs.inputs_ESA.device_info.coordinates_devices_y]
    my_inputs_processed["inputs_EIA"]["input_elec"]["Coordinates of the Devices"] = [my_inputs.inputs_ESA.electrical_info.collection_points_coordinates_x,my_inputs.inputs_ESA.electrical_info.collection_points_coordinates_y]
    my_inputs_processed["inputs_EIA"]["input_moor"]["Coordinates of the Devices"] = [my_inputs.inputs_ESA.device_info.coordinates_devices_x,my_inputs.inputs_ESA.device_info.coordinates_devices_y]
    [my_inputs_processed["inputs_EIA"]["input_hydro"].pop(key) for key in ["__type__", "name","description","Coordinates_of_the_Devices_x","Coordinates_of_the_Devices_y"]]
    [my_inputs_processed["inputs_EIA"]["input_elec"].pop(key) for key in ["__type__", "name","description","Coordinates_of_the_Devices_x","Coordinates_of_the_Devices_y"]]
    [my_inputs_processed["inputs_EIA"]["input_moor"].pop(key) for key in ["__type__", "name","description","Coordinates_of_the_Devices_x","Coordinates_of_the_Devices_y"]]
    [my_inputs_processed["inputs_EIA"]["input_insta"].pop(key) for key in ["__type__", "name","description"]]
    [my_inputs_processed["inputs_EIA"]["input_maint"].pop(key) for key in ["__type__", "name","description"]]
    [my_inputs_processed["inputs_EIA"]["receptors_dict"].pop(key) for key in ["__type__", "name","description"]]
    [my_inputs_processed["inputs_EIA"]["weighting_dict"].pop(key) for key in ["__type__", "name","description"]]
    [my_inputs_processed["inputs_CFP"]["materials"].pop(key) for key in ["__type__", "name","description"]]
    [my_inputs_processed["inputs_CFP"]["materials_to_recycle"].pop(key) for key in ["__type__", "name","description"]]

	# Receptors table

    receptors_dict = my_inputs_processed["inputs_EIA"]["receptors_dict"]
    
    receptors_dict = {k.replace("_"," "): v for k,v in receptors_dict.items()}
    receptors_dict = {k.replace("Large odontocete Mysticete","Large odontocete_Mysticete"): v for k,v in receptors_dict.items()}
    receptors_dict = {k.replace("Odontoncete dolphinds","Odontoncete_dolphinds"): v for k,v in receptors_dict.items()}
    receptors_table = pd.DataFrame.from_dict(receptors_dict, orient = 'index')
    receptors_table.columns = ["observed", 'observed january', 'observed february',
                        'observed march',
                        'observed april',
                        'observed may',
                        'observed june',
                        'observed july',
                        'observed august',
                        'observed september',
                        'observed october',
                        'observed november',
                        'observed december']
    receptors_table = receptors_table.replace(False,"NaN")
    # Protected table
    protected_dict = my_inputs_processed["inputs_EIA"]["protected_dict"]
    protected_dict = {k.replace("_"," "): v for k,v in protected_dict.items()}
    protected_table = pd.DataFrame(protected_dict)
    protected_table = protected_table.set_index("species name")

    # Weighting table
    weighting_dict = my_inputs_processed["inputs_EIA"]["weighting_dict"]
    for key in weighting_dict:
        weighting_dict[key] = {k.replace("_"," "): v for k,v in weighting_dict[key].items()}
    # my_inputs["inputs_EIA"]["input_hydro"] = {k.replace("Size_Devices","Size of the Devices"): v for k,v in inputs_hydro.items()}

    inputs_hydro = my_inputs_processed["inputs_EIA"]["input_hydro"]
    my_inputs_processed["inputs_EIA"]["input_hydro"] = {k.replace("_"," "): v for k,v in inputs_hydro.items()}
    print(my_inputs_processed["inputs_EIA"]["input_hydro"]["Initial Noise dB re 1muPa"])

    inputs_elec = my_inputs_processed["inputs_EIA"]["input_elec"]
    my_inputs_processed["inputs_EIA"]["input_elec"] = {k.replace("_"," "): v for k,v in inputs_elec.items()}

    inputs_moor = my_inputs_processed["inputs_EIA"]["input_moor"]
    my_inputs_processed["inputs_EIA"]["input_moor"] = {k.replace("_"," "): v for k,v in inputs_moor.items()}

    inputs_insta = my_inputs_processed["inputs_EIA"]["input_insta"]
    my_inputs_processed["inputs_EIA"]["input_insta"] = {k.replace("_"," "): v for k,v in inputs_insta.items()}
    inputs_maint = my_inputs_processed["inputs_EIA"]["input_maint"]
    my_inputs_processed["inputs_EIA"]["input_maint"] = {k.replace("_"," "): v for k,v in inputs_maint.items()}


    return my_inputs_processed, receptors_table, protected_table, weighting_dict

def run_main(my_inputs, inputs_init, receptors_table, protected_table, weighting_table, complexity_level):
    '''
    Main function, which launches all environmental assessments
    Returns environmental and social acceptance results of the project
    Args:
        - all processed inputs
        - receptors table
        - protected table
        - weighting table

    '''
    # Process inputs EIA


    myproject = Project(name = my_inputs["name"])
    myproject._init_from_files(inputs_CFP = my_inputs["inputs_CFP"],
                                inputs_EIA = my_inputs["inputs_EIA"], 
                                inputs_ES = my_inputs["inputs_ES"],
                                inputs_SA = my_inputs["inputs_SA"])
    print("Complexity level ", complexity_level)
    if complexity_level == 1:
        myproject.compute_es_assessment(database_species)

    # if myproject.endangered_species.chondrichtyes:
    #     receptors_table.iloc["observed","Magnetosensitive species"]=True
    #     receptors_table["Electrosensitive species"]["observed"]=True
    #     receptors_table["Elasmobranchs"]["observed"]=True
    if complexity_level == 2:
        myproject.compute_es_assessment(database_species)
        myproject.compute_eia_assessment(protected_table,receptors_table,  weighting_table)
        myproject.compute_cfp_assessment(dict_fuel, dict_materials, dict_gases_impact_factor)
        myproject.compute_sa_assessment()
        
    if complexity_level == 3:
        myproject.compute_es_assessment(database_species)
        myproject.compute_eia_assessment(protected_table,receptors_table,  weighting_table)
        myproject.compute_cfp_assessment(dict_fuel, dict_materials, dict_gases_impact_factor)
        myproject.compute_sa_assessment()

    myproject.compute_DR(receptors_table, inputs_init)
        # filename = os.path.join(PATH_STORAGE,my_inputs["name"] + '_outputs.json')
    # myproject.saveJSON(filename)
    print("**** Environmental and Social Acceptance assessment done ****")
    return myproject