# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""
Created on Thu Jun 20 15:20:42 2019

@author: ykervell

This file is the default input file for databases info (will come with DTOceanPlus software).
"""
import os
from pathlib import Path

working= os.getcwd()
data_path = Path('src/dtop_esa/ressources/')
PATH_DATABASES = os.path.join(working, data_path)
PATH_DATABASES_ALL = os.path.join(PATH_DATABASES, 'All')
PATH_DATABASES_WAVES = os.path.join(PATH_DATABASES, 'Waves')
PATH_DATABASES_WINDS = os.path.join(PATH_DATABASES, 'Winds')
PATH_DATABASES_CURRENTS = os.path.join(PATH_DATABASES, 'Currents')
PATH_DATABASES_BATHYMETRY = os.path.join(PATH_DATABASES, 'Bathymetry')
PATH_DATABASES_SEABED = os.path.join(PATH_DATABASES, 'Seabed')
PATH_DATABASES_SPECIES = os.path.join(PATH_DATABASES, 'Species')
      
SingleValueDatabases = {
            'GEBCO':{
            'driver': {'path': 'Y:\\01 P1\\DATA\\GEBCO\\GEBCO_2019', 'file_ref': 'GEBCO_2019.nc', 'type': 'nc'},
            'extent': {'lon_min': -180., 'lat_min': -90., 'lon_max': 180., 'lat_max': 90},
            'variables': {'name': ('elevation',), 'units': ('m from MSL',)},
                    },
            'GEBCO_Europa_downgrade20':{
            'driver': {'path': PATH_DATABASES_BATHYMETRY, 'file_ref': PATH_DATABASES_BATHYMETRY+'/Europa-TEST2_GEBCO2019_downgrade20.nc', 'type': 'nc'},
            'extent': {'lon_min': -20., 'lat_min': 35., 'lon_max': 20., 'lat_max': 70},
            'variables': {'name': ('elevation',), 'units': ('m from MSL',)},
                    },
            'Seabed_Types':{
            'driver': {'path': PATH_DATABASES_SEABED, 'file_ref': PATH_DATABASES_SEABED+'/Europa-TEST2_GEBCO2019_downgrade20_SEABED-TYPE.nc', 'type': 'nc'},
            'extent': {'lon_min': -20., 'lat_min': 35., 'lon_max': 20., 'lat_max': 70},
            'variables': {'name': ('sediment_type',), 'units': (None,)},
            'conversion': {'0': 'No data', '1': 'Rock', '2': 'Peeble', '3': 'Gravel', '4': 'Sand', '5': 'Fine sand', '6': 'Mud'}
                    },
            'Seabed_Roughness_Length':{
            'driver': {'path': PATH_DATABASES_SEABED, 'file_ref': PATH_DATABASES_SEABED+'/Europa-TEST2_GEBCO2019_downgrade20_SEABED-ROUGHNESS-LENGTH.nc', 'type': 'nc'},
            'extent': {'lon_min': -20., 'lat_min': 35., 'lon_max': 20., 'lat_max': 70},
            'variables': {'name': ('roughness_length',), 'units': ('m',)}
                    },
            'Species_default':{
            'driver': {'path': PATH_DATABASES_SPECIES, 'file_ref': PATH_DATABASES_SPECIES+'/species.nc', 'type': 'nc'},
            'extent': {'lon_min': -180., 'lat_min': -90., 'lon_max': 90., 'lat_max': 180},
            'variables': {'name': ('probability',), 'units': (None)},
            'species': {'names': ('Acipenser_sturio', 'Anguilla_anguilla', 'Balaenoptera_borealis', 'Balaenoptera_musculus', 'Carcharodon_carcharias', 'Caretta_caretta', 'Cetorhinus_maximus', 'Chelonia_mydas', 'Dermochelys_coriacea', 'Dipturus_batis', 'Eretmochelys_imbricata', 'Eubalaena_glacialis', 'Lamna_nasus', 'Lepidochelys_kempii', 'Monachus_monachus', 'Phocoena_phocoena', 'Physeter_macrocephalus', 'Rostroraja_alba', 'Squatina_squatina', 'Thynnus_thunnus', 'Tursiops_truncatus')}
                    }
        }
