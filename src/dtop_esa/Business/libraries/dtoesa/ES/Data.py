# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Created on Tue Jun 18 15:51:55 2019

@author: ykervell

Overview
========
This module defines the Data class that is used to extract Physical Databases.

Usage
=====
The main use of this class is to extract data at a location.

    - Extract at a given location:
        import Data.MyData as MD
        outs = MD.extract_time_series(*args, **kwargs) (refer to extract_time_series)

    - Extract the bathymetry:
        outs = MD.extract_bathymetry(*args, **kwargs) # refer to extract_bathymetry

Notes
=====
The extraction can take a long time (particularly on a large number of non-pivoted files).
"""
import os
import netCDF4

class MyData:
    """ Creation of data from netCDF files to statistics"""

    def __init__(self, data_name, **kw):
        '''Initializes the MyData instance.

        Arguments
        ---------
            - data_name: name of the data (in the file data_catalog or user
            input)

        Keyword arguments
        -----------------
            - data_catalog: a user-defined input file from which the object
            MyData can be initialized.

        '''

        data_catalog = kw.get("user_data_catalog", None)
        self._loaded = False
        self.name = data_name
        self._catalog = data_catalog

        data_info = self._find_data_in_catalog(data_catalog)

        self.driver_type = data_info['driver']['type']
        self.driver_path = data_info['driver']['path']
        self.driver_file_ref = data_info['driver']['file_ref']
        if 'species' in data_info.keys():
            self.species_names = data_info['species']['names']
        self.driver_file = os.path.join(self.driver_path, self.driver_file_ref)
        try:
            self.driver_template = data_info['driver']['template']
            self.driver_year_start = data_info['driver']['year_start']
            self.driver_year_end = data_info['driver']['year_end']
        except:
            print("Endangered Species --- single value database ...")


        #       Default Dataset method is the netCDF4 one.
        if self.driver_type == "shp":
            from useful_functions import shpDataset
            self.Dataset = shpDataset
        else:
            self.Dataset = netCDF4.Dataset
        
        self.data_lon_min = data_info['extent']['lon_min']
        self.data_lat_min = data_info['extent']['lat_min']
        self.data_lon_max = data_info['extent']['lon_max']
        self.data_lat_max = data_info['extent']['lat_max']
        
        self.variables = data_info['variables']['name']
        self.variables_units = data_info['variables']['units']
        # special case with a conversion dictionary (for seabed type for example)
        try:
            self.conversion = data_info['conversion']
        except:
            pass
        
          
    def _find_data_in_catalog(self, data_catalog=None):
        '''Tries to find the database in either:
            - the file given as a parameter by the user (data_catalog)
            - the default catalog file (/BusinessLogic/catalogs/data_catalog.py)
        '''
        # Look for the database in the user input file
        if isinstance(data_catalog, str):
            if not os.path.isfile(data_catalog):
                print('\033[1;41mI cannot find the catalog %s\033[1;0m' %data_catalog)
                raise Exception('NotExistingCatalog')

        if data_catalog is None:
            # No user input file was given, look for our database in the default catalog
            data_info = None
            import dtop_esa.Business.libraries.dtoesa.ES.data_catalog as daca
            try:
                data_info = daca.TimeSeriesDatabases[self.name]
            except:
                try:
                    data_info = daca.SingleValueDatabases[self.name]
                except:
                    print('\033[1;41m%s is not in catalogs/data_catalog.py\033[1;0m' %self.name)
                    raise Exception('NotInCatalog')

        else:   # user catalog
            print("TODO: read user catalog")

        self._catalog = data_catalog
        return data_info

    def extract_single_value(self, point_name, lon, lat):#**indices):
        '''Extract a single value (no temporal dimension) from self.driver_file'''
        from dtop_esa.Business.libraries.dtoesa.ES.Location import MyPoint
        point = MyPoint(point_name, lon, lat)
        return point.get_value(data_file=self.driver_file, data_var=self.variables[0])
