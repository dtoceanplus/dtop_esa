# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import Risks
#------------------------------------
# @ USER DEFINED IMPORTS START
import re
# @ USER DEFINED IMPORTS END
#------------------------------------

class Species():

    """Results of ES for a species
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._Class=''
        self._Order=''
        self._Family=''
        self._Commun_name=''
        self._IUCN_status=''
        self._Probability_of_presence=0.0
        self._Risks_mitigation=[]
        self._Surveys=''
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
    '''
    Species class includes information on endangered species found in the area:
        - taxonomic information (Class, Order, Family, Commun name)
        - IUCN status of the species
        - Probability of presence in the area
        - List of element of class risks associated with the species
        - Recommandation on surveys to carry in the area to monitor the species
    '''
    def init_from_file(self, database, Probability_of_presence= None):
        '''
        check for the endangered species in the database and return all available informations

        Args:
            - database (including 26 endangered species, see technical note for more information)

        '''
        self.name = re.sub('_',' ',self.name)
        if self.name in database :
            self.Class = database[self.name]['Class']
            self.Order = database[self.name]['Order']
            self.Family = database[self.name]['Family']
            self.Commun_name = database[self.name]['Common name']
            self.IUCN_status = database[self.name]['IUCN status']
            for risk in range(len(database[self.name]['Risks'])):
                elt = Risks.Risks()
                elt.Risk = database[self.name]['Risks'][risk]
                elt.Mitigation = database[self.name]['Mitigation'][risk]
                # elt.Survey = database[self.name]['Survey'][risk]
                # ind = [database[self.name]['Risks'].index(risk)]
                self.Risks_mitigation.append(elt)
                # self.Risks_Mitigation[ind].Mitigation = database[self.name]['Mitigation'][ind]
            self.Surveys = database[self.name]['Surveys']

            if Probability_of_presence:
                self.Probability_of_presence = "%.3f" % Probability_of_presence
            else:
                self.Probability_of_presence = 999
            self.description = "Information for species: " + self.name
        else:
            if self.Class == "Mammals":
                name = "Balaenoptera musculus"
            elif self.Class == "Chondrichtyes":
                name = "Carcharodon carcharias"
            elif self.Class == "Actinopterygii":
                name = "Acipenser sturio"
            elif self.Class == "Aves":
                name = "Polysticta stelleri"
            elif self.Class == "Reptilia":
                name = "Squatina squatina"

            for risk in range(len(database[name]['Risks'])):
                elt = Risks.Risks()
                elt.Risk = database[name]['Risks'][risk]
                elt.Mitigation = database[name]['Mitigation'][risk]
                    # elt.Survey = database[self.name]['Survey'][risk]
                    # ind = [database[self.name]['Risks'].index(risk)]
                self.Risks_mitigation.append(elt)
                    # self.Risks_Mitigation[ind].Mitigation = database[self.name]['Mitigation'][ind]
            self.Surveys = database[name]['Surveys']
            if Probability_of_presence:
                self.Probability_of_presence = "%.3f" % Probability_of_presence
            else:
                self.Probability_of_presence = 999
            self.description = "Information for species: " + self.name


# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def Class(self): # pragma: no cover
        """str: Taxonomic Class of the species
        """
        return self._Class
    #------------
    @ property
    def Order(self): # pragma: no cover
        """str: Taxonomic Order of the species
        """
        return self._Order
    #------------
    @ property
    def Family(self): # pragma: no cover
        """str: Taxonomic Family of the species
        """
        return self._Family
    #------------
    @ property
    def Commun_name(self): # pragma: no cover
        """str: English commun name of the species
        """
        return self._Commun_name
    #------------
    @ property
    def IUCN_status(self): # pragma: no cover
        """str: International Union of Conservation of Nature status of the species
        """
        return self._IUCN_status
    #------------
    @ property
    def Probability_of_presence(self): # pragma: no cover
        """float: Probability of presence of the species at the closest coordinates []
        """
        return self._Probability_of_presence
    #------------
    @ property
    def Risks_mitigation(self): # pragma: no cover
        """:obj:`list` of :obj:`~.Risks.Risks`: none
        """
        return self._Risks_mitigation
    #------------
    @ property
    def Surveys(self): # pragma: no cover
        """str: Recommendations on surveys for the species
        """
        return self._Surveys
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ Class.setter
    def Class(self,val): # pragma: no cover
        self._Class=str(val)
    #------------
    @ Order.setter
    def Order(self,val): # pragma: no cover
        self._Order=str(val)
    #------------
    @ Family.setter
    def Family(self,val): # pragma: no cover
        self._Family=str(val)
    #------------
    @ Commun_name.setter
    def Commun_name(self,val): # pragma: no cover
        self._Commun_name=str(val)
    #------------
    @ IUCN_status.setter
    def IUCN_status(self,val): # pragma: no cover
        self._IUCN_status=str(val)
    #------------
    @ Probability_of_presence.setter
    def Probability_of_presence(self,val): # pragma: no cover
        self._Probability_of_presence=float(val)
    #------------
    @ Risks_mitigation.setter
    def Risks_mitigation(self,val): # pragma: no cover
        self._Risks_mitigation=val
    #------------
    @ Surveys.setter
    def Surveys(self,val): # pragma: no cover
        self._Surveys=str(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:ES:Species"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:ES:Species"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("Class"):
            rep["Class"] = self.Class
        if self.is_set("Order"):
            rep["Order"] = self.Order
        if self.is_set("Family"):
            rep["Family"] = self.Family
        if self.is_set("Commun_name"):
            rep["Commun_name"] = self.Commun_name
        if self.is_set("IUCN_status"):
            rep["IUCN_status"] = self.IUCN_status
        if self.is_set("Probability_of_presence"):
            rep["Probability_of_presence"] = self.Probability_of_presence
        if self.is_set("Risks_mitigation"):
            rep["Risks_mitigation"] = []
            for i in range(0,len(self.Risks_mitigation)):
                if (short and not(deep)):
                    itemType = self.Risks_mitigation[i].type_rep()
                    rep["Risks_mitigation"].append( itemType )
                else:
                    rep["Risks_mitigation"].append( self.Risks_mitigation[i].prop_rep(short, deep) )
        else:
            rep["Risks_mitigation"] = []
        if self.is_set("Surveys"):
            rep["Surveys"] = self.Surveys
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "Class"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Order"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Family"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Commun_name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "IUCN_status"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Probability_of_presence"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Risks_mitigation"
        try :
            if data[varName] != None:
                self.Risks_mitigation=[]
                for i in range(0,len(data[varName])):
                    self.Risks_mitigation.append(Risks.Risks())
                    self.Risks_mitigation[i].loadFromJSONDict(data[varName][i])
            else:
                self.Risks_mitigation = []
        except :
            pass
        varName = "Surveys"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
