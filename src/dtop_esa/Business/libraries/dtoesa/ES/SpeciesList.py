# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import Species
#------------------------------------
# @ USER DEFINED IMPORTS START
from . import Data
# @ USER DEFINED IMPORTS END
#------------------------------------

class SpeciesList():

    """List of all endangered species potentialy present in the area
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._chondrichtyes=[]
        self._actinopterygii=[]
        self._mammals=[]
        self._aves=[]
        self._reptilia=[]
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
    '''
    SpeciesList class include all results of endangered species of five taxonomic classes (Chondrichtyes, Actinopterygii, Mammals, Aves and Reptilia)
    '''
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def init_from_files(self,database_species, inputs_ES):
        '''
        Launch data extraction from coordinates of the farm and return latin names and probability of presence of endangered species

        Args:
            - latitude of the center farm
            - longitude of the center farm

        '''

        self.inputs = inputs_ES
        self.inputs["extracted"]={}
        self.database = database_species
        if self.inputs['Coordinates_of_the_farm_y']:
            print("Endangered Species --- extraction of information from netcdf database")
            self.dat_species = Data.MyData(data_name='Species_default')
            extracted_values = []
            self.points={}
            self.points['names'] = ('center_farm',)
            self.points["lons"] = (self.inputs['Coordinates_of_the_farm_y'],)
            self.points["lats"] = (self.inputs['Coordinates_of_the_farm_x'],)

            lons = self.points["lons"]
            lats = self.points["lats"]

            for ind, name in enumerate(self.points['names']):
                species_proba = np.ma.filled(self.dat_species.extract_single_value(name, lons[ind], lats[ind]), fill_value = 9999.)

            for spe in range(len(species_proba)):
                if species_proba[spe]>0 and species_proba[spe]<=1 and species_proba[spe] != 9999.0:
                    espece = self.dat_species.species_names[spe]
                    self.inputs["extracted"][espece]= species_proba[spe]
                else:
                    pass
        # Direct Values (center of farm for devices at early stage)

        return self

    def compute_all_species(self):
        '''
        Retrives all taxonomic infomartion and risks and mitigation measures for identified endangered species
        '''
        print("Endangered Species --- ")
        sc_spe = []

        if self.inputs['extracted']:
            print("Endangered Species --- compute information -- from extraction")
            for key, value in self.inputs['extracted'].items():
                sp = Species.Species(key)
                sp.init_from_file(self.database,value)
                if sp.Class == 'Chondrichtyes':
                    self.chondrichtyes.append(sp)
                elif sp.Class == 'Actinopterygii':
                    self.actinopterygii.append(sp)
                elif sp.Class == 'Mammals':
                    self.mammals.append(sp)
                elif sp.Class == 'Aves':
                    self.aves.append(sp)
                elif sp.Class == 'Reptilia':
                    self.reptilia.append(sp)

        if self.inputs['inputs_SC']:
            print("Endangered Species --- compute information -- from module site characterisation")
            # for key, value in self.inputs['inputs_SC'].items():
            #     sp = Species.Species(key)
            #     sp.init_from_file(self.database, value)

            for spe in range(len(self.inputs['inputs_SC'])):

                sc_spe.append(self.inputs['inputs_SC'][spe]["name"])
                sp = Species.Species(self.inputs['inputs_SC'][spe]["name"])
                sp.init_from_file(self.database, self.inputs['inputs_SC'][spe]["probability_of_presence"])
                if sp.Class == 'Chondrichtyes':
                    self.chondrichtyes.append(sp)
                elif sp.Class == 'Actinopterygii':
                    self.actinopterygii.append(sp)
                elif sp.Class == 'Mammals':
                    self.mammals.append(sp)
                elif sp.Class == 'Aves':
                    self.aves.append(sp)
                elif sp.Class == 'Reptilia':
                    self.reptilia.append(sp)

        if self.inputs['species_user']:
            print("Endangered Species --- compute information -- from user")
            # for key, value in self.inputs['species_user'].items():
            for spe in range(len(self.inputs['species_user'])):
                if self.inputs['species_user'][spe]["name"] not in sc_spe:
                    name_extra = {k.replace("_"," "): v for k,v in self.inputs['extracted'].items()}
                    if self.inputs['species_user'][spe]["name"] not in name_extra.keys():
                        if self.inputs['species_user'][spe]["name"] in self.database.keys():
                            sp = Species.Species(self.inputs['species_user'][spe]["name"])
                            sp.init_from_file(self.database)
                        else:
                            sp = Species.Species(self.inputs['species_user'][spe]["name"])
                            sp.Class = self.inputs['species_user'][spe]["Class"]
                            sp.init_from_file(self.database)
                        if sp.Class == 'Chondrichtyes':
                            self.chondrichtyes.append(sp)
                        elif sp.Class == 'Actinopterygii':
                            self.actinopterygii.append(sp)
                        elif sp.Class == 'Mammals':
                            self.mammals.append(sp)
                        elif sp.Class == 'Aves':
                            self.aves.append(sp)
                        elif sp.Class == 'Reptilia':
                            self.reptilia.append(sp)

        return self

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def chondrichtyes(self): # pragma: no cover
        """:obj:`list` of :obj:`~.Species.Species`: chondrichtyes
        """
        return self._chondrichtyes
    #------------
    @ property
    def actinopterygii(self): # pragma: no cover
        """:obj:`list` of :obj:`~.Species.Species`: actinopterygii
        """
        return self._actinopterygii
    #------------
    @ property
    def mammals(self): # pragma: no cover
        """:obj:`list` of :obj:`~.Species.Species`: mammals
        """
        return self._mammals
    #------------
    @ property
    def aves(self): # pragma: no cover
        """:obj:`list` of :obj:`~.Species.Species`: aves
        """
        return self._aves
    #------------
    @ property
    def reptilia(self): # pragma: no cover
        """:obj:`list` of :obj:`~.Species.Species`: reptilia
        """
        return self._reptilia
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ chondrichtyes.setter
    def chondrichtyes(self,val): # pragma: no cover
        self._chondrichtyes=val
    #------------
    @ actinopterygii.setter
    def actinopterygii(self,val): # pragma: no cover
        self._actinopterygii=val
    #------------
    @ mammals.setter
    def mammals(self,val): # pragma: no cover
        self._mammals=val
    #------------
    @ aves.setter
    def aves(self,val): # pragma: no cover
        self._aves=val
    #------------
    @ reptilia.setter
    def reptilia(self,val): # pragma: no cover
        self._reptilia=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:ES:SpeciesList"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:ES:SpeciesList"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("chondrichtyes"):
            rep["chondrichtyes"] = []
            for i in range(0,len(self.chondrichtyes)):
                if (short and not(deep)):
                    itemType = self.chondrichtyes[i].type_rep()
                    rep["chondrichtyes"].append( itemType )
                else:
                    rep["chondrichtyes"].append( self.chondrichtyes[i].prop_rep(short, deep) )
        else:
            rep["chondrichtyes"] = []
        if self.is_set("actinopterygii"):
            rep["actinopterygii"] = []
            for i in range(0,len(self.actinopterygii)):
                if (short and not(deep)):
                    itemType = self.actinopterygii[i].type_rep()
                    rep["actinopterygii"].append( itemType )
                else:
                    rep["actinopterygii"].append( self.actinopterygii[i].prop_rep(short, deep) )
        else:
            rep["actinopterygii"] = []
        if self.is_set("mammals"):
            rep["mammals"] = []
            for i in range(0,len(self.mammals)):
                if (short and not(deep)):
                    itemType = self.mammals[i].type_rep()
                    rep["mammals"].append( itemType )
                else:
                    rep["mammals"].append( self.mammals[i].prop_rep(short, deep) )
        else:
            rep["mammals"] = []
        if self.is_set("aves"):
            rep["aves"] = []
            for i in range(0,len(self.aves)):
                if (short and not(deep)):
                    itemType = self.aves[i].type_rep()
                    rep["aves"].append( itemType )
                else:
                    rep["aves"].append( self.aves[i].prop_rep(short, deep) )
        else:
            rep["aves"] = []
        if self.is_set("reptilia"):
            rep["reptilia"] = []
            for i in range(0,len(self.reptilia)):
                if (short and not(deep)):
                    itemType = self.reptilia[i].type_rep()
                    rep["reptilia"].append( itemType )
                else:
                    rep["reptilia"].append( self.reptilia[i].prop_rep(short, deep) )
        else:
            rep["reptilia"] = []
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "chondrichtyes"
        try :
            if data[varName] != None:
                self.chondrichtyes=[]
                for i in range(0,len(data[varName])):
                    self.chondrichtyes.append(Species.Species())
                    self.chondrichtyes[i].loadFromJSONDict(data[varName][i])
            else:
                self.chondrichtyes = []
        except :
            pass
        varName = "actinopterygii"
        try :
            if data[varName] != None:
                self.actinopterygii=[]
                for i in range(0,len(data[varName])):
                    self.actinopterygii.append(Species.Species())
                    self.actinopterygii[i].loadFromJSONDict(data[varName][i])
            else:
                self.actinopterygii = []
        except :
            pass
        varName = "mammals"
        try :
            if data[varName] != None:
                self.mammals=[]
                for i in range(0,len(data[varName])):
                    self.mammals.append(Species.Species())
                    self.mammals[i].loadFromJSONDict(data[varName][i])
            else:
                self.mammals = []
        except :
            pass
        varName = "aves"
        try :
            if data[varName] != None:
                self.aves=[]
                for i in range(0,len(data[varName])):
                    self.aves.append(Species.Species())
                    self.aves[i].loadFromJSONDict(data[varName][i])
            else:
                self.aves = []
        except :
            pass
        varName = "reptilia"
        try :
            if data[varName] != None:
                self.reptilia=[]
                for i in range(0,len(data[varName])):
                    self.reptilia.append(Species.Species())
                    self.reptilia[i].loadFromJSONDict(data[varName][i])
            else:
                self.reptilia = []
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
