# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Created on Tue Jun 18 15:51:55 2019

@author: ykervell

Overview
========
This module provides objects that handle the locations of the Site.

"""
WHITE = "\033[0m"

class MyPoint:
    """ Ponctual location"""

    def __init__(self, point_name, lon, lat):
        self._myvalue = None
        self.name = str(point_name)
        self.lon = float(lon)
        self.lat = float(lat)

    def get_value(self, **kwargs):
        '''Extracts a value from a netcdf file.
        Bathymetry must be given on a structured grid, within a netCDF format.
        Depth is positive in water.

        kwargs:
            - data_file (default: Y:\\01 P1\\DATA\\GEBCO\\GEBCO_2019\\GEBCO_2019.nc)
            - data_var (default: depth)
            - lon_var (default: longitude)
            - lat_var (default: latitude)
        '''
        import netCDF4
        import numpy as np

        data_file = kwargs.get('data_file', None)

        possible_lon_names = ['longitude', 'lon', 'x', 'X']
        possible_lat_names = ['latitude', 'lat', 'y', 'Y']
        possible_bathy_names = ['H0', 'Band1', 'elevation', 'Bathymetry', 'DEPTH', 'depth']
        possible_seabed_names = ['roughness_length', 'seabed_type', 'sediment_type']
        possible_species_names = ['probability',]

        with netCDF4.Dataset(data_file) as nc:
            # fetch the data var
            data_var = kwargs.get('data_var', 'depth')
            if data_var not in (possible_bathy_names + possible_seabed_names + possible_species_names):
                print("%s is not a standard name for a variable" %data_var)
                pass
            bvar = nc.variables[data_var]

            # Coordinates of the nodes and elements (for structured grid)
            lon_var = kwargs.get('lon_var', 'longitude')
#            lon_var = kwargs.get('lon_var', 'lon')
            if lon_var not in possible_lon_names:
                print("%s is not a standard name for longitude" %lon_var)
                pass
            lat_var = kwargs.get('lat_var', 'latitude')
#            lat_var = kwargs.get('lat_var', 'lat')
            if lat_var not in possible_lat_names:
                print("%s is not a standard name for latitude" %lat_var)
                pass

            # fetch lon / lat values
            lon_nodes = nc.variables[lon_var][:]
            lat_nodes = nc.variables[lat_var][:]

            ilon = np.argmin(np.abs(lon_nodes - self.lon))
            ilat = np.argmin(np.abs(lat_nodes - self.lat))
            if data_var == 'probability':   # Marine Species case
                self._myvalue = bvar[:, ilat, ilon]
            else:
                self._myvalue = bvar[ilat, ilon]

        # Compute the slope if needed
#        return np.round(self._myvalue, 5)
        return self._myvalue
