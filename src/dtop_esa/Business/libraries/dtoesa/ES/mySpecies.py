# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

class MySpecies:

    def __init__(self, my_databases, **kwargs):
            '''
            Arguments
            ---------
                :my_databases:
                    user info, provided by gui, databases respectively for waves,
                    currents, wind, levels, bathy, seabed type and roughness.
                :storage:
                    path to a specific storage

            Keyword arguments
            -----------------
                :todo:
                    todo
            '''

            self.species_database = my_databases['SPECIES']

            # instance objects MyData
            self.dat_species = Data.MyData(self.species_database)

    def __extract_single_values_at_points__(self, points, data_type):
            '''Extract data from databases at farm, corridor or devices points.
            points: dictionary of geographical coordinates
            data_type: bathy, seabed or roughness
            '''
            
            import numpy as np
            extracted_values = []
            lons = points['lons']
            lats = points['lats']

            for ind, name in enumerate(points['names']):

                if data_type == "species":
                    species_proba = np.ma.filled(self.dat_species.extract_single_value(name, lons[ind], lats[ind]), fill_value = 9999.)
                    extracted_values.append(species_proba)
                else:
                    print("not implemented yet")

            return extracted_values
