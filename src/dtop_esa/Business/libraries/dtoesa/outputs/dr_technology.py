# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import dr_array
from . import dr_device
from . import dr_power
from . import dr_sk
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class dr_technology():

    """Technology family digital representation
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._array=dr_array.dr_array()
        self._device=dr_device.dr_device()
        self._power_transmission=dr_power.dr_power()
        self._station_keeping=dr_sk.dr_sk()
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def compute_dr_array(self, EIA_results):
        try:
            self.array.assessment.overall_negative_environmental_impact.value = EIA_results.eia_global.negative_impact
            self.array.assessment.overall_positive_environmental_impact.value = EIA_results.eia_global.positive_impact
        except:
            pass
    def compute_dr_device(self, EIA_results):

        self.device.assessment.overall_negative_environmental_impact.value = EIA_results.eia_tech_group.impact_hydrodynamics.global_eis.negative_impact
        self.device.assessment.overall_positive_environmental_impact.value = EIA_results.eia_tech_group.impact_hydrodynamics.global_eis.positive_impact
        for pressure in EIA_results.eia_tech_group.impact_hydrodynamics.eis_dict:
            if pressure.function_name == "Collision Risk":
                self.device.assessment.collision_risk.value = pressure.score
            if pressure.function_name == "Energy Modification":
                self.device.assessment.energy_modification.value = pressure.score
            if pressure.function_name == "Reef Effect":
                self.device.assessment.reef_effect.value = pressure.score
            if pressure.function_name == "Reserve Effect":
                self.device.assessment.reserve_effect.value = pressure.score
            if pressure.function_name == "Resting Effect":
                self.device.assessment.resting_effect.value = pressure.score
            if pressure.function_name == "Turbidity":
                self.device.assessment.turbidity.value = pressure.score
            if pressure.function_name == "Underwater Noise":
                self.device.assessment.underwater_noise.value = pressure.score


    def compute_dr_PT(self,EIA_results ):

        self.power_transmission.assessment.overall_negative_environmental_impact.value = EIA_results.eia_tech_group._impact_electrical.global_eis.negative_impact
        self.power_transmission.assessment.overall_positive_environmental_impact.value = EIA_results.eia_tech_group._impact_electrical.global_eis.positive_impact

        for pressure in EIA_results.eia_tech_group.impact_electrical.eis_dict:

            if pressure.function_name == "Collision Risk":
                self.power_transmission.assessment.collision_risk.value = pressure.score

            if pressure.function_name == "Energy Modification":
                self.power_transmission.assessment.energy_modification.value = pressure.score

            if pressure.function_name == "Electric Fields":
                self.power_transmission.assessment.electrical_fieds.value = pressure.score

            if pressure.function_name == "Magnetic Fields":
                self.power_transmission.assessment.magnetic_fieds.value = pressure.score

            if pressure.function_name == "Reef Effect":
                self.power_transmission.assessment.reef_effect.value = pressure.score

            if pressure.function_name == "Reserve Effect":
                self.power_transmission.assessment.reserve_effect.value = pressure.score

            if pressure.function_name == "Resting Effect":
                self.power_transmission.assessment.resting_effect.value = pressure.score

            if pressure.function_name == "Temperature":
                self.power_transmission.assessment.temperature_modification.value = pressure.score

            if pressure.function_name == "Footprint":
                self.power_transmission.assessment.footprint.value = pressure.score

            if pressure.function_name == "Underwater Noise":
                self.power_transmission.assessment.underwater_noise.value = pressure.score

    def compute_dr_SK(self, EIA_results):
        self.station_keeping.assessment.overall_negative_environmental_impact.value = EIA_results.eia_tech_group.impact_station_keeping.global_eis.negative_impact
        self.station_keeping.assessment.overall_positive_environmental_impact.value = EIA_results.eia_tech_group.impact_station_keeping.global_eis.positive_impact

        for pressure in EIA_results.eia_tech_group.impact_station_keeping.eis_dict:
            if pressure.function_name == "Collision Risk":
                self.station_keeping.assessment.collision_risk.value = pressure.score
            if pressure.function_name == "Footprint":
                self.station_keeping.assessment.footprint.value = pressure.score
            if pressure.function_name == "Reef Effect":
                self.station_keeping.assessment.reef_effect.value = pressure.score
            if pressure.function_name == "Underwater Noise":
                self.station_keeping.assessment.underwater_noise.value = pressure.score

    # def compute_dr_power(self):

    # def compute_dr_SK(self):
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def array(self): # pragma: no cover
        """:obj:`~.dr_array.dr_array`: none
        """
        return self._array
    #------------
    @ property
    def device(self): # pragma: no cover
        """:obj:`~.dr_device.dr_device`: none
        """
        return self._device
    #------------
    @ property
    def power_transmission(self): # pragma: no cover
        """:obj:`~.dr_power.dr_power`: none
        """
        return self._power_transmission
    #------------
    @ property
    def station_keeping(self): # pragma: no cover
        """:obj:`~.dr_sk.dr_sk`: none
        """
        return self._station_keeping
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ array.setter
    def array(self,val): # pragma: no cover
        self._array=val
    #------------
    @ device.setter
    def device(self,val): # pragma: no cover
        self._device=val
    #------------
    @ power_transmission.setter
    def power_transmission(self,val): # pragma: no cover
        self._power_transmission=val
    #------------
    @ station_keeping.setter
    def station_keeping(self,val): # pragma: no cover
        self._station_keeping=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:outputs:dr_technology"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:outputs:dr_technology"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("array"):
            if (short and not(deep)):
                rep["array"] = self.array.type_rep()
            else:
                rep["array"] = self.array.prop_rep(short, deep)
        if self.is_set("device"):
            if (short and not(deep)):
                rep["device"] = self.device.type_rep()
            else:
                rep["device"] = self.device.prop_rep(short, deep)
        if self.is_set("power_transmission"):
            if (short and not(deep)):
                rep["power_transmission"] = self.power_transmission.type_rep()
            else:
                rep["power_transmission"] = self.power_transmission.prop_rep(short, deep)
        if self.is_set("station_keeping"):
            if (short and not(deep)):
                rep["station_keeping"] = self.station_keeping.type_rep()
            else:
                rep["station_keeping"] = self.station_keeping.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "array"
        try :
            if data[varName] != None:
                self.array=dr_array.dr_array()
                self.array.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "device"
        try :
            if data[varName] != None:
                self.device=dr_device.dr_device()
                self.device.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "power_transmission"
        try :
            if data[varName] != None:
                self.power_transmission=dr_power.dr_power()
                self.power_transmission.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "station_keeping"
        try :
            if data[varName] != None:
                self.station_keeping=dr_sk.dr_sk()
                self.station_keeping.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
