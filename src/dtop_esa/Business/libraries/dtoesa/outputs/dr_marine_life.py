# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import dr_env_parameters
from . import dr_ES
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class dr_marine_life():

    """Marine life digital representation
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._receptors=[]
        self._initial_environmental_conditions=dr_env_parameters.dr_env_parameters()
        self._endangered_species=[]
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def compute_endangered_species(self,list_ES):
        for Class, list_species in list_ES.__dict__.items():
            try:
                for species in list_species:
                    spe = dr_ES.dr_ES()
                    spe.Class = species.Class
                    spe.Order = species.Order
                    spe.Family = species.Family
                    spe.Commun_name = species.Commun_name
                    spe.IUCN_status = species.IUCN_status
                    spe.Probability_of_presence = species.Probability_of_presence
                    self.endangered_species.append(spe)
            except:
                pass

        return self

    def compute_environmental_conditions(self,inputs_EIA):
        self.initial_environmental_conditions.initial_turbidity.value = inputs_EIA["input_hydro"]["Initial Turbidity"]
        self.initial_environmental_conditions.initial_underwater_noise.value = inputs_EIA["input_hydro"]["Initial Noise dB re 1muPa"]
        self.initial_environmental_conditions.initial_electrical_fields.value = inputs_EIA["input_elec"]["Initial Electric Field"]
        self.initial_environmental_conditions.initial_magnetic_fields.value = inputs_EIA["input_elec"]["Initial Magnetic Field"]
        self.initial_environmental_conditions.initial_temperature.value = inputs_EIA["input_elec"]["Initial Temperature"]

        return self

    def compute_receptors(self, receptors):
        print(receptors)
        # if receptors[index].valueself.initial_environmental_conditions.receptors.append(receptors)
        for receptor in receptors.index:
            if receptors.loc[receptor, "observed"]:
                self.receptors.append(receptor)
        return self

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def receptors(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:Receptors digital representation
        """
        return self._receptors
    #------------
    @ property
    def initial_environmental_conditions(self): # pragma: no cover
        """:obj:`~.dr_env_parameters.dr_env_parameters`: none
        """
        return self._initial_environmental_conditions
    #------------
    @ property
    def endangered_species(self): # pragma: no cover
        """:obj:`list` of :obj:`~.dr_ES.dr_ES`: none
        """
        return self._endangered_species
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ receptors.setter
    def receptors(self,val): # pragma: no cover
        self._receptors=val
    #------------
    @ initial_environmental_conditions.setter
    def initial_environmental_conditions(self,val): # pragma: no cover
        self._initial_environmental_conditions=val
    #------------
    @ endangered_species.setter
    def endangered_species(self,val): # pragma: no cover
        self._endangered_species=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:outputs:dr_marine_life"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:outputs:dr_marine_life"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("receptors"):
            if short:
                rep["receptors"] = str(len(self.receptors))
            else:
                rep["receptors"] = self.receptors
        else:
            rep["receptors"] = []
        if self.is_set("initial_environmental_conditions"):
            if (short and not(deep)):
                rep["initial_environmental_conditions"] = self.initial_environmental_conditions.type_rep()
            else:
                rep["initial_environmental_conditions"] = self.initial_environmental_conditions.prop_rep(short, deep)
        if self.is_set("endangered_species"):
            rep["endangered_species"] = []
            for i in range(0,len(self.endangered_species)):
                if (short and not(deep)):
                    itemType = self.endangered_species[i].type_rep()
                    rep["endangered_species"].append( itemType )
                else:
                    rep["endangered_species"].append( self.endangered_species[i].prop_rep(short, deep) )
        else:
            rep["endangered_species"] = []
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "receptors"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "initial_environmental_conditions"
        try :
            if data[varName] != None:
                self.initial_environmental_conditions=dr_env_parameters.dr_env_parameters()
                self.initial_environmental_conditions.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "endangered_species"
        try :
            if data[varName] != None:
                self.endangered_species=[]
                for i in range(0,len(data[varName])):
                    self.endangered_species.append(dr_ES.dr_ES())
                    self.endangered_species[i].loadFromJSONDict(data[varName][i])
            else:
                self.endangered_species = []
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
