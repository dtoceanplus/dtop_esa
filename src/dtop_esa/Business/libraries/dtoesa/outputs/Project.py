# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from ..ES import SpeciesList
from ..CFP import CFPList
from ..EIA import EIAList
from ..SA import SocialList
from . import digital_representation
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class Project():

    """ESA project results
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._projectId=''
        self._endangered_species=SpeciesList.SpeciesList()
        self._endangered_species.description = 'endangered species'
        self._carbon_footprint=CFPList.CFPList()
        self._carbon_footprint.description = 'carbon footprint results'
        self._environmental_impact_assessment=EIAList.EIAList()
        self._environmental_impact_assessment.description = 'environmental impact assessment results'
        self._social_acceptance=SocialList.SocialList()
        self._social_acceptance.description = 'social acceptance results'
        self._digital_representation=digital_representation.digital_representation()
        self._digital_representation.description = 'digital representation of the project'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
        self.projectId =  name
    def _init_from_files(self, inputs_CFP = None,inputs_EIA = None, inputs_ES= None,inputs_SA = None):
        self.inputs_CFP = inputs_CFP
        self.inputs_EIA = inputs_EIA
        self.inputs_ES  = inputs_ES
        self.inputs_SA  = inputs_SA
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def compute_es_assessment(self, database_species):
        self.endangered_species.init_from_files(database_species, self.inputs_ES)
        self.endangered_species.compute_all_species()

    def compute_cfp_assessment(self, dict_fuel, dict_materials, dict_gases):
        self.carbon_footprint.init_from_files(self.inputs_CFP, dict_fuel, dict_materials, dict_gases)
        # self.carbon_footprint.compute_cfp_phase()
        self.carbon_footprint.compute_cfp_global()

    def compute_eia_assessment(self, protected, receptors, weighting):

        self.environmental_impact_assessment.init_from_files(self.inputs_EIA, protected, receptors, weighting)
        # self.environmental_impact_assessment.compute_eia_tech_group()
        self.environmental_impact_assessment.compute_eia_global()
        self.environmental_impact_assessment.compute_eia_pressure()


    def compute_sa_assessment(self):
        self.social_acceptance.cost_of_consenting = self.inputs_SA["cost_of_consenting"]
        self.social_acceptance.nb_vessel_crew = self.inputs_SA["nb_vessel_crew"]

    def compute_DR(self, receptors, inputs):
        self.digital_representation.compute_dr_environment(self.endangered_species, self.inputs_EIA, receptors)
        self.digital_representation.compute_dr_technology(self.environmental_impact_assessment)
        self.digital_representation.compute_dr_phases(self.carbon_footprint, self.environmental_impact_assessment, inputs)

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def projectId(self): # pragma: no cover
        """str: project id
        """
        return self._projectId
    #------------
    @ property
    def endangered_species(self): # pragma: no cover
        """:obj:`~.SpeciesList.SpeciesList`: endangered species
        """
        return self._endangered_species
    #------------
    @ property
    def carbon_footprint(self): # pragma: no cover
        """:obj:`~.CFPList.CFPList`: carbon footprint results
        """
        return self._carbon_footprint
    #------------
    @ property
    def environmental_impact_assessment(self): # pragma: no cover
        """:obj:`~.EIAList.EIAList`: environmental impact assessment results
        """
        return self._environmental_impact_assessment
    #------------
    @ property
    def social_acceptance(self): # pragma: no cover
        """:obj:`~.SocialList.SocialList`: social acceptance results
        """
        return self._social_acceptance
    #------------
    @ property
    def digital_representation(self): # pragma: no cover
        """:obj:`~.digital_representation.digital_representation`: digital representation of the project
        """
        return self._digital_representation
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ projectId.setter
    def projectId(self,val): # pragma: no cover
        self._projectId=str(val)
    #------------
    @ endangered_species.setter
    def endangered_species(self,val): # pragma: no cover
        self._endangered_species=val
    #------------
    @ carbon_footprint.setter
    def carbon_footprint(self,val): # pragma: no cover
        self._carbon_footprint=val
    #------------
    @ environmental_impact_assessment.setter
    def environmental_impact_assessment(self,val): # pragma: no cover
        self._environmental_impact_assessment=val
    #------------
    @ social_acceptance.setter
    def social_acceptance(self,val): # pragma: no cover
        self._social_acceptance=val
    #------------
    @ digital_representation.setter
    def digital_representation(self,val): # pragma: no cover
        self._digital_representation=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:outputs:Project"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:outputs:Project"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("projectId"):
            rep["projectId"] = self.projectId
        if self.is_set("endangered_species"):
            if (short and not(deep)):
                rep["endangered_species"] = self.endangered_species.type_rep()
            else:
                rep["endangered_species"] = self.endangered_species.prop_rep(short, deep)
        if self.is_set("carbon_footprint"):
            if (short and not(deep)):
                rep["carbon_footprint"] = self.carbon_footprint.type_rep()
            else:
                rep["carbon_footprint"] = self.carbon_footprint.prop_rep(short, deep)
        if self.is_set("environmental_impact_assessment"):
            if (short and not(deep)):
                rep["environmental_impact_assessment"] = self.environmental_impact_assessment.type_rep()
            else:
                rep["environmental_impact_assessment"] = self.environmental_impact_assessment.prop_rep(short, deep)
        if self.is_set("social_acceptance"):
            if (short and not(deep)):
                rep["social_acceptance"] = self.social_acceptance.type_rep()
            else:
                rep["social_acceptance"] = self.social_acceptance.prop_rep(short, deep)
        if self.is_set("digital_representation"):
            if (short and not(deep)):
                rep["digital_representation"] = self.digital_representation.type_rep()
            else:
                rep["digital_representation"] = self.digital_representation.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "projectId"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "endangered_species"
        try :
            if data[varName] != None:
                self.endangered_species=SpeciesList.SpeciesList()
                self.endangered_species.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "carbon_footprint"
        try :
            if data[varName] != None:
                self.carbon_footprint=CFPList.CFPList()
                self.carbon_footprint.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "environmental_impact_assessment"
        try :
            if data[varName] != None:
                self.environmental_impact_assessment=EIAList.EIAList()
                self.environmental_impact_assessment.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "social_acceptance"
        try :
            if data[varName] != None:
                self.social_acceptance=SocialList.SocialList()
                self.social_acceptance.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "digital_representation"
        try :
            if data[varName] != None:
                self.digital_representation=digital_representation.digital_representation()
                self.digital_representation.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
