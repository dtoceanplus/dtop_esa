# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import dr_assessment
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class dr_assessment_sk():

    """assessment
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._overall_negative_environmental_impact=dr_assessment.dr_assessment()
        self._overall_positive_environmental_impact=dr_assessment.dr_assessment()
        self._collision_risk=dr_assessment.dr_assessment()
        self._footprint=dr_assessment.dr_assessment()
        self._reef_effect=dr_assessment.dr_assessment()
        self._underwater_noise=dr_assessment.dr_assessment()
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def overall_negative_environmental_impact(self): # pragma: no cover
        """:obj:`~.dr_assessment.dr_assessment`: none
        """
        return self._overall_negative_environmental_impact
    #------------
    @ property
    def overall_positive_environmental_impact(self): # pragma: no cover
        """:obj:`~.dr_assessment.dr_assessment`: none
        """
        return self._overall_positive_environmental_impact
    #------------
    @ property
    def collision_risk(self): # pragma: no cover
        """:obj:`~.dr_assessment.dr_assessment`: none
        """
        return self._collision_risk
    #------------
    @ property
    def footprint(self): # pragma: no cover
        """:obj:`~.dr_assessment.dr_assessment`: none
        """
        return self._footprint
    #------------
    @ property
    def reef_effect(self): # pragma: no cover
        """:obj:`~.dr_assessment.dr_assessment`: none
        """
        return self._reef_effect
    #------------
    @ property
    def underwater_noise(self): # pragma: no cover
        """:obj:`~.dr_assessment.dr_assessment`: none
        """
        return self._underwater_noise
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ overall_negative_environmental_impact.setter
    def overall_negative_environmental_impact(self,val): # pragma: no cover
        self._overall_negative_environmental_impact=val
    #------------
    @ overall_positive_environmental_impact.setter
    def overall_positive_environmental_impact(self,val): # pragma: no cover
        self._overall_positive_environmental_impact=val
    #------------
    @ collision_risk.setter
    def collision_risk(self,val): # pragma: no cover
        self._collision_risk=val
    #------------
    @ footprint.setter
    def footprint(self,val): # pragma: no cover
        self._footprint=val
    #------------
    @ reef_effect.setter
    def reef_effect(self,val): # pragma: no cover
        self._reef_effect=val
    #------------
    @ underwater_noise.setter
    def underwater_noise(self,val): # pragma: no cover
        self._underwater_noise=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:outputs:dr_assessment_sk"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:outputs:dr_assessment_sk"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("overall_negative_environmental_impact"):
            if (short and not(deep)):
                rep["overall_negative_environmental_impact"] = self.overall_negative_environmental_impact.type_rep()
            else:
                rep["overall_negative_environmental_impact"] = self.overall_negative_environmental_impact.prop_rep(short, deep)
        if self.is_set("overall_positive_environmental_impact"):
            if (short and not(deep)):
                rep["overall_positive_environmental_impact"] = self.overall_positive_environmental_impact.type_rep()
            else:
                rep["overall_positive_environmental_impact"] = self.overall_positive_environmental_impact.prop_rep(short, deep)
        if self.is_set("collision_risk"):
            if (short and not(deep)):
                rep["collision_risk"] = self.collision_risk.type_rep()
            else:
                rep["collision_risk"] = self.collision_risk.prop_rep(short, deep)
        if self.is_set("footprint"):
            if (short and not(deep)):
                rep["footprint"] = self.footprint.type_rep()
            else:
                rep["footprint"] = self.footprint.prop_rep(short, deep)
        if self.is_set("reef_effect"):
            if (short and not(deep)):
                rep["reef_effect"] = self.reef_effect.type_rep()
            else:
                rep["reef_effect"] = self.reef_effect.prop_rep(short, deep)
        if self.is_set("underwater_noise"):
            if (short and not(deep)):
                rep["underwater_noise"] = self.underwater_noise.type_rep()
            else:
                rep["underwater_noise"] = self.underwater_noise.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "overall_negative_environmental_impact"
        try :
            if data[varName] != None:
                self.overall_negative_environmental_impact=dr_assessment.dr_assessment()
                self.overall_negative_environmental_impact.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "overall_positive_environmental_impact"
        try :
            if data[varName] != None:
                self.overall_positive_environmental_impact=dr_assessment.dr_assessment()
                self.overall_positive_environmental_impact.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "collision_risk"
        try :
            if data[varName] != None:
                self.collision_risk=dr_assessment.dr_assessment()
                self.collision_risk.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "footprint"
        try :
            if data[varName] != None:
                self.footprint=dr_assessment.dr_assessment()
                self.footprint.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "reef_effect"
        try :
            if data[varName] != None:
                self.reef_effect=dr_assessment.dr_assessment()
                self.reef_effect.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "underwater_noise"
        try :
            if data[varName] != None:
                self.underwater_noise=dr_assessment.dr_assessment()
                self.underwater_noise.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
