# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import dr_prod
from . import dr_insta
from . import dr_maint
from . import dr_decom
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class dr_phases():

    """Digital representation of phases family
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._planning_development=dr_prod.dr_prod()
        self._installation_commissioning=dr_insta.dr_insta()
        self._operation_maintenance=dr_maint.dr_maint()
        self._decomissioning=dr_decom.dr_decom()
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def compute_dr_planning(self,CFP_results):
        self.planning_development.assessment.global_warming_potential.value = CFP_results.CFP_global.GWP
        self.planning_development.assessment.global_warming_potential.unit = "gCO2eq/kWh"
        self.planning_development.assessment.cumulative_energy_demand.value = CFP_results.CFP_global.CED
        self.planning_development.assessment.cumulative_energy_demand.unit = "MJ/kWh"

    def compute_dr_phases(self,CFP_results,EIA_results, inputs):
        self.installation_commissioning.assessment.global_warming_potential.value = CFP_results.CFP_phase.cfp_installation.GWP
        self.installation_commissioning.assessment.global_warming_potential.unit = "gCO2eq/kWh"
        self.installation_commissioning.assessment.cumulative_energy_demand.value = CFP_results.CFP_phase.cfp_installation.CED
        self.installation_commissioning.assessment.cumulative_energy_demand.unit = "MJ/kWh"
        self.installation_commissioning.assessment.overall_negative_environmental_impact.value = EIA_results.eia_tech_group.impact_installation.global_eis.negative_impact
        self.installation_commissioning.assessment.overall_positive_environmental_impact.value = EIA_results.eia_tech_group.impact_installation.global_eis.positive_impact
        self.installation_commissioning.assessment.number_crew_member.value = inputs.inputs_ESA.logistics_info.installation.Number_of_passengers

        for pressure in EIA_results.eia_tech_group.impact_installation.eis_dict:
                if pressure.function_name == "Collision Risk Vessel":
                    self._operation_maintenance.assessment.collision_risk.value = pressure.score
                if pressure.function_name == "Footprint":
                    self.installation_commissioning.assessment.footprint.value = pressure.score
                if pressure.function_name == "Chemical Pollution":
                    self.installation_commissioning.assessment.chemical_pollution.value = pressure.score
                if pressure.function_name == "Turbidity":
                    self.installation_commissioning.assessment.turbidity.value = pressure.score
                if pressure.function_name == "Underwater Noise":
                    self.installation_commissioning.assessment.underwater_noise.value = pressure.score

        self._operation_maintenance.assessment.global_warming_potential.value = CFP_results.CFP_phase.cfp_exploitation.GWP
        self._operation_maintenance.assessment.global_warming_potential.unit = "gCO2eq/kWh"
        self._operation_maintenance.assessment.cumulative_energy_demand.value = CFP_results.CFP_phase.cfp_exploitation.CED
        self._operation_maintenance.assessment.cumulative_energy_demand.unit = "MJ/kWh"
        self._operation_maintenance.assessment.overall_negative_environmental_impact.value = EIA_results.eia_tech_group.impact_maintenance.global_eis.negative_impact
        self._operation_maintenance.assessment.overall_positive_environmental_impact.value = EIA_results.eia_tech_group.impact_maintenance.global_eis.positive_impact
        self._operation_maintenance.assessment.number_crew_member.value = inputs.inputs_ESA.logistics_info.exploitation.Number_of_passengers

        for pressure in EIA_results.eia_tech_group.impact_maintenance.eis_dict:
                if pressure.function_name == "Collision Risk Vessel":
                    self._operation_maintenance.assessment.collision_risk.value = pressure.score
                if pressure.function_name == "Footprint":
                    self._operation_maintenance.assessment.footprint.value= pressure.score
                if pressure.function_name == "Chemical Pollution":
                    self._operation_maintenance.assessment.chemical_pollution.value = pressure.score
                if pressure.function_name == "Turbidity":
                    self._operation_maintenance.assessment.turbidity.value = pressure.score
                if pressure.function_name == "Underwater Noise":
                    self._operation_maintenance.assessment.underwater_noise.value = pressure.score

        self.decomissioning.assessment.global_warming_potential.value = CFP_results.CFP_phase.cfp_dismantling.GWP
        self.decomissioning.assessment.global_warming_potential.unit = "gCO2eq/kWh"
        self.decomissioning.assessment.cumulative_energy_demand.value = CFP_results.CFP_phase.cfp_dismantling.CED
        self.decomissioning.assessment.cumulative_energy_demand.unit = "MJ/kWh"
        self.decomissioning.assessment.number_crew_member.value = inputs.inputs_ESA.logistics_info.exploitation.Number_of_passengers

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def planning_development(self): # pragma: no cover
        """:obj:`~.dr_prod.dr_prod`: none
        """
        return self._planning_development
    #------------
    @ property
    def installation_commissioning(self): # pragma: no cover
        """:obj:`~.dr_insta.dr_insta`: none
        """
        return self._installation_commissioning
    #------------
    @ property
    def operation_maintenance(self): # pragma: no cover
        """:obj:`~.dr_maint.dr_maint`: none
        """
        return self._operation_maintenance
    #------------
    @ property
    def decomissioning(self): # pragma: no cover
        """:obj:`~.dr_decom.dr_decom`: none
        """
        return self._decomissioning
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ planning_development.setter
    def planning_development(self,val): # pragma: no cover
        self._planning_development=val
    #------------
    @ installation_commissioning.setter
    def installation_commissioning(self,val): # pragma: no cover
        self._installation_commissioning=val
    #------------
    @ operation_maintenance.setter
    def operation_maintenance(self,val): # pragma: no cover
        self._operation_maintenance=val
    #------------
    @ decomissioning.setter
    def decomissioning(self,val): # pragma: no cover
        self._decomissioning=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:outputs:dr_phases"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:outputs:dr_phases"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("planning_development"):
            if (short and not(deep)):
                rep["planning_development"] = self.planning_development.type_rep()
            else:
                rep["planning_development"] = self.planning_development.prop_rep(short, deep)
        if self.is_set("installation_commissioning"):
            if (short and not(deep)):
                rep["installation_commissioning"] = self.installation_commissioning.type_rep()
            else:
                rep["installation_commissioning"] = self.installation_commissioning.prop_rep(short, deep)
        if self.is_set("operation_maintenance"):
            if (short and not(deep)):
                rep["operation_maintenance"] = self.operation_maintenance.type_rep()
            else:
                rep["operation_maintenance"] = self.operation_maintenance.prop_rep(short, deep)
        if self.is_set("decomissioning"):
            if (short and not(deep)):
                rep["decomissioning"] = self.decomissioning.type_rep()
            else:
                rep["decomissioning"] = self.decomissioning.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "planning_development"
        try :
            if data[varName] != None:
                self.planning_development=dr_prod.dr_prod()
                self.planning_development.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "installation_commissioning"
        try :
            if data[varName] != None:
                self.installation_commissioning=dr_insta.dr_insta()
                self.installation_commissioning.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "operation_maintenance"
        try :
            if data[varName] != None:
                self.operation_maintenance=dr_maint.dr_maint()
                self.operation_maintenance.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "decomissioning"
        try :
            if data[varName] != None:
                self.decomissioning=dr_decom.dr_decom()
                self.decomissioning.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
