# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import CFP_global
from . import CFP_phase
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class CFPList():

    """List of carbon footprint results
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._CFP_global=CFP_global.CFP_global()
        self._CFP_phase=CFP_phase.CFP_phase()
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def init_from_files(self,inputs_CFP,dict_fuel,dict_material,dict_gases):

        self.dict_fuel = dict_fuel
        self.dict_materials = dict_material
        self.dict_gases_impact_factor = dict_gases
        self.inputs = inputs_CFP      # dans le main??
    def compute_cfp_phase(self):
        '''
        Compute carbon footprint assessment for each phase of the life cycle of the farm (Production, installation, exploitation, dismantling, treatment)
        '''
        print("Carbon FootPrint --- compute CFP phase")
        print("Carbon FootPrint --- compute CFP phase -- Production")
        self.CFP_phase.get_cfp_phase('Production',inputs=self.inputs, dict_fuel=self.dict_fuel, dict_materials= self.dict_materials, dict_gases_impact_factor= self.dict_gases_impact_factor)
        print("Carbon FootPrint --- compute CFP phase -- Installation")
        self.CFP_phase.get_cfp_phase('Installation',inputs=self.inputs, dict_fuel=self.dict_fuel, dict_materials= self.dict_materials, dict_gases_impact_factor= self.dict_gases_impact_factor)
        print("Carbon FootPrint --- compute CFP phase -- Exploitation")
        self.CFP_phase.get_cfp_phase('Exploitation',inputs=self.inputs, dict_fuel=self.dict_fuel, dict_materials= self.dict_materials, dict_gases_impact_factor= self.dict_gases_impact_factor)
        print("Carbon FootPrint --- compute CFP phase -- Dismantling")
        self.CFP_phase.get_cfp_phase('Dismantling',inputs=self.inputs, dict_fuel=self.dict_fuel, dict_materials= self.dict_materials, dict_gases_impact_factor= self.dict_gases_impact_factor)
        print("Carbon FootPrint --- compute CFP phase -- Treatment")
        self.CFP_phase.get_cfp_phase('Treatment',inputs=self.inputs, dict_fuel=self.dict_fuel, dict_materials= self.dict_materials, dict_gases_impact_factor= self.dict_gases_impact_factor)

    def compute_cfp_global(self):
        '''
        Summarise carbon footprint assessment for the lifetime of the farm
        '''
        self.compute_cfp_phase()
        print("Carbon FootPrint --- compute CFP global")
        self.CFP_global.GWPb = self.CFP_phase.cfp_production.GWPb + self.CFP_phase.cfp_installation.GWPb + self.CFP_phase.cfp_exploitation.GWPb + self.CFP_phase.cfp_dismantling.GWPb + self.CFP_phase.cfp_treatment.GWPb
        GWP = self.CFP_phase.cfp_production.GWP + self.CFP_phase.cfp_installation.GWP + self.CFP_phase.cfp_exploitation.GWP + self.CFP_phase.cfp_dismantling.GWP + self.CFP_phase.cfp_treatment.GWP
        self.CFP_global.GWP = "%.2f" % GWP
        self.CFP_global.CEDb = self.CFP_phase.cfp_production.CEDb + self.CFP_phase.cfp_installation.CEDb + self.CFP_phase.cfp_exploitation.CEDb + self.CFP_phase.cfp_dismantling.CEDb + self.CFP_phase.cfp_treatment.CEDb
        CED = self.CFP_phase.cfp_production.CED + self.CFP_phase.cfp_installation.CED + self.CFP_phase.cfp_exploitation.CED + self.CFP_phase.cfp_dismantling.CED + self.CFP_phase.cfp_treatment.CED
        self.CFP_global.CED = "%.2f" % CED
        try:
            EPP = self.CFP_global.CEDb/(self.inputs["energy_produced"]["value"])
            self.CFP_global.EPP = "%.2f" % EPP
        except:
            pass

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def CFP_global(self): # pragma: no cover
        """:obj:`~.CFP_global.CFP_global`: none
        """
        return self._CFP_global
    #------------
    @ property
    def CFP_phase(self): # pragma: no cover
        """:obj:`~.CFP_phase.CFP_phase`: none
        """
        return self._CFP_phase
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ CFP_global.setter
    def CFP_global(self,val): # pragma: no cover
        self._CFP_global=val
    #------------
    @ CFP_phase.setter
    def CFP_phase(self,val): # pragma: no cover
        self._CFP_phase=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:CFP:CFPList"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:CFP:CFPList"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("CFP_global"):
            if (short and not(deep)):
                rep["CFP_global"] = self.CFP_global.type_rep()
            else:
                rep["CFP_global"] = self.CFP_global.prop_rep(short, deep)
        if self.is_set("CFP_phase"):
            if (short and not(deep)):
                rep["CFP_phase"] = self.CFP_phase.type_rep()
            else:
                rep["CFP_phase"] = self.CFP_phase.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "CFP_global"
        try :
            if data[varName] != None:
                self.CFP_global=CFP_global.CFP_global()
                self.CFP_global.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "CFP_phase"
        try :
            if data[varName] != None:
                self.CFP_phase=CFP_phase.CFP_phase()
                self.CFP_phase.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
