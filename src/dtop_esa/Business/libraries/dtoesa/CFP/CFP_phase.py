# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import CFP_results
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class CFP_phase():

    """Carbon footprint results per phase of the life cycle of the project
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._cfp_production=CFP_results.CFP_results()
        self._cfp_production.description = 'Carbon footprint results for production phase'
        self._cfp_installation=CFP_results.CFP_results()
        self._cfp_installation.description = 'Carbon footprint results for installation phase'
        self._cfp_exploitation=CFP_results.CFP_results()
        self._cfp_exploitation.description = 'Carbon footprint results for exploitation phase'
        self._cfp_dismantling=CFP_results.CFP_results()
        self._cfp_dismantling.description = 'Carbon footprint results for decommissioning phase'
        self._cfp_treatment=CFP_results.CFP_results()
        self._cfp_treatment.description = 'Carbon footprint results for treatment phase'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
        self.cfp_production.name = "Production"
        self.cfp_production.get_phase()
        self.cfp_installation.name = "Installation"
        self.cfp_installation.get_phase()
        self.cfp_exploitation.name = "Exploitation"
        self.cfp_exploitation.get_phase()
        self.cfp_dismantling.name = "Dismantling"
        self.cfp_dismantling.get_phase()
        self.cfp_treatment.name = "Treatment"
        self.cfp_treatment.get_phase()
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def get_cfp_phase(self, phase, inputs, dict_fuel, dict_materials, dict_gases_impact_factor):

        if phase =='Installation':
            self.cfp_installation.get_GWP(inputs, dict_fuel, dict_materials, dict_gases_impact_factor)
            self.cfp_installation.get_CED(inputs, dict_fuel, dict_materials, dict_gases_impact_factor)

        elif phase == "Production":
            self.cfp_production.get_GWP(inputs, dict_fuel, dict_materials, dict_gases_impact_factor)
            self.cfp_production.get_CED(inputs, dict_fuel, dict_materials, dict_gases_impact_factor)

        elif phase == "Exploitation":
            self.cfp_exploitation.get_GWP(inputs, dict_fuel, dict_materials, dict_gases_impact_factor)
            self.cfp_exploitation.get_CED(inputs, dict_fuel, dict_materials, dict_gases_impact_factor)

        elif phase == "Dismantling":
            self.cfp_dismantling.get_GWP(inputs, dict_fuel, dict_materials, dict_gases_impact_factor)
            self.cfp_dismantling.get_CED(inputs, dict_fuel, dict_materials, dict_gases_impact_factor)

        elif phase == "Treatment":
            self.cfp_treatment.get_GWP(inputs, dict_fuel, dict_materials, dict_gases_impact_factor)
            self.cfp_treatment.get_CED(inputs, dict_fuel, dict_materials, dict_gases_impact_factor)

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def cfp_production(self): # pragma: no cover
        """:obj:`~.CFP_results.CFP_results`: Carbon footprint results for production phase
        """
        return self._cfp_production
    #------------
    @ property
    def cfp_installation(self): # pragma: no cover
        """:obj:`~.CFP_results.CFP_results`: Carbon footprint results for installation phase
        """
        return self._cfp_installation
    #------------
    @ property
    def cfp_exploitation(self): # pragma: no cover
        """:obj:`~.CFP_results.CFP_results`: Carbon footprint results for exploitation phase
        """
        return self._cfp_exploitation
    #------------
    @ property
    def cfp_dismantling(self): # pragma: no cover
        """:obj:`~.CFP_results.CFP_results`: Carbon footprint results for decommissioning phase
        """
        return self._cfp_dismantling
    #------------
    @ property
    def cfp_treatment(self): # pragma: no cover
        """:obj:`~.CFP_results.CFP_results`: Carbon footprint results for treatment phase
        """
        return self._cfp_treatment
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ cfp_production.setter
    def cfp_production(self,val): # pragma: no cover
        self._cfp_production=val
    #------------
    @ cfp_installation.setter
    def cfp_installation(self,val): # pragma: no cover
        self._cfp_installation=val
    #------------
    @ cfp_exploitation.setter
    def cfp_exploitation(self,val): # pragma: no cover
        self._cfp_exploitation=val
    #------------
    @ cfp_dismantling.setter
    def cfp_dismantling(self,val): # pragma: no cover
        self._cfp_dismantling=val
    #------------
    @ cfp_treatment.setter
    def cfp_treatment(self,val): # pragma: no cover
        self._cfp_treatment=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:CFP:CFP_phase"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:CFP:CFP_phase"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("cfp_production"):
            if (short and not(deep)):
                rep["cfp_production"] = self.cfp_production.type_rep()
            else:
                rep["cfp_production"] = self.cfp_production.prop_rep(short, deep)
        if self.is_set("cfp_installation"):
            if (short and not(deep)):
                rep["cfp_installation"] = self.cfp_installation.type_rep()
            else:
                rep["cfp_installation"] = self.cfp_installation.prop_rep(short, deep)
        if self.is_set("cfp_exploitation"):
            if (short and not(deep)):
                rep["cfp_exploitation"] = self.cfp_exploitation.type_rep()
            else:
                rep["cfp_exploitation"] = self.cfp_exploitation.prop_rep(short, deep)
        if self.is_set("cfp_dismantling"):
            if (short and not(deep)):
                rep["cfp_dismantling"] = self.cfp_dismantling.type_rep()
            else:
                rep["cfp_dismantling"] = self.cfp_dismantling.prop_rep(short, deep)
        if self.is_set("cfp_treatment"):
            if (short and not(deep)):
                rep["cfp_treatment"] = self.cfp_treatment.type_rep()
            else:
                rep["cfp_treatment"] = self.cfp_treatment.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "cfp_production"
        try :
            if data[varName] != None:
                self.cfp_production=CFP_results.CFP_results()
                self.cfp_production.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "cfp_installation"
        try :
            if data[varName] != None:
                self.cfp_installation=CFP_results.CFP_results()
                self.cfp_installation.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "cfp_exploitation"
        try :
            if data[varName] != None:
                self.cfp_exploitation=CFP_results.CFP_results()
                self.cfp_exploitation.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "cfp_dismantling"
        try :
            if data[varName] != None:
                self.cfp_dismantling=CFP_results.CFP_results()
                self.cfp_dismantling.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "cfp_treatment"
        try :
            if data[varName] != None:
                self.cfp_treatment=CFP_results.CFP_results()
                self.cfp_treatment.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
