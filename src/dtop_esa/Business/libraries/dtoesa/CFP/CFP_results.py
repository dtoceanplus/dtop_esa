# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class CFP_results():

    """Carbon footprint results
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._GWPb=0.0
        self._GWP=0.0
        self._CEDb=0.0
        self._CED=0.0
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def get_phase(self):
        '''
        Determine which phase of the life cycle to assess
        '''

        if self.name =="Fabrication" or self.name =="fabrication" or self.name =="production" or self.name =="Production":
            self.phase = "Production"
            self.description = "Carbon footprint for phase " + self.phase + " considering manufacturing processes of materials"

        elif self.name == "Installation" or self.name == "installation":
            self.phase = "Installation"
            self.description = "Carbon footprint for marine operations in phase " + self.phase

        elif self.name =="Exploitation" or self.name =="exploitation":
            self.phase = "Exploitation"
            self.description = "Carbon footprint for marine operations in phase " + self.phase

        elif self.name =="Dismantling" or self.name =="Dismantling":
            self.phase = "Dismantling"
            self.description = "Carbon footprint for marine operations in phase " + self.phase

        elif self.name =="Treatment" or self.name =="treatment":
            self.phase = "Treatment"
            self.description = "Carbon footprint for phase  " + self.phase + "considering recycling of materials"

        else:
            self.phase = "Phase not found"

        return self.phase

    def get_GWP(self, inputs, dict_fuel, dict_materials, dict_gases_impact_factor):

        """
        Assess Global Warming Potential depending on the life cycle phase

        - GWP : Global Warming Potential of marine operation (gCO2-eq)
                Fabrication includes production of the material, manufacturing processes and half of recycling
                Treatment considers the recycling of the components that are brought back to land, it reduces GWP and CED scores. Half of recycling is allocated to fabrication.

        ARGS:
            - total_Vessel_consumption : Total consumption of diesel fuel (kg) (input given by LMO)
            - LCV_fuel : lower calorific value of diesel fuel (MJ/t)
            - GWP_fuel : 100-year global warming potential of diesel fuel (kgCO2-eq.t-1)
            - emission_factor_CO2 : CO2 emission factor of diesel fuel combustion
            - emission_factor_CH4 : CH4 emission factor of diesel fuel combustion
            - emission_factor_N2O : N2O emission factor of diesel fuel combustion
            - impact_factor_CO2 : Impact factor of CO2 on global warming
            - impact_factor_CH4 : Impact factor of CH4 on global warming
            - impact_factor_N2O : Impact factor of N2O on global warming
            - quantity_materials: list of total quantities of every materials used in the farm (kg) and quantity of materials brought back to land
            - annual energy produced  the farm (kWh)
        """
        self.get_phase()
        if self.phase =="Installation" or self.phase =="Exploitation" or self.phase =="Dismantling":
            gwp_marine_operation = (inputs["vessel_consumption"][self.phase]["value"] * dict_fuel["LCV"]["value"]/1000) *(dict_fuel["emission_factor_CO2"]["value"] * dict_gases_impact_factor["CO2"]["value"] + dict_fuel["emission_factor_CH4"]["value"] * dict_gases_impact_factor["CH4"]["value"]/1000 + dict_fuel["emission_factor_N2O"]["value"] * dict_gases_impact_factor["N2O"]["value"]/1000) + (inputs["vessel_consumption"][self.phase]["value"]* dict_fuel["GWP"]["value"])
            self.GWPb = gwp_marine_operation*1000
            try:
                GWP =  self.GWPb/(inputs["energy_produced"]["value"]*inputs["project_lifetime"]["value"])
                self.GWP = "%.2f" % GWP
            except:
                pass

        elif self.phase =="Production":
            gwp_materials=[]
            listmat = list(inputs["materials"].keys())

            for mat in listmat:
                gwp_materials.append((inputs["materials"][mat]["value"]/1000)*dict_materials["GWP"][mat]['fabrication'] + ((inputs["materials_to_recycle"][mat]["value"]/1000)*(dict_materials["GWP"][mat]["manufacturing"] + dict_materials["GWP"][mat]['treatment'])))
            self.GWPb = sum(gwp_materials)*1000
            try:
                GWP = self.GWPb/(inputs["energy_produced"]["value"]*inputs["project_lifetime"]["value"])
                self.GWP = "%.2f" % GWP
            except:
                pass

        elif self.phase == "Treatment":
            gwp_materials=[]
            listmat = list(inputs["materials_to_recycle"].keys())
            for mat in listmat:
                gwp_materials.append(dict_materials['GWP'][mat]['treatment']*inputs["materials_to_recycle"][mat]['value']/1000)
            self.GWPb = sum(gwp_materials)*1000
            try:
                GWP = self.GWPb/(inputs["energy_produced"]["value"]*inputs["project_lifetime"]["value"])
                self.GWP = "%.2f" % GWP
            except:
                pass

        else:
            self.GWP = 0

        return self.GWP


    # Cumulative Energy Demand (CED)

    def get_CED(self, inputs,dict_fuel, dict_materials, dict_gases_impact_factor):
        """
        Assess Cumulative Energy Demand depending on the life cycle phase

        - CED : Cumulative Energy Demand of marine operation (MJ)
                calculated from diesel fuel consumption

        ARGS:
            - total_Vessel_consumption : Total consumption of diesel fuel (t) (input given by LMO)
            - CED : Cumulative Energy demand of diesel fuel (MJ)
            - quantity_materials: list of total quantities of every materials used in the farm (t)
            - annual energy produced by the farm (kWh)

        """
        self.get_phase()
        if self.phase == "Installation" or self.phase =="Exploitation" or self.phase =="Dismantling":
            ced_marine_operation = inputs["vessel_consumption"][self.phase]["value"] * dict_fuel["CED"]["value"]
            self.CEDb = ced_marine_operation

            try:
                CED = self.CEDb/(inputs["energy_produced"]["value"]*inputs["project_lifetime"]["value"])
                self.CED = "%.2f" % CED
            except:
                pass

        elif self.phase == "Production":
            ced_materials=[]
            listmat = list(inputs["materials"].keys())

            for mat in listmat:
                ced_materials.append((inputs["materials"][mat]["value"]/1000)*dict_materials["CED"][mat]['fabrication'] + ((inputs["materials_to_recycle"][mat]["value"]/1000)*(dict_materials["CED"][mat]["manufacturing"] + dict_materials["CED"][mat]['treatment'])))
            self.CEDb = sum(ced_materials)
            try:
                CED = self.CEDb/(inputs["energy_produced"]["value"]*inputs["project_lifetime"]["value"])
                self.CED = "%.2f" % CED
            except:
                pass

        elif self.phase == "Treatment":
            ced_materials=[]
            listmat = list(inputs["materials_to_recycle"].keys())
            for mat in listmat:
                ced_materials.append(dict_materials['CED'][mat]['treatment']*inputs["materials_to_recycle"][mat]["value"]/1000)
            self.CEDb = sum(ced_materials)
            try:
                CED = self.CEDb/(inputs["energy_produced"]["value"]*inputs["project_lifetime"]["value"])
                self.CED = "%.2f" % CED
            except:
                pass
        else:
            self.CED = 0

        return  self.CED
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def GWPb(self): # pragma: no cover
        """float: Global warming potential (brut) [gCO2-eq]
        """
        return self._GWPb
    #------------
    @ property
    def GWP(self): # pragma: no cover
        """float: Global warming potential per unit of energy [gCO2-eq/kWh]
        """
        return self._GWP
    #------------
    @ property
    def CEDb(self): # pragma: no cover
        """float: Cumulative energy demand (brut) [MJ]
        """
        return self._CEDb
    #------------
    @ property
    def CED(self): # pragma: no cover
        """float: Cumulative energy demand per unit of energy [MJ/kWh]
        """
        return self._CED
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ GWPb.setter
    def GWPb(self,val): # pragma: no cover
        self._GWPb=float(val)
    #------------
    @ GWP.setter
    def GWP(self,val): # pragma: no cover
        self._GWP=float(val)
    #------------
    @ CEDb.setter
    def CEDb(self,val): # pragma: no cover
        self._CEDb=float(val)
    #------------
    @ CED.setter
    def CED(self,val): # pragma: no cover
        self._CED=float(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:CFP:CFP_results"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:CFP:CFP_results"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("GWPb"):
            rep["GWPb"] = self.GWPb
        if self.is_set("GWP"):
            rep["GWP"] = self.GWP
        if self.is_set("CEDb"):
            rep["CEDb"] = self.CEDb
        if self.is_set("CED"):
            rep["CED"] = self.CED
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "GWPb"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "GWP"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "CEDb"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "CED"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
