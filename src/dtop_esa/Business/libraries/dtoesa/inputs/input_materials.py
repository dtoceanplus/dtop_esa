# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import value
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class input_materials():

    """Necessary inputs to run the carbon footprint assessment of a project
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._ABS=value.value()
        self._unalloyed_steel=value.value()
        self._low_alloyed_steel=value.value()
        self._reinforcing_steel=value.value()
        self._aluminium=value.value()
        self._concrete=value.value()
        self._brick=value.value()
        self._copper=value.value()
        self._fiberglass=value.value()
        self._synthetic_fiber=value.value()
        self._cast_iron=value.value()
        self._gravel=value.value()
        self._brass=value.value()
        self._magnesium=value.value()
        self._kraft_paper=value.value()
        self._alkyd_paint=value.value()
        self._polybutadiene=value.value()
        self._polypropylene=value.value()
        self._PVC=value.value()
        self._polyethylene=value.value()
        self._epoxy_resin=value.value()
        self._sand=value.value()
        self._ceramic=value.value()
        self._zinc=value.value()
        self._lead=value.value()
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def ABS(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._ABS
    #------------
    @ property
    def unalloyed_steel(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._unalloyed_steel
    #------------
    @ property
    def low_alloyed_steel(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._low_alloyed_steel
    #------------
    @ property
    def reinforcing_steel(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._reinforcing_steel
    #------------
    @ property
    def aluminium(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._aluminium
    #------------
    @ property
    def concrete(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._concrete
    #------------
    @ property
    def brick(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._brick
    #------------
    @ property
    def copper(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._copper
    #------------
    @ property
    def fiberglass(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._fiberglass
    #------------
    @ property
    def synthetic_fiber(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._synthetic_fiber
    #------------
    @ property
    def cast_iron(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._cast_iron
    #------------
    @ property
    def gravel(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._gravel
    #------------
    @ property
    def brass(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._brass
    #------------
    @ property
    def magnesium(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._magnesium
    #------------
    @ property
    def kraft_paper(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._kraft_paper
    #------------
    @ property
    def alkyd_paint(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._alkyd_paint
    #------------
    @ property
    def polybutadiene(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._polybutadiene
    #------------
    @ property
    def polypropylene(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._polypropylene
    #------------
    @ property
    def PVC(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._PVC
    #------------
    @ property
    def polyethylene(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._polyethylene
    #------------
    @ property
    def epoxy_resin(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._epoxy_resin
    #------------
    @ property
    def sand(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._sand
    #------------
    @ property
    def ceramic(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._ceramic
    #------------
    @ property
    def zinc(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._zinc
    #------------
    @ property
    def lead(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._lead
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ ABS.setter
    def ABS(self,val): # pragma: no cover
        self._ABS=val
    #------------
    @ unalloyed_steel.setter
    def unalloyed_steel(self,val): # pragma: no cover
        self._unalloyed_steel=val
    #------------
    @ low_alloyed_steel.setter
    def low_alloyed_steel(self,val): # pragma: no cover
        self._low_alloyed_steel=val
    #------------
    @ reinforcing_steel.setter
    def reinforcing_steel(self,val): # pragma: no cover
        self._reinforcing_steel=val
    #------------
    @ aluminium.setter
    def aluminium(self,val): # pragma: no cover
        self._aluminium=val
    #------------
    @ concrete.setter
    def concrete(self,val): # pragma: no cover
        self._concrete=val
    #------------
    @ brick.setter
    def brick(self,val): # pragma: no cover
        self._brick=val
    #------------
    @ copper.setter
    def copper(self,val): # pragma: no cover
        self._copper=val
    #------------
    @ fiberglass.setter
    def fiberglass(self,val): # pragma: no cover
        self._fiberglass=val
    #------------
    @ synthetic_fiber.setter
    def synthetic_fiber(self,val): # pragma: no cover
        self._synthetic_fiber=val
    #------------
    @ cast_iron.setter
    def cast_iron(self,val): # pragma: no cover
        self._cast_iron=val
    #------------
    @ gravel.setter
    def gravel(self,val): # pragma: no cover
        self._gravel=val
    #------------
    @ brass.setter
    def brass(self,val): # pragma: no cover
        self._brass=val
    #------------
    @ magnesium.setter
    def magnesium(self,val): # pragma: no cover
        self._magnesium=val
    #------------
    @ kraft_paper.setter
    def kraft_paper(self,val): # pragma: no cover
        self._kraft_paper=val
    #------------
    @ alkyd_paint.setter
    def alkyd_paint(self,val): # pragma: no cover
        self._alkyd_paint=val
    #------------
    @ polybutadiene.setter
    def polybutadiene(self,val): # pragma: no cover
        self._polybutadiene=val
    #------------
    @ polypropylene.setter
    def polypropylene(self,val): # pragma: no cover
        self._polypropylene=val
    #------------
    @ PVC.setter
    def PVC(self,val): # pragma: no cover
        self._PVC=val
    #------------
    @ polyethylene.setter
    def polyethylene(self,val): # pragma: no cover
        self._polyethylene=val
    #------------
    @ epoxy_resin.setter
    def epoxy_resin(self,val): # pragma: no cover
        self._epoxy_resin=val
    #------------
    @ sand.setter
    def sand(self,val): # pragma: no cover
        self._sand=val
    #------------
    @ ceramic.setter
    def ceramic(self,val): # pragma: no cover
        self._ceramic=val
    #------------
    @ zinc.setter
    def zinc(self,val): # pragma: no cover
        self._zinc=val
    #------------
    @ lead.setter
    def lead(self,val): # pragma: no cover
        self._lead=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:input_materials"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:input_materials"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("ABS"):
            if (short and not(deep)):
                rep["ABS"] = self.ABS.type_rep()
            else:
                rep["ABS"] = self.ABS.prop_rep(short, deep)
        if self.is_set("unalloyed_steel"):
            if (short and not(deep)):
                rep["unalloyed_steel"] = self.unalloyed_steel.type_rep()
            else:
                rep["unalloyed_steel"] = self.unalloyed_steel.prop_rep(short, deep)
        if self.is_set("low_alloyed_steel"):
            if (short and not(deep)):
                rep["low_alloyed_steel"] = self.low_alloyed_steel.type_rep()
            else:
                rep["low_alloyed_steel"] = self.low_alloyed_steel.prop_rep(short, deep)
        if self.is_set("reinforcing_steel"):
            if (short and not(deep)):
                rep["reinforcing_steel"] = self.reinforcing_steel.type_rep()
            else:
                rep["reinforcing_steel"] = self.reinforcing_steel.prop_rep(short, deep)
        if self.is_set("aluminium"):
            if (short and not(deep)):
                rep["aluminium"] = self.aluminium.type_rep()
            else:
                rep["aluminium"] = self.aluminium.prop_rep(short, deep)
        if self.is_set("concrete"):
            if (short and not(deep)):
                rep["concrete"] = self.concrete.type_rep()
            else:
                rep["concrete"] = self.concrete.prop_rep(short, deep)
        if self.is_set("brick"):
            if (short and not(deep)):
                rep["brick"] = self.brick.type_rep()
            else:
                rep["brick"] = self.brick.prop_rep(short, deep)
        if self.is_set("copper"):
            if (short and not(deep)):
                rep["copper"] = self.copper.type_rep()
            else:
                rep["copper"] = self.copper.prop_rep(short, deep)
        if self.is_set("fiberglass"):
            if (short and not(deep)):
                rep["fiberglass"] = self.fiberglass.type_rep()
            else:
                rep["fiberglass"] = self.fiberglass.prop_rep(short, deep)
        if self.is_set("synthetic_fiber"):
            if (short and not(deep)):
                rep["synthetic_fiber"] = self.synthetic_fiber.type_rep()
            else:
                rep["synthetic_fiber"] = self.synthetic_fiber.prop_rep(short, deep)
        if self.is_set("cast_iron"):
            if (short and not(deep)):
                rep["cast_iron"] = self.cast_iron.type_rep()
            else:
                rep["cast_iron"] = self.cast_iron.prop_rep(short, deep)
        if self.is_set("gravel"):
            if (short and not(deep)):
                rep["gravel"] = self.gravel.type_rep()
            else:
                rep["gravel"] = self.gravel.prop_rep(short, deep)
        if self.is_set("brass"):
            if (short and not(deep)):
                rep["brass"] = self.brass.type_rep()
            else:
                rep["brass"] = self.brass.prop_rep(short, deep)
        if self.is_set("magnesium"):
            if (short and not(deep)):
                rep["magnesium"] = self.magnesium.type_rep()
            else:
                rep["magnesium"] = self.magnesium.prop_rep(short, deep)
        if self.is_set("kraft_paper"):
            if (short and not(deep)):
                rep["kraft_paper"] = self.kraft_paper.type_rep()
            else:
                rep["kraft_paper"] = self.kraft_paper.prop_rep(short, deep)
        if self.is_set("alkyd_paint"):
            if (short and not(deep)):
                rep["alkyd_paint"] = self.alkyd_paint.type_rep()
            else:
                rep["alkyd_paint"] = self.alkyd_paint.prop_rep(short, deep)
        if self.is_set("polybutadiene"):
            if (short and not(deep)):
                rep["polybutadiene"] = self.polybutadiene.type_rep()
            else:
                rep["polybutadiene"] = self.polybutadiene.prop_rep(short, deep)
        if self.is_set("polypropylene"):
            if (short and not(deep)):
                rep["polypropylene"] = self.polypropylene.type_rep()
            else:
                rep["polypropylene"] = self.polypropylene.prop_rep(short, deep)
        if self.is_set("PVC"):
            if (short and not(deep)):
                rep["PVC"] = self.PVC.type_rep()
            else:
                rep["PVC"] = self.PVC.prop_rep(short, deep)
        if self.is_set("polyethylene"):
            if (short and not(deep)):
                rep["polyethylene"] = self.polyethylene.type_rep()
            else:
                rep["polyethylene"] = self.polyethylene.prop_rep(short, deep)
        if self.is_set("epoxy_resin"):
            if (short and not(deep)):
                rep["epoxy_resin"] = self.epoxy_resin.type_rep()
            else:
                rep["epoxy_resin"] = self.epoxy_resin.prop_rep(short, deep)
        if self.is_set("sand"):
            if (short and not(deep)):
                rep["sand"] = self.sand.type_rep()
            else:
                rep["sand"] = self.sand.prop_rep(short, deep)
        if self.is_set("ceramic"):
            if (short and not(deep)):
                rep["ceramic"] = self.ceramic.type_rep()
            else:
                rep["ceramic"] = self.ceramic.prop_rep(short, deep)
        if self.is_set("zinc"):
            if (short and not(deep)):
                rep["zinc"] = self.zinc.type_rep()
            else:
                rep["zinc"] = self.zinc.prop_rep(short, deep)
        if self.is_set("lead"):
            if (short and not(deep)):
                rep["ad"] = self.lead.type_rep()
            else:
                rep["lead"] = self.lead.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "ABS"
        try :
            if data[varName] != None:
                self.ABS=value.value()
                self.ABS.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "unalloyed_steel"
        try :
            if data[varName] != None:
                self.unalloyed_steel=value.value()
                self.unalloyed_steel.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "low_alloyed_steel"
        try :
            if data[varName] != None:
                self.low_alloyed_steel=value.value()
                self.low_alloyed_steel.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "reinforcing_steel"
        try :
            if data[varName] != None:
                self.reinforcing_steel=value.value()
                self.reinforcing_steel.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "aluminium"
        try :
            if data[varName] != None:
                self.aluminium=value.value()
                self.aluminium.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "concrete"
        try :
            if data[varName] != None:
                self.concrete=value.value()
                self.concrete.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "brick"
        try :
            if data[varName] != None:
                self.brick=value.value()
                self.brick.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "copper"
        try :
            if data[varName] != None:
                self.copper=value.value()
                self.copper.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "fiberglass"
        try :
            if data[varName] != None:
                self.fiberglass=value.value()
                self.fiberglass.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "synthetic_fiber"
        try :
            if data[varName] != None:
                self.synthetic_fiber=value.value()
                self.synthetic_fiber.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "cast_iron"
        try :
            if data[varName] != None:
                self.cast_iron=value.value()
                self.cast_iron.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "gravel"
        try :
            if data[varName] != None:
                self.gravel=value.value()
                self.gravel.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "brass"
        try :
            if data[varName] != None:
                self.brass=value.value()
                self.brass.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "magnesium"
        try :
            if data[varName] != None:
                self.magnesium=value.value()
                self.magnesium.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "kraft_paper"
        try :
            if data[varName] != None:
                self.kraft_paper=value.value()
                self.kraft_paper.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "alkyd_paint"
        try :
            if data[varName] != None:
                self.alkyd_paint=value.value()
                self.alkyd_paint.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "polybutadiene"
        try :
            if data[varName] != None:
                self.polybutadiene=value.value()
                self.polybutadiene.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "polypropylene"
        try :
            if data[varName] != None:
                self.polypropylene=value.value()
                self.polypropylene.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "PVC"
        try :
            if data[varName] != None:
                self.PVC=value.value()
                self.PVC.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "polyethylene"
        try :
            if data[varName] != None:
                self.polyethylene=value.value()
                self.polyethylene.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "epoxy_resin"
        try :
            if data[varName] != None:
                self.epoxy_resin=value.value()
                self.epoxy_resin.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "sand"
        try :
            if data[varName] != None:
                self.sand=value.value()
                self.sand.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "ceramic"
        try :
            if data[varName] != None:
                self.ceramic=value.value()
                self.ceramic.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "zinc"
        try :
            if data[varName] != None:
                self.zinc=value.value()
                self.zinc.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "lead"
        try :
            if data[varName] != None:
                self.lead=value.value()
                self.lead.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
