# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class logistics_phase():

    """Logistics information for one phase of the life cycle of the project
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._Number_of_Vessels=0.0
        self._mean_Size_of_Vessels=0.0
        self._Number_of_passengers=0.0
        self._measure_noise=0.0
        self._measured_turbidity=0.0
        self._chemical_polutant=''
        self._fuel_consumption=0.0
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def Number_of_Vessels(self): # pragma: no cover
        """float: Number of boat mobilized during the phase []
        """
        return self._Number_of_Vessels
    #------------
    @ property
    def mean_Size_of_Vessels(self): # pragma: no cover
        """float: Mean size of the vessels mobilized during the phase [m]
        """
        return self._mean_Size_of_Vessels
    #------------
    @ property
    def Number_of_passengers(self): # pragma: no cover
        """float: Numbver of crews members on boat []
        """
        return self._Number_of_passengers
    #------------
    @ property
    def measure_noise(self): # pragma: no cover
        """float: Measured noise of the vessel [dB re 1muPa]
        """
        return self._measure_noise
    #------------
    @ property
    def measured_turbidity(self): # pragma: no cover
        """float: Measured Turbidity of the device [mg/L]
        """
        return self._measured_turbidity
    #------------
    @ property
    def chemical_polutant(self): # pragma: no cover
        """str: bunker oil, highly toxic antifouling, moderate toxic antifouling, natural antifouling
        """
        return self._chemical_polutant
    #------------
    @ property
    def fuel_consumption(self): # pragma: no cover
        """float: Total fuel consumption during the phase [kg]
        """
        return self._fuel_consumption
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ Number_of_Vessels.setter
    def Number_of_Vessels(self,val): # pragma: no cover
        self._Number_of_Vessels=float(val)
    #------------
    @ mean_Size_of_Vessels.setter
    def mean_Size_of_Vessels(self,val): # pragma: no cover
        self._mean_Size_of_Vessels=float(val)
    #------------
    @ Number_of_passengers.setter
    def Number_of_passengers(self,val): # pragma: no cover
        self._Number_of_passengers=float(val)
    #------------
    @ measure_noise.setter
    def measure_noise(self,val): # pragma: no cover
        self._measure_noise=float(val)
    #------------
    @ measured_turbidity.setter
    def measured_turbidity(self,val): # pragma: no cover
        self._measured_turbidity=float(val)
    #------------
    @ chemical_polutant.setter
    def chemical_polutant(self,val): # pragma: no cover
        self._chemical_polutant=str(val)
    #------------
    @ fuel_consumption.setter
    def fuel_consumption(self,val): # pragma: no cover
        self._fuel_consumption=float(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:logistics_phase"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:logistics_phase"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("Number_of_Vessels"):
            rep["Number_of_Vessels"] = self.Number_of_Vessels
        if self.is_set("mean_Size_of_Vessels"):
            rep["mean_Size_of_Vessels"] = self.mean_Size_of_Vessels
        if self.is_set("Number_of_passengers"):
            rep["Number_of_passengers"] = self.Number_of_passengers
        if self.is_set("measure_noise"):
            rep["measure_noise"] = self.measure_noise
        if self.is_set("measured_turbidity"):
            rep["measured_turbidity"] = self.measured_turbidity
        if self.is_set("chemical_polutant"):
            rep["chemical_polutant"] = self.chemical_polutant
        if self.is_set("fuel_consumption"):
            rep["fuel_consumption"] = self.fuel_consumption
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "Number_of_Vessels"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mean_Size_of_Vessels"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Number_of_passengers"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "measure_noise"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "measured_turbidity"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "chemical_polutant"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "fuel_consumption"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
