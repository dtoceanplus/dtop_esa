# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json

#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class inputs_ESA_status():

    """
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._farm_info_page='False'
        self._device_info_page='False'
        self._electrical_info_page='False'
        self._logistics_info_page='False'
        self._complexity_level=0
        self._flag_url_sc_get_farm='False'
        self._flag_url_sc_get_point='False'
        self._flag_url_sc_get_current='False'
        self._flag_url_ec_get_farm='False'
        self._flag_url_ec_get_project='False'
        self._flag_url_mc_dimensions='False'
        self._flag_url_mc_general='False'
        self._flag_url_ed_get_results='False'
        self._flag_url_et_array='False'
        self._flag_url_sk_get_env_impact='False'
        self._flag_url_lmo_results='False'
        self._flag_url_lmo_catalogue='False'
        self._flag_url_ed_catalogue_static_cable='False'
        self._flag_url_ed_catalogue_dynamic_cable='False'
        self._flag_url_ed_catalogue_collection_point='False'
        self._entity_id=''
        self._entity_status=0
        self._length_collection_point=0
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def farm_info_page(self): # pragma: no cover
        """str: Check if the Farm_Info page was filled by the user.
        """
        return self._farm_info_page
    #------------
    @ property
    def device_info_page(self): # pragma: no cover
        """str: Check if the Device_Info page was filled by the user.
        """
        return self._device_info_page
    #------------
    @ property
    def electrical_info_page(self): # pragma: no cover
        """str: Check if the Electrical_Info page was filled by the user.
        """
        return self._electrical_info_page
    #------------
    @ property
    def logistics_info_page(self): # pragma: no cover
        """str: Check if the Logistics_Info page was filled by the user.
        """
        return self._logistics_info_page
    #------------
    @ property
    def complexity_level(self): # pragma: no cover
        """int: Complexity level. []
        """
        return self._complexity_level
    #------------
    @ property
    def flag_url_sc_get_farm(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. Define if data from /sc/${eid}/farm were loaded from SC
        """
        return self._flag_url_sc_get_farm
    #------------
    @ property
    def flag_url_sc_get_point(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. Define if data from /sc/${eid}/point were loaded from SC
        """
        return self._flag_url_sc_get_point
    #------------
    @ property
    def flag_url_sc_get_current(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. Define if data from /sc/${eid}/point/time_series/currents were loaded from SC
        """
        return self._flag_url_sc_get_current
    #------------
    @ property
    def flag_url_ec_get_farm(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. Define if data from /ec/${eid}/farm were loaded from EC
        """
        return self._flag_url_ec_get_farm
    #------------
    @ property
    def flag_url_ec_get_project(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. Define if data from /ec/${eid} were loaded from EC
        """
        return self._flag_url_ec_get_project
    #------------
    @ property
    def flag_url_mc_dimensions(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. Define if data from /mc/${eid}/dimensions were loaded from MC
        """
        return self._flag_url_mc_dimensions
    #------------
    @ property
    def flag_url_mc_general(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. Define if data from /mc/${eid}/general were loaded from MC
        """
        return self._flag_url_mc_general
    #------------
    @ property
    def flag_url_ed_get_results(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. Define if data from /ed/${eid}/results/${id} were loaded from ED
        """
        return self._flag_url_ed_get_results
    #------------
    @ property
    def flag_url_et_array(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. Define if data from /ed/${eid}/array were loaded from ET
        """
        return self._flag_url_et_array
    #------------
    @ property
    def flag_url_sk_get_env_impact(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. Define if data from /ed/${eid}/environmental_impact were loaded from SK
        """
        return self._flag_url_sk_get_env_impact
    #------------
    @ property
    def flag_url_lmo_results(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. Define if data from /ed/${eid}/phases were loaded from LMO
        """
        return self._flag_url_lmo_results
    #------------
    @ property
    def flag_url_lmo_catalogue(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. Define if data from LMO catalogue were loaded
        """
        return self._flag_url_lmo_catalogue
    #------------
    @ property
    def flag_url_ed_catalogue_static_cable(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. Define if data from ED static cable catalogue were loaded
        """
        return self._flag_url_ed_catalogue_static_cable
    #------------
    @ property
    def flag_url_ed_catalogue_dynamic_cable(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. Define if data from ED dynamic cable catalogue were loaded
        """
        return self._flag_url_ed_catalogue_dynamic_cable
    #------------
    @ property
    def flag_url_ed_catalogue_collection_point(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. Define if data from ED collection point catalogue were loaded
        """
        return self._flag_url_ed_catalogue_collection_point
    #------------
    @ property
    def entity_id(self): # pragma: no cover
        """int: Id of the entity []
        """
        return self._entity_id
    #------------
    @ property
    def entity_status(self): # pragma: no cover
        """int: Running status of the entity []
        """
        return self._entity_status
    #------------
    @ property
    def length_collection_point(self): # pragma: no cover
        """int: Used only if the module is in integrated mode. Define the number of collection points coming from the ED module. []
        """
        return self._length_collection_point
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ farm_info_page.setter
    def farm_info_page(self,val): # pragma: no cover
        self._farm_info_page=str(val)
    #------------
    @ device_info_page.setter
    def device_info_page(self,val): # pragma: no cover
        self._device_info_page=str(val)
    #------------
    @ electrical_info_page.setter
    def electrical_info_page(self,val): # pragma: no cover
        self._electrical_info_page=str(val)
    #------------
    @ logistics_info_page.setter
    def logistics_info_page(self,val): # pragma: no cover
        self._logistics_info_page=str(val)
    #------------
    @ complexity_level.setter
    def complexity_level(self,val): # pragma: no cover
        self._complexity_level=int(val)
    #------------
    @ flag_url_sc_get_farm.setter
    def flag_url_sc_get_farm(self,val): # pragma: no cover
        self._flag_url_sc_get_farm=str(val)
    #------------
    @ flag_url_sc_get_point.setter
    def flag_url_sc_get_point(self,val): # pragma: no cover
        self._flag_url_sc_get_point=str(val)
    #------------
    @ flag_url_sc_get_current.setter
    def flag_url_sc_get_current(self,val): # pragma: no cover
        self._flag_url_sc_get_current=str(val)
    #------------
    @ flag_url_ec_get_farm.setter
    def flag_url_ec_get_farm(self,val): # pragma: no cover
        self._flag_url_ec_get_farm=str(val)
    #------------
    @ flag_url_ec_get_project.setter
    def flag_url_ec_get_project(self,val): # pragma: no cover
        self._flag_url_ec_get_project=str(val)
    #------------
    @ flag_url_mc_dimensions.setter
    def flag_url_mc_dimensions(self,val): # pragma: no cover
        self._flag_url_mc_dimensions=str(val)
    #------------
    @ flag_url_mc_general.setter
    def flag_url_mc_general(self,val): # pragma: no cover
        self._flag_url_mc_general=str(val)
    #------------
    @ flag_url_ed_get_results.setter
    def flag_url_ed_get_results(self,val): # pragma: no cover
        self._flag_url_ed_get_results=str(val)
    #------------
    @ flag_url_et_array.setter
    def flag_url_et_array(self,val): # pragma: no cover
        self._flag_url_et_array=str(val)
    #------------
    @ flag_url_sk_get_env_impact.setter
    def flag_url_sk_get_env_impact(self,val): # pragma: no cover
        self._flag_url_sk_get_env_impact=str(val)
    #------------
    @ flag_url_lmo_results.setter
    def flag_url_lmo_results(self,val): # pragma: no cover
        self._flag_url_lmo_results=str(val)
    #------------
    @ flag_url_lmo_catalogue.setter
    def flag_url_lmo_catalogue(self,val): # pragma: no cover
        self._flag_url_lmo_catalogue=str(val)
    #------------
    @ flag_url_ed_catalogue_static_cable.setter
    def flag_url_ed_catalogue_static_cable(self,val): # pragma: no cover
        self._flag_url_ed_catalogue_static_cable=str(val)
    #------------
    @ flag_url_ed_catalogue_dynamic_cable.setter
    def flag_url_ed_catalogue_dynamic_cable(self,val): # pragma: no cover
        self._flag_url_ed_catalogue_dynamic_cable=str(val)
    #------------
    @ flag_url_ed_catalogue_collection_point.setter
    def flag_url_ed_catalogue_collection_point(self,val): # pragma: no cover
        self._flag_url_ed_catalogue_collection_point=str(val)
    #------------
    @ entity_id.setter
    def entity_id(self,val): # pragma: no cover
        self._entity_id=int(val)
    #------------
    @ entity_status.setter
    def entity_status(self,val): # pragma: no cover
        self._entity_status=int(val)
    #------------
    @ length_collection_point.setter
    def length_collection_point(self,val): # pragma: no cover
        self._length_collection_point=int(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:inputs_ESA_status"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:inputs_ESA_status"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("farm_info_page"):
            rep["farm_info_page"] = self.farm_info_page
        if self.is_set("device_info_page"):
            rep["device_info_page"] = self.device_info_page
        if self.is_set("electrical_info_page"):
            rep["electrical_info_page"] = self.electrical_info_page
        if self.is_set("logistics_info_page"):
            rep["logistics_info_page"] = self.logistics_info_page
        if self.is_set("complexity_level"):
            rep["complexity_level"] = self.complexity_level
        if self.is_set("flag_url_sc_get_farm"):
            rep["flag_url_sc_get_farm"] = self.flag_url_sc_get_farm
        if self.is_set("flag_url_sc_get_point"):
            rep["flag_url_sc_get_point"] = self.flag_url_sc_get_point
        if self.is_set("flag_url_sc_get_current"):
            rep["flag_url_sc_get_current"] = self.flag_url_sc_get_current
        if self.is_set("flag_url_ec_get_farm"):
            rep["flag_url_ec_get_farm"] = self.flag_url_ec_get_farm
        if self.is_set("flag_url_ec_get_project"):
            rep["flag_url_ec_get_project"] = self.flag_url_ec_get_project
        if self.is_set("flag_url_mc_dimensions"):
            rep["flag_url_mc_dimensions"] = self.flag_url_mc_dimensions
        if self.is_set("flag_url_mc_general"):
            rep["flag_url_mc_general"] = self.flag_url_mc_general
        if self.is_set("flag_url_ed_get_results"):
            rep["flag_url_ed_get_results"] = self.flag_url_ed_get_results
        if self.is_set("flag_url_et_array"):
            rep["flag_url_et_array"] = self.flag_url_et_array
        if self.is_set("flag_url_sk_get_env_impact"):
            rep["flag_url_sk_get_env_impact"] = self.flag_url_sk_get_env_impact
        if self.is_set("flag_url_lmo_results"):
            rep["flag_url_lmo_results"] = self.flag_url_lmo_results
        if self.is_set("flag_url_lmo_catalogue"):
            rep["flag_url_lmo_catalogue"] = self.flag_url_lmo_catalogue
        if self.is_set("flag_url_ed_catalogue_static_cable"):
            rep["flag_url_ed_catalogue_static_cable"] = self.flag_url_ed_catalogue_static_cable
        if self.is_set("flag_url_ed_catalogue_dynamic_cable"):
            rep["flag_url_ed_catalogue_dynamic_cable"] = self.flag_url_ed_catalogue_dynamic_cable
        if self.is_set("flag_url_ed_catalogue_collection_point"):
            rep["flag_url_ed_catalogue_collection_point"] = self.flag_url_ed_catalogue_collection_point
        if self.is_set("entity_id"):
            rep["entity_id"] = self.entity_id
        if self.is_set("entity_status"):
            rep["entity_status"] = self.entity_status
        if self.is_set("length_collection_point"):
            rep["length_collection_point"] = self.length_collection_point
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "farm_info_page"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "device_info_page"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "electrical_info_page"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "logistics_info_page"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "complexity_level"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_sc_get_farm"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_sc_get_point"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_sc_get_current"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_ec_get_farm"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_ec_get_project"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_mc_dimensions"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_mc_general"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_ed_get_results"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_et_array"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_sk_get_env_impact"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_lmo_results"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_lmo_catalogue"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_ed_catalogue_static_cable"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_ed_catalogue_dynamic_cable"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_ed_catalogue_collection_point"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "entity_id"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "entity_status"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "length_collection_point"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass

    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
