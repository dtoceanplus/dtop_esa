# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import input_materials
from . import vessel_consumption
from . import value
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class inputs_CFP():

    """Necessary inputs to run the carbon footprint assessment of a project
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._materials=input_materials.input_materials()
        self._materials_to_recycle=input_materials.input_materials()
        self._vessel_consumption=vessel_consumption.vessel_consumption()
        self._energy_produced=value.value()
        self._project_lifetime=value.value()
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def materials(self): # pragma: no cover
        """:obj:`~.input_materials.input_materials`: none
        """
        return self._materials
    #------------
    @ property
    def materials_to_recycle(self): # pragma: no cover
        """:obj:`~.input_materials.input_materials`: none
        """
        return self._materials_to_recycle
    #------------
    @ property
    def vessel_consumption(self): # pragma: no cover
        """:obj:`~.vessel_consumption.vessel_consumption`: none
        """
        return self._vessel_consumption
    #------------
    @ property
    def energy_produced(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._energy_produced
    #------------
    @ property
    def project_lifetime(self): # pragma: no cover
        """:obj:`~.value.value`: none
        """
        return self._project_lifetime
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ materials.setter
    def materials(self,val): # pragma: no cover
        self._materials=val
    #------------
    @ materials_to_recycle.setter
    def materials_to_recycle(self,val): # pragma: no cover
        self._materials_to_recycle=val
    #------------
    @ vessel_consumption.setter
    def vessel_consumption(self,val): # pragma: no cover
        self._vessel_consumption=val
    #------------
    @ energy_produced.setter
    def energy_produced(self,val): # pragma: no cover
        self._energy_produced=val
    #------------
    @ project_lifetime.setter
    def project_lifetime(self,val): # pragma: no cover
        self._project_lifetime=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:inputs_CFP"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:inputs_CFP"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("materials"):
            if (short and not(deep)):
                rep["materials"] = self.materials.type_rep()
            else:
                rep["materials"] = self.materials.prop_rep(short, deep)
        if self.is_set("materials_to_recycle"):
            if (short and not(deep)):
                rep["materials_to_recycle"] = self.materials_to_recycle.type_rep()
            else:
                rep["materials_to_recycle"] = self.materials_to_recycle.prop_rep(short, deep)
        if self.is_set("vessel_consumption"):
            if (short and not(deep)):
                rep["vessel_consumption"] = self.vessel_consumption.type_rep()
            else:
                rep["vessel_consumption"] = self.vessel_consumption.prop_rep(short, deep)
        if self.is_set("energy_produced"):
            if (short and not(deep)):
                rep["energy_produced"] = self.energy_produced.type_rep()
            else:
                rep["energy_produced"] = self.energy_produced.prop_rep(short, deep)
        if self.is_set("project_lifetime"):
            if (short and not(deep)):
                rep["project_lifetime"] = self.project_lifetime.type_rep()
            else:
                rep["project_lifetime"] = self.project_lifetime.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "materials"
        try :
            if data[varName] != None:
                self.materials=input_materials.input_materials()
                self.materials.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "materials_to_recycle"
        try :
            if data[varName] != None:
                self.materials_to_recycle=input_materials.input_materials()
                self.materials_to_recycle.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "vessel_consumption"
        try :
            if data[varName] != None:
                self.vessel_consumption=vessel_consumption.vessel_consumption()
                self.vessel_consumption.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "energy_produced"
        try :
            if data[varName] != None:
                self.energy_produced=value.value()
                self.energy_produced.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "project_lifetime"
        try :
            if data[varName] != None:
                self.project_lifetime=value.value()
                self.project_lifetime.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
