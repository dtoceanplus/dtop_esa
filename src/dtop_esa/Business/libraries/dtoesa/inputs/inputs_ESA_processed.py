# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import inputs_EIA
from . import inputs_ES
from . import inputs_SA
from . import inputs_CFP
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class inputs_ESA_processed():

    """
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._inputs_EIA=inputs_EIA.inputs_EIA()
        self._inputs_ES=inputs_ES.inputs_ES()
        self._inputs_SA=inputs_SA.inputs_SA()
        self._inputs_CFP=inputs_CFP.inputs_CFP()
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def inputs_EIA(self): # pragma: no cover
        """:obj:`~.inputs_EIA.inputs_EIA`: none
        """
        return self._inputs_EIA
    #------------
    @ property
    def inputs_ES(self): # pragma: no cover
        """:obj:`~.inputs_ES.inputs_ES`: none
        """
        return self._inputs_ES
    #------------
    @ property
    def inputs_SA(self): # pragma: no cover
        """:obj:`~.inputs_SA.inputs_SA`: none
        """
        return self._inputs_SA
    #------------
    @ property
    def inputs_CFP(self): # pragma: no cover
        """:obj:`~.inputs_CFP.inputs_CFP`: none
        """
        return self._inputs_CFP
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ inputs_EIA.setter
    def inputs_EIA(self,val): # pragma: no cover
        self._inputs_EIA=val
    #------------
    @ inputs_ES.setter
    def inputs_ES(self,val): # pragma: no cover
        self._inputs_ES=val
    #------------
    @ inputs_SA.setter
    def inputs_SA(self,val): # pragma: no cover
        self._inputs_SA=val
    #------------
    @ inputs_CFP.setter
    def inputs_CFP(self,val): # pragma: no cover
        self._inputs_CFP=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:inputs_ESA_processed"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:inputs_ESA_processed"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("inputs_EIA"):
            if (short and not(deep)):
                rep["inputs_EIA"] = self.inputs_EIA.type_rep()
            else:
                rep["inputs_EIA"] = self.inputs_EIA.prop_rep(short, deep)
        if self.is_set("inputs_ES"):
            if (short and not(deep)):
                rep["inputs_ES"] = self.inputs_ES.type_rep()
            else:
                rep["inputs_ES"] = self.inputs_ES.prop_rep(short, deep)
        if self.is_set("inputs_SA"):
            if (short and not(deep)):
                rep["inputs_SA"] = self.inputs_SA.type_rep()
            else:
                rep["inputs_SA"] = self.inputs_SA.prop_rep(short, deep)
        if self.is_set("inputs_CFP"):
            if (short and not(deep)):
                rep["inputs_CFP"] = self.inputs_CFP.type_rep()
            else:
                rep["inputs_CFP"] = self.inputs_CFP.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "inputs_EIA"
        try :
            if data[varName] != None:
                self.inputs_EIA=inputs_EIA.inputs_EIA()
                self.inputs_EIA.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "inputs_ES"
        try :
            if data[varName] != None:
                self.inputs_ES=inputs_ES.inputs_ES()
                self.inputs_ES.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "inputs_SA"
        try :
            if data[varName] != None:
                self.inputs_SA=inputs_SA.inputs_SA()
                self.inputs_SA.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "inputs_CFP"
        try :
            if data[varName] != None:
                self.inputs_CFP=inputs_CFP.inputs_CFP()
                self.inputs_CFP.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
