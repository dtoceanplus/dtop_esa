# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import device_dimension
from . import input_materials
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class electrical_info():

    """Global information about the electrical parts
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._energy_produced=0.0
        self._Surface_Area_of_electrical_Part=0.0
        self._footprint=0.0
        self._collection_points_coordinates_x=np.zeros(shape=(1), dtype=float)
        self._collection_points_coordinates_y=np.zeros(shape=(1), dtype=float)
        self._collection_points_dimensions=device_dimension.device_dimension()
        self._collection_points_dimensions.description = 'if present'
        self._substation=False
        self._Burial=False
        self._Fishery_Restriction_Surface_around_cables=0.0
        self._materials=input_materials.input_materials()
        self._materials_to_recycle=input_materials.input_materials()
        self._measured_noise=0.0
        self._Measured_Electric_Field=0.0
        self._Measured_Magnetic_Field=0.0
        self._Measured_Temperature=0.0
        self._Initial_material_list=[]
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def energy_produced(self): # pragma: no cover
        """float: Annual energy produced []
        """
        return self._energy_produced
    #------------
    @ property
    def Surface_Area_of_electrical_Part(self): # pragma: no cover
        """float: Colonisable surface of electrical parts []
        """
        return self._Surface_Area_of_electrical_Part
    #------------
    @ property
    def footprint(self): # pragma: no cover
        """float: Footprint of electrical parts []
        """
        return self._footprint
    #------------
    @ property
    def collection_points_coordinates_x(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:if present dim(*) []
        """
        return self._collection_points_coordinates_x
    #------------
    @ property
    def collection_points_coordinates_y(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:if present dim(*) []
        """
        return self._collection_points_coordinates_y
    #------------
    @ property
    def collection_points_dimensions(self): # pragma: no cover
        """:obj:`~.device_dimension.device_dimension`: if present
        """
        return self._collection_points_dimensions
    #------------
    @ property
    def substation(self): # pragma: no cover
        """bool: True or False []
        """
        return self._substation
    #------------
    @ property
    def Burial(self): # pragma: no cover
        """bool: True or False []
        """
        return self._Burial
    #------------
    @ property
    def Fishery_Restriction_Surface_around_cables(self): # pragma: no cover
        """float: Total Surface around cables closed to fisheries []
        """
        return self._Fishery_Restriction_Surface_around_cables
    #------------
    @ property
    def materials(self): # pragma: no cover
        """:obj:`~.input_materials.input_materials`: none
        """
        return self._materials
    #------------
    @ property
    def materials_to_recycle(self): # pragma: no cover
        """:obj:`~.input_materials.input_materials`: none
        """
        return self._materials_to_recycle
    #------------
    @ property
    def measured_noise(self): # pragma: no cover
        """float: Measured noise around cables [dB re 1muPa]
        """
        return self._measured_noise
    #------------
    @ property
    def Measured_Electric_Field(self): # pragma: no cover
        """float: Measured electrical fields around cables [V/m]
        """
        return self._Measured_Electric_Field
    #------------
    @ property
    def Measured_Magnetic_Field(self): # pragma: no cover
        """float: Measured magnetic fields around cables [uT]
        """
        return self._Measured_Magnetic_Field
    #------------
    @ property
    def Measured_Temperature(self): # pragma: no cover
        """float: temperature around cables [°C]
        """
        return self._Measured_Temperature
    #------------
    @ property
    def Initial_material_list(self): # pragma: no cover
        """list: List of material names. Only used in the GUI.
        """
        return self._Initial_material_list
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ energy_produced.setter
    def energy_produced(self,val): # pragma: no cover
        self._energy_produced=float(val)
    #------------
    @ Surface_Area_of_electrical_Part.setter
    def Surface_Area_of_electrical_Part(self,val): # pragma: no cover
        self._Surface_Area_of_electrical_Part=float(val)
    #------------
    @ footprint.setter
    def footprint(self,val): # pragma: no cover
        self._footprint=float(val)
    #------------
    @ collection_points_coordinates_x.setter
    def collection_points_coordinates_x(self,val): # pragma: no cover
        self._collection_points_coordinates_x=val
    #------------
    @ collection_points_coordinates_y.setter
    def collection_points_coordinates_y(self,val): # pragma: no cover
        self._collection_points_coordinates_y=val
    #------------
    @ collection_points_dimensions.setter
    def collection_points_dimensions(self,val): # pragma: no cover
        self._collection_points_dimensions=val
    #------------
    @ substation.setter
    def substation(self,val): # pragma: no cover
        self._substation=val
    #------------
    @ Burial.setter
    def Burial(self,val): # pragma: no cover
        self._Burial=val
    #------------
    @ Fishery_Restriction_Surface_around_cables.setter
    def Fishery_Restriction_Surface_around_cables(self,val): # pragma: no cover
        self._Fishery_Restriction_Surface_around_cables=float(val)
    #------------
    @ materials.setter
    def materials(self,val): # pragma: no cover
        self._materials=val
    #------------
    @ materials_to_recycle.setter
    def materials_to_recycle(self,val): # pragma: no cover
        self._materials_to_recycle=val
    #------------
    @ measured_noise.setter
    def measured_noise(self,val): # pragma: no cover
        self._measured_noise=float(val)
    #------------
    @ Measured_Electric_Field.setter
    def Measured_Electric_Field(self,val): # pragma: no cover
        self._Measured_Electric_Field=float(val)
    #------------
    @ Measured_Magnetic_Field.setter
    def Measured_Magnetic_Field(self,val): # pragma: no cover
        self._Measured_Magnetic_Field=float(val)
    #------------
    @ Measured_Temperature.setter
    def Measured_Temperature(self,val): # pragma: no cover
        self._Measured_Temperature=float(val)
    #------------
    @ Initial_material_list.setter
    def Initial_material_list(self,val): # pragma: no cover
        self._Initial_material_list=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:electrical_info"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:electrical_info"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("energy_produced"):
            rep["energy_produced"] = self.energy_produced
        if self.is_set("Surface_Area_of_electrical_Part"):
            rep["Surface_Area_of_electrical_Part"] = self.Surface_Area_of_electrical_Part
        if self.is_set("footprint"):
            rep["footprint"] = self.footprint
        if self.is_set("collection_points_coordinates_x"):
            if (short):
                rep["collection_points_coordinates_x"] = str(self.collection_points_coordinates_x.shape)
            else:
                try:
                    rep["collection_points_coordinates_x"] = self.collection_points_coordinates_x.tolist()
                except:
                    rep["collection_points_coordinates_x"] = self.collection_points_coordinates_x
        if self.is_set("collection_points_coordinates_y"):
            if (short):
                rep["collection_points_coordinates_y"] = str(self.collection_points_coordinates_y.shape)
            else:
                try:
                    rep["collection_points_coordinates_y"] = self.collection_points_coordinates_y.tolist()
                except:
                    rep["collection_points_coordinates_y"] = self.collection_points_coordinates_y
        if self.is_set("collection_points_dimensions"):
            if (short and not(deep)):
                rep["collection_points_dimensions"] = self.collection_points_dimensions.type_rep()
            else:
                rep["collection_points_dimensions"] = self.collection_points_dimensions.prop_rep(short, deep)
        if self.is_set("substation"):
            rep["substation"] = self.substation
        if self.is_set("Burial"):
            rep["Burial"] = self.Burial
        if self.is_set("Fishery_Restriction_Surface_around_cables"):
            rep["Fishery_Restriction_Surface_around_cables"] = self.Fishery_Restriction_Surface_around_cables
        if self.is_set("materials"):
            if (short and not(deep)):
                rep["materials"] = self.materials.type_rep()
            else:
                rep["materials"] = self.materials.prop_rep(short, deep)
        if self.is_set("materials_to_recycle"):
            if (short and not(deep)):
                rep["materials_to_recycle"] = self.materials_to_recycle.type_rep()
            else:
                rep["materials_to_recycle"] = self.materials_to_recycle.prop_rep(short, deep)
        if self.is_set("measured_noise"):
            rep["measured_noise"] = self.measured_noise
        if self.is_set("Measured_Electric_Field"):
            rep["Measured_Electric_Field"] = self.Measured_Electric_Field
        if self.is_set("Measured_Magnetic_Field"):
            rep["Measured_Magnetic_Field"] = self.Measured_Magnetic_Field
        if self.is_set("Measured_Temperature"):
            rep["Measured_Temperature"] = self.Measured_Temperature
        if self.is_set("Initial_material_list"):
            rep["Initial_material_list"] = self.Initial_material_list
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "energy_produced"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Surface_Area_of_electrical_Part"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "footprint"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "collection_points_coordinates_x"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "collection_points_coordinates_y"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "collection_points_dimensions"
        try :
            if data[varName] != None:
                self.collection_points_dimensions=device_dimension.device_dimension()
                self.collection_points_dimensions.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "substation"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Burial"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Fishery_Restriction_Surface_around_cables"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "materials"
        try :
            if data[varName] != None:
                self.materials=input_materials.input_materials()
                self.materials.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "materials_to_recycle"
        try :
            if data[varName] != None:
                self.materials_to_recycle=input_materials.input_materials()
                self.materials_to_recycle.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "measured_noise"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Measured_Electric_Field"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Measured_Magnetic_Field"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Measured_Temperature"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Initial_material_list"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
