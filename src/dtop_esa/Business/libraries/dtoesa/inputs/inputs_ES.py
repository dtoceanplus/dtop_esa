# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import input_species
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class inputs_ES():

    """Necessary inputs to run the endangered species assessment of the ESA module
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._Coordinates_of_the_farm_x=0.0
        self._Coordinates_of_the_farm_y=0.0
        self._species_user=[]
        self._inputs_SC=[]
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def Coordinates_of_the_farm_x(self): # pragma: no cover
        """float: none []
        """
        return self._Coordinates_of_the_farm_x
    #------------
    @ property
    def Coordinates_of_the_farm_y(self): # pragma: no cover
        """float: none []
        """
        return self._Coordinates_of_the_farm_y
    #------------
    @ property
    def species_user(self): # pragma: no cover
        """:obj:`list` of :obj:`~.input_species.input_species`: none
        """
        return self._species_user
    #------------
    @ property
    def inputs_SC(self): # pragma: no cover
        """:obj:`list` of :obj:`~.input_species.input_species`: none
        """
        return self._inputs_SC
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ Coordinates_of_the_farm_x.setter
    def Coordinates_of_the_farm_x(self,val): # pragma: no cover
        self._Coordinates_of_the_farm_x=float(val)
    #------------
    @ Coordinates_of_the_farm_y.setter
    def Coordinates_of_the_farm_y(self,val): # pragma: no cover
        self._Coordinates_of_the_farm_y=float(val)
    #------------
    @ species_user.setter
    def species_user(self,val): # pragma: no cover
        self._species_user=val
    #------------
    @ inputs_SC.setter
    def inputs_SC(self,val): # pragma: no cover
        self._inputs_SC=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:inputs_ES"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:inputs_ES"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("Coordinates_of_the_farm_x"):
            rep["Coordinates_of_the_farm_x"] = self.Coordinates_of_the_farm_x
        if self.is_set("Coordinates_of_the_farm_y"):
            rep["Coordinates_of_the_farm_y"] = self.Coordinates_of_the_farm_y
        if self.is_set("species_user"):
            rep["species_user"] = []
            for i in range(0,len(self.species_user)):
                if (short and not(deep)):
                    itemType = self.species_user[i].type_rep()
                    rep["species_user"].append( itemType )
                else:
                    rep["species_user"].append( self.species_user[i].prop_rep(short, deep) )
        else:
            rep["species_user"] = []
        if self.is_set("inputs_SC"):
            rep["inputs_SC"] = []
            for i in range(0,len(self.inputs_SC)):
                if (short and not(deep)):
                    itemType = self.inputs_SC[i].type_rep()
                    rep["inputs_SC"].append( itemType )
                else:
                    rep["inputs_SC"].append( self.inputs_SC[i].prop_rep(short, deep) )
        else:
            rep["inputs_SC"] = []
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "Coordinates_of_the_farm_x"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Coordinates_of_the_farm_y"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "species_user"
        try :
            if data[varName] != None:
                self.species_user=[]
                for i in range(0,len(data[varName])):
                    self.species_user.append(input_species.input_species())
                    self.species_user[i].loadFromJSONDict(data[varName][i])
            else:
                self.species_user = []
        except :
            pass
        varName = "inputs_SC"
        try :
            if data[varName] != None:
                self.inputs_SC=[]
                for i in range(0,len(data[varName])):
                    self.inputs_SC.append(input_species.input_species())
                    self.inputs_SC[i].loadFromJSONDict(data[varName][i])
            else:
                self.inputs_SC = []
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
