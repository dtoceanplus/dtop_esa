# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import input_hydro
from . import input_elec
from . import input_moor
from . import input_insta
from . import input_maint
from . import receptors_dict
from . import protected_dict
from . import weighting_dict
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class inputs_EIA():

    """List of all inputs necessary to run the environmental impacts assessment of the ESA module
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._input_hydro=input_hydro.input_hydro()
        self._input_elec=input_elec.input_elec()
        self._input_moor=input_moor.input_moor()
        self._input_insta=input_insta.input_insta()
        self._input_maint=input_maint.input_maint()
        self._receptors_dict=receptors_dict.receptors_dict()
        self._protected_dict=protected_dict.protected_dict()
        self._weighting_dict=weighting_dict.weighting_dict()
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def input_hydro(self): # pragma: no cover
        """:obj:`~.input_hydro.input_hydro`: none
        """
        return self._input_hydro
    #------------
    @ property
    def input_elec(self): # pragma: no cover
        """:obj:`~.input_elec.input_elec`: none
        """
        return self._input_elec
    #------------
    @ property
    def input_moor(self): # pragma: no cover
        """:obj:`~.input_moor.input_moor`: none
        """
        return self._input_moor
    #------------
    @ property
    def input_insta(self): # pragma: no cover
        """:obj:`~.input_insta.input_insta`: none
        """
        return self._input_insta
    #------------
    @ property
    def input_maint(self): # pragma: no cover
        """:obj:`~.input_maint.input_maint`: none
        """
        return self._input_maint
    #------------
    @ property
    def receptors_dict(self): # pragma: no cover
        """:obj:`~.receptors_dict.receptors_dict`: none
        """
        return self._receptors_dict
    #------------
    @ property
    def protected_dict(self): # pragma: no cover
        """:obj:`~.protected_dict.protected_dict`: none
        """
        return self._protected_dict
    #------------
    @ property
    def weighting_dict(self): # pragma: no cover
        """:obj:`~.weighting_dict.weighting_dict`: none
        """
        return self._weighting_dict
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ input_hydro.setter
    def input_hydro(self,val): # pragma: no cover
        self._input_hydro=val
    #------------
    @ input_elec.setter
    def input_elec(self,val): # pragma: no cover
        self._input_elec=val
    #------------
    @ input_moor.setter
    def input_moor(self,val): # pragma: no cover
        self._input_moor=val
    #------------
    @ input_insta.setter
    def input_insta(self,val): # pragma: no cover
        self._input_insta=val
    #------------
    @ input_maint.setter
    def input_maint(self,val): # pragma: no cover
        self._input_maint=val
    #------------
    @ receptors_dict.setter
    def receptors_dict(self,val): # pragma: no cover
        self._receptors_dict=val
    #------------
    @ protected_dict.setter
    def protected_dict(self,val): # pragma: no cover
        self._protected_dict=val
    #------------
    @ weighting_dict.setter
    def weighting_dict(self,val): # pragma: no cover
        self._weighting_dict=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:inputs_EIA"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:inputs_EIA"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("input_hydro"):
            if (short and not(deep)):
                rep["input_hydro"] = self.input_hydro.type_rep()
            else:
                rep["input_hydro"] = self.input_hydro.prop_rep(short, deep)
        if self.is_set("input_elec"):
            if (short and not(deep)):
                rep["input_elec"] = self.input_elec.type_rep()
            else:
                rep["input_elec"] = self.input_elec.prop_rep(short, deep)
        if self.is_set("input_moor"):
            if (short and not(deep)):
                rep["input_moor"] = self.input_moor.type_rep()
            else:
                rep["input_moor"] = self.input_moor.prop_rep(short, deep)
        if self.is_set("input_insta"):
            if (short and not(deep)):
                rep["input_insta"] = self.input_insta.type_rep()
            else:
                rep["input_insta"] = self.input_insta.prop_rep(short, deep)
        if self.is_set("input_maint"):
            if (short and not(deep)):
                rep["input_maint"] = self.input_maint.type_rep()
            else:
                rep["input_maint"] = self.input_maint.prop_rep(short, deep)
        if self.is_set("receptors_dict"):
            if (short and not(deep)):
                rep["receptors_dict"] = self.receptors_dict.type_rep()
            else:
                rep["receptors_dict"] = self.receptors_dict.prop_rep(short, deep)
        if self.is_set("protected_dict"):
            if (short and not(deep)):
                rep["protected_dict"] = self.protected_dict.type_rep()
            else:
                rep["protected_dict"] = self.protected_dict.prop_rep(short, deep)
        if self.is_set("weighting_dict"):
            if (short and not(deep)):
                rep["weighting_dict"] = self.weighting_dict.type_rep()
            else:
                rep["weighting_dict"] = self.weighting_dict.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "input_hydro"
        try :
            if data[varName] != None:
                self.input_hydro=input_hydro.input_hydro()
                self.input_hydro.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "input_elec"
        try :
            if data[varName] != None:
                self.input_elec=input_elec.input_elec()
                self.input_elec.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "input_moor"
        try :
            if data[varName] != None:
                self.input_moor=input_moor.input_moor()
                self.input_moor.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "input_insta"
        try :
            if data[varName] != None:
                self.input_insta=input_insta.input_insta()
                self.input_insta.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "input_maint"
        try :
            if data[varName] != None:
                self.input_maint=input_maint.input_maint()
                self.input_maint.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "receptors_dict"
        try :
            if data[varName] != None:
                self.receptors_dict=receptors_dict.receptors_dict()
                self.receptors_dict.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "protected_dict"
        try :
            if data[varName] != None:
                self.protected_dict=protected_dict.protected_dict()
                self.protected_dict.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "weighting_dict"
        try :
            if data[varName] != None:
                self.weighting_dict=weighting_dict.weighting_dict()
                self.weighting_dict.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
