# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import inputs_ESA
from . import URL
#------------------------------------
# @ USER DEFINED IMPORTS START
from . import input_species
from collections import Counter

# @ USER DEFINED IMPORTS END
#------------------------------------

class Inputs():

    """data model representing input data
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._entity_name=''
        self._inputs_ESA=inputs_ESA.inputs_ESA()
        self._inputs_ESA.description = 'All input data necessary to run ESA module'
        self._run_mode='standalone'
        self._url_sc_get_farm=URL.URL()
        self._url_sc_get_farm.description = 'url calling SC module for reading farm direct values and infos : /sc/{ProjectId}/farm, receiving dataformat dtosc_farm_outputs_FarmResults.yaml'
        self._url_sc_get_point=URL.URL()
        self._url_sc_get_point.description = 'url calling SC module for reading farm direct values and infos : /sc/{ProjectId}/point, receiving dataformat dtosc_farm_outputs_FarmResults.yaml'
        self._url_sc_get_current=URL.URL()
        self._url_sc_get_current.description = 'url calling SC module for reading farm direct values and infos : /sc/{ProjectId}/point/time_series/currents, receiving dataformat dtosc_farm_outputs_FarmResults.yaml'
        self._url_ec_get_farm=URL.URL()
        self._url_ec_get_farm.description = 'url calling EC module for reading farm layout : /ec/{ecId}/farm, receiving dataformat Farm.yaml'
        self._url_ec_get_project=URL.URL()
        self._url_ec_get_project.description = 'url calling EC module for reading project design : /ec/{ecId}, receiving dataformat design.yaml'
        self._url_mc_dimensions=URL.URL()
        self._url_mc_dimensions.description = 'url calling MC module for reading machine dimensions : /mc/{mcId}/dimensions, receiving dataformat Dimensions.yaml'
        self._url_mc_general=URL.URL()
        self._url_mc_general.description = 'url calling MC module for reading general information (materials) : /mc/{mcId}/general, receiving dataformat General.yaml'
        self._url_ed_get_results=URL.URL()
        self._url_ed_get_results.description = 'url calling ET module for reading  farm layout : /api/energy-deliv-studies/{edId}/results/{ednwId}  , receiving dataformat EnergyDelivOutputResults.yaml'
        self._url_et_array=URL.URL()
        self._url_et_array.description = 'url calling ED module for reading materials for pto : /energy_transf/{etId}/array  , receiving dataformat array.yaml'
        self._url_sk_get_env_impact=URL.URL()
        self._url_sk_get_env_impact.description = 'url calling SK module for reading farm layout : /sk/{ProjectId}/environmental_impact  , receiving dataformat dtosk_outputs_EnvironmentalImpact.yaml'
        self._url_lmo_insta_logistic=URL.URL()
        self._url_lmo_insta_logistic.description = 'url calling LMO module for reading all results : /api/{lmoid}/phases  , receiving dataformat phases.yaml'
        self._url_lmo_insta_consumption=URL.URL()
        self._url_lmo_insta_consumption.description = 'url calling LMO module for reading all results : /api/{lmoid}/phases  , receiving dataformat phases.yaml'
        self._url_lmo_insta_downtime=URL.URL()
        self._url_lmo_insta_downtime.description = 'url calling LMO module for reading all results : /api/{lmoid}/phases  , receiving dataformat phases.yaml'
        self._url_lmo_maint_logistic=URL.URL()
        self._url_lmo_maint_logistic.description = 'url calling LMO module for reading all results : /api/{lmoid}/phases  , receiving dataformat phases.yaml'
        self._url_lmo_maint_consumption=URL.URL()
        self._url_lmo_maint_consumption.description = 'url calling LMO module for reading all results : /api/{lmoid}/phases  , receiving dataformat phases.yaml'
        self._url_lmo_decom_logistic=URL.URL()
        self._url_lmo_decom_logistic.description = 'url calling LMO module for reading all results : /api/{lmoid}/phases  , receiving dataformat phases.yaml'
        self._url_lmo_decom_consumption=URL.URL()
        self._url_lmo_decom_consumption.description = 'url calling LMO module for reading all results : /api/{lmoid}/phases  , receiving dataformat phases.yaml'
        self._url_lmo_catalogue=URL.URL()
        self._url_lmo_catalogue.description = 'url calling LMO catalogue for reading vessels : /vessels  , receiving dataformat Vessels_get.yaml'
        self._url_ed_catalogue_static_cable=URL.URL()
        self._url_ed_catalogue_static_cable.description = 'url calling ED catalogue for reading static cables catalogue : /static_cable  , receiving dataformat Vessels_get.yaml'
        self._url_ed_catalogue_dynamic_cable=URL.URL()
        self._url_ed_catalogue_dynamic_cable.description = 'url calling ED catalogue for reading dynamic cables catalogue : /dynamic_cable  , receiving dataformat Vessels_get.yaml'
        self._url_ed_catalogue_collection_point=URL.URL()
        self._url_ed_catalogue_collection_point.description = 'url calling ED catalogue for reading collection point catalogue : /collection_point  , receiving dataformat Vessels_get.yaml'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
    def populate(self):

        if self.run_mode=='dto':


            # Populate data from ec
            self.populate_from_url_ec_get_farm()

            # Populate data from lmo
            self.populate_from_url_lmo_results()

            # Populate data from sc
            self.populate_from_url_sc_get_farm()

            # Populate data from sk
            self.populate_from_url_sk_get_env_impact()

            # Populate data from ed # to finish when materials added to catalogue
            self.populate_from_url_ed_get_results()

            # Populate data from mc
            self.populate_from_url_mc_general()
            self.populate_from_url_mc_dimensions()

            # Populate data from et
            self.populate_from_url_et_array()






    def populate_from_url_sc_get_farm(self):

        data = self.fetch_data_from_url(self.url_sc_get_farm,'url_sc_get_farm')
        data_point = self.fetch_data_from_url(self.url_sc_get_point,'url_sc_get_point')
        data_current = self.fetch_data_from_url(self.url_sc_get_current,'url_sc_get_current')

        if(len(data)!=0):

            try:
                # Fetch data
                data = data["data"]
                data_point = data_point["data"]
                data_current = data_current["data"]
            except:
                pass
            
            # Populate input structure with data from SC module
            self.inputs_ESA.farm_info.water_depth = data.get('direct_values')['bathymetry']['value']
            list_soil_type = data.get('direct_values')['seabed_type']['value']
            maj_soil_type = Counter(list_soil_type).most_common(1)
            self.inputs_ESA.farm_info.soil_type = maj_soil_type[0][0]

            if (maj_soil_type[0][0] == 'No data'):
                try:
                    maj_soil_type2 = Counter(list_soil_type).most_common(2)
                    self.inputs_ESA.farm_info.soil_type = maj_soil_type2[1][0]
                except:
                    pass
            else:
                self.inputs_ESA.farm_info.soil_type = maj_soil_type[0][0]
            
            # endangered species
            species_name = data.get('direct_values')['marine_species']['species']
            species_proba = data.get('direct_values')['marine_species']['probability']
            liste_species = species_name
            proba_species = species_proba[0]

            for i in range(len(proba_species)):
                if (proba_species[i] >1):
                    proba_species[i] = -1

            for point in range(len(species_proba)):
                for spe in range(len(species_proba[point])):
                    if ((species_proba[point][spe]>proba_species[liste_species.index(species_name[spe])])and(species_proba[point][spe]<=1)):
                        proba_species[liste_species.index(species_name[spe])] = species_proba[point][spe]
                    else:
                        pass
            # for key, value in self.inputs_ESA.farm_info.protected_species.items():
            # if spe in key.species_name or self.inputs_ESA.farm_info.protected_species:
            if (len(self.inputs_ESA.farm_info.protected_species)==0):
                for esp in range(len(liste_species)):
                    if(proba_species[esp]==-1):
                        pass
                    else:
                        espece = input_species.input_species()
                        espece.name = liste_species[esp]
                        espece.probability_of_presence = proba_species[esp]
                        self.inputs_ESA.farm_info.protected_species.append(espece)
            if (self.inputs_ESA.farm_info.protected_species_length == 0):
                self.inputs_ESA.farm_info.protected_species_length = len(self.inputs_ESA.farm_info.protected_species)

            self.inputs_ESA.farm_info.Coordinates_of_the_farm_x = data_point.get('latitude')
            self.inputs_ESA.farm_info.Coordinates_of_the_farm_y = data_point.get('longitude')
            self.inputs_ESA.farm_info.total_surface_area = data.get('info')['surface']*(10**6)

            mag = data_current.get('mag')['values']
            ind = mag.index(max(mag))
            self.inputs_ESA.farm_info.current_direction = data_current.get('theta')['values'][ind]

    def populate_from_url_ec_get_farm(self):

        # Fetch data
        data = self.fetch_data_from_url(self.url_ec_get_farm,'url_ec_get_farm')
        if(len(data)!=0):
            data_project = self.fetch_data_from_url(self.url_ec_get_project,'url_ec_get_project')
            self.inputs_ESA.device_info.machine_type = data_project.get("type")
            # Populate input structure with data from EC module
            self.inputs_ESA.device_info.coordinates_devices_y=data.get("layout")['easting']
            self.inputs_ESA.device_info.coordinates_devices_x=data.get("layout")['northing']
            self.inputs_ESA.device_info.number_of_device=data.get("number_devices")
            resource_red = data.get("resource_reduction")
            if resource_red <1:
                self.inputs_ESA.device_info.resource_reduction= resource_red
            else:
                self.inputs_ESA.device_info.resource_reduction= resource_red/100
    def populate_from_url_sk_get_env_impact(self):

        # Fetch data
        data = self.fetch_data_from_url(self.url_sk_get_env_impact,'url_sk_get_env_impact')

        if(len(data)!=0):

            data = data["data"]
            # Populate input structure with data from SK module
            self.inputs_ESA.device_info.foundation.footprint = data.get("farm")["footprint"]
            self.inputs_ESA.device_info.foundation.submerged_surface = data.get("farm")["submerged_surface"]
            materials_sk = data.get("farm")["material_quantity_list"]

            # materials_db = ['ABS', 'unalloyed_steel', 'low_alloyed_steel', 'aluminium', 'concrete', 'brick', 'copper', 'reinforcing_steel', 'fiberglass', 'synthetic_fiber', 'cast_iron', 'gravel', 'oil', 'brass', 'magnesium', 'kraft_paper', 'alkyd_paint', 'lead', 'polybutadiene', 'polyethylene', 'polypropylene', 'PVC', 'epoxy_resin', 'sand', 'ceramic', 'zinc']
            for liste in range(len(materials_sk)):
                mat = materials_sk[liste]["material_name"]
                qty = materials_sk[liste]["material_quantity"]
                if (mat == 'nylon'):
                    mat = 'synthetic_fiber'
                if (mat == 'steel'):
                    mat = 'unalloyed_steel'

                if ((mat in self.inputs_ESA.device_info.foundation.materials.prop_rep())and(getattr(self.inputs_ESA.device_info.foundation.materials,mat).value == 0)):
                    getattr(self.inputs_ESA.device_info.foundation.materials,mat).value += qty
                else:
                    pass

    def populate_from_url_ed_get_results(self):

        # Fetch data
        data = self.fetch_data_from_url(self.url_ed_get_results,'url_ed_get_results')
        # catalogue_static = self.fetch_data_from_url(self.url_ed_catalogue_static_cable,'url_ed_catalogue_static_cable')
        # catalogue_dynamic = self.fetch_data_from_url(self.url_ed_catalogue_dynamic_cable,'url_ed_catalogue_dynamic_cable')
        # catalogue_collection = self.fetch_data_from_url(self.url_ed_catalogue_collection_point,'url_ed_catalogue_collection_point')
        # catalogue = self.fetch_data_from_url(self.url_ed_get_catalogue,'url_ed_get_catalogue')

        if(len(data)!=0):
            # Populate input structure with data from EC module
            array_cable_total_length = data.get("array_cable_total_length")
            export_cable_total_length = data.get("export_cable_total_length")
            umbilical_cable_total_length = data.get("umbilical_cable_total_length")
            connector_dict = data.get("connectors_dict")
            collection_point_dict = data.get("collection_point_dict")
            # diameter in mm
            footprint_total = []
            coord_x = []
            coord_y = []
            surface_cable_tot = []
            materials = []

            for cable in data.get("cable_dict"):
                diameter = cable["diameter"]*0.001       #mm to m
                length = cable["length"]
                footprint = diameter*length
                surface=(2*3.14*(diameter/2)*length)
                footprint_total.append(footprint)
                surface_cable_tot.append(surface)
                for mat in range(len(cable["materials"])):
                    materials.append(cable["materials"][mat])

            if (data.get("umbilical_dict")!= None):
                for cable in data.get("umbilical_dict"):
                    diameter = cable["diameter"]
                    length = cable["length"]
                    footprint = diameter*length
                    surface=(2*3.14*(diameter/2)*length)
                    footprint_total.append(footprint)
                    surface_cable_tot.append(surface)
                    for mat in range(len(cable["materials"])):
                        materials.append(cable["materials"][mat])

            for connector in connector_dict:
                for mat in range(len(connector["materials"])):
                    materials.append(connector["materials"][mat])

            for ind in data.get("b_o_m_dict")["install_type"]:
                nb = data.get("b_o_m_dict")["quantity"][data.get("b_o_m_dict")["install_type"].index(ind)]

                if ind =="substation":
                    self.inputs_ESA.electrical_info.substation = True
                    number_of_substation = nb
                    coord_x.append(data.get("b_o_m_dict")["utm_x"][data.get("b_o_m_dict")["install_type"].index(ind)])
                    coord_y.append(data.get("b_o_m_dict")["utm_y"][data.get("b_o_m_dict")["install_type"].index(ind)])

            if (len(data.get("collection_point_dict")) != 0):
                for sub in data.get("collection_point_dict"):
                        # for attributs in catalogue_collection.get("data"):
                        #     if catalogue_id == attributs["id"]:
                    self.inputs_ESA.electrical_info.collection_points_dimensions.width = sub['width']
                    self.inputs_ESA.electrical_info.collection_points_dimensions.height = sub['height']
                    self.inputs_ESA.electrical_info.collection_points_dimensions.length = sub['length']

                    surface_substation = (sub['width']*sub["length"]*2 + 4*sub['width']*sub['height'])*number_of_substation
                    for mat in range(len(sub["materials"])):
                        materials.append(sub["materials"][mat])

                    if sub["CP_height_seabed"]==0:
                        footprint_substation = collection["width"]*collection["length"]*number_of_substation
                        footprint_total.append(footprint)
                    else:
                        footprint_substation = 0
            else:
                footprint_substation = 0
                surface_substation = 0
            # self.inputs_ESA.electrical_info.collection_points_dimensions.wet_area = surface_substation

            self.inputs_ESA.electrical_info.energy_produced = data.get("annual_yield")
            if data.get("cable_dict")[0]["burial_depth"][0]>0:
                self.inputs_ESA.electrical_info.Burial = True
                self.inputs_ESA.electrical_info.Surface_Area_of_electrical_Part = surface_substation - footprint_substation
            else:
                self.inputs_ESA.electrical_info.Burial = False
                self.inputs_ESA.electrical_info.Surface_Area_of_electrical_Part = (surface_substation + sum(surface_cable_tot)) - sum(footprint_total)

            self.inputs_ESA.electrical_info.footprint = sum(footprint_total)
            self.inputs_ESA.electrical_info.collection_points_coordinates_x = coord_x
            self.inputs_ESA.electrical_info.collection_points_coordinates_y = coord_y
            self.inputs_ESA.electrical_info.collection_points_dimensions.wet_area = surface_substation
            for liste in range(len(materials)):
                mat = materials[liste]["material_name"]
                self.inputs_ESA.electrical_info.Initial_material_list.append(mat)
                qty = materials[liste]["material_quantity"]

                if mat in self.inputs_ESA.electrical_info.materials.prop_rep():
                    getattr(self.inputs_ESA.electrical_info.materials,mat).value += qty
                else:
                    pass

    def populate_from_url_mc_general(self):
        # Fetch data
        data = self.fetch_data_from_url(self.url_mc_general,'url_mc_general')
        qty_US = 0

        if(len(data)!=0):
            self.inputs_ESA.device_info.floating = data.get('floating')
            # Populate input structure with data from MC module general
            materials_mc = data.get('materials') # to process
            for liste in range(len(materials_mc)):
                mat = materials_mc[liste]["material_name"]
                if (mat == 'undefined'):
                    mat = 'unalloyed_steel'
                if (mat == 'abs'):
                    mat = 'ABS'
                if (mat == 'low_unalloyed_steel'):
                    mat = 'low_alloyed_steel'
                if (mat == 'steel'):
                    mat = 'reinforcing_steel'
                if (mat == 'fibre'):
                    mat = 'fiberglass'
                if (mat == 'alkyd_pain'):
                    mat = 'alkyd_paint'
                if (mat == 'granulate'):
                    mat = 'polypropylene'
                if (mat == 'pvc'):
                    mat = 'PVC'
                if (mat == 'hd'):
                    mat = 'polyethylene'
                if (mat == 'tile'):
                    mat = 'ceramic'
                qty = materials_mc[liste]["material_quantity"]
                if ((mat == 'undefined')or(mat == 'unalloyed_steel')):
                    qty_US += materials_mc[liste]["material_quantity"] * self.inputs_ESA.device_info.number_of_device

                if ((mat in self.inputs_ESA.device_info.materials.prop_rep())and(getattr(self.inputs_ESA.device_info.materials,mat).value == 0)):
                    getattr(self.inputs_ESA.device_info.materials,mat).value += qty * self.inputs_ESA.device_info.number_of_device
                getattr(self.inputs_ESA.device_info.materials,'unalloyed_steel').value = qty_US

    def populate_from_url_mc_dimensions(self):
        # Fetch data
        data = self.fetch_data_from_url(self.url_mc_dimensions,'url_mc_dimensions')

        if(len(data)!=0):
            # Populate input structure with data from MC module dimensions
            self.inputs_ESA.device_info.dimensions.width = data.get('width') #a checker
            self.inputs_ESA.device_info.dimensions.height = data.get('height') # a checker
            self.inputs_ESA.device_info.dimensions.length = data.get('length') # a checker
            self.inputs_ESA.device_info.dimensions.wet_area=data.get("wet_area") * self.inputs_ESA.device_info.number_of_device
            area = data.get('width')*data.get('height')*2 + data.get('height')*data.get('length')*4
            self.inputs_ESA.device_info.dimensions.dry_area=area - data.get("wet_area")

    def populate_from_url_et_array(self):
        data = self.fetch_data_from_url(self.url_et_array,'url_et_array')
        if(len(data)!=0):
            materials_pto = data.get('materials')['value'] # to process
            for liste in range(len(materials_pto)):
                mat = materials_pto[liste]["material_name"]
                qty = materials_pto[liste]["material_quantity"]

                if mat in self.inputs_ESA.device_info.materials.prop_rep():
                    getattr(self.inputs_ESA.device_info.materials,mat).value += qty

    def populate_from_url_lmo_results(self):
        # Fetch data

        catalogue = self.fetch_data_from_url(self.url_lmo_catalogue, 'url_lmo_catalogue')
        # catalogue = catalogue["lmo_catalogue"]
        # Populate input structure with data from lmo results
        
        # Installation phase
        insta_logistic = self.fetch_data_from_url(self.url_lmo_insta_logistic,'url_lmo_insta_logistic')
        insta_consumption = self.fetch_data_from_url(self.url_lmo_insta_consumption,'url_lmo_insta_consumption')
        insta_downtime = self.fetch_data_from_url(self.url_lmo_insta_downtime,'url_lmo_insta_downtime')

        if(len(insta_logistic)!=0):

            self.inputs_ESA.farm_info.project_lifetime = insta_downtime.get("project_life")

            insta_vessels = insta_logistic.get("vessels")

            self.inputs_ESA.logistics_info.installation.fuel_consumption =+ insta_consumption.get("value")
            self.inputs_ESA.logistics_info.installation.Number_of_Vessels = len(insta_vessels)

            # faire le lien avec catalogue pour les passengers
            loa = []
            passenger = []
            for vessel in insta_vessels:
                for attributs in catalogue.get("data"):
                    if vessel == attributs["attributes"]["id_name"]:
                        loa_split=attributs["attributes"]["loa"].split(";")
                        passengers_split=attributs["attributes"]["passengers"].split(";")
                        loa_float = [float(i) for i in loa_split]
                        passenger_float = [float(i) for i in passengers_split]
                        loa.extend(loa_float)
                        passenger.extend(passenger_float)

            self.inputs_ESA.logistics_info.installation.mean_Size_of_Vessels = np.nanmean(loa)
            self.inputs_ESA.logistics_info.installation.Number_of_passengers = np.sum(passenger)

        # Maintenance phase
        maint_logistic = self.fetch_data_from_url(self.url_lmo_maint_logistic,'url_lmo_maint_logistic')
        maint_consumption = self.fetch_data_from_url(self.url_lmo_maint_consumption,'url_lmo_maint_consumption')

        if(len(maint_logistic)!=0):

            maint_vessels = maint_logistic.get("vessels")

            self.inputs_ESA.logistics_info.maintenance.fuel_consumption =+ maint_consumption.get("value")
            self.inputs_ESA.logistics_info.maintenance.Number_of_Vessels = len(maint_vessels)

            loa = []
            passenger = []
            for vessel in maint_vessels:
                for attributs in catalogue.get("data"):
                    if vessel == attributs["attributes"]["id_name"]:
                        loa_split=attributs["attributes"]["loa"].split(";")
                        passengers_split=attributs["attributes"]["passengers"].split(";")
                        loa_float = [float(i) for i in loa_split]
                        passenger_float = [float(i) for i in passengers_split]
                        loa.extend(loa_float)
                        passenger.extend(passenger_float)
            self.inputs_ESA.logistics_info.exploitation.mean_Size_of_Vessels = np.nanmean(loa)
            self.inputs_ESA.logistics_info.exploitation.Number_of_passengers = np.sum(passenger)
            # CFP

        # Decommissioning phase
        decom_logistic = self.fetch_data_from_url(self.url_lmo_decom_logistic,'url_lmo_maint_logistic')
        decom_consumption = self.fetch_data_from_url(self.url_lmo_decom_consumption,'url_lmo_maint_consumption')

        if(len(decom_logistic)!=0):

            decom_vessels = decom_logistic.get("vessels")

            self.inputs_ESA.logistics_info.decommisioning.fuel_consumption =+ decom_consumption.get("value")
            self.inputs_ESA.logistics_info.decommisioning.Number_of_Vessels = len(maint_vessels)

            loa = []
            passenger = []
            for vessel in decom_vessels:
                for attributs in catalogue.get("data"):
                    if vessel == attributs["attributes"]["id_name"]:
                        loa_split=attributs["attributes"]["loa"].split(";")
                        passengers_split=attributs["attributes"]["passengers"].split(";")
                        loa_float = [float(i) for i in loa_split]
                        passenger_float = [float(i) for i in passengers_split]
                        loa.extend(loa_float)
                        passenger.extend(passenger_float)
            self.inputs_ESA.logistics_info.decommisioning.mean_Size_of_Vessels = np.nanmean(loa)
            self.inputs_ESA.logistics_info.decommisioning.Number_of_passengers = np.sum(passenger)

    def fetch_data_from_url(self,url,url_name):
        if isinstance(url,URL.URL):
            #data = call API with self.url_ec_get_farm.url
            data = dict()
            pass
        else:
            if isinstance(url,dict):
                data = url # We also accept the structure data instead of the URL. This is for testing purposes only.
            else:
                raise Exception('Error : ' + url_name + ' should be a dictionnary or a url')
        return data
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def entity_name(self): # pragma: no cover
        """str: Name of the entity
        """
        return self._entity_name
    #------------
    @ property
    def inputs_ESA(self): # pragma: no cover
        """:obj:`~.inputs_ESA.inputs_ESA`: All input data necessary to run ESA module
        """
        return self._inputs_ESA
    #------------
    @ property
    def run_mode(self): # pragma: no cover
        """str: standalone or dto. If dto, urls need to be provided.
        """
        return self._run_mode
    #------------
    @ property
    def url_sc_get_farm(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling SC module for reading farm direct values and infos : /sc/{ProjectId}/farm, receiving dataformat dtosc_farm_outputs_FarmResults.yaml
        """
        return self._url_sc_get_farm
    #------------
    @ property
    def url_sc_get_point(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling SC module for reading farm direct values and infos : /sc/{ProjectId}/point, receiving dataformat dtosc_farm_outputs_FarmResults.yaml
        """
        return self._url_sc_get_point
    #------------
    @ property
    def url_sc_get_current(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling SC module for reading farm direct values and infos : /sc/{ProjectId}/point/time_series/currents, receiving dataformat dtosc_farm_outputs_FarmResults.yaml
        """
        return self._url_sc_get_current
    #------------
    @ property
    def url_ec_get_farm(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling EC module for reading farm layout : /ec/{ecId}/farm, receiving dataformat Farm.yaml
        """
        return self._url_ec_get_farm
    #------------
    @ property
    def url_ec_get_project(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling EC module for reading project design : /ec/{ecId}, receiving dataformat design.yaml
        """
        return self._url_ec_get_project
    #------------
    @ property
    def url_mc_dimensions(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling MC module for reading machine dimensions : /mc/{mcId}/dimensions, receiving dataformat Dimensions.yaml
        """
        return self._url_mc_dimensions
    #------------
    @ property
    def url_mc_general(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling MC module for reading general information (materials) : /mc/{mcId}/general, receiving dataformat General.yaml
        """
        return self._url_mc_general
    #------------
    @ property
    def url_ed_get_results(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling ET module for reading  farm layout : /api/energy-deliv-studies/{edId}/results/{ednwId}  , receiving dataformat EnergyDelivOutputResults.yaml
        """
        return self._url_ed_get_results
    #------------
    @ property
    def url_et_array(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling ED module for reading materials for pto : /energy_transf/{etId}/array  , receiving dataformat array.yaml
        """
        return self._url_et_array
    #------------
    @ property
    def url_sk_get_env_impact(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling SK module for reading farm layout : /sk/{ProjectId}/environmental_impact  , receiving dataformat dtosk_outputs_EnvironmentalImpact.yaml
        """
        return self._url_sk_get_env_impact
    #------------
    @ property
    def url_lmo_insta_logistic(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling LMO module for reading all results : /api/{lmoid}/phases  , receiving dataformat phases.yaml
        """
        return self._url_lmo_insta_logistic
    #------------
    @ property
    def url_lmo_insta_consumption(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling LMO module for reading all results : /api/{lmoid}/phases  , receiving dataformat phases.yaml
        """
        return self._url_lmo_insta_consumption
    #------------
    @ property
    def url_lmo_insta_downtime(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling LMO module for reading all results : /api/{lmoid}/phases  , receiving dataformat phases.yaml
        """
        return self._url_lmo_insta_downtime
    #------------
    @ property
    def url_lmo_maint_logistic(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling LMO module for reading all results : /api/{lmoid}/phases  , receiving dataformat phases.yaml
        """
        return self._url_lmo_maint_logistic
    #------------
    @ property
    def url_lmo_maint_consumption(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling LMO module for reading all results : /api/{lmoid}/phases  , receiving dataformat phases.yaml
        """
        return self._url_lmo_maint_consumption
    #------------
    @ property
    def url_lmo_decom_logistic(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling LMO module for reading all results : /api/{lmoid}/phases  , receiving dataformat phases.yaml
        """
        return self._url_lmo_decom_logistic
    #------------
    @ property
    def url_lmo_decom_consumption(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling LMO module for reading all results : /api/{lmoid}/phases  , receiving dataformat phases.yaml
        """
        return self._url_lmo_decom_consumption
    #------------
    @ property
    def url_lmo_catalogue(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling LMO catalogue for reading vessels : /vessels  , receiving dataformat Vessels_get.yaml
        """
        return self._url_lmo_catalogue
    #------------
    @ property
    def url_ed_catalogue_static_cable(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling ED catalogue for reading static cables catalogue : /static_cable  , receiving dataformat Vessels_get.yaml
        """
        return self._url_ed_catalogue_static_cable
    #------------
    @ property
    def url_ed_catalogue_dynamic_cable(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling ED catalogue for reading dynamic cables catalogue : /dynamic_cable  , receiving dataformat Vessels_get.yaml
        """
        return self._url_ed_catalogue_dynamic_cable
    #------------
    @ property
    def url_ed_catalogue_collection_point(self): # pragma: no cover
        """:obj:`~.URL.URL`: url calling ED catalogue for reading collection point catalogue : /collection_point  , receiving dataformat Vessels_get.yaml
        """
        return self._url_ed_catalogue_collection_point
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ entity_name.setter
    def entity_name(self,val): # pragma: no cover
        self._entity_name=str(val)
    #------------
    @ inputs_ESA.setter
    def inputs_ESA(self,val): # pragma: no cover
        self._inputs_ESA=val
    #------------
    @ run_mode.setter
    def run_mode(self,val): # pragma: no cover
        self._run_mode=str(val)
    #------------
    @ url_sc_get_farm.setter
    def url_sc_get_farm(self,val): # pragma: no cover
        self._url_sc_get_farm=val
    #------------
    @ url_sc_get_point.setter
    def url_sc_get_point(self,val): # pragma: no cover
        self._url_sc_get_point=val
    #------------
    @ url_sc_get_current.setter
    def url_sc_get_current(self,val): # pragma: no cover
        self._url_sc_get_current=val
    #------------
    @ url_ec_get_farm.setter
    def url_ec_get_farm(self,val): # pragma: no cover
        self._url_ec_get_farm=val
    #------------
    @ url_ec_get_project.setter
    def url_ec_get_project(self,val): # pragma: no cover
        self._url_ec_get_project=val
    #------------
    @ url_mc_dimensions.setter
    def url_mc_dimensions(self,val): # pragma: no cover
        self._url_mc_dimensions=val
    #------------
    @ url_mc_general.setter
    def url_mc_general(self,val): # pragma: no cover
        self._url_mc_general=val
    #------------
    @ url_ed_get_results.setter
    def url_ed_get_results(self,val): # pragma: no cover
        self._url_ed_get_results=val
    #------------
    @ url_et_array.setter
    def url_et_array(self,val): # pragma: no cover
        self._url_et_array=val
    #------------
    @ url_sk_get_env_impact.setter
    def url_sk_get_env_impact(self,val): # pragma: no cover
        self._url_sk_get_env_impact=val
    #------------
    @ url_lmo_insta_logistic.setter
    def url_lmo_insta_logistic(self,val): # pragma: no cover
        self._url_lmo_insta_logistic=val
    #------------
    @ url_lmo_insta_consumption.setter
    def url_lmo_insta_consumption(self,val): # pragma: no cover
        self._url_lmo_insta_consumption=val
    #------------
    @ url_lmo_insta_downtime.setter
    def url_lmo_insta_downtime(self,val): # pragma: no cover
        self._url_lmo_insta_downtime=val
    #------------
    @ url_lmo_maint_logistic.setter
    def url_lmo_maint_logistic(self,val): # pragma: no cover
        self._url_lmo_maint_logistic=val
    #------------
    @ url_lmo_maint_consumption.setter
    def url_lmo_maint_consumption(self,val): # pragma: no cover
        self._url_lmo_maint_consumption=val
    #------------
    @ url_lmo_decom_logistic.setter
    def url_lmo_decom_logistic(self,val): # pragma: no cover
        self._url_lmo_decom_logistic=val
    #------------
    @ url_lmo_decom_consumption.setter
    def url_lmo_decom_consumption(self,val): # pragma: no cover
        self._url_lmo_decom_consumption=val
    #------------
    @ url_lmo_catalogue.setter
    def url_lmo_catalogue(self,val): # pragma: no cover
        self._url_lmo_catalogue=val
    #------------
    @ url_ed_catalogue_static_cable.setter
    def url_ed_catalogue_static_cable(self,val): # pragma: no cover
        self._url_ed_catalogue_static_cable=val
    #------------
    @ url_ed_catalogue_dynamic_cable.setter
    def url_ed_catalogue_dynamic_cable(self,val): # pragma: no cover
        self._url_ed_catalogue_dynamic_cable=val
    #------------
    @ url_ed_catalogue_collection_point.setter
    def url_ed_catalogue_collection_point(self,val): # pragma: no cover
        self._url_ed_catalogue_collection_point=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:Inputs"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:Inputs"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("entity_name"):
            rep["entity_name"] = self.entity_name
        if self.is_set("inputs_ESA"):
            if (short and not(deep)):
                rep["inputs_ESA"] = self.inputs_ESA.type_rep()
            else:
                rep["inputs_ESA"] = self.inputs_ESA.prop_rep(short, deep)
        if self.is_set("run_mode"):
            rep["run_mode"] = self.run_mode
        if self.is_set("url_sc_get_farm"):
            if isinstance(self.url_sc_get_farm,dict):
                if (short and not(deep)):
                    rep["url_sc_get_farm"] = "dict"
                else:
                    rep["url_sc_get_farm"] = self.url_sc_get_farm
            else:
                if (short and not(deep)):
                    rep["url_sc_get_farm"] = self.url_sc_get_farm.type_rep()
                else:
                    rep["url_sc_get_farm"] = self.url_sc_get_farm.prop_rep(short, deep)
        if self.is_set("url_sc_get_point"):
            if isinstance(self.url_sc_get_point,dict):
                if (short and not(deep)):
                    rep["url_sc_get_point"] = "dict"
                else:
                    rep["url_sc_get_point"] = self.url_sc_get_point
            else:
                if (short and not(deep)):
                    rep["url_sc_get_point"] = self.url_sc_get_point.type_rep()
                else:
                    rep["url_sc_get_point"] = self.url_sc_get_point.prop_rep(short, deep)
        if self.is_set("url_sc_get_current"):
            if isinstance(self.url_sc_get_current,dict):
                if (short and not(deep)):
                    rep["url_sc_get_current"] = "dict"
                else:
                    rep["url_sc_get_current"] = self.url_sc_get_current
            else:
                if (short and not(deep)):
                    rep["url_sc_get_current"] = self.url_sc_get_current.type_rep()
                else:
                    rep["url_sc_get_current"] = self.url_sc_get_current.prop_rep(short, deep)
        if self.is_set("url_ec_get_farm"):
            if isinstance(self.url_ec_get_farm,dict):
                if (short and not(deep)):
                    rep["url_ec_get_farm"] = "dict"
                else:
                    rep["url_ec_get_farm"] = self.url_ec_get_farm
            else:
                if (short and not(deep)):
                    rep["url_ec_get_farm"] = self.url_ec_get_farm.type_rep()
                else:
                    rep["url_ec_get_farm"] = self.url_ec_get_farm.prop_rep(short, deep)
        if self.is_set("url_ec_get_project"):
            if isinstance(self.url_ec_get_project,dict):
                if (short and not(deep)):
                    rep["url_ec_get_project"] = "dict"
                else:
                    rep["url_ec_get_project"] = self.url_ec_get_project
            else:
                if (short and not(deep)):
                    rep["url_ec_get_project"] = self.url_ec_get_project.type_rep()
                else:
                    rep["url_ec_get_project"] = self.url_ec_get_project.prop_rep(short, deep)
        if self.is_set("url_mc_dimensions"):
            if isinstance(self.url_mc_dimensions,dict):
                if (short and not(deep)):
                    rep["url_mc_dimensions"] = "dict"
                else:
                    rep["url_mc_dimensions"] = self.url_mc_dimensions
            else:
                if (short and not(deep)):
                    rep["url_mc_dimensions"] = self.url_mc_dimensions.type_rep()
                else:
                    rep["url_mc_dimensions"] = self.url_mc_dimensions.prop_rep(short, deep)
        if self.is_set("url_mc_general"):
            if isinstance(self.url_mc_general,dict):
                if (short and not(deep)):
                    rep["url_mc_general"] = "dict"
                else:
                    rep["url_mc_general"] = self.url_mc_general
            else:
                if (short and not(deep)):
                    rep["url_mc_general"] = self.url_mc_general.type_rep()
                else:
                    rep["url_mc_general"] = self.url_mc_general.prop_rep(short, deep)
        if self.is_set("url_ed_get_results"):
            if isinstance(self.url_ed_get_results,dict):
                if (short and not(deep)):
                    rep["url_ed_get_results"] = "dict"
                else:
                    rep["url_ed_get_results"] = self.url_ed_get_results
            else:
                if (short and not(deep)):
                    rep["url_ed_get_results"] = self.url_ed_get_results.type_rep()
                else:
                    rep["url_ed_get_results"] = self.url_ed_get_results.prop_rep(short, deep)
        if self.is_set("url_et_array"):
            if isinstance(self.url_et_array,dict):
                if (short and not(deep)):
                    rep["url_et_array"] = "dict"
                else:
                    rep["url_et_array"] = self.url_et_array
            else:
                if (short and not(deep)):
                    rep["url_et_array"] = self.url_et_array.type_rep()
                else:
                    rep["url_et_array"] = self.url_et_array.prop_rep(short, deep)
        if self.is_set("url_sk_get_env_impact"):
            if isinstance(self.url_sk_get_env_impact,dict):
                if (short and not(deep)):
                    rep["url_sk_get_env_impact"] = "dict"
                else:
                    rep["url_sk_get_env_impact"] = self.url_sk_get_env_impact
            else:
                if (short and not(deep)):
                    rep["url_sk_get_env_impact"] = self.url_sk_get_env_impact.type_rep()
                else:
                    rep["url_sk_get_env_impact"] = self.url_sk_get_env_impact.prop_rep(short, deep)
        if self.is_set("url_lmo_insta_logistic"):
            if isinstance(self.url_lmo_insta_logistic,dict):
                if (short and not(deep)):
                    rep["url_lmo_insta_logistic"] = "dict"
                else:
                    rep["url_lmo_insta_logistic"] = self.url_lmo_insta_logistic
            else:
                if (short and not(deep)):
                    rep["url_lmo_insta_logistic"] = self.url_lmo_insta_logistic.type_rep()
                else:
                    rep["url_lmo_insta_logistic"] = self.url_lmo_insta_logistic.prop_rep(short, deep)
        if self.is_set("url_lmo_insta_consumption"):
            if isinstance(self.url_lmo_insta_consumption,dict):
                if (short and not(deep)):
                    rep["url_lmo_insta_consumption"] = "dict"
                else:
                    rep["url_lmo_insta_consumption"] = self.url_lmo_insta_consumption
            else:
                if (short and not(deep)):
                    rep["url_lmo_insta_consumption"] = self.url_lmo_insta_consumption.type_rep()
                else:
                    rep["url_lmo_insta_consumption"] = self.url_lmo_insta_consumption.prop_rep(short, deep)
        if self.is_set("url_lmo_insta_downtime"):
            if isinstance(self.url_lmo_insta_downtime,dict):
                if (short and not(deep)):
                    rep["url_lmo_insta_downtime"] = "dict"
                else:
                    rep["url_lmo_insta_downtime"] = self.url_lmo_insta_downtime
            else:
                if (short and not(deep)):
                    rep["url_lmo_insta_downtime"] = self.url_lmo_insta_downtime.type_rep()
                else:
                    rep["url_lmo_insta_downtime"] = self.url_lmo_insta_downtime.prop_rep(short, deep)
        if self.is_set("url_lmo_maint_logistic"):
            if isinstance(self.url_lmo_maint_logistic,dict):
                if (short and not(deep)):
                    rep["url_lmo_maint_logistic"] = "dict"
                else:
                    rep["url_lmo_maint_logistic"] = self.url_lmo_maint_logistic
            else:
                if (short and not(deep)):
                    rep["url_lmo_maint_logistic"] = self.url_lmo_maint_logistic.type_rep()
                else:
                    rep["url_lmo_maint_logistic"] = self.url_lmo_maint_logistic.prop_rep(short, deep)
        if self.is_set("url_lmo_maint_consumption"):
            if isinstance(self.url_lmo_maint_consumption,dict):
                if (short and not(deep)):
                    rep["url_lmo_maint_consumption"] = "dict"
                else:
                    rep["url_lmo_maint_consumption"] = self.url_lmo_maint_consumption
            else:
                if (short and not(deep)):
                    rep["url_lmo_maint_consumption"] = self.url_lmo_maint_consumption.type_rep()
                else:
                    rep["url_lmo_maint_consumption"] = self.url_lmo_maint_consumption.prop_rep(short, deep)
        if self.is_set("url_lmo_decom_logistic"):
            if isinstance(self.url_lmo_decom_logistic,dict):
                if (short and not(deep)):
                    rep["url_lmo_decom_logistic"] = "dict"
                else:
                    rep["url_lmo_decom_logistic"] = self.url_lmo_decom_logistic
            else:
                if (short and not(deep)):
                    rep["url_lmo_decom_logistic"] = self.url_lmo_decom_logistic.type_rep()
                else:
                    rep["url_lmo_decom_logistic"] = self.url_lmo_decom_logistic.prop_rep(short, deep)
        if self.is_set("url_lmo_decom_consumption"):
            if isinstance(self.url_lmo_decom_consumption,dict):
                if (short and not(deep)):
                    rep["url_lmo_decom_consumption"] = "dict"
                else:
                    rep["url_lmo_decom_consumption"] = self.url_lmo_decom_consumption
            else:
                if (short and not(deep)):
                    rep["url_lmo_decom_consumption"] = self.url_lmo_decom_consumption.type_rep()
                else:
                    rep["url_lmo_decom_consumption"] = self.url_lmo_decom_consumption.prop_rep(short, deep)
        if self.is_set("url_lmo_catalogue"):
            if isinstance(self.url_lmo_catalogue,dict):
                if (short and not(deep)):
                    rep["url_lmo_catalogue"] = "dict"
                else:
                    rep["url_lmo_catalogue"] = self.url_lmo_catalogue
            else:
                if (short and not(deep)):
                    rep["url_lmo_catalogue"] = self.url_lmo_catalogue.type_rep()
                else:
                    rep["url_lmo_catalogue"] = self.url_lmo_catalogue.prop_rep(short, deep)
        if self.is_set("url_ed_catalogue_static_cable"):
            if isinstance(self.url_ed_catalogue_static_cable,dict):
                if (short and not(deep)):
                    rep["url_ed_catalogue_static_cable"] = "dict"
                else:
                    rep["url_ed_catalogue_static_cable"] = self.url_ed_catalogue_static_cable
            else:
                if (short and not(deep)):
                    rep["url_ed_catalogue_static_cable"] = self.url_ed_catalogue_static_cable.type_rep()
                else:
                    rep["url_ed_catalogue_static_cable"] = self.url_ed_catalogue_static_cable.prop_rep(short, deep)
        if self.is_set("url_ed_catalogue_dynamic_cable"):
            if isinstance(self.url_ed_catalogue_dynamic_cable,dict):
                if (short and not(deep)):
                    rep["url_ed_catalogue_dynamic_cable"] = "dict"
                else:
                    rep["url_ed_catalogue_dynamic_cable"] = self.url_ed_catalogue_dynamic_cable
            else:
                if (short and not(deep)):
                    rep["url_ed_catalogue_dynamic_cable"] = self.url_ed_catalogue_dynamic_cable.type_rep()
                else:
                    rep["url_ed_catalogue_dynamic_cable"] = self.url_ed_catalogue_dynamic_cable.prop_rep(short, deep)
        if self.is_set("url_ed_catalogue_collection_point"):
            if isinstance(self.url_ed_catalogue_collection_point,dict):
                if (short and not(deep)):
                    rep["url_ed_catalogue_collection_point"] = "dict"
                else:
                    rep["url_ed_catalogue_collection_point"] = self.url_ed_catalogue_collection_point
            else:
                if (short and not(deep)):
                    rep["url_ed_catalogue_collection_point"] = self.url_ed_catalogue_collection_point.type_rep()
                else:
                    rep["url_ed_catalogue_collection_point"] = self.url_ed_catalogue_collection_point.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "entity_name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "inputs_ESA"
        try :
            if data[varName] != None:
                self.inputs_ESA=inputs_ESA.inputs_ESA()
                self.inputs_ESA.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "run_mode"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "url_sc_get_farm"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_sc_get_farm=URL.URL()
                        self.url_sc_get_farm.loadFromJSONDict(data[varName])
                else:
                    self.url_sc_get_farm=data[varName]
        except :
            pass
        varName = "url_sc_get_point"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_sc_get_point=URL.URL()
                        self.url_sc_get_point.loadFromJSONDict(data[varName])
                else:
                    self.url_sc_get_point=data[varName]
        except :
            pass
        varName = "url_sc_get_current"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_sc_get_current=URL.URL()
                        self.url_sc_get_current.loadFromJSONDict(data[varName])
                else:
                    self.url_sc_get_current=data[varName]
        except :
            pass
        varName = "url_ec_get_farm"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_ec_get_farm=URL.URL()
                        self.url_ec_get_farm.loadFromJSONDict(data[varName])
                else:
                    self.url_ec_get_farm=data[varName]
        except :
            pass
        varName = "url_ec_get_project"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_ec_get_project=URL.URL()
                        self.url_ec_get_project.loadFromJSONDict(data[varName])
                else:
                    self.url_ec_get_project=data[varName]
        except :
            pass
        varName = "url_mc_dimensions"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_mc_dimensions=URL.URL()
                        self.url_mc_dimensions.loadFromJSONDict(data[varName])
                else:
                    self.url_mc_dimensions=data[varName]
        except :
            pass
        varName = "url_mc_general"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_mc_general=URL.URL()
                        self.url_mc_general.loadFromJSONDict(data[varName])
                else:
                    self.url_mc_general=data[varName]
        except :
            pass
        varName = "url_ed_get_results"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_ed_get_results=URL.URL()
                        self.url_ed_get_results.loadFromJSONDict(data[varName])
                else:
                    self.url_ed_get_results=data[varName]
        except :
            pass
        varName = "url_et_array"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_et_array=URL.URL()
                        self.url_et_array.loadFromJSONDict(data[varName])
                else:
                    self.url_et_array=data[varName]
        except :
            pass
        varName = "url_sk_get_env_impact"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_sk_get_env_impact=URL.URL()
                        self.url_sk_get_env_impact.loadFromJSONDict(data[varName])
                else:
                    self.url_sk_get_env_impact=data[varName]
        except :
            pass
        varName = "url_lmo_insta_logistic"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_lmo_insta_logistic=URL.URL()
                        self.url_lmo_insta_logistic.loadFromJSONDict(data[varName])
                else:
                    self.url_lmo_insta_logistic=data[varName]
        except :
            pass
        varName = "url_lmo_insta_consumption"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_lmo_insta_consumption=URL.URL()
                        self.url_lmo_insta_consumption.loadFromJSONDict(data[varName])
                else:
                    self.url_lmo_insta_consumption=data[varName]
        except :
            pass
        varName = "url_lmo_insta_downtime"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_lmo_insta_downtime=URL.URL()
                        self.url_lmo_insta_downtime.loadFromJSONDict(data[varName])
                else:
                    self.url_lmo_insta_downtime=data[varName]
        except :
            pass
        varName = "url_lmo_maint_logistic"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_lmo_maint_logistic=URL.URL()
                        self.url_lmo_maint_logistic.loadFromJSONDict(data[varName])
                else:
                    self.url_lmo_maint_logistic=data[varName]
        except :
            pass
        varName = "url_lmo_maint_consumption"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_lmo_maint_consumption=URL.URL()
                        self.url_lmo_maint_consumption.loadFromJSONDict(data[varName])
                else:
                    self.url_lmo_maint_consumption=data[varName]
        except :
            pass
        varName = "url_lmo_decom_logistic"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_lmo_decom_logistic=URL.URL()
                        self.url_lmo_decom_logistic.loadFromJSONDict(data[varName])
                else:
                    self.url_lmo_decom_logistic=data[varName]
        except :
            pass
        varName = "url_lmo_decom_consumption"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_lmo_decom_consumption=URL.URL()
                        self.url_lmo_decom_consumption.loadFromJSONDict(data[varName])
                else:
                    self.url_lmo_decom_consumption=data[varName]
        except :
            pass
        varName = "url_lmo_catalogue"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_lmo_catalogue=URL.URL()
                        self.url_lmo_catalogue.loadFromJSONDict(data[varName])
                else:
                    self.url_lmo_catalogue=data[varName]
        except :
            pass
        varName = "url_ed_catalogue_static_cable"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_ed_catalogue_static_cable=URL.URL()
                        self.url_ed_catalogue_static_cable.loadFromJSONDict(data[varName])
                else:
                    self.url_ed_catalogue_static_cable=data[varName]
        except :
            pass
        varName = "url_ed_catalogue_dynamic_cable"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_ed_catalogue_dynamic_cable=URL.URL()
                        self.url_ed_catalogue_dynamic_cable.loadFromJSONDict(data[varName])
                else:
                    self.url_ed_catalogue_dynamic_cable=data[varName]
        except :
            pass
        varName = "url_ed_catalogue_collection_point"
        try :
            if data[varName] != None:
                if "__type__" in data[varName]:
                    if data[varName]["__type__"] == "dtoesa:inputs:URL":
                        self.url_ed_catalogue_collection_point=URL.URL()
                        self.url_ed_catalogue_collection_point.loadFromJSONDict(data[varName])
                else:
                    self.url_ed_catalogue_collection_point=data[varName]
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
