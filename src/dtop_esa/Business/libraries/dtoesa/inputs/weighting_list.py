# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class weighting_list():

    """List of receptors dictionnary
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._Energy_Modification=''
        self._Collision_Risk=''
        self._Collision_Risk_Vessel=''
        self._Chemical_Pollution=''
        self._Turbidity=''
        self._Underwater_Noise=''
        self._Reserve_Effect=''
        self._Reef_Effect=''
        self._Resting_Place=''
        self._Footprint=''
        self._Electric_Fields=''
        self._Magnetic_Fields=''
        self._Temperature_Modification=''
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def Energy_Modification(self): # pragma: no cover
        """str: none
        """
        return self._Energy_Modification
    #------------
    @ property
    def Collision_Risk(self): # pragma: no cover
        """str: none
        """
        return self._Collision_Risk
    #------------
    @ property
    def Collision_Risk_Vessel(self): # pragma: no cover
        """str: none
        """
        return self._Collision_Risk_Vessel
    #------------
    @ property
    def Chemical_Pollution(self): # pragma: no cover
        """str: none
        """
        return self._Chemical_Pollution
    #------------
    @ property
    def Turbidity(self): # pragma: no cover
        """str: none
        """
        return self._Turbidity
    #------------
    @ property
    def Underwater_Noise(self): # pragma: no cover
        """str: none
        """
        return self._Underwater_Noise
    #------------
    @ property
    def Reserve_Effect(self): # pragma: no cover
        """str: none
        """
        return self._Reserve_Effect
    #------------
    @ property
    def Reef_Effect(self): # pragma: no cover
        """str: none
        """
        return self._Reef_Effect
    #------------
    @ property
    def Resting_Place(self): # pragma: no cover
        """str: none
        """
        return self._Resting_Place
    #------------
    @ property
    def Footprint(self): # pragma: no cover
        """str: none
        """
        return self._Footprint
    #------------
    @ property
    def Electric_Fields(self): # pragma: no cover
        """str: none
        """
        return self._Electric_Fields
    #------------
    @ property
    def Magnetic_Fields(self): # pragma: no cover
        """str: none
        """
        return self._Magnetic_Fields
    #------------
    @ property
    def Temperature_Modification(self): # pragma: no cover
        """str: none
        """
        return self._Temperature_Modification
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ Energy_Modification.setter
    def Energy_Modification(self,val): # pragma: no cover
        self._Energy_Modification=str(val)
    #------------
    @ Collision_Risk.setter
    def Collision_Risk(self,val): # pragma: no cover
        self._Collision_Risk=str(val)
    #------------
    @ Collision_Risk_Vessel.setter
    def Collision_Risk_Vessel(self,val): # pragma: no cover
        self._Collision_Risk_Vessel=str(val)
    #------------
    @ Chemical_Pollution.setter
    def Chemical_Pollution(self,val): # pragma: no cover
        self._Chemical_Pollution=str(val)
    #------------
    @ Turbidity.setter
    def Turbidity(self,val): # pragma: no cover
        self._Turbidity=str(val)
    #------------
    @ Underwater_Noise.setter
    def Underwater_Noise(self,val): # pragma: no cover
        self._Underwater_Noise=str(val)
    #------------
    @ Reserve_Effect.setter
    def Reserve_Effect(self,val): # pragma: no cover
        self._Reserve_Effect=str(val)
    #------------
    @ Reef_Effect.setter
    def Reef_Effect(self,val): # pragma: no cover
        self._Reef_Effect=str(val)
    #------------
    @ Resting_Place.setter
    def Resting_Place(self,val): # pragma: no cover
        self._Resting_Place=str(val)
    #------------
    @ Footprint.setter
    def Footprint(self,val): # pragma: no cover
        self._Footprint=str(val)
    #------------
    @ Electric_Fields.setter
    def Electric_Fields(self,val): # pragma: no cover
        self._Electric_Fields=str(val)
    #------------
    @ Magnetic_Fields.setter
    def Magnetic_Fields(self,val): # pragma: no cover
        self._Magnetic_Fields=str(val)
    #------------
    @ Temperature_Modification.setter
    def Temperature_Modification(self,val): # pragma: no cover
        self._Temperature_Modification=str(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:weighting_list"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:weighting_list"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("Energy_Modification"):
            rep["Energy_Modification"] = self.Energy_Modification
        if self.is_set("Collision_Risk"):
            rep["Collision_Risk"] = self.Collision_Risk
        if self.is_set("Collision_Risk_Vessel"):
            rep["Collision_Risk_Vessel"] = self.Collision_Risk_Vessel
        if self.is_set("Chemical_Pollution"):
            rep["Chemical_Pollution"] = self.Chemical_Pollution
        if self.is_set("Turbidity"):
            rep["Turbidity"] = self.Turbidity
        if self.is_set("Underwater_Noise"):
            rep["Underwater_Noise"] = self.Underwater_Noise
        if self.is_set("Reserve_Effect"):
            rep["Reserve_Effect"] = self.Reserve_Effect
        if self.is_set("Reef_Effect"):
            rep["Reef_Effect"] = self.Reef_Effect
        if self.is_set("Resting_Place"):
            rep["Resting_Place"] = self.Resting_Place
        if self.is_set("Footprint"):
            rep["Footprint"] = self.Footprint
        if self.is_set("Electric_Fields"):
            rep["Electric_Fields"] = self.Electric_Fields
        if self.is_set("Magnetic_Fields"):
            rep["Magnetic_Fields"] = self.Magnetic_Fields
        if self.is_set("Temperature_Modification"):
            rep["Temperature_Modification"] = self.Temperature_Modification
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "Energy_Modification"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Collision_Risk"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Collision_Risk_Vessel"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Chemical_Pollution"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Turbidity"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Underwater_Noise"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Reserve_Effect"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Reef_Effect"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Resting_Place"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Footprint"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Electric_Fields"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Magnetic_Fields"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Temperature_Modification"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
