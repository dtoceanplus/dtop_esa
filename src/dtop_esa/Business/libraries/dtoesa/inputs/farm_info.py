# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import input_species
from . import receptors_dict
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class farm_info():

    """Global information about the farm
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._Coordinates_of_the_farm_x=0.0
        self._Coordinates_of_the_farm_y=0.0
        self._project_lifetime=0.0
        self._lcoe=0.0
        self._zone_type='open water'
        self._water_depth=np.zeros(shape=(1), dtype=float)
        self._current_direction=0.0
        self._total_surface_area=0.0
        self._soil_type=[]
        self._Initial_Turbidity=0.0
        self._Initial_Noise_dB_re_1muPa=0.0
        self._Initial_Electric_Field=0.0
        self._Initial_Magnetic_Field=0.0
        self._Initial_Temperature=0.0
        self._Fisheries='Complete prohibition'
        self._protected_species=[]
        self._protected_species_length=0
        self._receptors=receptors_dict.receptors_dict()
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def Coordinates_of_the_farm_x(self): # pragma: no cover
        """float: Latitude of the center of the farm [Degree decimal]
        """
        return self._Coordinates_of_the_farm_x
    #------------
    @ property
    def Coordinates_of_the_farm_y(self): # pragma: no cover
        """float: Longitude of the center of the farm [Degree decimal]
        """
        return self._Coordinates_of_the_farm_y
    #------------
    @ property
    def project_lifetime(self): # pragma: no cover
        """float: none [years]
        """
        return self._project_lifetime
    #------------
    @ property
    def lcoe(self): # pragma: no cover
        """float: none [€/MW]
        """
        return self._lcoe
    #------------
    @ property
    def zone_type(self): # pragma: no cover
        """str: open water, sea loch entrance or sounds
        """
        return self._zone_type
    #------------
    @ property
    def water_depth(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Water depth dim(*) [m]
        """
        return self._water_depth
    #------------
    @ property
    def current_direction(self): # pragma: no cover
        """float: Current direction [degree]
        """
        return self._current_direction
    #------------
    @ property
    def total_surface_area(self): # pragma: no cover
        """float: Total surface are of the farm [m²]
        """
        return self._total_surface_area
    #------------
    @ property
    def soil_type(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:Types of soil
        """
        return self._soil_type
    #------------
    @ property
    def Initial_Turbidity(self): # pragma: no cover
        """float: Initial measure of turbidity [mg/L]
        """
        return self._Initial_Turbidity
    #------------
    @ property
    def Initial_Noise_dB_re_1muPa(self): # pragma: no cover
        """float: Initial measure of noise [dB re 1muPa]
        """
        return self._Initial_Noise_dB_re_1muPa
    #------------
    @ property
    def Initial_Electric_Field(self): # pragma: no cover
        """float: Initial measure of electrical field [V/m]
        """
        return self._Initial_Electric_Field
    #------------
    @ property
    def Initial_Magnetic_Field(self): # pragma: no cover
        """float: Initial measure of magnetic fields [uT]
        """
        return self._Initial_Magnetic_Field
    #------------
    @ property
    def Initial_Temperature(self): # pragma: no cover
        """float: Initial measure of temperature [°C]
        """
        return self._Initial_Temperature
    #------------
    @ property
    def Fisheries(self): # pragma: no cover
        """str: Complete prohibition, Cast net fising, Trawler fishing or others or no restriction
        """
        return self._Fisheries
    #------------
    @ property
    def protected_species(self): # pragma: no cover
        """:obj:`list` of :obj:`~.input_species.input_species`: Optional; Protected species identified in the area
        """
        return self._protected_species
    #------------
    @ property
    def protected_species_length(self): # pragma: no cover
        """int: Used only if the module is in integrated mode. Define the number of imported species coming from the SC module.
        """
        return self._protected_species_length
    #------------
    @ property
    def receptors(self): # pragma: no cover
        """:obj:`~.receptors_dict.receptors_dict`: none
        """
        return self._receptors
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ Coordinates_of_the_farm_x.setter
    def Coordinates_of_the_farm_x(self,val): # pragma: no cover
        self._Coordinates_of_the_farm_x=float(val)
    #------------
    @ Coordinates_of_the_farm_y.setter
    def Coordinates_of_the_farm_y(self,val): # pragma: no cover
        self._Coordinates_of_the_farm_y=float(val)
    #------------
    @ project_lifetime.setter
    def project_lifetime(self,val): # pragma: no cover
        self._project_lifetime=float(val)
    #------------
    @ lcoe.setter
    def lcoe(self,val): # pragma: no cover
        self._lcoe=float(val)
    #------------
    @ zone_type.setter
    def zone_type(self,val): # pragma: no cover
        self._zone_type=str(val)
    #------------
    @ water_depth.setter
    def water_depth(self,val): # pragma: no cover
        self._water_depth=val
    #------------
    @ current_direction.setter
    def current_direction(self,val): # pragma: no cover
        self._current_direction=float(val)
    #------------
    @ total_surface_area.setter
    def total_surface_area(self,val): # pragma: no cover
        self._total_surface_area=float(val)
    #------------
    @ soil_type.setter
    def soil_type(self,val): # pragma: no cover
        self._soil_type=val
    #------------
    @ Initial_Turbidity.setter
    def Initial_Turbidity(self,val): # pragma: no cover
        self._Initial_Turbidity=float(val)
    #------------
    @ Initial_Noise_dB_re_1muPa.setter
    def Initial_Noise_dB_re_1muPa(self,val): # pragma: no cover
        self._Initial_Noise_dB_re_1muPa=float(val)
    #------------
    @ Initial_Electric_Field.setter
    def Initial_Electric_Field(self,val): # pragma: no cover
        self._Initial_Electric_Field=float(val)
    #------------
    @ Initial_Magnetic_Field.setter
    def Initial_Magnetic_Field(self,val): # pragma: no cover
        self._Initial_Magnetic_Field=float(val)
    #------------
    @ Initial_Temperature.setter
    def Initial_Temperature(self,val): # pragma: no cover
        self._Initial_Temperature=float(val)
    #------------
    @ Fisheries.setter
    def Fisheries(self,val): # pragma: no cover
        self._Fisheries=str(val)
    #------------
    @ protected_species.setter
    def protected_species(self,val): # pragma: no cover
        self._protected_species=val
    #------------
    @ protected_species_length.setter
    def protected_species_length(self,val): # pragma: no cover
        self._protected_species_length=val
    #------------
    @ receptors.setter
    def receptors(self,val): # pragma: no cover
        self._receptors=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:farm_info"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:farm_info"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("Coordinates_of_the_farm_x"):
            rep["Coordinates_of_the_farm_x"] = self.Coordinates_of_the_farm_x
        if self.is_set("Coordinates_of_the_farm_y"):
            rep["Coordinates_of_the_farm_y"] = self.Coordinates_of_the_farm_y
        if self.is_set("project_lifetime"):
            rep["project_lifetime"] = self.project_lifetime
        if self.is_set("lcoe"):
            rep["lcoe"] = self.lcoe
        if self.is_set("zone_type"):
            rep["zone_type"] = self.zone_type
        if self.is_set("water_depth"):
            if (short):
                rep["water_depth"] = str(self.water_depth.shape)
            else:
                try:
                    rep["water_depth"] = self.water_depth.tolist()
                except:
                    rep["water_depth"] = self.water_depth
        if self.is_set("current_direction"):
            rep["current_direction"] = self.current_direction
        if self.is_set("total_surface_area"):
            rep["total_surface_area"] = self.total_surface_area
        if self.is_set("soil_type"):
            if short:
                rep["soil_type"] = str(len(self.soil_type))
            else:
                rep["soil_type"] = self.soil_type
        else:
            rep["soil_type"] = []
        if self.is_set("Initial_Turbidity"):
            rep["Initial_Turbidity"] = self.Initial_Turbidity
        if self.is_set("Initial_Noise_dB_re_1muPa"):
            rep["Initial_Noise_dB_re_1muPa"] = self.Initial_Noise_dB_re_1muPa
        if self.is_set("Initial_Electric_Field"):
            rep["Initial_Electric_Field"] = self.Initial_Electric_Field
        if self.is_set("Initial_Magnetic_Field"):
            rep["Initial_Magnetic_Field"] = self.Initial_Magnetic_Field
        if self.is_set("Initial_Temperature"):
            rep["Initial_Temperature"] = self.Initial_Temperature
        if self.is_set("Fisheries"):
            rep["Fisheries"] = self.Fisheries
        if self.is_set("protected_species"):
            rep["protected_species"] = []
            for i in range(0,len(self.protected_species)):
                if (short and not(deep)):
                    itemType = self.protected_species[i].type_rep()
                    rep["protected_species"].append( itemType )
                else:
                    rep["protected_species"].append( self.protected_species[i].prop_rep(short, deep) )
        else:
            rep["protected_species"] = []
        if self.is_set("protected_species_length"):
            rep["protected_species_length"] = self.protected_species_length
        if self.is_set("receptors"):
            if (short and not(deep)):
                rep["receptors"] = self.receptors.type_rep()
            else:
                rep["receptors"] = self.receptors.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "Coordinates_of_the_farm_x"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Coordinates_of_the_farm_y"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "project_lifetime"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "lcoe"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "zone_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "water_depth"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "current_direction"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "total_surface_area"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "soil_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Initial_Turbidity"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Initial_Noise_dB_re_1muPa"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Initial_Electric_Field"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Initial_Magnetic_Field"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Initial_Temperature"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Fisheries"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "protected_species"
        try :
            if data[varName] != None:
                self.protected_species=[]
                for i in range(0,len(data[varName])):
                    self.protected_species.append(input_species.input_species())
                    self.protected_species[i].loadFromJSONDict(data[varName][i])
            else:
                self.protected_species = []
        except :
            pass
        varName = "protected_species_length"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "receptors"
        try :
            if data[varName] != None:
                self.receptors=receptors_dict.receptors_dict()
                self.receptors.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
