# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import device_dimension
from . import input_materials
from . import foundation
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class device_info():

    """Global information about the device
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._coordinates_devices_x=np.zeros(shape=(1), dtype=float)
        self._coordinates_devices_y=np.zeros(shape=(1), dtype=float)
        self._machine_type=''
        self._dimensions=device_dimension.device_dimension()
        self._floating=False
        self._number_of_device=0.0
        self._resource_reduction=0.0
        self._materials=input_materials.input_materials()
        self._materials_to_recycle=input_materials.input_materials()
        self._measured_noise=0.0
        self._measured_turbidity=0.0
        self._Fishery_Restriction_Surface_around_devices=0.0
        self._foundation=foundation.foundation()
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def coordinates_devices_x(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Coordinates x of the device dim(*) []
        """
        return self._coordinates_devices_x
    #------------
    @ property
    def coordinates_devices_y(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Coordinates y of the device dim(*) []
        """
        return self._coordinates_devices_y
    #------------
    @ property
    def machine_type(self): # pragma: no cover
        """str: WEC or TEC
        """
        return self._machine_type
    #------------
    @ property
    def dimensions(self): # pragma: no cover
        """:obj:`~.device_dimension.device_dimension`: none
        """
        return self._dimensions
    #------------
    @ property
    def floating(self): # pragma: no cover
        """bool: Floating or not []
        """
        return self._floating
    #------------
    @ property
    def number_of_device(self): # pragma: no cover
        """float: Number of device in the farm []
        """
        return self._number_of_device
    #------------
    @ property
    def resource_reduction(self): # pragma: no cover
        """float: Pourcentage of energy reduction []
        """
        return self._resource_reduction
    #------------
    @ property
    def materials(self): # pragma: no cover
        """:obj:`~.input_materials.input_materials`: none
        """
        return self._materials
    #------------
    @ property
    def materials_to_recycle(self): # pragma: no cover
        """:obj:`~.input_materials.input_materials`: none
        """
        return self._materials_to_recycle
    #------------
    @ property
    def measured_noise(self): # pragma: no cover
        """float: Measured noise of the device [dB re 1muPa]
        """
        return self._measured_noise
    #------------
    @ property
    def measured_turbidity(self): # pragma: no cover
        """float: Turbidity due to device installation [mg/L]
        """
        return self._measured_turbidity
    #------------
    @ property
    def Fishery_Restriction_Surface_around_devices(self): # pragma: no cover
        """float: Total Surface around devices closed to fisheries [m²]
        """
        return self._Fishery_Restriction_Surface_around_devices
    #------------
    @ property
    def foundation(self): # pragma: no cover
        """:obj:`~.foundation.foundation`: none
        """
        return self._foundation
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ coordinates_devices_x.setter
    def coordinates_devices_x(self,val): # pragma: no cover
        self._coordinates_devices_x=val
    #------------
    @ coordinates_devices_y.setter
    def coordinates_devices_y(self,val): # pragma: no cover
        self._coordinates_devices_y=val
    #------------
    @ machine_type.setter
    def machine_type(self,val): # pragma: no cover
        self._machine_type=str(val)
    #------------
    @ dimensions.setter
    def dimensions(self,val): # pragma: no cover
        self._dimensions=val
    #------------
    @ floating.setter
    def floating(self,val): # pragma: no cover
        self._floating=val
    #------------
    @ number_of_device.setter
    def number_of_device(self,val): # pragma: no cover
        self._number_of_device=float(val)
    #------------
    @ resource_reduction.setter
    def resource_reduction(self,val): # pragma: no cover
        self._resource_reduction=float(val)
    #------------
    @ materials.setter
    def materials(self,val): # pragma: no cover
        self._materials=val
    #------------
    @ materials_to_recycle.setter
    def materials_to_recycle(self,val): # pragma: no cover
        self._materials_to_recycle=val
    #------------
    @ measured_noise.setter
    def measured_noise(self,val): # pragma: no cover
        self._measured_noise=float(val)
    #------------
    @ measured_turbidity.setter
    def measured_turbidity(self,val): # pragma: no cover
        self._measured_turbidity=float(val)
    #------------
    @ Fishery_Restriction_Surface_around_devices.setter
    def Fishery_Restriction_Surface_around_devices(self,val): # pragma: no cover
        self._Fishery_Restriction_Surface_around_devices=float(val)
    #------------
    @ foundation.setter
    def foundation(self,val): # pragma: no cover
        self._foundation=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:device_info"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:device_info"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("coordinates_devices_x"):
            if (short):
                rep["coordinates_devices_x"] = str(self.coordinates_devices_x.shape)
            else:
                try:
                    rep["coordinates_devices_x"] = self.coordinates_devices_x.tolist()
                except:
                    rep["coordinates_devices_x"] = self.coordinates_devices_x
        if self.is_set("coordinates_devices_y"):
            if (short):
                rep["coordinates_devices_y"] = str(self.coordinates_devices_y.shape)
            else:
                try:
                    rep["coordinates_devices_y"] = self.coordinates_devices_y.tolist()
                except:
                    rep["coordinates_devices_y"] = self.coordinates_devices_y
        if self.is_set("machine_type"):
            rep["machine_type"] = self.machine_type
        if self.is_set("dimensions"):
            if (short and not(deep)):
                rep["dimensions"] = self.dimensions.type_rep()
            else:
                rep["dimensions"] = self.dimensions.prop_rep(short, deep)
        if self.is_set("floating"):
            rep["floating"] = self.floating
        if self.is_set("number_of_device"):
            rep["number_of_device"] = self.number_of_device
        if self.is_set("resource_reduction"):
            rep["resource_reduction"] = self.resource_reduction
        if self.is_set("materials"):
            if (short and not(deep)):
                rep["materials"] = self.materials.type_rep()
            else:
                rep["materials"] = self.materials.prop_rep(short, deep)
        if self.is_set("materials_to_recycle"):
            if (short and not(deep)):
                rep["materials_to_recycle"] = self.materials_to_recycle.type_rep()
            else:
                rep["materials_to_recycle"] = self.materials_to_recycle.prop_rep(short, deep)
        if self.is_set("measured_noise"):
            rep["measured_noise"] = self.measured_noise
        if self.is_set("measured_turbidity"):
            rep["measured_turbidity"] = self.measured_turbidity
        if self.is_set("Fishery_Restriction_Surface_around_devices"):
            rep["Fishery_Restriction_Surface_around_devices"] = self.Fishery_Restriction_Surface_around_devices
        if self.is_set("foundation"):
            if (short and not(deep)):
                rep["foundation"] = self.foundation.type_rep()
            else:
                rep["foundation"] = self.foundation.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "coordinates_devices_x"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "coordinates_devices_y"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "machine_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "dimensions"
        try :
            if data[varName] != None:
                self.dimensions=device_dimension.device_dimension()
                self.dimensions.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "floating"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "number_of_device"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "resource_reduction"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "materials"
        try :
            if data[varName] != None:
                self.materials=input_materials.input_materials()
                self.materials.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "materials_to_recycle"
        try :
            if data[varName] != None:
                self.materials_to_recycle=input_materials.input_materials()
                self.materials_to_recycle.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "measured_noise"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "measured_turbidity"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Fishery_Restriction_Surface_around_devices"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "foundation"
        try :
            if data[varName] != None:
                self.foundation=foundation.foundation()
                self.foundation.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
