# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class receptors_dict():

    """List of receptors dictionnary
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._Hard_substrate_benthic_habitat=['']*13
        self._Soft_substrate_benthic_habitat=['']*13
        self._Particular_habitat=['']*13
        self._Shallow_diving_birds=['']*13
        self._Medium_diving_birds=['']*13
        self._Deep_diving_birds=['']*13
        self._Large_odontocete_Mysticete=['']*13
        self._Odontoncete_dolphinds=['']*13
        self._Seals=['']*13
        self._Fishes=['']*13
        self._Bony_fishes=['']*13
        self._Magnetosensitive_species=['']*13
        self._Electrosensitive_species=['']*13
        self._Elasmobranchs=['']*13
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def Hard_substrate_benthic_habitat(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:none
        """
        return self._Hard_substrate_benthic_habitat
    #------------
    @ property
    def Soft_substrate_benthic_habitat(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:none
        """
        return self._Soft_substrate_benthic_habitat
    #------------
    @ property
    def Particular_habitat(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:none
        """
        return self._Particular_habitat
    #------------
    @ property
    def Shallow_diving_birds(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:none
        """
        return self._Shallow_diving_birds
    #------------
    @ property
    def Medium_diving_birds(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:none
        """
        return self._Medium_diving_birds
    #------------
    @ property
    def Deep_diving_birds(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:none
        """
        return self._Deep_diving_birds
    #------------
    @ property
    def Large_odontocete_Mysticete(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:none
        """
        return self._Large_odontocete_Mysticete
    #------------
    @ property
    def Odontoncete_dolphinds(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:none
        """
        return self._Odontoncete_dolphinds
    #------------
    @ property
    def Seals(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:none
        """
        return self._Seals
    #------------
    @ property
    def Fishes(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:none
        """
        return self._Fishes
    #------------
    @ property
    def Bony_fishes(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:none
        """
        return self._Bony_fishes
    #------------
    @ property
    def Magnetosensitive_species(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:none
        """
        return self._Magnetosensitive_species
    #------------
    @ property
    def Electrosensitive_species(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:none
        """
        return self._Electrosensitive_species
    #------------
    @ property
    def Elasmobranchs(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:none
        """
        return self._Elasmobranchs
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ Hard_substrate_benthic_habitat.setter
    def Hard_substrate_benthic_habitat(self,val): # pragma: no cover
        self._Hard_substrate_benthic_habitat=val
    #------------
    @ Soft_substrate_benthic_habitat.setter
    def Soft_substrate_benthic_habitat(self,val): # pragma: no cover
        self._Soft_substrate_benthic_habitat=val
    #------------
    @ Particular_habitat.setter
    def Particular_habitat(self,val): # pragma: no cover
        self._Particular_habitat=val
    #------------
    @ Shallow_diving_birds.setter
    def Shallow_diving_birds(self,val): # pragma: no cover
        self._Shallow_diving_birds=val
    #------------
    @ Medium_diving_birds.setter
    def Medium_diving_birds(self,val): # pragma: no cover
        self._Medium_diving_birds=val
    #------------
    @ Deep_diving_birds.setter
    def Deep_diving_birds(self,val): # pragma: no cover
        self._Deep_diving_birds=val
    #------------
    @ Large_odontocete_Mysticete.setter
    def Large_odontocete_Mysticete(self,val): # pragma: no cover
        self._Large_odontocete_Mysticete=val
    #------------
    @ Odontoncete_dolphinds.setter
    def Odontoncete_dolphinds(self,val): # pragma: no cover
        self._Odontoncete_dolphinds=val
    #------------
    @ Seals.setter
    def Seals(self,val): # pragma: no cover
        self._Seals=val
    #------------
    @ Fishes.setter
    def Fishes(self,val): # pragma: no cover
        self._Fishes=val
    #------------
    @ Bony_fishes.setter
    def Bony_fishes(self,val): # pragma: no cover
        self._Bony_fishes=val
    #------------
    @ Magnetosensitive_species.setter
    def Magnetosensitive_species(self,val): # pragma: no cover
        self._Magnetosensitive_species=val
    #------------
    @ Electrosensitive_species.setter
    def Electrosensitive_species(self,val): # pragma: no cover
        self._Electrosensitive_species=val
    #------------
    @ Elasmobranchs.setter
    def Elasmobranchs(self,val): # pragma: no cover
        self._Elasmobranchs=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:receptors_dict"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:receptors_dict"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("Hard_substrate_benthic_habitat"):
            if short:
                rep["Hard_substrate_benthic_habitat"] = str(len(self.Hard_substrate_benthic_habitat))
            else:
                rep["Hard_substrate_benthic_habitat"] = self.Hard_substrate_benthic_habitat
        else:
            rep["Hard_substrate_benthic_habitat"] = []
        if self.is_set("Soft_substrate_benthic_habitat"):
            if short:
                rep["Soft_substrate_benthic_habitat"] = str(len(self.Soft_substrate_benthic_habitat))
            else:
                rep["Soft_substrate_benthic_habitat"] = self.Soft_substrate_benthic_habitat
        else:
            rep["Soft_substrate_benthic_habitat"] = []
        if self.is_set("Particular_habitat"):
            if short:
                rep["Particular_habitat"] = str(len(self.Particular_habitat))
            else:
                rep["Particular_habitat"] = self.Particular_habitat
        else:
            rep["Particular_habitat"] = []
        if self.is_set("Shallow_diving_birds"):
            if short:
                rep["Shallow_diving_birds"] = str(len(self.Shallow_diving_birds))
            else:
                rep["Shallow_diving_birds"] = self.Shallow_diving_birds
        else:
            rep["Shallow_diving_birds"] = []
        if self.is_set("Medium_diving_birds"):
            if short:
                rep["Medium_diving_birds"] = str(len(self.Medium_diving_birds))
            else:
                rep["Medium_diving_birds"] = self.Medium_diving_birds
        else:
            rep["Medium_diving_birds"] = []
        if self.is_set("Deep_diving_birds"):
            if short:
                rep["Deep_diving_birds"] = str(len(self.Deep_diving_birds))
            else:
                rep["Deep_diving_birds"] = self.Deep_diving_birds
        else:
            rep["Deep_diving_birds"] = []
        if self.is_set("Large_odontocete_Mysticete"):
            if short:
                rep["Large_odontocete_Mysticete"] = str(len(self.Large_odontocete_Mysticete))
            else:
                rep["Large_odontocete_Mysticete"] = self.Large_odontocete_Mysticete
        else:
            rep["Large_odontocete_Mysticete"] = []
        if self.is_set("Odontoncete_dolphinds"):
            if short:
                rep["Odontoncete_dolphinds"] = str(len(self.Odontoncete_dolphinds))
            else:
                rep["Odontoncete_dolphinds"] = self.Odontoncete_dolphinds
        else:
            rep["Odontoncete_dolphinds"] = []
        if self.is_set("Seals"):
            if short:
                rep["Seals"] = str(len(self.Seals))
            else:
                rep["Seals"] = self.Seals
        else:
            rep["Seals"] = []
        if self.is_set("Fishes"):
            if short:
                rep["Fishes"] = str(len(self.Fishes))
            else:
                rep["Fishes"] = self.Fishes
        else:
            rep["Fishes"] = []
        if self.is_set("Bony_fishes"):
            if short:
                rep["Bony_fishes"] = str(len(self.Bony_fishes))
            else:
                rep["Bony_fishes"] = self.Bony_fishes
        else:
            rep["Bony_fishes"] = []
        if self.is_set("Magnetosensitive_species"):
            if short:
                rep["Magnetosensitive_species"] = str(len(self.Magnetosensitive_species))
            else:
                rep["Magnetosensitive_species"] = self.Magnetosensitive_species
        else:
            rep["Magnetosensitive_species"] = []
        if self.is_set("Electrosensitive_species"):
            if short:
                rep["Electrosensitive_species"] = str(len(self.Electrosensitive_species))
            else:
                rep["Electrosensitive_species"] = self.Electrosensitive_species
        else:
            rep["Electrosensitive_species"] = []
        if self.is_set("Elasmobranchs"):
            if short:
                rep["Elasmobranchs"] = str(len(self.Elasmobranchs))
            else:
                rep["Elasmobranchs"] = self.Elasmobranchs
        else:
            rep["Elasmobranchs"] = []
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "Hard_substrate_benthic_habitat"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Soft_substrate_benthic_habitat"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Particular_habitat"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Shallow_diving_birds"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Medium_diving_birds"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Deep_diving_birds"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Large_odontocete_Mysticete"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Odontoncete_dolphinds"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Seals"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Fishes"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Bony_fishes"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Magnetosensitive_species"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Electrosensitive_species"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Elasmobranchs"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
