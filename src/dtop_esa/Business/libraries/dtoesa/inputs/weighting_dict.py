# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import weighting_list
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class weighting_dict():

    """List of receptors dictionnary
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._weighting_hydro=weighting_list.weighting_list()
        self._weighting_elec=weighting_list.weighting_list()
        self._weighting_moor=weighting_list.weighting_list()
        self._weighting_insta=weighting_list.weighting_list()
        self._weighting_maint=weighting_list.weighting_list()
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def weighting_hydro(self): # pragma: no cover
        """:obj:`~.weighting_list.weighting_list`: none
        """
        return self._weighting_hydro
    #------------
    @ property
    def weighting_elec(self): # pragma: no cover
        """:obj:`~.weighting_list.weighting_list`: none
        """
        return self._weighting_elec
    #------------
    @ property
    def weighting_moor(self): # pragma: no cover
        """:obj:`~.weighting_list.weighting_list`: none
        """
        return self._weighting_moor
    #------------
    @ property
    def weighting_insta(self): # pragma: no cover
        """:obj:`~.weighting_list.weighting_list`: none
        """
        return self._weighting_insta
    #------------
    @ property
    def weighting_maint(self): # pragma: no cover
        """:obj:`~.weighting_list.weighting_list`: none
        """
        return self._weighting_maint
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ weighting_hydro.setter
    def weighting_hydro(self,val): # pragma: no cover
        self._weighting_hydro=val
    #------------
    @ weighting_elec.setter
    def weighting_elec(self,val): # pragma: no cover
        self._weighting_elec=val
    #------------
    @ weighting_moor.setter
    def weighting_moor(self,val): # pragma: no cover
        self._weighting_moor=val
    #------------
    @ weighting_insta.setter
    def weighting_insta(self,val): # pragma: no cover
        self._weighting_insta=val
    #------------
    @ weighting_maint.setter
    def weighting_maint(self,val): # pragma: no cover
        self._weighting_maint=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:weighting_dict"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:weighting_dict"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("weighting_hydro"):
            if (short and not(deep)):
                rep["weighting_hydro"] = self.weighting_hydro.type_rep()
            else:
                rep["weighting_hydro"] = self.weighting_hydro.prop_rep(short, deep)
        if self.is_set("weighting_elec"):
            if (short and not(deep)):
                rep["weighting_elec"] = self.weighting_elec.type_rep()
            else:
                rep["weighting_elec"] = self.weighting_elec.prop_rep(short, deep)
        if self.is_set("weighting_moor"):
            if (short and not(deep)):
                rep["weighting_moor"] = self.weighting_moor.type_rep()
            else:
                rep["weighting_moor"] = self.weighting_moor.prop_rep(short, deep)
        if self.is_set("weighting_insta"):
            if (short and not(deep)):
                rep["weighting_insta"] = self.weighting_insta.type_rep()
            else:
                rep["weighting_insta"] = self.weighting_insta.prop_rep(short, deep)
        if self.is_set("weighting_maint"):
            if (short and not(deep)):
                rep["weighting_maint"] = self.weighting_maint.type_rep()
            else:
                rep["weighting_maint"] = self.weighting_maint.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "weighting_hydro"
        try :
            if data[varName] != None:
                self.weighting_hydro=weighting_list.weighting_list()
                self.weighting_hydro.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "weighting_elec"
        try :
            if data[varName] != None:
                self.weighting_elec=weighting_list.weighting_list()
                self.weighting_elec.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "weighting_moor"
        try :
            if data[varName] != None:
                self.weighting_moor=weighting_list.weighting_list()
                self.weighting_moor.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "weighting_insta"
        try :
            if data[varName] != None:
                self.weighting_insta=weighting_list.weighting_list()
                self.weighting_insta.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "weighting_maint"
        try :
            if data[varName] != None:
                self.weighting_maint=weighting_list.weighting_list()
                self.weighting_maint.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
