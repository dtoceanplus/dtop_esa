# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class input_hydro():

    """
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._Energy_Modification=0.0
        self._Coordinates_of_the_Devices_x=np.zeros(shape=(1), dtype=float)
        self._Coordinates_of_the_Devices_y=np.zeros(shape=(1), dtype=float)
        self._Size_of_the_Devices=0.0
        self._Immersed_Height_of_the_Devices=0.0
        self._Water_Depth=0.0
        self._Current_Direction=0.0
        self._Initial_Turbidity=0
        self._Measured_Turbidity=0.0
        self._Initial_Noise_dB_re_1muPa=0.0
        self._Measured_Noise_dB_re_1muPa=0.0
        self._Fishery_Restriction_Surface=0.0
        self._Total_Surface_Area=0.0
        self._Number_of_Objects=0.0
        self._Object_Emerged_Surface=0.0
        self._Surface_Area_of_Underwater_Part=0.0
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def Energy_Modification(self): # pragma: no cover
        """float: none []
        """
        return self._Energy_Modification
    #------------
    @ property
    def Coordinates_of_the_Devices_x(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:none dim(*) []
        """
        return self._Coordinates_of_the_Devices_x
    #------------
    @ property
    def Coordinates_of_the_Devices_y(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:none dim(*) []
        """
        return self._Coordinates_of_the_Devices_y
    #------------
    @ property
    def Size_of_the_Devices(self): # pragma: no cover
        """float: none []
        """
        return self._Size_of_the_Devices
    #------------
    @ property
    def Immersed_Height_of_the_Devices(self): # pragma: no cover
        """float: none []
        """
        return self._Immersed_Height_of_the_Devices
    #------------
    @ property
    def Water_Depth(self): # pragma: no cover
        """float: none []
        """
        return self._Water_Depth
    #------------
    @ property
    def Current_Direction(self): # pragma: no cover
        """float: none []
        """
        return self._Current_Direction
    #------------
    @ property
    def Initial_Turbidity(self): # pragma: no cover
        """float: none []
        """
        return self._Initial_Turbidity
    #------------
    @ property
    def Measured_Turbidity(self): # pragma: no cover
        """float: none []
        """
        return self._Measured_Turbidity
    #------------
    @ property
    def Initial_Noise_dB_re_1muPa(self): # pragma: no cover
        """float: none []
        """
        return self._Initial_Noise_dB_re_1muPa
    #------------
    @ property
    def Measured_Noise_dB_re_1muPa(self): # pragma: no cover
        """float: none []
        """
        return self._Measured_Noise_dB_re_1muPa
    #------------
    @ property
    def Fishery_Restriction_Surface(self): # pragma: no cover
        """float: none []
        """
        return self._Fishery_Restriction_Surface
    #------------
    @ property
    def Total_Surface_Area(self): # pragma: no cover
        """float: none []
        """
        return self._Total_Surface_Area
    #------------
    @ property
    def Number_of_Objects(self): # pragma: no cover
        """float: none []
        """
        return self._Number_of_Objects
    #------------
    @ property
    def Object_Emerged_Surface(self): # pragma: no cover
        """float: none []
        """
        return self._Object_Emerged_Surface
    #------------
    @ property
    def Surface_Area_of_Underwater_Part(self): # pragma: no cover
        """float: none []
        """
        return self._Surface_Area_of_Underwater_Part
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ Energy_Modification.setter
    def Energy_Modification(self,val): # pragma: no cover
        self._Energy_Modification=float(val)
    #------------
    @ Coordinates_of_the_Devices_x.setter
    def Coordinates_of_the_Devices_x(self,val): # pragma: no cover
        self._Coordinates_of_the_Devices_x=val
    #------------
    @ Coordinates_of_the_Devices_y.setter
    def Coordinates_of_the_Devices_y(self,val): # pragma: no cover
        self._Coordinates_of_the_Devices_y=val
    #------------
    @ Size_of_the_Devices.setter
    def Size_of_the_Devices(self,val): # pragma: no cover
        self._Size_of_the_Devices=float(val)
    #------------
    @ Immersed_Height_of_the_Devices.setter
    def Immersed_Height_of_the_Devices(self,val): # pragma: no cover
        self._Immersed_Height_of_the_Devices=float(val)
    #------------
    @ Water_Depth.setter
    def Water_Depth(self,val): # pragma: no cover
        self._Water_Depth=float(val)
    #------------
    @ Current_Direction.setter
    def Current_Direction(self,val): # pragma: no cover
        self._Current_Direction=float(val)
    #------------
    @ Initial_Turbidity.setter
    def Initial_Turbidity(self,val): # pragma: no cover
        self._Initial_Turbidity=float(val)
    #------------
    @ Measured_Turbidity.setter
    def Measured_Turbidity(self,val): # pragma: no cover
        self._Measured_Turbidity=float(val)
    #------------
    @ Initial_Noise_dB_re_1muPa.setter
    def Initial_Noise_dB_re_1muPa(self,val): # pragma: no cover
        self._Initial_Noise_dB_re_1muPa=float(val)
    #------------
    @ Measured_Noise_dB_re_1muPa.setter
    def Measured_Noise_dB_re_1muPa(self,val): # pragma: no cover
        self._Measured_Noise_dB_re_1muPa=float(val)
    #------------
    @ Fishery_Restriction_Surface.setter
    def Fishery_Restriction_Surface(self,val): # pragma: no cover
        self._Fishery_Restriction_Surface=float(val)
    #------------
    @ Total_Surface_Area.setter
    def Total_Surface_Area(self,val): # pragma: no cover
        self._Total_Surface_Area=float(val)
    #------------
    @ Number_of_Objects.setter
    def Number_of_Objects(self,val): # pragma: no cover
        self._Number_of_Objects=float(val)
    #------------
    @ Object_Emerged_Surface.setter
    def Object_Emerged_Surface(self,val): # pragma: no cover
        self._Object_Emerged_Surface=float(val)
    #------------
    @ Surface_Area_of_Underwater_Part.setter
    def Surface_Area_of_Underwater_Part(self,val): # pragma: no cover
        self._Surface_Area_of_Underwater_Part=float(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:input_hydro"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:inputs:input_hydro"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("Energy_Modification"):
            rep["Energy_Modification"] = self.Energy_Modification
        if self.is_set("Coordinates_of_the_Devices_x"):
            if (short):
                rep["Coordinates_of_the_Devices_x"] = str(self.Coordinates_of_the_Devices_x.shape)
            else:
                try:
                    rep["Coordinates_of_the_Devices_x"] = self.Coordinates_of_the_Devices_x.tolist()
                except:
                    rep["Coordinates_of_the_Devices_x"] = self.Coordinates_of_the_Devices_x
        if self.is_set("Coordinates_of_the_Devices_y"):
            if (short):
                rep["Coordinates_of_the_Devices_y"] = str(self.Coordinates_of_the_Devices_y.shape)
            else:
                try:
                    rep["Coordinates_of_the_Devices_y"] = self.Coordinates_of_the_Devices_y.tolist()
                except:
                    rep["Coordinates_of_the_Devices_y"] = self.Coordinates_of_the_Devices_y
        if self.is_set("Size_of_the_Devices"):
            rep["Size_of_the_Devices"] = self.Size_of_the_Devices
        if self.is_set("Immersed_Height_of_the_Devices"):
            rep["Immersed_Height_of_the_Devices"] = self.Immersed_Height_of_the_Devices
        if self.is_set("Water_Depth"):
            rep["Water_Depth"] = self.Water_Depth
        if self.is_set("Current_Direction"):
            rep["Current_Direction"] = self.Current_Direction
        if self.is_set("Initial_Turbidity"):
            rep["Initial_Turbidity"] = self.Initial_Turbidity
        if self.is_set("Measured_Turbidity"):
            rep["Measured_Turbidity"] = self.Measured_Turbidity
        if self.is_set("Initial_Noise_dB_re_1muPa"):
            rep["Initial_Noise_dB_re_1muPa"] = self.Initial_Noise_dB_re_1muPa
        if self.is_set("Measured_Noise_dB_re_1muPa"):
            rep["Measured_Noise_dB_re_1muPa"] = self.Measured_Noise_dB_re_1muPa
        if self.is_set("Fishery_Restriction_Surface"):
            rep["Fishery_Restriction_Surface"] = self.Fishery_Restriction_Surface
        if self.is_set("Total_Surface_Area"):
            rep["Total_Surface_Area"] = self.Total_Surface_Area
        if self.is_set("Number_of_Objects"):
            rep["Number_of_Objects"] = self.Number_of_Objects
        if self.is_set("Object_Emerged_Surface"):
            rep["Object_Emerged_Surface"] = self.Object_Emerged_Surface
        if self.is_set("Surface_Area_of_Underwater_Part"):
            rep["Surface_Area_of_Underwater_Part"] = self.Surface_Area_of_Underwater_Part
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "Energy_Modification"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Coordinates_of_the_Devices_x"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "Coordinates_of_the_Devices_y"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "Size_of_the_Devices"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Immersed_Height_of_the_Devices"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Water_Depth"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Current_Direction"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Initial_Turbidity"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Measured_Turbidity"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Initial_Noise_dB_re_1muPa"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Measured_Noise_dB_re_1muPa"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Fishery_Restriction_Surface"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Total_Surface_Area"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Number_of_Objects"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Object_Emerged_Surface"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Surface_Area_of_Underwater_Part"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
