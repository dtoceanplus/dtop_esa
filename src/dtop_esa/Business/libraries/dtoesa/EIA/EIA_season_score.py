# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class EIA_season_score():

    """EIA results per month
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._January=0.0
        self._February=0.0
        self._March=0.0
        self._April=0.0
        self._May=0.0
        self._June=0.0
        self._July=0.0
        self._August=0.0
        self._September=0.0
        self._Oktober=0.0
        self._November=0.0
        self._December=0.0
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def January(self): # pragma: no cover
        """float: none []
        """
        return self._January
    #------------
    @ property
    def February(self): # pragma: no cover
        """float: none []
        """
        return self._February
    #------------
    @ property
    def March(self): # pragma: no cover
        """float: none []
        """
        return self._March
    #------------
    @ property
    def April(self): # pragma: no cover
        """float: none []
        """
        return self._April
    #------------
    @ property
    def May(self): # pragma: no cover
        """float: none []
        """
        return self._May
    #------------
    @ property
    def June(self): # pragma: no cover
        """float: none []
        """
        return self._June
    #------------
    @ property
    def July(self): # pragma: no cover
        """float: none []
        """
        return self._July
    #------------
    @ property
    def August(self): # pragma: no cover
        """float: none []
        """
        return self._August
    #------------
    @ property
    def September(self): # pragma: no cover
        """float: none []
        """
        return self._September
    #------------
    @ property
    def Oktober(self): # pragma: no cover
        """float: none []
        """
        return self._Oktober
    #------------
    @ property
    def November(self): # pragma: no cover
        """float: none []
        """
        return self._November
    #------------
    @ property
    def December(self): # pragma: no cover
        """float: none []
        """
        return self._December
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ January.setter
    def January(self,val): # pragma: no cover
        self._January=float(val)
    #------------
    @ February.setter
    def February(self,val): # pragma: no cover
        self._February=float(val)
    #------------
    @ March.setter
    def March(self,val): # pragma: no cover
        self._March=float(val)
    #------------
    @ April.setter
    def April(self,val): # pragma: no cover
        self._April=float(val)
    #------------
    @ May.setter
    def May(self,val): # pragma: no cover
        self._May=float(val)
    #------------
    @ June.setter
    def June(self,val): # pragma: no cover
        self._June=float(val)
    #------------
    @ July.setter
    def July(self,val): # pragma: no cover
        self._July=float(val)
    #------------
    @ August.setter
    def August(self,val): # pragma: no cover
        self._August=float(val)
    #------------
    @ September.setter
    def September(self,val): # pragma: no cover
        self._September=float(val)
    #------------
    @ Oktober.setter
    def Oktober(self,val): # pragma: no cover
        self._Oktober=float(val)
    #------------
    @ November.setter
    def November(self,val): # pragma: no cover
        self._November=float(val)
    #------------
    @ December.setter
    def December(self,val): # pragma: no cover
        self._December=float(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:EIA:EIA_season_score"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:EIA:EIA_season_score"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("January"):
            rep["January"] = self.January
        if self.is_set("February"):
            rep["February"] = self.February
        if self.is_set("March"):
            rep["March"] = self.March
        if self.is_set("April"):
            rep["April"] = self.April
        if self.is_set("May"):
            rep["May"] = self.May
        if self.is_set("June"):
            rep["June"] = self.June
        if self.is_set("July"):
            rep["July"] = self.July
        if self.is_set("August"):
            rep["August"] = self.August
        if self.is_set("September"):
            rep["September"] = self.September
        if self.is_set("Oktober"):
            rep["Oktober"] = self.Oktober
        if self.is_set("November"):
            rep["November"] = self.November
        if self.is_set("December"):
            rep["December"] = self.December
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "January"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "February"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "March"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "April"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "May"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "June"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "July"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "August"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "September"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Oktober"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "November"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "December"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
