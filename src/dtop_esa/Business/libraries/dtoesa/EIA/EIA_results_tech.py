# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import EIA_tech
#------------------------------------
# @ USER DEFINED IMPORTS START
from . import EIA_results
from . import EIA_recommendation_dict
from . import EIA_confidence_dict
from . import EIA_season
from . import EIA_season_score
from .dtocean_environment import main
# @ USER DEFINED IMPORTS END
#------------------------------------

class EIA_results_tech():

    """List of results per technology group
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._impact_hydrodynamics=EIA_tech.EIA_tech()
        self._impact_electrical=EIA_tech.EIA_tech()
        self._impact_station_keeping=EIA_tech.EIA_tech()
        self._impact_installation=EIA_tech.EIA_tech()
        self._impact_maintenance=EIA_tech.EIA_tech()
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def compute_impact_hydrodynamics(self, input_hydro, protected, receptors, weighting):
        hydro = main.HydroStage(protected, receptors, weighting)
        (confidence_dict, eis_dict, recommendation_dict, seasons, global_eis) = hydro(input_hydro)
        self.impact_hydrodynamics.description = "Environmental impacts of the hydrodynamics technology group"
        self.impact_hydrodynamics.global_eis.description = "Global environmental impact of the hydrodynamics technology group"
        self.impact_hydrodynamics.global_eis.negative_impact = "%.f" % global_eis["Negative Impact"]
        self.impact_hydrodynamics.global_eis.max_negative_impact = global_eis["Max Negative Impact"]
        self.impact_hydrodynamics.global_eis.min_negative_impact = global_eis["Min Negative Impact"]
        self.impact_hydrodynamics.global_eis.positive_impact = "%.f" % global_eis["Positive Impact"]
        self.impact_hydrodynamics.global_eis.max_positive_impact = global_eis["Max Positive Impact"]
        self.impact_hydrodynamics.global_eis.min_positive_impact = global_eis["Min Positive Impact"]

        eis = []
        confidence = []
        recommendation = []
        season = []

        for key, value in eis_dict.items():
            NFA = EIA_results.EIA_results()
            NFA.init_from_files(function_name = key, score= "%.f" % value)
            eis.append(NFA)

        self.impact_hydrodynamics.eis_dict = eis

        for key, value in confidence_dict.items():
            NFA = EIA_confidence_dict.EIA_confidence_dict()
            NFA.init_from_files(function_name = key, confidence_level= value)
            confidence.append(NFA)

        self.impact_hydrodynamics.confidence_dict = confidence

        for key,value in recommendation_dict.items():
            NFA = EIA_recommendation_dict.EIA_recommendation_dict()
            NFA.init_from_files(function_name = key, generic_explanation = recommendation_dict[key]["Generic Explanation"], general_recommendation = recommendation_dict[key]["General Recommendation"], detailed_recommendation = recommendation_dict[key]["Detailed Recommendation"])
            recommendation.append(NFA)

        self.impact_hydrodynamics.recommendation_dict = recommendation

        sonsea = np.transpose(seasons)

        for key,value in sonsea.items():
            NFA = EIA_season.EIA_season()
            NFA.init_from_files(function_name = key, season_info = value)
            season.append(NFA)

        self.impact_hydrodynamics.seasons = season



    def compute_impact_electrical(self, input_elec, protected, receptors, weighting):
        elec = main.ElectricalStage(protected, receptors, weighting)
        (confidence_dict, eis_dict, recommendation_dict, seasons, global_eis) = elec(input_elec)
        self.impact_electrical.description = "Environmental impacts of the electrical technology group"
        self.impact_electrical.global_eis.description = "Global environmental impact of the electrical technology group"
        # if type(global_eis["Negative Impact"]) == float:
        self.impact_electrical.global_eis.negative_impact = "%.f" % global_eis["Negative Impact"]
        self.impact_electrical.global_eis.max_negative_impact = global_eis["Max Negative Impact"]
        self.impact_electrical.global_eis.min_negative_impact = global_eis["Min Negative Impact"]
        # else:
        #     self.impact_electrical.global_eis.negative_impact = 999
        #     self.impact_electrical.global_eis.max_negative_impact = 999
        #     self.impact_electrical.global_eis.min_negative_impact = 999
        self.impact_electrical.global_eis.positive_impact =  "%.f" % global_eis["Positive Impact"]
        self.impact_electrical.global_eis.max_positive_impact = global_eis["Max Positive Impact"]
        self.impact_electrical.global_eis.min_positive_impact = global_eis["Min Positive Impact"]

        eis = []
        confidence = []
        recommendation = []
        season = []

        for key, value in eis_dict.items():
            NFA = EIA_results.EIA_results()
            NFA.init_from_files(function_name = key, score= "%.f" % value)
            eis.append(NFA)

        self.impact_electrical.eis_dict = eis

        for key, value in confidence_dict.items():
            NFA = EIA_confidence_dict.EIA_confidence_dict()
            NFA.init_from_files(function_name = key, confidence_level= value)
            confidence.append(NFA)

        self.impact_electrical.confidence_dict = confidence

        for key,value in recommendation_dict.items():
            NFA = EIA_recommendation_dict.EIA_recommendation_dict()
            NFA.init_from_files(function_name = key, generic_explanation = recommendation_dict[key]["Generic Explanation"], general_recommendation = recommendation_dict[key]["General Recommendation"], detailed_recommendation = recommendation_dict[key]["Detailed Recommendation"])
            recommendation.append(NFA)

        self.impact_electrical.recommendation_dict = recommendation

        sonsea = np.transpose(seasons)

        for key,value in sonsea.items():
            NFA = EIA_season.EIA_season()
            NFA.init_from_files(function_name = key, season_info = value)
            season.append(NFA)

        self.impact_electrical.seasons = season

    def compute_impact_station_keeping(self, input_SK, protected, receptors, weighting):
        moor = main.MooringStage(protected, receptors, weighting)
        (confidence_dict, eis_dict, recommendation_dict, seasons, global_eis) = moor(input_SK)
        self.impact_station_keeping.description = "Environmental impacts of the station keeping technology group"
        self.impact_station_keeping.global_eis.description = "Global environmental impact of the station keeping technology group"

        self.impact_station_keeping.global_eis.negative_impact = "%.f" % global_eis["Negative Impact"]
        self.impact_station_keeping.global_eis.max_negative_impact = global_eis["Max Negative Impact"]
        self.impact_station_keeping.global_eis.min_negative_impact = global_eis["Min Negative Impact"]
        self.impact_station_keeping.global_eis.positive_impact = "%.f" % global_eis["Positive Impact"]
        self.impact_station_keeping.global_eis.max_positive_impact = global_eis["Max Positive Impact"]
        self.impact_station_keeping.global_eis.min_positive_impact = global_eis["Min Positive Impact"]

        eis = []
        confidence = []
        recommendation = []
        season = []

        for key, value in eis_dict.items():
            NFA = EIA_results.EIA_results()
            NFA.init_from_files(function_name = key, score= "%.f" % value)
            eis.append(NFA)

        self.impact_station_keeping.eis_dict = eis

        for key, value in confidence_dict.items():
            NFA = EIA_confidence_dict.EIA_confidence_dict()
            NFA.init_from_files(function_name = key, confidence_level= value)
            confidence.append(NFA)

        self.impact_station_keeping.confidence_dict = confidence

        for key,value in recommendation_dict.items():
            NFA = EIA_recommendation_dict.EIA_recommendation_dict()
            NFA.init_from_files(function_name = key, generic_explanation = recommendation_dict[key]["Generic Explanation"], general_recommendation = recommendation_dict[key]["General Recommendation"], detailed_recommendation = recommendation_dict[key]["Detailed Recommendation"])
            recommendation.append(NFA)

        self.impact_station_keeping.recommendation_dict = recommendation

        sonsea = np.transpose(seasons)

        for key,value in sonsea.items():
            NFA = EIA_season.EIA_season()
            NFA.init_from_files(function_name = key, season_info = value)
            season.append(NFA)

        self.impact_station_keeping.seasons = season

    def compute_impact_installation(self, input_insta, protected, receptors, weighting):
        instal = main.InstallationStage(protected, receptors, weighting)
        (confidence_dict, eis_dict, recommendation_dict, seasons, global_eis) = instal(input_insta)
        self.impact_installation.description = "Environmental impacts of the the installation phase"
        self.impact_installation.global_eis.description = "Global environmental impact of the installation phase"
        if type(global_eis["Negative Impact"]) == np.float64:
            self.impact_installation.global_eis.negative_impact = "%.f" % global_eis["Negative Impact"]
            self.impact_installation.global_eis.max_negative_impact = global_eis["Max Negative Impact"]
            self.impact_installation.global_eis.min_negative_impact = global_eis["Min Negative Impact"]
        else:
            self.impact_installation.global_eis.negative_impact = 999
            self.impact_installation.global_eis.max_negative_impact = 999
            self.impact_installation.global_eis.min_negative_impact = 999
        # self.impact_installation.global_eis.negative_impact = global_eis["Negative Impact"]
        # self.impact_installation.global_eis.max_negative_impact = global_eis["Max Negative Impact"]
        # self.impact_installation.global_eis.min_negative_impact = global_eis["Min Negative Impact"]

        if type(global_eis["Positive Impact"]) == np.float64:
            self.impact_installation.global_eis.positive_impact = "%.f" % global_eis["Positive Impact"]
            self.impact_installation.global_eis.max_positive_impact = global_eis["Max Positive Impact"]
            self.impact_installation.global_eis.min_positive_impact = global_eis["Min Positive Impact"]
        else:
            self.impact_installation.global_eis.positive_impact = 999
            self.impact_installation.global_eis.max_positive_impact = 999
            self.impact_installation.global_eis.min_positive_impact = 999
        # self.impact_installation.global_eis.positive_impact = global_eis["Positive Impact"]
        # self.impact_installation.global_eis.max_positive_impact = global_eis["Max Positive Impact"]
        # self.impact_installation.global_eis.min_positive_impact = global_eis["Min Positive Impact"]

        eis = []
        confidence = []
        recommendation = []
        season = []

        for key, value in eis_dict.items():
            NFA = EIA_results.EIA_results()
            NFA.init_from_files(function_name = key, score= "%.f" % value)
            eis.append(NFA)

        self.impact_installation.eis_dict = eis

        for key, value in confidence_dict.items():
            NFA = EIA_confidence_dict.EIA_confidence_dict()
            NFA.init_from_files(function_name = key, confidence_level= value)
            confidence.append(NFA)

        self.impact_installation.confidence_dict = confidence

        for key,value in recommendation_dict.items():
            NFA = EIA_recommendation_dict.EIA_recommendation_dict()
            NFA.init_from_files(function_name = key, generic_explanation = recommendation_dict[key]["Generic Explanation"], general_recommendation = recommendation_dict[key]["General Recommendation"], detailed_recommendation = recommendation_dict[key]["Detailed Recommendation"])
            recommendation.append(NFA)

        self.impact_installation.recommendation_dict = recommendation

        sonsea = np.transpose(seasons)

        for key,value in sonsea.items():
            NFA = EIA_season.EIA_season()
            NFA.init_from_files(function_name = key, season_info = value)
            season.append(NFA)

        self.impact_installation.seasons = season

    def compute_impact_maintenance(self, input_maint, protected, receptors, weighting):
        maint = main.OperationMaintenanceStage(protected, receptors, weighting)
        (confidence_dict, eis_dict, recommendation_dict, seasons, global_eis) = maint(input_maint)
        self.impact_maintenance.description = "Environmental impacts of the operations and maintenance phase"
        self.impact_maintenance.global_eis.description = "Global environmental impact of the operations and maintenance phase"

        if type(global_eis["Negative Impact"]) == np.float64:
            self.impact_maintenance.global_eis.negative_impact = "%.f" % global_eis["Negative Impact"]
            self.impact_maintenance.global_eis.max_negative_impact = global_eis["Max Negative Impact"]
            self.impact_maintenance.global_eis.min_negative_impact = global_eis["Min Negative Impact"]
        else:
            self.impact_maintenance.global_eis.negative_impact = 999
            self.impact_maintenance.global_eis.max_negative_impact = 999
            self.impact_maintenance.global_eis.min_negative_impact = 999

        if type(global_eis["Positive Impact"]) == np.float64:
            self.impact_maintenance.global_eis.positive_impact = "%.f" % global_eis["Positive Impact"]
            self.impact_maintenance.global_eis.max_positive_impact = global_eis["Max Positive Impact"]
            self.impact_maintenance.global_eis.min_positive_impact = global_eis["Min Positive Impact"]
        else:
            self.impact_maintenance.global_eis.positive_impact = 999
            self.impact_maintenance.global_eis.max_positive_impact = 999
            self.impact_maintenance.global_eis.min_positive_impact = 999

        eis = []
        confidence = []
        recommendation = []
        season = []

        for key, value in eis_dict.items():
            NFA = EIA_results.EIA_results()
            NFA.init_from_files(function_name = key, score= "%.f" % value)
            eis.append(NFA)

        self.impact_maintenance.eis_dict = eis

        for key, value in confidence_dict.items():
            NFA = EIA_confidence_dict.EIA_confidence_dict()
            NFA.init_from_files(function_name = key, confidence_level= value)
            confidence.append(NFA)

        self.impact_maintenance.confidence_dict = confidence

        for key,value in recommendation_dict.items():
            NFA = EIA_recommendation_dict.EIA_recommendation_dict()
            NFA.init_from_files(function_name = key, generic_explanation = recommendation_dict[key]["Generic Explanation"], general_recommendation = recommendation_dict[key]["General Recommendation"], detailed_recommendation = recommendation_dict[key]["Detailed Recommendation"])
            recommendation.append(NFA)

        self.impact_maintenance.recommendation_dict = recommendation

        sonsea = np.transpose(seasons)

        for key,value in sonsea.items():
            NFA = EIA_season.EIA_season()
            NFA.init_from_files(function_name = key, season_info = value)
            season.append(NFA)

        self.impact_maintenance.seasons = season
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def impact_hydrodynamics(self): # pragma: no cover
        """:obj:`~.EIA_tech.EIA_tech`: none
        """
        return self._impact_hydrodynamics
    #------------
    @ property
    def impact_electrical(self): # pragma: no cover
        """:obj:`~.EIA_tech.EIA_tech`: none
        """
        return self._impact_electrical
    #------------
    @ property
    def impact_station_keeping(self): # pragma: no cover
        """:obj:`~.EIA_tech.EIA_tech`: none
        """
        return self._impact_station_keeping
    #------------
    @ property
    def impact_installation(self): # pragma: no cover
        """:obj:`~.EIA_tech.EIA_tech`: none
        """
        return self._impact_installation
    #------------
    @ property
    def impact_maintenance(self): # pragma: no cover
        """:obj:`~.EIA_tech.EIA_tech`: none
        """
        return self._impact_maintenance
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ impact_hydrodynamics.setter
    def impact_hydrodynamics(self,val): # pragma: no cover
        self._impact_hydrodynamics=val
    #------------
    @ impact_electrical.setter
    def impact_electrical(self,val): # pragma: no cover
        self._impact_electrical=val
    #------------
    @ impact_station_keeping.setter
    def impact_station_keeping(self,val): # pragma: no cover
        self._impact_station_keeping=val
    #------------
    @ impact_installation.setter
    def impact_installation(self,val): # pragma: no cover
        self._impact_installation=val
    #------------
    @ impact_maintenance.setter
    def impact_maintenance(self,val): # pragma: no cover
        self._impact_maintenance=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:EIA:EIA_results_tech"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:EIA:EIA_results_tech"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("impact_hydrodynamics"):
            if (short and not(deep)):
                rep["impact_hydrodynamics"] = self.impact_hydrodynamics.type_rep()
            else:
                rep["impact_hydrodynamics"] = self.impact_hydrodynamics.prop_rep(short, deep)
        if self.is_set("impact_electrical"):
            if (short and not(deep)):
                rep["impact_electrical"] = self.impact_electrical.type_rep()
            else:
                rep["impact_electrical"] = self.impact_electrical.prop_rep(short, deep)
        if self.is_set("impact_station_keeping"):
            if (short and not(deep)):
                rep["impact_station_keeping"] = self.impact_station_keeping.type_rep()
            else:
                rep["impact_station_keeping"] = self.impact_station_keeping.prop_rep(short, deep)
        if self.is_set("impact_installation"):
            if (short and not(deep)):
                rep["impact_installation"] = self.impact_installation.type_rep()
            else:
                rep["impact_installation"] = self.impact_installation.prop_rep(short, deep)
        if self.is_set("impact_maintenance"):
            if (short and not(deep)):
                rep["impact_maintenance"] = self.impact_maintenance.type_rep()
            else:
                rep["impact_maintenance"] = self.impact_maintenance.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "impact_hydrodynamics"
        try :
            if data[varName] != None:
                self.impact_hydrodynamics=EIA_tech.EIA_tech()
                self.impact_hydrodynamics.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "impact_electrical"
        try :
            if data[varName] != None:
                self.impact_electrical=EIA_tech.EIA_tech()
                self.impact_electrical.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "impact_station_keeping"
        try :
            if data[varName] != None:
                self.impact_station_keeping=EIA_tech.EIA_tech()
                self.impact_station_keeping.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "impact_installation"
        try :
            if data[varName] != None:
                self.impact_installation=EIA_tech.EIA_tech()
                self.impact_installation.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "impact_maintenance"
        try :
            if data[varName] != None:
                self.impact_maintenance=EIA_tech.EIA_tech()
                self.impact_maintenance.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
