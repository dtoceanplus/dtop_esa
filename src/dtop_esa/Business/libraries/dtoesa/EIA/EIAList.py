# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import EIA_global
from . import EIA_results_tech
from . import EIA_pressure
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class EIAList():

    """List of all EnvironmentalImpact Assessment results
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._eia_global=EIA_global.EIA_global()
        self._eia_tech_group=EIA_results_tech.EIA_results_tech()
        self._eia_pressure=EIA_pressure.EIA_pressure()
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
    def init_from_files(self, inputs_EIA, protected_table, receptors_table, weighting_dict):
        self.inputs = inputs_EIA
        self.receptors = receptors_table
        self.protected = protected_table
        self.weighting = weighting_dict
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def compute_eia_global(self):
        self.compute_eia_tech_group()
        print("Environmental_Impact_Assessment --- compute eia global")
        self.eia_global.description = "Global results of environmental impacts for the entire project"

        neg = [self.eia_tech_group.impact_hydrodynamics.global_eis.negative_impact , self.eia_tech_group.impact_electrical.global_eis.negative_impact , self.eia_tech_group.impact_station_keeping.global_eis.negative_impact , self.eia_tech_group.impact_installation.global_eis.negative_impact , self.eia_tech_group.impact_maintenance.global_eis.negative_impact]
        min_neg = [self.eia_tech_group.impact_hydrodynamics.global_eis.min_negative_impact , self.eia_tech_group.impact_electrical.global_eis.min_negative_impact ,self.eia_tech_group.impact_station_keeping.global_eis.min_negative_impact, self.eia_tech_group.impact_installation.global_eis.min_negative_impact, self.eia_tech_group.impact_maintenance.global_eis.min_negative_impact]
        max_neg = [self.eia_tech_group.impact_hydrodynamics.global_eis.max_negative_impact , self.eia_tech_group.impact_electrical.global_eis.max_negative_impact ,self.eia_tech_group.impact_station_keeping.global_eis.max_negative_impact, self.eia_tech_group.impact_installation.global_eis.max_negative_impact, self.eia_tech_group.impact_maintenance.global_eis.max_negative_impact]
        while 999 in neg:
            neg.remove(999)
        while 999 in min_neg:
            min_neg.remove(999)
        while 999 in max_neg:
            max_neg.remove(999)

        #Quick fix for NaN error
        neg = [x for x in neg if np.isnan(x) == False]
        min_neg = [x for x in min_neg if np.isnan(x) == False]
        max_neg = [x for x in max_neg if np.isnan(x) == False]

        if len(neg)==0:
            self.eia_global.negative_impact = 999
            self.eia_global.max_negative_impact= 999
            self.eia_global.min_negative_impact= 999
        else:
            min_negative_impacts = np.array(min_neg, dtype=np.float)
            max_negative_impacts = np.array(max_neg, dtype=np.float)

            negative = np.nanmean(neg)
            min_negative= min(min_negative_impacts)
            max_negative= max(max_negative_impacts)

            self.eia_global.negative_impact = np.around(negative, decimals = 0)
            self.eia_global.max_negative_impact= np.around(max_negative, decimals = 0)
            self.eia_global.min_negative_impact= np.around(min_negative, decimals = 0)

        pos = [self.eia_tech_group.impact_hydrodynamics.global_eis.positive_impact , self.eia_tech_group.impact_electrical.global_eis.positive_impact , self.eia_tech_group.impact_station_keeping.global_eis.positive_impact , self.eia_tech_group.impact_installation.global_eis.positive_impact , self.eia_tech_group.impact_maintenance.global_eis.positive_impact]
        min_pos = [self.eia_tech_group.impact_hydrodynamics.global_eis.min_positive_impact , self.eia_tech_group.impact_electrical.global_eis.min_positive_impact ,self.eia_tech_group.impact_station_keeping.global_eis.min_positive_impact, self.eia_tech_group.impact_installation.global_eis.min_positive_impact, self.eia_tech_group.impact_maintenance.global_eis.min_positive_impact]
        max_pos = [self.eia_tech_group.impact_hydrodynamics.global_eis.max_positive_impact , self.eia_tech_group.impact_electrical.global_eis.max_positive_impact ,self.eia_tech_group.impact_station_keeping.global_eis.max_positive_impact, self.eia_tech_group.impact_installation.global_eis.max_positive_impact, self.eia_tech_group.impact_maintenance.global_eis.max_positive_impact]
        while 999 in pos:
            pos.remove(999)
        while 999 in min_pos:
            min_pos.remove(999)
        while 999 in max_pos:
            max_pos.remove(999)

        #Quick fix for NaN error
        pos = [x for x in pos if np.isnan(x) == False]
        min_pos = [x for x in min_pos if np.isnan(x) == False]
        max_pos = [x for x in max_pos if np.isnan(x) == False]

        if len(pos)==0:
            self.eia_global.positive_impact = 999
            self.eia_global.max_positive_impact= 999
            self.eia_global.min_positive_impact= 999
        else:
            min_positive_impacts = np.array(min_pos, dtype=np.float)
            max_positive_impacts = np.array(max_pos, dtype=np.float)
            positive = np.nanmean(pos)
            max_positive=max(max_positive_impacts)
            min_positive=min(min_positive_impacts)

            self.eia_global.positive_impact = np.around(positive, decimals = 0)
            self.eia_global.max_positive_impact= np.around(max_positive, decimals = 0)
            self.eia_global.min_positive_impact= np.around(min_positive, decimals = 0)

    def compute_eia_tech_group(self):
        print("Environmental_Impact_Assessment --- compute eia tech group")
        print("Environmental_Impact_Assessment --- compute eia tech group -- hydrodynamics")
        self.eia_tech_group.compute_impact_hydrodynamics(self.inputs["input_hydro"], self.protected, self.receptors, self.weighting["weighting_hydro"])
        print("Environmental_Impact_Assessment --- compute eia tech group -- electrical")
        self.eia_tech_group.compute_impact_electrical(self.inputs["input_elec"], self.protected, self.receptors, self.weighting["weighting_elec"])
        print("Environmental_Impact_Assessment --- compute eia tech group -- station keeping")
        self.eia_tech_group.compute_impact_station_keeping(self.inputs["input_moor"], self.protected, self.receptors, self.weighting["weighting_moor"])
        print("Environmental_Impact_Assessment --- compute eia tech group -- installation")
        self.eia_tech_group.compute_impact_installation(self.inputs["input_insta"], self.protected, self.receptors, self.weighting["weighting_insta"])
        print("Environmental_Impact_Assessment --- compute eia tech group -- maintenance")
        self.eia_tech_group.compute_impact_maintenance(self.inputs["input_maint"], self.protected, self.receptors, self.weighting["weighting_maint"])

    def compute_eia_pressure(self):
        print("Environmental_Impact_Assessment --- compute eia pressure")
        list_pressure = self.eia_pressure.__dict__
        self.eia_pressure.prop_rep()
        for pressure in list_pressure:
            pressu = pressure[1:]
            prop = self.eia_tech_group.prop_rep()
            [prop.pop(key) for key in ["__type__", "name","description"]]
            for impact in prop:
                for function in prop[impact]["eis_dict"]:
                    name = function["function_name"].replace(" ","_")
                    name = name.lower()
                    if name == pressu:
                        # self.eia_pressure[pressure].score += function["score"]
                        qty = function["score"]
                        old_score = getattr(self.eia_pressure,pressure).score
                        if getattr(self.eia_pressure,pressure).score == 0:
                            new_score= "%.f" %qty
                        else:
                            new_score = (old_score+qty)/2
                            new_score = "%.f" % new_score
                        getattr(self.eia_pressure,pressure).score = new_score
        # for self.eia_pressure
        # self.eia_pressure.energy_modification.score = self.eia_tech_group.impact_hydrodynamics.eis_dict.
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def eia_global(self): # pragma: no cover
        """:obj:`~.EIA_global.EIA_global`: none
        """
        return self._eia_global
    #------------
    @ property
    def eia_tech_group(self): # pragma: no cover
        """:obj:`~.EIA_results_tech.EIA_results_tech`: none
        """
        return self._eia_tech_group
    #------------
    @ property
    def eia_pressure(self): # pragma: no cover
        """:obj:`~.EIA_pressure.EIA_pressure`: none
        """
        return self._eia_pressure
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ eia_global.setter
    def eia_global(self,val): # pragma: no cover
        self._eia_global=val
    #------------
    @ eia_tech_group.setter
    def eia_tech_group(self,val): # pragma: no cover
        self._eia_tech_group=val
    #------------
    @ eia_pressure.setter
    def eia_pressure(self,val): # pragma: no cover
        self._eia_pressure=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:EIA:EIAList"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:EIA:EIAList"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("eia_global"):
            if (short and not(deep)):
                rep["eia_global"] = self.eia_global.type_rep()
            else:
                rep["eia_global"] = self.eia_global.prop_rep(short, deep)
        if self.is_set("eia_tech_group"):
            if (short and not(deep)):
                rep["eia_tech_group"] = self.eia_tech_group.type_rep()
            else:
                rep["eia_tech_group"] = self.eia_tech_group.prop_rep(short, deep)
        if self.is_set("eia_pressure"):
            if (short and not(deep)):
                rep["eia_pressure"] = self.eia_pressure.type_rep()
            else:
                rep["eia_pressure"] = self.eia_pressure.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "eia_global"
        try :
            if data[varName] != None:
                self.eia_global=EIA_global.EIA_global()
                self.eia_global.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "eia_tech_group"
        try :
            if data[varName] != None:
                self.eia_tech_group=EIA_results_tech.EIA_results_tech()
                self.eia_tech_group.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "eia_pressure"
        try :
            if data[varName] != None:
                self.eia_pressure=EIA_pressure.EIA_pressure()
                self.eia_pressure.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
