# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import EIA_results
from . import EIA_global
from . import EIA_confidence_dict
from . import EIA_recommendation_dict
from . import EIA_season
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class EIA_tech():

    """List of EIA results for a technology group
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._eis_dict=[]
        self._global_eis=EIA_global.EIA_global()
        self._confidence_dict=[]
        self._recommendation_dict=[]
        self._seasons=[]
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def eis_dict(self): # pragma: no cover
        """:obj:`list` of :obj:`~.EIA_results.EIA_results`: none
        """
        return self._eis_dict
    #------------
    @ property
    def global_eis(self): # pragma: no cover
        """:obj:`~.EIA_global.EIA_global`: none
        """
        return self._global_eis
    #------------
    @ property
    def confidence_dict(self): # pragma: no cover
        """:obj:`list` of :obj:`~.EIA_confidence_dict.EIA_confidence_dict`: none
        """
        return self._confidence_dict
    #------------
    @ property
    def recommendation_dict(self): # pragma: no cover
        """:obj:`list` of :obj:`~.EIA_recommendation_dict.EIA_recommendation_dict`: none
        """
        return self._recommendation_dict
    #------------
    @ property
    def seasons(self): # pragma: no cover
        """:obj:`list` of :obj:`~.EIA_season.EIA_season`: none
        """
        return self._seasons
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ eis_dict.setter
    def eis_dict(self,val): # pragma: no cover
        self._eis_dict=val
    #------------
    @ global_eis.setter
    def global_eis(self,val): # pragma: no cover
        self._global_eis=val
    #------------
    @ confidence_dict.setter
    def confidence_dict(self,val): # pragma: no cover
        self._confidence_dict=val
    #------------
    @ recommendation_dict.setter
    def recommendation_dict(self,val): # pragma: no cover
        self._recommendation_dict=val
    #------------
    @ seasons.setter
    def seasons(self,val): # pragma: no cover
        self._seasons=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:EIA:EIA_tech"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:EIA:EIA_tech"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("eis_dict"):
            rep["eis_dict"] = []
            for i in range(0,len(self.eis_dict)):
                if (short and not(deep)):
                    itemType = self.eis_dict[i].type_rep()
                    rep["eis_dict"].append( itemType )
                else:
                    rep["eis_dict"].append( self.eis_dict[i].prop_rep(short, deep) )
        else:
            rep["eis_dict"] = []
        if self.is_set("global_eis"):
            if (short and not(deep)):
                rep["global_eis"] = self.global_eis.type_rep()
            else:
                rep["global_eis"] = self.global_eis.prop_rep(short, deep)
        if self.is_set("confidence_dict"):
            rep["confidence_dict"] = []
            for i in range(0,len(self.confidence_dict)):
                if (short and not(deep)):
                    itemType = self.confidence_dict[i].type_rep()
                    rep["confidence_dict"].append( itemType )
                else:
                    rep["confidence_dict"].append( self.confidence_dict[i].prop_rep(short, deep) )
        else:
            rep["confidence_dict"] = []
        if self.is_set("recommendation_dict"):
            rep["recommendation_dict"] = []
            for i in range(0,len(self.recommendation_dict)):
                if (short and not(deep)):
                    itemType = self.recommendation_dict[i].type_rep()
                    rep["recommendation_dict"].append( itemType )
                else:
                    rep["recommendation_dict"].append( self.recommendation_dict[i].prop_rep(short, deep) )
        else:
            rep["recommendation_dict"] = []
        if self.is_set("seasons"):
            rep["seasons"] = []
            for i in range(0,len(self.seasons)):
                if (short and not(deep)):
                    itemType = self.seasons[i].type_rep()
                    rep["seasons"].append( itemType )
                else:
                    rep["seasons"].append( self.seasons[i].prop_rep(short, deep) )
        else:
            rep["seasons"] = []
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "eis_dict"
        try :
            if data[varName] != None:
                self.eis_dict=[]
                for i in range(0,len(data[varName])):
                    self.eis_dict.append(EIA_results.EIA_results())
                    self.eis_dict[i].loadFromJSONDict(data[varName][i])
            else:
                self.eis_dict = []
        except :
            pass
        varName = "global_eis"
        try :
            if data[varName] != None:
                self.global_eis=EIA_global.EIA_global()
                self.global_eis.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "confidence_dict"
        try :
            if data[varName] != None:
                self.confidence_dict=[]
                for i in range(0,len(data[varName])):
                    self.confidence_dict.append(EIA_confidence_dict.EIA_confidence_dict())
                    self.confidence_dict[i].loadFromJSONDict(data[varName][i])
            else:
                self.confidence_dict = []
        except :
            pass
        varName = "recommendation_dict"
        try :
            if data[varName] != None:
                self.recommendation_dict=[]
                for i in range(0,len(data[varName])):
                    self.recommendation_dict.append(EIA_recommendation_dict.EIA_recommendation_dict())
                    self.recommendation_dict[i].loadFromJSONDict(data[varName][i])
            else:
                self.recommendation_dict = []
        except :
            pass
        varName = "seasons"
        try :
            if data[varName] != None:
                self.seasons=[]
                for i in range(0,len(data[varName])):
                    self.seasons.append(EIA_season.EIA_season())
                    self.seasons[i].loadFromJSONDict(data[varName][i])
            else:
                self.seasons = []
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
