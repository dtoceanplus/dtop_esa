# This is the Environmental and Social Acceptance module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Emma Araignous, Nicolas Michelet, Rui Duarte, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
from . import EIA_results
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class EIA_pressure():

    """Environmental impact assessment results at pressure level
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._footprint=EIA_results.EIA_results()
        self._collision_risk=EIA_results.EIA_results()
        self._collision_risk_vessel=EIA_results.EIA_results()
        self._energy_modification=EIA_results.EIA_results()
        self._reserve_effect=EIA_results.EIA_results()
        self._reef_effect=EIA_results.EIA_results()
        self._resting_place=EIA_results.EIA_results()
        self._magnetic_fields=EIA_results.EIA_results()
        self._electrical_fields=EIA_results.EIA_results()
        self._chemical_pollution=EIA_results.EIA_results()
        self._temperature_modification=EIA_results.EIA_results()
        self._turbidity=EIA_results.EIA_results()
        self._underwater_noise=EIA_results.EIA_results()
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def footprint(self): # pragma: no cover
        """:obj:`~.EIA_results.EIA_results`: none
        """
        return self._footprint
    #------------
    @ property
    def collision_risk(self): # pragma: no cover
        """:obj:`~.EIA_results.EIA_results`: none
        """
        return self._collision_risk
    #------------
    @ property
    def collision_risk_vessel(self): # pragma: no cover
        """:obj:`~.EIA_results.EIA_results`: none
        """
        return self._collision_risk_vessel
    #------------
    @ property
    def energy_modification(self): # pragma: no cover
        """:obj:`~.EIA_results.EIA_results`: none
        """
        return self._energy_modification
    #------------
    @ property
    def reserve_effect(self): # pragma: no cover
        """:obj:`~.EIA_results.EIA_results`: none
        """
        return self._reserve_effect
    #------------
    @ property
    def reef_effect(self): # pragma: no cover
        """:obj:`~.EIA_results.EIA_results`: none
        """
        return self._reef_effect
    #------------
    @ property
    def resting_place(self): # pragma: no cover
        """:obj:`~.EIA_results.EIA_results`: none
        """
        return self._resting_place
    #------------
    @ property
    def magnetic_fields(self): # pragma: no cover
        """:obj:`~.EIA_results.EIA_results`: none
        """
        return self._magnetic_fields
    #------------
    @ property
    def electrical_fields(self): # pragma: no cover
        """:obj:`~.EIA_results.EIA_results`: none
        """
        return self._electrical_fields
    #------------
    @ property
    def chemical_pollution(self): # pragma: no cover
        """:obj:`~.EIA_results.EIA_results`: none
        """
        return self._chemical_pollution
    #------------
    @ property
    def temperature_modification(self): # pragma: no cover
        """:obj:`~.EIA_results.EIA_results`: none
        """
        return self._temperature_modification
    #------------
    @ property
    def turbidity(self): # pragma: no cover
        """:obj:`~.EIA_results.EIA_results`: none
        """
        return self._turbidity
    #------------
    @ property
    def underwater_noise(self): # pragma: no cover
        """:obj:`~.EIA_results.EIA_results`: none
        """
        return self._underwater_noise
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ footprint.setter
    def footprint(self,val): # pragma: no cover
        self._footprint=val
    #------------
    @ collision_risk.setter
    def collision_risk(self,val): # pragma: no cover
        self._collision_risk=val
    #------------
    @ collision_risk_vessel.setter
    def collision_risk_vessel(self,val): # pragma: no cover
        self._collision_risk_vessel=val
    #------------
    @ energy_modification.setter
    def energy_modification(self,val): # pragma: no cover
        self._energy_modification=val
    #------------
    @ reserve_effect.setter
    def reserve_effect(self,val): # pragma: no cover
        self._reserve_effect=val
    #------------
    @ reef_effect.setter
    def reef_effect(self,val): # pragma: no cover
        self._reef_effect=val
    #------------
    @ resting_place.setter
    def resting_place(self,val): # pragma: no cover
        self._resting_place=val
    #------------
    @ magnetic_fields.setter
    def magnetic_fields(self,val): # pragma: no cover
        self._magnetic_fields=val
    #------------
    @ electrical_fields.setter
    def electrical_fields(self,val): # pragma: no cover
        self._electrical_fields=val
    #------------
    @ chemical_pollution.setter
    def chemical_pollution(self,val): # pragma: no cover
        self._chemical_pollution=val
    #------------
    @ temperature_modification.setter
    def temperature_modification(self,val): # pragma: no cover
        self._temperature_modification=val
    #------------
    @ turbidity.setter
    def turbidity(self,val): # pragma: no cover
        self._turbidity=val
    #------------
    @ underwater_noise.setter
    def underwater_noise(self,val): # pragma: no cover
        self._underwater_noise=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:EIA:EIA_pressure"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtoesa:EIA:EIA_pressure"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("footprint"):
            if (short and not(deep)):
                rep["footprint"] = self.footprint.type_rep()
            else:
                rep["footprint"] = self.footprint.prop_rep(short, deep)
        if self.is_set("collision_risk"):
            if (short and not(deep)):
                rep["collision_risk"] = self.collision_risk.type_rep()
            else:
                rep["collision_risk"] = self.collision_risk.prop_rep(short, deep)
        if self.is_set("collision_risk_vessel"):
            if (short and not(deep)):
                rep["collision_risk_vessel"] = self.collision_risk_vessel.type_rep()
            else:
                rep["collision_risk_vessel"] = self.collision_risk_vessel.prop_rep(short, deep)
        if self.is_set("energy_modification"):
            if (short and not(deep)):
                rep["energy_modification"] = self.energy_modification.type_rep()
            else:
                rep["energy_modification"] = self.energy_modification.prop_rep(short, deep)
        if self.is_set("reserve_effect"):
            if (short and not(deep)):
                rep["reserve_effect"] = self.reserve_effect.type_rep()
            else:
                rep["reserve_effect"] = self.reserve_effect.prop_rep(short, deep)
        if self.is_set("reef_effect"):
            if (short and not(deep)):
                rep["reef_effect"] = self.reef_effect.type_rep()
            else:
                rep["reef_effect"] = self.reef_effect.prop_rep(short, deep)
        if self.is_set("resting_place"):
            if (short and not(deep)):
                rep["resting_place"] = self.resting_place.type_rep()
            else:
                rep["resting_place"] = self.resting_place.prop_rep(short, deep)
        if self.is_set("magnetic_fields"):
            if (short and not(deep)):
                rep["magnetic_fields"] = self.magnetic_fields.type_rep()
            else:
                rep["magnetic_fields"] = self.magnetic_fields.prop_rep(short, deep)
        if self.is_set("electrical_fields"):
            if (short and not(deep)):
                rep["electrical_fields"] = self.electrical_fields.type_rep()
            else:
                rep["electrical_fields"] = self.electrical_fields.prop_rep(short, deep)
        if self.is_set("chemical_pollution"):
            if (short and not(deep)):
                rep["chemical_pollution"] = self.chemical_pollution.type_rep()
            else:
                rep["chemical_pollution"] = self.chemical_pollution.prop_rep(short, deep)
        if self.is_set("temperature_modification"):
            if (short and not(deep)):
                rep["temperature_modification"] = self.temperature_modification.type_rep()
            else:
                rep["temperature_modification"] = self.temperature_modification.prop_rep(short, deep)
        if self.is_set("turbidity"):
            if (short and not(deep)):
                rep["turbidity"] = self.turbidity.type_rep()
            else:
                rep["turbidity"] = self.turbidity.prop_rep(short, deep)
        if self.is_set("underwater_noise"):
            if (short and not(deep)):
                rep["underwater_noise"] = self.underwater_noise.type_rep()
            else:
                rep["underwater_noise"] = self.underwater_noise.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "footprint"
        try :
            if data[varName] != None:
                self.footprint=EIA_results.EIA_results()
                self.footprint.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "collision_risk"
        try :
            if data[varName] != None:
                self.collision_risk=EIA_results.EIA_results()
                self.collision_risk.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "collision_risk_vessel"
        try :
            if data[varName] != None:
                self.collision_risk_vessel=EIA_results.EIA_results()
                self.collision_risk_vessel.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "energy_modification"
        try :
            if data[varName] != None:
                self.energy_modification=EIA_results.EIA_results()
                self.energy_modification.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "reserve_effect"
        try :
            if data[varName] != None:
                self.reserve_effect=EIA_results.EIA_results()
                self.reserve_effect.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "reef_effect"
        try :
            if data[varName] != None:
                self.reef_effect=EIA_results.EIA_results()
                self.reef_effect.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "resting_place"
        try :
            if data[varName] != None:
                self.resting_place=EIA_results.EIA_results()
                self.resting_place.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "magnetic_fields"
        try :
            if data[varName] != None:
                self.magnetic_fields=EIA_results.EIA_results()
                self.magnetic_fields.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "electrical_fields"
        try :
            if data[varName] != None:
                self.electrical_fields=EIA_results.EIA_results()
                self.electrical_fields.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "chemical_pollution"
        try :
            if data[varName] != None:
                self.chemical_pollution=EIA_results.EIA_results()
                self.chemical_pollution.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "temperature_modification"
        try :
            if data[varName] != None:
                self.temperature_modification=EIA_results.EIA_results()
                self.temperature_modification.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "turbidity"
        try :
            if data[varName] != None:
                self.turbidity=EIA_results.EIA_results()
                self.turbidity.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "underwater_noise"
        try :
            if data[varName] != None:
                self.underwater_noise=EIA_results.EIA_results()
                self.underwater_noise.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
