.. _esa-home:

Environmental and Social Acceptance
===================================

Introduction 
------------

The Environmental and Social Acceptance module (ESA) aims to assess the environmental and social impacts generated  by  the  various  technology  choices  and  array  configurations  of  wave  or  tidal devices.

Structure
---------

This module's documentation is divided into four main sections:

- :ref:`esa-tutorials` to give step-by-step instructions on using ESA for new users. 

- :ref:`esa-how-to` that show how to achieve specific outcomes using ESA. 

- A section on :ref:`background, theory and calculation methods <esa-explanation>` that describes how ESA works and aims to give confidence in the tools. 

- The :ref:`API reference <esa-reference>` section documents the code of modules, classes, API, and GUI. 

.. toctree::
   :hidden:
   :maxdepth: 1

   tutorials/index
   how_to/index
   explanation/index
   reference/index

Functionalities
---------------

Following the regulatory context of environmental risk assessment and socio-economic opportunities, the Environmental and Social Acceptance (ESA) module was structured into four different parts representing four complementary assessments: 

#. **Identification of the potential presence of endangered species** in the area of MRE installation (i.e. species included in the IUCN red list);  

#. **Environmental impact assessment** estimated for the main environmental stressors pre-identified as potential stressors from MRE such as the underwater noise or the collision risk between vessels/devices and the marine wildlife;  

#. **Estimation of the carbon footprint** and the green-house effect of the project in terms of Global Warming Potential (GWP) and Cumulative Energy Demand (CED) ; and 

#. Information to improve the **social acceptance** of the project considering socio-economic opportunities. 

Workflow for using the tool
---------------------------

The workflow for using the Environmental and Social Acceptance module
can be summarised as 1) provide inputs, 2) perform an assessment
depending on the complexity level, and 3) view the results, as shown below.

.. image:: figures/workflow.png
   :width: 500in
   :height: 450

Overview of data requirements
-----------------------------

+-------------+--------------+----------------------------+-----------------------------+-------+
| Section     | Complexity 1 | Complexity 2               | Complexity 3                | Units |
+=============+==============+============================+=============================+=======+
|             |              |                            |                             | €/MW  |
| Farm        | Coordinates  | Global area description,   | Global area description,    |       |
|             |              |                            |                             |       |
| inputs      | of the farm  | Fishing regulation         | Fishing regulation          |       |
|             |              |                            |                             |       |
|             |              |                            | Initial environmental state |       |
+-------------+--------------+----------------------------+-----------------------------+-------+
|             |              |                            |                             | -     |
| Devices     |              | General information,       | General information,        |       |
|             |              |                            |                             |       |
| inputs      |              | Dimensions,                | Dimensions,                 |       |
|             |              |                            |                             |       |
|             |              | Resources,                 | Resources,                  |       |
|             |              |                            |                             |       |
|             |              | Fishing restriction,       | Fishing restriction,        |       |
|             |              |                            |                             |       |
|             |              | Foundations information    | foundations information     |       |
|             |              |                            |                             |       |
|             |              |                            | Measurements of             |       |
|             |              |                            |                             |       |
|             |              |                            | environmental parameters    |       |
+-------------+--------------+----------------------------+-----------------------------+-------+
|             |              |                            |                             | GWh   |
| Electrical  |              | General information,       | General information,        |       |
|             |              |                            |                             |       |
| inputs      |              | Installation information,  | Fishing restriction,        |       |
|             |              |                            |                             |       |
|             |              | fishing restriction,       | Resources                   |       |
|             |              |                            |                             |       |
|             |              | Resources                  | Measurements of             |       |
|             |              |                            |                             |       |
|             |              |                            | environmental parameters    |       |
+-------------+--------------+----------------------------+-----------------------------+-------+
|             |              |                            |                             |       |
| Logistics   |              | Characteristics of         | Characteristics of the      |       |
|             |              |                            |                             |       |
| inputs      |              | the boats,                 | boat in each phase,         |       |
|             |              |                            |                             |       |
|             |              | Fuel   consumption         | fuel consumption            |       |
|             |              |                            |                             |       |
|             |              |                            | Measurements of             |       |
|             |              |                            |                             |       |
|             |              |                            | environmental parameters    |       |
+-------------+--------------+----------------------------+-----------------------------+-------+

Summary of optional inputs 
-----------------------------

=========== =====================
**Section** **Sub category**
=========== =====================
Farm inputs Protected species
            
            Receptors presence
=========== =====================
