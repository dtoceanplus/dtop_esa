.. _es_dto:

Endangered species 
===================

The first assessment of the ESA module aims to provide insight on Endangered Species (ES) potentially present in the project area.
The Endangered Species assessments can be used to:

	- Identify very sensitive species potentially present in the lease area 
	- Identify aspects of the design that can be considered as a risk for the present endangered species
	- Identify possible improvements to work on to minimise the impacts on the endangered species 
	- Provide recommendations for design processes based on the main risks associated and also provide global recommendations including monitoring survey protocols that are relevant to monitor the species in the array area

.. contents::

Principles and objectives
~~~~~~~~~~~~~~~~~~~~~~~~~

The first assessment of the ESA module aims to provide insight on
Endangered Species (ES) potentially present in the project area.

ES will inform on the potential presence of 26 endangered species. The selection was based on their IUCN red list status and presence in European directives and international conventions. Five Class of animals were included: seven mammals, three Actinopterygii (bony fishes), six Chondrichthyes (cartilaginous fishes), five Aves (birds) and five Reptilia. 

A local database of maps of large-scale probability of presence for each species has been integrated to DTOceanPlus Site Characterisation module. This NetCDF file including the global geographical information of all species has been built from AquaMaps. This collaborative project aims at producing computer-generated (and ultimately, expert reviewed) predicted global distribution maps for marine species on a 0.5 x 0.5 degree grid of the oceans. 

Models are constructed from estimates of the environmental tolerance of a given species with respect to depth, salinity, temperature, primary productivity, and its association with sea ice or coastal areas. Maps represent mean annual distributions of species and do not account for changes in species occurrence due to migration or unusual environmental events such as El Niño. They are based on data available through online species databases such as FishBase and SeaLifeBase and species occurrence records from OBIS or GBIF and using an environmental envelope model in conjunction with expert input.

Given the chosen location of implementation, ES will inform on the potential presence of 26 endangered species (Table 3.1), notify on associated risks and give recommendations to have a better consideration to these species in the design processes and during the life cycle of the project. Considering that all marine birds and mammals are protected, a global warning advising to set up surveys and monitoring to improve general marine life knowledge in the area is systematically displayed.


List of the 26 endangered species considered in ESA module:

.. image:: ../figures/species_list.png



Inputs
------

The inputs needed to identify endangered species in the area are the farm's coordinates. 

*Table : Inputs for the identification of endangered species*

+-------------------------+-------------------+----------------+
| Origin of the Data      | Data Model in ESA | Units          |
+=========================+===================+================+
| Coordinates of the farm | SC, User          | Decimal degree |
+-------------------------+-------------------+----------------+

Outputs
--------
The Endangered Species assessments can be used to:

-  Identify very sensitive species potentially present in the lease area

-  Identify aspects of the design that can be considered as a risk for
   the present endangered species

-  Identify possible improvements to work on to minimise the impacts on
   the endangered species

-  Provide recommendations for design processes based on the main risks
   associated and also provide global recommendations including
   monitoring survey protocols that are relevant to monitor the species
   in the array area

From the farm's coordinates, ES will look for the nearest coordinates to retrieve probability of presence of the 26 species at this point into the local database netCDF file. ES will provide information:

1)	On the probability of presence of 26 endangered species listed in international and European conventions
2)	About taxonomic classification of the identified endangered species:

   a.	Class
   b.	Order
   c.	Family
   d.	Latin name
   e.	Common name
   f.	IUCN status 

3)	Recommendations for design processes based on the main risks associated with the identified species: recommendations to mitigate identified impacts and to improve knowledge on the particular species on site (see Table below for example of ES recommendations)

Recommandations
---------------

Mammals
^^^^^^^

Noise 
'''''

-  **Use of the acoustic mitigation devices**

**Air Bubble curtains** are characterised by freely rising bubbles
created by compressed air injected into the water through a ring of
perforated pipes encircling the pipe (Koschinski and Lüdemann, 2013). A
disparity of impedance is created due to a great difference in density
and speed between water and air when deployed around a pile. The air
bubble modifies the compressibility of the water and thus the speed of
sound propagation. Stimulation of gas bubbles at their resonant
frequency reduces the amplitude of sound waves by dispersion and
absorption. The interaction between the multitude of bubbles in a
curtain increases the noise reduction (Koschinski and Lüdemann, 2013).

**Hydro Sound Dampers** are small gas filled elastic balloons and robust
foam elements fixed to nets or frames places around the pile(Koschinski
and Lüdemann, 2013). It uses nets with air-filled elastic balloons and
foam elements (high dissipation properties) to reduce continuous noise
and impulsive noise. The resonance frequency is controlled: it is
inversely proportional to the diameter of the balloons (Jolivet *et al,*
2015). Comparing to the air bubble curtains, the advantage of HDS there
is no compressor required.

**Isolation casing** consists of surrounding the pile with a rigid steel
tube from seabed to the surface. Some systems include additional layers
of foam, composites. Sound insulation is achieved through reflections
and between the air, the steel and the water ((Koschinski and Lüdemann,
2013); Jolivet *et al,* 2015).

**Cofferdam** is similar to the isolation casing but there is no air
between the pile and the steel tube and this device can be installed up
to 45m deep.

-  **Noise mitigation screen**

**BEKA_shells** are a single construction that incorporate multiple
noise mitigation methods in order to reduce noise. The applicability of
BEKA shells is so far restricted to monopiles or tripiles.

**Pingers** emit pulses of sound or frequency-modulated signals to cause
sensitive animals to avoid an area where they could be exposed to high
noise levels.

-  **Pre-watch**

The **pre-watch** is the period an exclusion zone must be clear before
noise activation. It can vary between 30 or 60 min depending on the
region and the depth of the water (Todd et al., 2015)

-  **Soft-start protocol**

The **soft-start** is defined as a gradual increase in power output over
a set period. They are conducted each time the sound is activated. It
has been designed to give animals time to get away from the noise source
and the operation area prior to full power being reached (Todd *et al,*
2015). During the duration of the soft-start, the MMO/PAM operators need
to watch for the animals and monitor the duration of the procedure.The
soft-start is frequently and commonly required to be a gradual increase
in acoustic energy over 20 to 40 mins. But if applicable, operational
guidelines specify the duration and method of soft-start, which must be
carried out. Usually a single low-powered airgun is activated and fired
a set number of times before higher power airguns are added gradually
over a series of stages until they are firing at maximum capacity. The
length of time a soft-start takes is considered carefully during
planning for the different stages in order to minimise noise exposure
and disturbance to marine mammals (Todd et al., 2015).

-  **Monitoring exclusion zone during construction**

This is an area where the MMO/PAM operators will monitor marine mammals
visually and acoustically before the noise from the construction starts.

The exclusion zone must be clear prior the activation of the sound
source and it’s typically a radius of 500 or 1,000m but can be greater
under some circumstances (Todd et al., 2015).

This is an area in which a marine mammal could be exposed to sound that
could cause injury and will be determined by different factors such as
the site-specific source level, in situ measurements of the environment
conditions (water depth, salinity, temperature, seabed type…) (JNCC,
2010).

In some countries, they specify different zones. In Australia they are
known as observation (3 km), low power (1 or 2 km, depending on the size
of the seismic survey) and shut-down (500m) zones. Thus, the mitigation
action depends on the size in which an animal is sighted (Todd et al.,
2015). If an animal is sighted in the observation zone, an additional
MMO is required to enhance visual effort.

For other countries, the exclusion zones will depend on the species and
if calves are present. This is the case in New Zealand.

The MMO and PAM operators should be positioned as near to the source as
possible for effective monitoring of the Exclusion Zone. They also
should be located in an appropriate viewing platform to ensure effective
coverage of the mitigation zone (JNCC, 2010).

-  **Delays and Shut-down**

Delays can occur if the MMOs sight marine mammals in the exclusion zone
during the time of the pre-watch. If a marine mammal is sighted in the
exclusion zone during the operation, a shut-down can occur and last 30
and 60 mins after the last sighting (Todd et al., 2015).

-  **Listening Space Reduction (LSR) method**

The listening space reduction (LSR) method can be used as a new approach
to evaluate the effects of the noise from MREs on the listening space of
marine mammals.

The listening space is referred to the immediate surrounding area where
an animal can detect biologically-important sounds (Pine et al., 2019).
If an animal is present in a field with device’s noise, the
anthropogenic noise will interfere with the other sounds that could be
biologically important and then the listening space around the animal
will be reduced.

Thus, that could be an impact in the life of the animal such as
reduction of the area to detect a prey, acoustic signal not detected
from the conspecifics (no communication) and predator can be closer to
the prey before being detected (Pine et al., 2019).

The marine mammals frequent high energetic environment due to the MREs
sites. The tidal turbine can emit sound in low frequency and some
species of marine mammals have a hearing sensitivity to signal in a low
frequency. Then an auditory masking (interferences of important
biological signals by anthropogenic sounds) is expected and this is
considered as the most pervasive impact.

From the paper of (Pine et al., 2019), **“the effect of the turbine
noise on the listening space varied between the type of turbine, the
species, the season and the ambient sound conditions within both
seasons**\ ”.

As the marine mammals used sound for critical life processes, the
reduction of the listening space can be very detramental for their life!
The extent of masking is really dependent on:

-  The device’s noise spectrum

-  The source level of the biologically important signal

-  The rate of propagation of loss

-  Frequency-dependant hearing sensitivity of the listener (the animal)



*The recommendation from this paper:*

“Quantifying the range-dependant LSRs could be useful for informing
marine spatial planning and design for arrays” (Pine et al., 2019)

Using the LSR logarithm in order to avoid as much as possible the
masking effects and should be recommended as part of the environmental
impact assessment.

“A key benefit of the LSR algorithm: only requires quantification of the
change to the listener’s perceived soundscape”

The following parameters are needed as inputs for the LSR algorithm:

-  The change in masking noise level

-  The bandwidth of the biological-important signal (communication, or
   call from a prey so the frequency-dependant propagation coefficient
   can be calculated)

-  The species audiogram

The advantage of this method is that the parameters are predominately
environmental and obtained during environmental impact assessments for
tidal turbine device arrays.

Important to know some conditions of this method:

-  The device’s noise level and ambient sound level will change with
   environment

-  If no audiogram is available for the species in question (no
   audiogram exists for large baleen whales), it’s recommended to use
   the audiogram of the closest phylogenetic relative, or modelled an
   audiogram

Finally, this method can be applicable to any MRE device type as long as
the propagation loss coefficient for the region is known, measurements
of the local ambient sound and the audiogram of the species in concern
are available.

Thus, this method can be an effective way of translating predictive
noise propagation models, as a part of the EIA, in certain zones where a
level of masking can be occurred.

-  **Seasonal and temporal restrictions during the construction**

The presence and vulnerability of species can be highlighted through a
seasonal variation and the impacts can be avoided or reduced by
restricting certain activities to period of law abundance or
vulnerability. This is the case in Germany where pile driving is
restricted during the summer due to the nursing areas of harbour
porpoise (Defingou et al., 2019).

Thus, restrictions can be applied for different circumstances but vital
for the life of the cetaceans. It can be due to nursing areas, main
migration routes and feeding grounds.

Monk seal reproductive sites and caves in order to avoid impacts due to
light and general human disturbance: for pinnipeds

For other countries, the exclusion zones will depend on the species and
if calves are present. This is the case in New Zealand.

The MMO and PAM operators should be positioned as near to the source as
possible for effective monitoring of the Exclusion Zone. They also
should be located in an appropriate viewing platform to ensure effective
coverage of the mitigation zone (JNCC, 2010).

Collision
'''''''''

Important to know that collision is the main cause of mortality
particularly for the large cetaceans such as fin whales, blue whales,
the North-Atlantic right whale…

-  **Vessel speed restriction during the lifetime of the project**

All vessels present in or transiting to/from windfarm area should have a
speed restriction, especially during period of reproduction, migration,
aggregations of animals, surface active groups and/or mothers with
calves (NRDC, 2019).

The method of LRS can be used in this context in order to determine
which boat can occur more masking effect for different type of species
(Pine et al., 2019).

As stated previously, the ability to detect an obstacle for marine
mammal depends on the ambient noise level, and the type and speed of
boat.

In order to limit collisions during the construction phase, an
independent software could be created that can calculate the variables
of boat type and/or speed of the boat during the construction phase.
Additionally, this software will need to account for the listening
sensitivities of marine mammals, therefore effectively reducing the
impact of a masking effect.

Thus, either the speed of the boat as a function of the both boat type
and the marine mammals present in the area, or we adjust the type of
boat as a function of both the desired boat speed and the marine mammals
present.

-  **Encourage awareness among the ship pilots**

The increase in marine traffic, during construction and operation, can
create disturbances for marine mammals and birds.

Ship pilots in charge of transit to the windfarms would benefit from
awareness training to clarify the behaviours to avoid when observing
bird or marine mammal groups during transits.

Actinopterigii/ Chondrichthyes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Electro-magnetic fields
'''''''''''''''''''''''

**NOTE:** A substantial data gaps exist for the interaction of pelagic
species (like pelagic sharks, marine mammals or fishes) and
electromagnetic fields emitted by dynamic cables. This gap remain partly
owing to difficulties in evaluating impacts at population scale around
these deployments and the recentness of this type of cables.

-  **Burying cables**

To reduce exposure of sensitive species to electromagnetic fields, the
physical distance between animals and cables can be increased by burying
them deep (Carlier et al., 2019b; Taormina et al., 2018). According to
magnetic fields emission models, burying a cable at 2 m deep allows a
75% reduction in the intensity received by species present at the
water-sediment interface (compared to the maximum intensity present at
the cable surface). This reduction is only 40% for a 1 m burying
(Carlier et al., 2019a).

-  **Cable technology**

Some cable technologies allow reduction of magnetic fields emissions,
such as three-phase AC cables and bipolar HVDC transmission systems
(Merck and Wasserthal, 2009; Taormina et al., 2018).

-  **Surveys/monitoring**

In order to fill the important data gap of *in situ* measures, it is
highly recommended to measure the actual EMFs emitted by the submarine
power cables. This monitoring should be done using a fixed monitoring
station in the vicinity of the cable which allow to capture the full EMF
variation spectrum (*i.e.* to take into account the variation over time
of the electricity production according to the farm performance).

.. _noise-1:

Noise
'''''

Species with swim bladders can especially be harmed by sound pressure.
Barotrauma can cause the swim bladders to expand and contract rapidly,
causing tissue damage of the swim bladders and then leading to the death
of the animal (Popper et al., 2014).

-  **Avoidance construction during time of special sensitivity**

Not carrying out construction during time of special sensitivity of fish
such as migration or spawning time of fishes can limit impacts (Thomsen
et al., 2006).

-  **Site selection**

Select an area that isn’t a corridor of migration or a zone that doesn’t
impact the different life stage of fishes.

Habitat loss/change
'''''''''''''''''''

-  **Avoidance of the presence of turbines in specific areas**

Due to the construction of the MRE structure, there is potential for
severe damage on the marine fauna. The life cycle of fish can require
different types of habitats at different life stages. Some species can
be pelagic with a benthic egg stage, thus some habitats, such as
seagrass, act like an important nurseries. In contrast, some benthic
species can spend their larvae stage in the column water (Ruso and
Bayle-Sempere, 2006)

Aves
^^^^

.. _collision-1:

Collision
'''''''''

-  **Site selection**

The method of avoiding the most of collisions of birds with wind farm
turbines is to avoid building these structures in high avian abundance
areas, particularly in migration corridors.

Also strategic planning should be based on detailed sensitivity mapping
of bird populations, habitats and flight paths in order to identify the
most sensitive locations (Marques et al., 2014).

-  **Turbine shut-down on demand**

This strategy can be applied in high level of risk and can be limited to
a specific period when a dangerous situation is occurring such as bird
flying in a high collision risk area (Marques et al., 2014).

-  **Restrict turbine operation**

The turbine in operation can be restricted at certain times of the day,
seasons or specific weather conditions to reduce impact (Marques et al.,
2014).But the inconvenient of that method is not well received by the
wind-energy companies due to a great loss in the energy production.

-  **Encourage the awareness among ship pilots**

The increasing of traffic, especially during the construction phase, can
cause disturbances to birds in an area. Avoiding this area and/or also
limiting the speed of boats can be a way to decrease the disturbance.

Awareness training by ship pilots in charge of transit to the windfarms
would clarify the behaviours to avoid when observing birds’ groups
during transits.

Reptilia
^^^^^^^^

.. _noise-2:

Noise
'''''

In context of seismic testing, only 3 countries (Brazil, Canada and USA)
developed mandatory mitigation guidelines that include turtles (Nelms et
al., 2016).These recommendations below can be also included in the
context of MRE due to the important impact of the noise especially
during the construction phase.

-  **Pre-watch**

Guidelines recommended 30mins of pre-watch period to monitor turtles by
the marine mammals observers (MMO) before starting the construction
(Nelms et al., 2016).

-  **Soft start protocol**

The guidelines for marine turtles recommend a soft-start of 20 mins,
except Canada, which recommends a period of 40 mins.

-  **Monitoring the exclusion zones**

The size of the exclusion can be different with countries. For the case
of turtles, the guidelines recommend an exclusion zone of 500m, except
for the Brazilian guidelines that recommend a size of 1000m around the
noise source (Nelms et al., 2016).

However note that the context here if for seismic surveys. For an MRE
context, if the sound source level is lower, the exclusion zone might
change.

-  **Shut-down**

Canadian and Brazilian guidelines have a shut-down in the case of a
turtle entering in the exclusion zone (500m or 1000m). The guidelines
for the USA don’t require a shut-down for turtles.

-  **Time-area closures**

Only the Brazilian guidelines give recommendations for avoiding
sensitive area and specific times of the year, such as reproduction
period or migration time (Nelms et al., 2016).

-  **No construction during the night and inclement weather**

The guidelines recommend to not start construction during night or
inclement weather due to the difficulties of monitoring turtles in these
conditions.

.. _collision-2:

Collision
'''''''''

-  **Vessel speed restriction during the lifetime of the project**

All vessels present in or transiting to/from windfarm areas should have
a speed restriction, in the same case as the marine mammals, especially
during periods of reproduction, migration, or surface active (NRDC,
2019).

-  **Encourage awareness among the ship pilots**

The increase in marine traffic, during construction and operation, can
create disturbances for marine mammals but also for turtles.

Increased turtle awareness by ship pilots in charge of transit to the
windfarms would clarify the behaviours to avoid when observing turtles
in their trajectory.

Artificial light impact 
'''''''''''''''''''''''

-  **Not using artificial light during specific time of the year**

The artificial lights can strongly attract the hatchlings. Light is a
navigational cue when the hatchling enters the water, therefore if the
artificial light disrupts the behaviour of the turtles in their
orientation and swimming, it could increase the chance of the mortality
by predation (Thums et al., 2016)

.. toctree::
   :hidden:
   :maxdepth: 2
