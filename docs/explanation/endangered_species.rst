.. _endangered-species:

Potential presence of endangered species
****************************************

Oceans are home to thousands of marine species that evolve from coastal shallow habitats to deep-depth canyons. With the unpreceded expansion of human anthropogenic activities, many marine habitats and associated marine species started to decline leading to the extinction of some species. The International Union for Conservation of Nature (IUCN), founded in 1948, is the main international organization that work on identifying the conservation status of worldwide species. Indeed, the organization is best known to the wider public for compiling and publishing the IUCN Red List of Threatened Species, which assesses the conservation status of species worldwide. IUCN's mission is to "influence, encourage and assist societies throughout the world to conserve nature and to ensure that any use of natural resources is equitable and ecologically sustainable". This organization is internationally recognized and is considered as reference in many countries, influencing by such national policies in terms of species protection measures for threatened species.
By assessing the conservation status of species, the IUCN classifies each assessed species into one of nine categories (Figure xx1). In descending order of threat, the IUCN Red List threat categories are as follows:

- Extinct or Extinct in the Wild
- Critically Endangered, Endangered and Vulnerable: species threatened with global extinction.
- Near Threatened: species close to the threatened thresholds or that would be threatened without ongoing conservation measures.
- Least Concern: species evaluated with a lower risk of extinction.
- Data Deficient: no assessment because of insufficient data.
- Not Evaluated: for species that are not yet assessed

.. image:: ../figures/IUCN.png 

Today, the IUCN list of threatened species comprises more than thirty-two thousand species classified as such worldwide. However, each state has its own list of locally threatened species. In this report, a focus has been made mainly on endangered species classified as such by the IUCN and listed in international conventions and European directives that represent the European Framework for marine species protection, namely: 

- Habitat Directive; 
- Birds Directive; 
- Marine Strategy Framework Directive
- Barcelona Convention; 
- Berne Convention; 
- Bonn Convention; 
- Helcom Convention; 
- Ospar Convention; 
- Washington Convention

See how potential presence of endangered species are considered in DTOceanPLUS in :ref:`es_dto`