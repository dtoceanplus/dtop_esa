.. _sa_dto:

Social Acceptance 
*****************

.. contents::


Principles and objectives
-------------------------

The social acceptance assessment of the ESA module aims at providing the user with an estimation of the number of created jobs (Njobs/MW) during MRE life cycle and the cost of consenting (€/MW) of the MRE. These metrics are important for social acceptance of MRE projects as the employment question is key in the acceptance of any new activity in a given region and the cost of consenting informs about the final cost of the energy produced. 

At this point of the module development, the social acceptance functionality only takes in consideration employments on vessels during marine operations. Further research needs to be carried out to complete other phases of the project such as manufacturing processes.

Inputs
------

+--------------+---------------------------------------------------------------------------+--------------------+---------------------+-------+
| ID           | Brief Description of the   Input Quantity                                 | Origin of the Data | Data   Model in ESA | Units |
+==============+===========================================================================+====================+=====================+=======+
| LCOE         | Levelized Cost Of Energy                                                  | SPEY               | Number, float       | €/MW  |
+--------------+---------------------------------------------------------------------------+--------------------+---------------------+-------+
| NbPassengers | Number of passengers working on vessels mobilized for   marine operations | LMO                | Number, integer     | -     |
+--------------+---------------------------------------------------------------------------+--------------------+---------------------+-------+
| TotProd      | Total energy production                                                   | ED                 | Number, float       | GWh   |
+--------------+---------------------------------------------------------------------------+--------------------+---------------------+-------+

Methods & outputs
-----------------

The social acceptance functions are dependant from the outputs of the LMO, SPEY and ED modules. The cost of consenting (€/MW) corresponds to the Levelized Cost of Energy that is calculated in the SPEY module. Regarding the number of Jobs function, the following formula is used to generate the output of this function:

Equation 2.38
	.. math:: Nb of Jobs=NbPassengers/TotProd


The social acceptance part of the ESA module evaluates and proposes recommendations to increase social acceptance through the number of jobs (Njobs/MW) during MRE life cycle (i.e. increasing the number of jobs) and the cost of consenting (€/MW) of the MRE (i.e. reducing the cost of energy).


.. toctree::
   :hidden:
   :maxdepth: 2
