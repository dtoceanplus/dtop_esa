.. _general-concept_to_structuring_functions:

From general concept to structuring environmental and social acceptance functions
*************************************************************************************

Following the regulatory context of environmental risk assessment and socio-economic opportunities, the Environmental and Social Acceptance (ESA) module was structured into four different parts representing four complementary assessments:

#. Identification of the :ref:`endangered-species` in the area of MRE installation

#. :ref:`environmental-impact_assessment` estimated for the main environmental stressors pre-identified as potential stressors from MRE 

#. Estimation of the :ref:`life_cycle_assessment` and the green-house effect of the project in terms of Global Warming Potential (GWP), Cumulative Energy Demand (CED) and Energy Payback period (EPP)

#. Information to improve the :ref:`social-acceptance` of the project considering socio-economic opportunities



.. toctree::
   :maxdepth: 10
   :hidden:

   endangered_species
   environmental_impact_assessment
   life_cycle_assessment
   social_acceptance