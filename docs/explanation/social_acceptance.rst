.. _social-acceptance:

Social Acceptance
*****************

Social Acceptance is central in many debates surrounding a proposed
development, such as ocean energies. Beside environmental concerns,
social acceptance is also influenced by socio-economic impacts which
addresses how a proposed development might affect the society as a whole
or the local population (Uihlein and Magagna, 2016). On the society
level, renewable energies are generally perceived as a sustainable
alternative for energy production useful to reduce the society
dependency on non-renewable resources, such as fossil fuels, especially
in a context of increasing energy needs. The important repercussions of
non-renewable resources on the environment and the near scarcity of
these resources so far used increases the awareness of society to the
need of diversifying energy sources. Society comes to understand that
the possibilities of supplying their energy needs, must guarantee the
coverage of current needs, as well as the future supply of next
generations and that this is feasible to achieve only if the resources
are renewed naturally and permanently. On the local level, various
issues can be addressed ranging from well-being and quality of life to
employment, income, local secure energy source and economic power
(Copping et al., 2020; Uihlein and Magagna, 2016). For ocean energy,
specific topics are negative effects due to visual impacts and the
reduction of access to space for other users of the marine environment.
Similar to the assessment of environmental impacts, both positive and
negative impacts of ocean energy deployment on society and economy needs
to be studied in order to support evidence-based policy making (Uihlein
and Magagna, 2016).

Social and economic effects can include benefits to or adverse effects
on employment, local infrastructure and services, businesses, and
communities (Freeman 2020). A list of social and economic categories of
concern is synthesized in Table below.

*Table : Socio-economic data and information related to Ocean
Energies. This table is modified from(Copping et al., 2020).*

+-------------------+----------------------------------+----------------------------------------------------+
|   Main category   |        Detailed categories       |      Relevant operational data or information      |
+===================+==================================+====================================================+
| Social concerns   | Social/cultural context          | Including impacts on communities ;                 |
|                   |                                  |                                                    |
|                   | and communities                  | Changes in power demand and supply ;               |
|                   |                                  |                                                    |
|                   |                                  | Displacement of traditional activities,            |
|                   |                                  |                                                    |
|                   |                                  | cultural heritage and recreational activities ;    |
|                   |                                  |                                                    |
|                   |                                  | Changes to valuation of area ; Visual impacts ;    |
|                   |                                  |                                                    |
|                   |                                  | Cost of living                                     |
+-------------------+----------------------------------+----------------------------------------------------+
|                   | Leisure, recreation,             | Including exclusion of activities ;                |
|                   |                                  |                                                    |
|                   | and tourism                      | Number of tourists ;                               |
|                   |                                  |                                                    |
|                   |                                  | Changes to recreation opportunities                |
+-------------------+----------------------------------+----------------------------------------------------+
|                   | Local services, infrastructure,  | Including pressure on services and                 |
|                   |                                  |                                                    |
|                   | and facilities                   | local infrastructure ;                             |
|                   |                                  |                                                    |
|                   |                                  | Workforce and their requirements such              |
|                   |                                  |                                                    |
|                   |                                  | as housing/accommodation ;                         |
|                   |                                  |                                                    |
|                   |                                  | Requirements to support community                  |
+-------------------+----------------------------------+----------------------------------------------------+
|                   | Health and well-being            | Including changes to well-being indicators ;       |
|                   |                                  |                                                    |
|                   |                                  | Impacts to health benefits ;                       |
|                   |                                  |                                                    |
|                   |                                  | Changes to community health                        |
+-------------------+----------------------------------+----------------------------------------------------+
|                   | Success stories and lessons      | Social acceptance and awareness ;                  |
|                   |                                  |                                                    |
|                   | learned                          | Community engagement and stakeholders inclusion ;  |
|                   |                                  |                                                    |
|                   |                                  | Changes in knowledge of MRE                        |
+-------------------+----------------------------------+----------------------------------------------------+
|                   | Energy security and clean energy | Including installed capacity and                   |
|                   |                                  |                                                    |
|                   |                                  | availability of power to the local community ;     |
|                   |                                  |                                                    |
|                   |                                  | Overview of potential for reduction,               |
|                   |                                  |                                                    |
|                   |                                  | carbon offsets, etc. ;                             |
|                   |                                  |                                                    |
|                   |                                  | Greenhouse gas and pollutants avoided              |
+-------------------+----------------------------------+----------------------------------------------------+
| Economic concerns | Businesses/                      | Incuding local and regional businesses ;           |
|                   |                                  |                                                    |
|                   | sectors and existing industries  | Inward investment potential at each                |
|                   |                                  |                                                    |
|                   |                                  | stage of development ;                             |
|                   |                                  |                                                    |
|                   |                                  | Exclusion of marine uses or users ;                |
|                   |                                  |                                                    |
|                   |                                  | Footprint of development ;                         |
|                   |                                  |                                                    |
|                   |                                  | Vessel management plan                             |
+-------------------+----------------------------------+----------------------------------------------------+
|                   | Employment and wages             | Including local job and skilled job creation ;     |
|                   |                                  |                                                    |
|                   |                                  | Job creation potential ; Employment multiplier ;   |
|                   |                                  |                                                    |
|                   |                                  | Jobs staffed locally or by outside sources ;       |
|                   |                                  |                                                    |
|                   |                                  | Net job gain or loss ; Changes to wages ;          |
|                   |                                  |                                                    |
|                   |                                  | Cross wages                                        |
+-------------------+----------------------------------+----------------------------------------------------+
|                   | Marine Renewable Energy (MRE)    | Including Gross value added ;                      |
|                   |                                  |                                                    |
|                   | industry/contribution            | Use of local economy vs. outside/external          |
|                   |                                  |                                                    |
|                   |                                  | sources ;                                          |
|                   |                                  |                                                    |
|                   |                                  | Changes in local and regional supply chain         |
+-------------------+----------------------------------+----------------------------------------------------+
|                   | Exports                          | Including changes in export of products and        |
|                   |                                  |                                                    |
|                   |                                  | services ;                                         |
|                   |                                  |                                                    |
|                   |                                  | Products created ;                                 |
|                   |                                  |                                                    |
|                   |                                  | New services ;                                     |
|                   |                                  |                                                    |
|                   |                                  | Value to local/regional economy                    |
+-------------------+----------------------------------+----------------------------------------------------+
|                   | Existing infrastructure          | Including added infrastructure or capacity ;       |
|                   |                                  |                                                    |
|                   |                                  | New infrastructure developments and/or projects    |
+-------------------+----------------------------------+----------------------------------------------------+
|                   | Coastal development              | Including new developments with direct links       |
|                   |                                  |                                                    |
|                   |                                  | to MRE project ;                                   |
|                   |                                  |                                                    |
|                   |                                  | Other industry developments                        |
|                   |                                  |                                                    |
|                   |                                  | (ports and harbors, shipping, navigation)          |
+-------------------+----------------------------------+----------------------------------------------------+
|                   | Local/regional economy           | Including Economic impact to community             |
|                   |                                  |                                                    |
|                   |                                  | (such as revenue generated, changes in valuation   |
|                   |                                  |                                                    |
|                   |                                  | of the area, clustering effect) ;                  |
|                   |                                  |                                                    |
|                   |                                  | Engagement with local suppliers and                |
|                   |                                  |                                                    |
|                   |                                  | new businesses ;                                   |
|                   |                                  |                                                    |
|                   |                                  | Inclusion of impact to remote and indigenous       |
|                   |                                  |                                                    |
|                   |                                  | communities(access to economic activity)           |
+-------------------+----------------------------------+----------------------------------------------------+

\*Note: This table does not include an exhaustive list of potential
effects, indicators or data types, or assessment methods. Instead, it
provides a general overview/description and some examples of social and
economic effects and data collection.