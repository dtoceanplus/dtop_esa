.. _cfp_dto:

===================
Carbon Footprint 
===================

More than any other industrial products, MRE systems (and generally renewable energy production) should always be subject to LCA. 
First of all, since they are supposed to provide an answer to the environmental problems posed by conventional means of energy production, it makes sense to assess their environmental performance. Thus, even before the impact studies are carried out locally on the site where a system is to be installed, it seems fundamental to have information which certifies that the system presents a positive assessment on large-scale criteria (e.g. impact on global warming). 

On the other hand, since the primary function of these systems is to produce energy, it seems logical to seek to trace their energy profile over the entire life cycle. In other words, how can we ensure that the energy that is required for these MRE systems to produce energy (i.e. manufacture, install, operate and dismantle MRE systems) is significantly less than what they will produce during their operational phase (this notion will be reflected in LCA using the “energy payback period” midpoint indicator). In addition, the implementation of these systems involves communities and companies in the long and medium term, with significant investments: having a complete view of the environmental performance of the systems is therefore legitimate. 

Finally, for sectors that have not yet converged on a concept, and where there are many technical possibilities (e.g. wave power, tidal power, floating wind power), LCA has its place as a tool contributing to concept selection. From limited elements (a rough product nomenclature, a power forecast and an installation and operation scenario), it is possible to conduct an LCA. This LCA would be limited to one or two criteria (e.g. contribution to climate change and energy consumption), but it would make it possible to situate a concept among its competitors and to judge, in the first degree, of its relevance.
The LCA performed in the ESA module of the DTOceanPlus set of tools follows the structure of LCA process defined by the standards ISO 14040 and ISO 14044. 

These standards define a four steps process to conduct an LCA as previously detailed in section 2.2.3 of this report. These four steps where applied in the ESA module and each step will be detailed in the following sections below.

.. contents::

Objective and scope of LCA in ESA module
----------------------------------------

The LCA is achieved in order to (1) translate the preliminary flows (e.g. bills of material required for the production of devices, fuel required for transportation during installation) of an MRE conceptual array into midpoint informative indicators (I.e. Global Warming Potential (gCO2-eq/kWh), Cumulative Energy Demand (MJ/kWh) and Energy Payback Period (Years)) and (2) make it possible to situate a concept among its alternative concepts and to judge, in the first degree, of its relevance.

#. The system main function and functional unit

	The type of product that can be evaluated by the ESA module is an MRE array installed at sea and connected to the onshore electricity network. The main function of the product is to convert renewable energy (kinetic, potential) into electrical energy and deliver it to the grid. The Functional Unit (FU) used is the kWh of electrical energy delivered to the network.

#. The system modelling applied

	The system is modelled following an attributional approach. The processes it uses are therefore average (technical, geographic, economic, etc.) and fixed images of existing processes, over which the system in turn has no influence. This approach is different from the consequential approach, whose objectives are both to determine the changes that the system can induce on the processes that it uses (or that surround it) and to assess the impacts caused by these changes.

#. The system’s limits

	The definition of system boundaries uses the process classification scale defined in the International Reference Life Cycle Data System (ILCD) Handbook (REF). Applied to MRE systems, this scale makes it possible to distinguish the following levels: 

	•	Level 0: MRE array allowing the conversion of renewable energy into electricity. 
	•	Level 1: manufacturing, assembly and processing activities of the constituent components of the MRE array. 
	•	Level 2: processes participating physically in the installation, operation and dismantling of the MRE array (e.g. maritime means for installation). 
	•	Level 3: processes involved in the installation, operation and dismantling of the MRE array, without physical involvement (e.g. design offices, administrative services). 

	In the ESA module, the system includes the processes of levels 0 to 2. This limitation is mainly related to data availability at the conceptual phase of MRE design in the DTOceanPlus set of tools.

#. The Life Cycle phases of a system

	The life cycle of the system is divided into 5 phases, as shown in Figure xx1. These phases are:

	•	Production phase: which concerns the manufacturing process. This is the assembly phase of the various components and materials, a priori on dedicated port facilities.
	•	Installation phase: that involves the site preparation, then shipping, setting up and connecting the MRE system.
	•	Exploitation phase: which is the longest phase during the life cycle during which the system produces energy and it is maintained to perform this function.
	•	Disassembling phase: where the system is dismantled and in large part brought ashore.
	•	Treatment phase: which includes sorting and transport phases, as well as the final processes that will be applied to the sorted elements (recycling, incineration, landfill).


.. image:: ../figures/cfp_phase.png 

Source of the data
------------------

The ESA module of the DTOceanPlus suite of tools uses two main sources of input data to produce LCA assessment: 

	- The other tools and modules in DTOceanPlus provide the ESA module with a certain amount of information regarding the different life cycle phases (e.g., the Bills of Materials (BoM) used during each phase, the total fuel consumption during operations and maintenance etc.).

	- As for the database in the ESA libraries, the information that is available is mainly from the ecoinvent version 3 online database (www.ecoinvent.org). Ecoinvent is a consistent and transparent life cycle inventory database in many areas such as energy supply, transport and biofuels, construction materials and waste treatment. Ecoinvent gives accurate values, of greenhouse impact for instance, of many materials and processes. The information needed to run LCA in ESA module, and that is not given by the other modules, was taken from this online database.


Identification of available data in DTOceanPlus suite of tools
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This section is divided into two main sub-sections that reflects specific phases of MRE projects’ life cycle. This division into two sub-sections is related to data availability in DTOceanPlus set of tools and its contribution to LCA assessment. The first subsection includes the Production and the Treatment phases of MRE projects’ life cycle. Both phases deal with the Bills of Materials (BoM) that are available for the construction of an MRE project and its treatment or recycling at the end of its life cycle. The second sub-section includes the Installation, Exploitation and Disassembling phases where the main impact on LCA assessment during these phases is related to maritime transports. These subsections are detailed below.

**Available data during Production and Treatment phases**

LCA of Production phase (including fabrication and manufacturing of materials) and Treatment phase requires to have from one hand, the list and quantity of materials used for manufacturing and from the other hand, the quantity of the material that is broad back to land to be recycled. This information is identified as the Bills of Materials (BoM) and is expected from the other DTOceanPlus suite of tools as shown in the table xx1 below.

*Table - Inputs for the evaluation of Global Warming Potential (GWP), Cumulative Energy Demand (CED) and Energy Pay-back Period (EPP) midpoint indicators of Life Cycle Assessment during production and treatment phases. BoM: Bills of Materials.*

+---------------------------------------------------+--------------------------------------+-------+
| Brief Description of the Input Quantity           | Origin of the Data                   | Units |
+===================================================+======================================+=======+
| BoM of moorings: Mass of materials of each        | Station Keeping tool and catalogue   |   t   |
|                                                   |                                      |       |
| component                                         |                                      |       |
+---------------------------------------------------+--------------------------------------+-------+
| Mass of materials of each component to be         | Station Keeping tool and user        |   t   |
|                                                   |                                      |       |
| recycled at the end of project lifetime           |                                      |       |
+---------------------------------------------------+--------------------------------------+-------+
| BoM of foundations: Mass of materials of          | Station Keeping tool and catalogue   |   t   |
|                                                   |                                      |       |
| each component                                    |                                      |       |
+---------------------------------------------------+--------------------------------------+-------+
| Mass of materials of each component to be         | Station Keeping tool and user        |   t   |
|                                                   |                                      |       |
| recycled at the end of project lifetime           |                                      |       |
+---------------------------------------------------+--------------------------------------+-------+
| BoM of Device: Mass of materials of each          | MC                                   |   t   |
|                                                   |                                      |       |
| component                                         |                                      |       |
+---------------------------------------------------+--------------------------------------+-------+
| Mass of materials of each component to be         | MC, User                             |   t   |
|                                                   |                                      |       |
| recycled at the end of project lifetime           |                                      |       |
+---------------------------------------------------+--------------------------------------+-------+
| BOM of PTOs: Mass of materials of each component  | Energy transport tool and catalogue  |   t   |
+---------------------------------------------------+--------------------------------------+-------+
| Mass of materials of each component               | Energy transport tool and User       |   t   |
|                                                   |                                      |       |
| to be recycled at the end of project lifetime     |                                      |       |
+---------------------------------------------------+--------------------------------------+-------+
| BOM of electrical infrastructure:                 | Energy delivery tool and catalogue   |   t   |
|                                                   |                                      |       |
| Mass of materials of each component               |                                      |       |
+---------------------------------------------------+--------------------------------------+-------+
| Mass of materials of each component               | Energy delivery tool and User        |   t   |
|                                                   |                                      |       |
| to be recycled at the end of project lifetime     |                                      |       |
+---------------------------------------------------+--------------------------------------+-------+
| Total energy production                           | Energy delivery tool                 |  kWh  |
+---------------------------------------------------+--------------------------------------+-------+

During the production phase, the carbon footprint (I.e. GWP) that is generated is mainly related to the production of materials (e.g. unalloyed steel, concrete etc.) that are used to build the devices, the electrical sub-systems and the moorings and foundations. This carbon footprint is reduced in the calculation of the midpoint indicators when recycled material is used. That is why the recycled material used is also requested from the other DTOceanPlus suite of tools.

Each listed material has its own environmental impact potential that is translated into GWP (kgCO2-eq/t) or CED (MJ/t) values (Table below). In other words, the fabrication and manufacturing of each material can produce separately a greenhouse effect gas such as CO2 and requires also the use of primary energy such as fuel for their production. 


This information is crucial in order to transform the quantities of material identified from the DTOceanPlus tools and modules (I.e. the preliminary flows) into midpoint indicators. The final GWP and CED Life Cycle Assessment values represent the sum of all GWP and CED of material used (for more details on the equation, refer to deliverable D6.5).

*Table - List of materials considered in DTOceanPLUS*

.. image:: ../figures/cfp_materials.png


**Available data during marine operations (Installation, Exploitation and Disassembling phases)**

The installation, operation and disassembling of the MRE systems call for naval resources allowing a variety of work (towing, lifting, etc.). Faced with the diversity of these means and the difficulty of describing them all, it was estimated more suitable to characterize them by a single but configurable parameter that produces the main GWP and CED impacts: the fuel consumption. Knowing the quantity of fuel consumed will first allow, using the Ecoinvent database, to quantify the extractions and emissions caused by the production of this fuel. This quantity of fuel consumed will also determine the emissions and extractions due to the combustion of the fuel.
	
The combustion data presented in Table below are those used by the Technical Reference Center for Air Pollution and Climate Change (CITEPA 2020). They lead to the determination of the quantities of greenhouse gases emitted by the combustion of diesel fuel which are Carbon dioxide (CO2), Methan (CH4) and Nitrous oxide (N2O). 

These gases do not have the same impact as their impact factor differs (i.e. Methan has 25 times more impact than CO2 and Nitrous Oxide has 298 times more impact than CO2).  
For example, during the combustion of 1 Kg of diesel fuel, the CO2 emission is due to direct emission of CO2 (74.7 x 1=74.7 KgCO2eq/GJ) but also to the emission of Ch4 (0.0025 x 25= 0.06 KgCO2eq/GJ) and N2O (0.0025 x 298= 0.75 KgCO2eq/GJ). The total amount of CO2 emitted by the combustion of 1 Kg of diesel is   (74.7 + 0.06 + 0.75) x 0.042 = 3.2 KgCO2eq (for more details on the equation included in ESA module, refer to deliverable D6.5).

*Table - Inventory of diesel parameters for combustion*

.. image:: ../figures/cfp_diesel.png


**Specific calculation for recycling**

From a qualitative point of view, we understand that the existence of recycling can reduce the impacts at both ends of the life cycle, i.e. during the manufacturing phase by the use of recycled materials and at the end of life cycle, during the disassembling phase, by the supply of materials to a recycling process. The question here is how to rigorously model the existence of recycling processes (in particular avoiding omissions or double counting in the inventory)? One should note that this problem only concerns the components or materials brought back to land at the end of the MRE farm's life cycle. Indeed, objects likely to be left at sea (anchors, electric cables, etc.) are considered non-recyclable and are processed by the calculator considering that they are made only of primary materials.

Life cycle inventory
--------------------

Life cycle inventory characterizes the flows entering and leaving the system (in kind and in quantity) for each stage of the life cycle. In other terms, this section describes the data and calculations necessary for carrying out the inventory, i.e. for the construction of the complete list of material flows exchanged by the system with its environment during the life cycle.

The outputs of the carbon footprint metrics in ESA module will inform the user about the project potential impact in terms of gas emissions and use of primary energy during its life cycle. GWP and CED metrics are thus used to understand potential global impact of every phase of the life cycle of a project in terms of CO2 emissions and non-renewable energy consumption. 

The two metrics proposed here are useful indicators to quickly compare different projects and scenarios.


Production phase
~~~~~~~~~~~~~~~~

**Inputs**

The inputs needed for carrying out the assessment of Global Warming Potential (GWP) and Cumulative Energy Demand (CED) during the production phase are represented in the Bills of Materials (BOM) listed in Table below:

+---------------+-------------------------------------------------------------------------------------+---------------------+-------------------+-------------+
| ID            | Brief Description of the Input   Quantity                                           | Origin of the Data  | Data Model in ESA | Units       |
+===============+=====================================================================================+=====================+===================+=============+
| BOMmoor       | BoM of   moorings: Mass of materials of each component                              | SK, catalogue       | Number, float     |      t      |
+---------------+-------------------------------------------------------------------------------------+---------------------+-------------------+-------------+
| BOMmoorR      | Mass of   materials of each component to be recycled at the end of project lifetime | SK, user            | Number, float     |      t      |
+---------------+-------------------------------------------------------------------------------------+---------------------+-------------------+-------------+
| BOMfound      | BoM of   foundations: Mass of materials of each component                           | SK, catalogue       | Number, float     |      t      |
+---------------+-------------------------------------------------------------------------------------+---------------------+-------------------+-------------+
| BOMfoundR     | Mass of   materials of each component to be recycled at the end of project lifetime | SK, user            | Number, float     |      t      |
+---------------+-------------------------------------------------------------------------------------+---------------------+-------------------+-------------+
| BOMdevice     | BoM of Device:   Mass of materials of each component                                | MC                  | Number, float     |      t      |
+---------------+-------------------------------------------------------------------------------------+---------------------+-------------------+-------------+
| BOMdeviceR    | Mass of   materials of each component to be recycled at the end of project lifetime | MC, User            | Number, float     |      t      |
+---------------+-------------------------------------------------------------------------------------+---------------------+-------------------+-------------+
| BOMpto        | BOM of PTOs:   Mass of materials of each component                                  | ET, catalogue       | Number, float     |      t      |
+---------------+-------------------------------------------------------------------------------------+---------------------+-------------------+-------------+
| BOMptoR       | Mass of   materials of each component to be recycled at the end of project lifetime | ET, User            | Number, float     |      t      |
+---------------+-------------------------------------------------------------------------------------+---------------------+-------------------+-------------+
| BOMelec       | BOM of   electrical infrastructure: Mass of materials of each component             | ED, catalogue       | Number, float     |      t      |
+---------------+-------------------------------------------------------------------------------------+---------------------+-------------------+-------------+
| BOMelecR      | Mass of   materials of each component to be recycled at the end of project lifetime | ED, User            | Number, float     |      t      |
+---------------+-------------------------------------------------------------------------------------+---------------------+-------------------+-------------+
| TotProd       | Total energy   production                                                           | ED                  | Number, float     |     GWh     |
+---------------+-------------------------------------------------------------------------------------+---------------------+-------------------+-------------+
| GWPmat        | GWP of each   material production                                                   | Local   database[1] | Number, float     | kgCO2-eq /t |
+---------------+-------------------------------------------------------------------------------------+---------------------+-------------------+-------------+
| GWPrecycl     | GWP related to   recycling process of each material                                 | Local database      | Number, float     | kgCO2-eq /t |
+---------------+-------------------------------------------------------------------------------------+---------------------+-------------------+-------------+
| GWPBeneFactor | Half-life   benefit factor from recycling                                           | Local database      | Number, float     | kgCO2-eq /t |
+---------------+-------------------------------------------------------------------------------------+---------------------+-------------------+-------------+
| CEDmat        | CED used to   produce each material                                                 | Local database      | Number, float     |     MJ/t    |
+---------------+-------------------------------------------------------------------------------------+---------------------+-------------------+-------------+
| CEDrecycl     | CED related to   recycling process of each material                                 | Local database      | Number, float     |     MJ/t    |
+---------------+-------------------------------------------------------------------------------------+---------------------+-------------------+-------------+
| CEDBeneFactor | Half-life   benefit factor from recycling                                           | Local database      | Number, float     |     MJ/t    |
+---------------+-------------------------------------------------------------------------------------+---------------------+-------------------+-------------+


During the production phase, the carbon footprint that is generated is mainly related to the production of materials (e.g. alloyed steel) that are used to build the devices, the electrical sub-systems and the moorings and foundations. The following formulas are applied to estimate:

**Methods and outputs**

 - GWP during production phase (GWPprod):

The first step is to calculate the GWP related to each technology group (i.e. Equation 2.21, Equation 2.22, Equation 2.24, Equation 2.25, Equation 2.26) before summing the obtained values in order to calculate the GWP of the production phase (i.e. Equation 2.26). In the following equations, GWPmat corresponds to the Global Warming Potential of the first fabrication of the material, if this material can be recycle, it is considered that GWPrecycle  is the GWP of the manufacturing processes of the secondary fabrication and GWPbenefactor is the half of the benefits of recycling (the other half being assigned to the treatment phase). 

Equation 2.21
  .. math:: 

  	GWPmoor =\frac{∑_1^n[BOMmoor× GWPmat+BOMmoorR×(GWPrecycl-GWPBeneFactor)]}{TotProd}

Equation 2.22
  .. math:: GWPfound=\frac{∑_1^n[BOMfound × GWPmat+BOMfoundR × (GWPrecycl-GWPBeneFactor)]}{TotProd}
Equation 2.23
  .. math:: GWPdevice=\frac{∑_1^n[BOMdevice × GWPmat+BOMdeviceR x (GWPrecycl-GWPBeneFactor)]}{TotProd}
Equation 2.24
  .. math:: GWPpto=\frac{∑_1^n[BOMpto × GWPmat+BOMptoR × (GWPrecycl-GWPBeneFactor)]}{TotProd}
Equation 2.25
  .. math:: GWPelec=\frac{∑_1^n[BOMelec × GWPmat+BOMelecR× (GWPrecycl-GWPBeneFactor)]}{TotProd}
Equation 2.26
  .. math:: GWP_prod=GWPmoor+GWPfound+GWPdevice+GWPpto+GWPelec


 - CED during production phase (CEDprod):
	
The first step is to calculate the CED related to each technology group (i.e. Equation 2.27, Equation 2.28, Equation 2.30, Equation 2.31, Equation 2.30, Equation 2.31) before summing the obtained values in order to calculate the CED of the production phase (i.e.Equation 2.32).

Equation 2.27
 .. math:: CEDmoor=\frac{∑_1^n[BOMmoor_n×CEDmat_n+BOMmoorR_n×(CEDrecycl_n-CEDBeneFactor_n)]}{TotProd}
Equation 2.28
 .. math:: CEDfound=\frac{∑_1^n[BOMfound_n×CEDmat_n+BOMfoundR_n×(CEDrecycl_n-CEDBeneFactor_n)]}{TotProd}
Equation 2.29
 .. math:: CEDdevice=\frac{∑_1^n[BOMdevice_n×CEDmat_n+BOMdeviceR_n×(CEDrecycl_n-CEDBeneFactor_n)]}{TotProd}
Equation 2.30
 .. math:: CEDpto=\frac{∑_1^n[BOMpto_n×CEDmat_n+BOMptoR_n×(CEDrecycl_n-CEDBeneFactor_n)]}{TotProd}
Equation 2.31
 .. math:: CEDelec=\frac{∑_1^n[BOMelec_n×CEDmat_n+BOMelecR_n×(CEDrecycl_n-CEDBeneFactor_n )]}{TotProd}
Equation 2.32
 .. math:: CED_prod=CEDmoor+CEDfound+CEDpto+CEDelec


Marine operations
~~~~~~~~~~~~~~~~~

 **Inputs**
	
 The inputs needed for carrying out the assessment of Global Warming Potential (GWP) and Cumulative Energy Demand (CED) during marine operations are listed in Table below:

	+-----------------+----------------------------------------------------+--------------------+-------------------+--------------+
	| ID              | Brief Description of the Input   Quantity          | Origin of the Data | Data Model in ESA | Units        |
	+=================+====================================================+====================+===================+==============+
	| Totalfuelconso  | Total fuel   consumption during installation phase | LMO                | Number, float     |       t      |
	+-----------------+----------------------------------------------------+--------------------+-------------------+--------------+
	| TotProd         | Total energy   production                          | ED                 | Number, float     |      GWh     |
	+-----------------+----------------------------------------------------+--------------------+-------------------+--------------+
	| LCVfuel         | Lower calorific   value of diesel fuel             | Local database     | Number, float     |     MJ/t     |
	+-----------------+----------------------------------------------------+--------------------+-------------------+--------------+
	| GWPfuel         | 100-year global   warming potential of diesel fuel | Local database     | Number, float     | kgCO2-eq.t-1 |
	+-----------------+----------------------------------------------------+--------------------+-------------------+--------------+
	| CEDfuel         | CED related to   fuel consumption                  | Local database     | Number, float     |     MJ/t     |
	+-----------------+----------------------------------------------------+--------------------+-------------------+--------------+
	| FactorCO2emis   | CO2 emission   factor of diesel fuel combustion    | Local database     | Number, float     |   kgCO2/GJ   |
	+-----------------+----------------------------------------------------+--------------------+-------------------+--------------+
	| FactorCH4emis   | CH4 emission   factor of diesel fuel combustion    | Local database     | Number, float     |   kgCO2/GJ   |
	+-----------------+----------------------------------------------------+--------------------+-------------------+--------------+
	| FactorN2Oemis   | N2O emission   factor of diesel fuel combustion    | Local database     | Number, float     |   kgCO2/GJ   |
	+-----------------+----------------------------------------------------+--------------------+-------------------+--------------+
	| ImpactFactorCO2 | Impact factor of   CO2 on global warming           | Local database     | Number, float     |  kgCO2-eq/kg |
	+-----------------+----------------------------------------------------+--------------------+-------------------+--------------+
	| ImpactFactorCH4 | Impact factor of   CH4 on global warming           | Local database     | Number, float     |  kgCO2-eq/kg |
	+-----------------+----------------------------------------------------+--------------------+-------------------+--------------+
	| ImpactFactorN2O | Impact factor of   N2O on global warming           | Local database     | Number, float     |  kgCO2-eq/kg |
	+-----------------+----------------------------------------------------+--------------------+-------------------+--------------+

 **Methods & outputs**

 During the marine operation phases (i.e. installation, exploitation and disassembling phases), the carbon footprint will mainly be due to operations and maintenance at sea. These operations use different types of vessel and the main input to estimate GWP (kgCO2-eq/ kWh) and CED (MJ/kWh) is thus the total fuel consumption during marine operations. The following formulas are applied to estimate:
		
 A first equation allows to calculate the gas emissions (in kgCO2-eq) related to fuel consumption (i.e. Equation 2.33). A second equation is used to calculate the GWPmarOp (i.e. Equation 2.34) which include gas emissions plus the global warming potential of diesel fuel (i.e. 100-year global warming potential of diesel fuel).
	
 - GWP during marine operations (GWPmarOp):

 .. math:: GasEmission=\frac{Totalfuelconso×LCVfuel/1000){FactorCO2emis×ImpactFactorCO2+FactorCH4emis×ImpactFactorCH4+FactorN2Oemis×ImpactFactorN2O}

 .. math:: GWPmarOp=\frac{GasEmission+(Totalfuelconso×GWPfuel )}{TotProd}

 - CED during marine operations (CEDmarOp):

 .. math:: CEDmarOp=\frac{Totalfuelconso×CEDfuel}{TotProd}


Treatment
~~~~~~~~~

 **Inputs**

 The inputs needed for carrying out the assessment of Global Warming Potential (GWP) and Cumulative Energy Demand (CED) during treatment phase are listed in Table below:

	+---------------+-------------------------------------------------------------------+--------------------+-------------------+-------------+
	| ID            | Brief Description of the Input   Quantity                         | Origin of the Data | Data Model in ESA | Units       |
	+===============+===================================================================+====================+===================+=============+
	| BOMmoorR      | Mass of materials   to be recycled at the end of project lifetime | SK                 | Number, float     | t           |
	+---------------+-------------------------------------------------------------------+--------------------+-------------------+-------------+
	| BOMfoundR     | Mass of materials   to be recycled at the end of project lifetime | SK                 | Number, float     | t           |
	+---------------+-------------------------------------------------------------------+--------------------+-------------------+-------------+
	| BOMdeviceR    | Mass of materials   to be recycled at the end of project lifetime | EC                 | Number, float     | t           |
	+---------------+-------------------------------------------------------------------+--------------------+-------------------+-------------+
	| BOMptoR       | Mass of materials   to be recycled at the end of project lifetime | ET                 | Number, float     | t           |
	+---------------+-------------------------------------------------------------------+--------------------+-------------------+-------------+
	| BOMelecR      | Mass of materials   to be recycled at the end of project lifetime | ED                 | Number, float     | t           |
	+---------------+-------------------------------------------------------------------+--------------------+-------------------+-------------+
	| TotProd       | Total energy   production                                         | ED                 | Number, float     | GWh         |
	+---------------+-------------------------------------------------------------------+--------------------+-------------------+-------------+
	| GWPBeneFactor | Half-life benefit   factor from recycling for each material       | Local database     | Number, float     | kgCO2-eq /t |
	+---------------+-------------------------------------------------------------------+--------------------+-------------------+-------------+

 **Methods and outputs**

 Recycling of materials at the end of the project life cycle is considered as a positive action which reduces the global carbon footprint of the project. GWPtreatment and CEDtreatment calculated here are subtracted from the global GWP and CED of the project life cycle. The following formulas are applied to estimate:

 - GWP during treatment phase (GWPtreatment):
  .. math:: GWPtreatment=\frac{∑_1^n((BOMmoorR_n×GWPBeneFactor_n+BOMfoundR_n×GWPBeneFactor_n+ BOMdeviceR_n×GWPBeneFactor_n+BOMptoR_n×GWPBeneFactor_n+ BOMelecR_n×GWPBeneFactor_n ))}{TotProd}

 - CED during treatment phase (CEDtreatment):
  .. math:: CEDtreatment=\frac{∑_1^n((BOMmoorR_n×CEDBeneFactor_n+BOMfoundR_n×CEDBeneFactor_n + BOMdeviceR_n×CEDBeneFactor_n+BOMptoR_n×CEDBeneFactor_n+ BOMelecR_n×CEDBeneFactor_n )) }{TotProd}


.. toctree::
   :hidden:
   :maxdepth: 4
