.. _references:

*********************
References
*********************

Andrulewicz, E., Napierska, D., Otremba, Z., 2003. The environmental effects of the installation and functioning of the submarine SwePol Link HVDC transmission line: a case study of the Polish Marine Area of the Baltic Sea. J. Sea Res., Proceedings of the 22nd Conference of the Baltic Oceanographers (CBO), Stockholm 2001 49, 337–345. https://doi.org/10.1016/S1385-1101(03)00020-0

Baeye, M., Fettweis, M., 2015. In situ observations of suspended particulate matter plumes at an offshore wind farm, southern North Sea. Geo-Mar. Lett. 35, 247–255. https://doi.org/10.1007/s00367-015-0404-8

Bailey, H., Senior, B., Simmons, D., Rusin, J., Picken, G., Thompson, P.M., 2010. Assessing underwater noise levels during pile-driving at an offshore windfarm and its potential effects on marine mammals. Mar. Pollut. Bull. 60, 888–897. https://doi.org/10.1016/j.marpolbul.2010.01.003

Benjamins, S., Harnois, V., Smith, H., Johanning, L., Greenhill, L., Carter, C., Wilson, B., 2014. Understanding the potential for marine megafauna entanglement risk from marine renewable energy developments 95.
Bicknell, 2013. The impact of marine renewable energy on birds’.,”. Mar Renew Biodivers Fish Mar Inst 22–25

Cada, G., Ahlgrimm, J., Bahleda, M., Bigford, T., Stavrakas, S., Hall, D., Moursund, R., Sale, M., 2007. Potential Impacts of Hydrokinetic and Wave Energy Conversion Technologies on Aquatic Environments. Fisheries 32, 174–181. https://doi.org/10.1577/1548-8446(2007)32[174:PIOHAW]2.0.CO;2

Camhi, M., Fowler, S., Musick, J., 2004. Les requins et autres poissons cartilagineux 51.

Carlier, A., Vogel, C., Alemany, J., 2019a. Synthèse des connaissances sur les impacts des câbles électriques sous-marins : phases de travaux et d’exploitation - Etude du compartiment benthique et des ressources halieutiques. https://doi.org/10.13155/61975

Carter, C., 2007. OFFSHORE WIND FARMS AND MARINE MAMMALS: IMPACTS & METHODOLOGIES  FOR ASSESSING IMPACTS: Marine renewable energy devices: a collision risk for marine mammals?

Cassoff, R.M., Moore, K.M., McLellan, W.A., Barco, S.G., Rotstein, D.S., Moore, M.J., 2011. Lethal entanglement in baleen whales. Dis. Aquat. Organ. 96, 175–185. https://doi.org/10.3354/dao02385

Cefas, 2010. Strategic Review of Offshore Wind Farm Monitoring Data Associated with FEPA Licence ConditionsProject CodeProject ManagerDateStrategic Review of Offshore Wind Farm Monitoring Data Associated with FEPA Licence 
conditions [WWW Document]. URL https://tethys.pnnl.gov/sites/default/files/publications/Cefas-2010.pdf (accessed 4.8.21).

Clark, W.W., 1991. Recent studies of temporary threshold shift (TTS) and permanent threshold shift (PTS) in animals. J. Acoust. Soc. Am. 90, 155–163. https://doi.org/10.1121/1.401309

Cloern, J.E., 2001. Our evolving conceptual model of the coastal eutrophication problem. Mar. Ecol. Prog. Ser. 210, 223–253. https://doi.org/10.3354/meps210223

Cooper, K.M., Curtis, M., Wan Hussin, W.M.R., Barrio Froján, C.R.S., Defew, E.C., Nye, V., Paterson, D.M., 2011. Implications of dredging induced changes in sediment particle size composition for the structure and function of marine benthic macrofaunal communities. Mar. Pollut. Bull. 62, 2087–2094. https://doi.org/10.1016/j.marpolbul.2011.07.021

Copping, A.E., Hemery, L.G., 2020. OES-Environmental 2020 State of the Science Report (No. PNNL-29976). Pacific Northwest National Lab. (PNNL), Richland, WA (United States). https://doi.org/10.2172/1632878

Copping, A.E., Hemery, L.G., Overhus, D.M., Garavelli, L., Freeman, M.C., Whiting, J.M., Gorton, A.M., Farr, H.K., Rose, D.J., Tugade, L.G., 2020. Potential Environmental Effects of Marine Renewable Energy Development—The State of the Science. J. Mar. Sci. Eng. 8, 879. https://doi.org/10.3390/jmse8110879

Dafforn, K.A., Lewis, J.A., Johnston, E.L., 2011. Antifouling strategies: History and regulation, ecological impacts and mitigation. Mar. Pollut. Bull. 62, 453–465. https://doi.org/10.1016/j.marpolbul.2011.01.012

Dam Hieu, C., 2013. Impacts environnementaux des champs électromagnétiques générés par des installations EMR.

Defingou, M., Bils, F., Horchler, B., Liesenjohann, T., Nehls, G., 2019. A REVIEW OF SOLUTIONS TO AVOID AND MITIGATE ENVIRONMENTAL IMPACTS OF OFFSHORE WINDFARMS 269.

Dernie, K., Kaiser, M., Warwick, R., 2003. Recovery rates of benthic communities following physical disturbance. J. Anim. Ecol.

Dolman, S., Simmonds, M., 2010. Towards best environmental practice for cetacean conservation in developing Scotland’s marine renewable energy. Mar. Policy 34, 1021–1027. https://doi.org/10.1016/j.marpol.2010.02.009
Dolman, S., Williams-Grey, V., Asmutis-Silvia, R., Isaac, S., 2006. Vessel collisions and cetaceans: What happens when they don’t miss the boat.

DTOcean Project, 2014. Deliverable 2.2, “Pre-evaluation of array layouts, identified key parameters and their relevant ranges.

Exo, K.M., Hoppop, O., Garthe, S., 2003. Birds and offshore wind farms: a hot topic in marine ecology 4.

Frid, C., Andonegi, E., Depestele, J., Judd, A., Rihan, D., Rogers, S.I., Kenchington, E., 2012. The environmental interactions of tidal and wave energy generation devices. Environ. Impact Assess. Rev. 32, 133–139. https://doi.org/10.1016/j.eiar.2011.06.002

Furness, R.W., Wade, H.M., Robbins, A.M.C., Masden, E.A., 2012. Assessing the sensitivity of seabird populations to adverse effects from tidal stream turbines and wave energy devices. ICES J. Mar. Sci. 69, 1466–1479. https://doi.org/10.1093/icesjms/fss131

GHYDRO, 2013. Guide d’évaluation des impacts environnementaux pour les technologies hydroliennes en mer : GHYDRO, Guide to the environmental impact evaluation of tidal stream technologies at sea : GHYDRO.

Gill, A., Barlett, M., 2010. Literature review on the potential effects of electromagnetic fields and subsea noise from marine renewable energy developments on Atlantic  salmon, sea trout and European eel 43.

Gill, A., Gloyne-Philips, I., Neal, K., Kimber, J., 2005. The Potential effects of electromagnetic fields generated by sub-sea power cables associated with offshore wind farm developments on electrically and magnetically sensitive marine organisms - a review. Electromagn. Fields 128.

Grecian, W.J., Inger, R., Attrill, M.J., Bearhop, S., Godley, B.J., Witt, M.J., Votier, S.C., 2010. Potential impacts of wave‐powered marine renewable energy installations on marine birds [WWW Document]. URL https://onlinelibrary.wiley.com/doi/full/10.1111/j.1474-919X.2010.01048.x (accessed 4.6.21).

Hill, J., Marzialetti, S., Pearce, B., 2011. Recovery of Seabed Resources Following Marine Aggregate Extraction Science Monograph Series. https://doi.org/10.13140/RG.2.2.17345.40804

Huntington, H.P., Daniel, R., Hartsig, A., Harun, K., Heiman, M., Meehan, R., Noongwook, G., Pearson, L., Prior-Parks, M., Robards, M., Stetson, G., 2015. Vessels, risks, and rules: Planning for safe shipping in Bering Strait. Mar. Policy 51, 119–127. https://doi.org/10.1016/j.marpol.2014.07.027

Inger, R., Attrill, M.J., Bearhop, S., Broderick, A., Grecian, W.J., Hodgson, D.J., Mills, C., Sheehan, E., Votier, S.C., Witt, M.J., Godley, B.J., 2009. Marine renewable energy: potential benefits to biodiversity? An urgent call for research [WWW Document]. URL https://besjournals.onlinelibrary.wiley.com/doi/full/10.1111/j.1365-2664.2009.01697.x (accessed 4.6.21).

Jackson, G., Nedwell, J., Turnpenny, A., Lovell, J., Parvin, S., Workman, R., Spinks, J., Howell, D., 2007. A validation of the dBht as a measure of the behavioural and auditory effects of underwater noise.

James, S.C., Seetho, E., Jones, C., Roberts, J., 2010. Simulating environmental changes due to marine hydrokinetic energy installations, in: OCEANS 2010 MTS/IEEE SEATTLE. Presented at the 2010 OCEANS MTS/IEEE SEATTLE, IEEE, Seattle, WA, pp. 1–10. https://doi.org/10.1109/OCEANS.2010.5663854

Kadiri, M., Ahmadian, R., Bockelmann-Evans, B., Rauen, W., Falconer, R., 2012. A review of the potential water quality impacts of tidal renewable energy systems. Renew. Sustain. Energy Rev. 16, 329–341. https://doi.org/10.1016/j.rser.2011.07.160

Kaiser, M., Clarke, K., Hinz, H., Austen, M., Somerfield, P., Karakassis, I., 2006. Global analysis of response and recovery of benthic biota to fishing. Mar. Ecol. Prog. Ser. 311, 1–14. https://doi.org/10.3354/meps311001

Keenan, G., Sparling, C., Williams, H., Fortune, F., 2011. SeaGen Environmental Monitoring Programme - Final Report.

Ketten, D.R., 2004. Marine Mammal Auditory Systems: A Summary of Audiometric and Anatomical Data and Implications for Underwater Acoustic Impacts 14.

Kogan, I., Paull, C.K., Kuhnz, L.A., Burton, E.J., Von Thun, S., Gary Greene, H., Barry, J.P., 2006. ATOC/Pioneer Seamount cable after 8 years on the seafloor: Observations, environmental impact. Cont. Shelf Res. 26, 771–787. https://doi.org/10.1016/j.csr.2006.01.010

Koschinski, S., Lüdemann, K., 2013. Development-of-Noise-Mitigation-Measures-in-Offshore-Wind-Farm-Construction.pdf [WWW Document]. URL https://www.researchgate.net/profile/Sven-Koschinski/publication/308110557_Development_of_Noise_Mitigation_Measures_in_Offshore_Wind_Farm_Construction/links/57da3bea08ae5f03b49a1f0b/Development-of-Noise-Mitigation-Measures-in-Offshore-Wind-Farm-Construction.pdf (accessed 4.9.21).
Lambert, S.J., Thomas, K.V., Davy, A.J., 2006. Assessment of the risk posed by the antifouling booster biocides Irgarol 1051 and diuron to freshwater macrophytes. Chemosphere 63, 734–743. https://doi.org/10.1016/j.chemosphere.2005.08.023

Langhamer, O., 2012. Artificial Reef Effect in relation to Offshore Renewable Energy Conversion: State of the Art. Sci. World J. 2012, e386713. https://doi.org/10.1100/2012/386713

Langhamer, O., Wilhelmsson, D., Engström, J., 2009. Artificial reef effect and fouling impacts on offshore wave power foundations and buoys – a pilot study. Estuar. Coast. Shelf Sci. 82, 426–432. https://doi.org/10.1016/j.ecss.2009.02.009

Langton, R., Davies, I.M., Scott, B.E., 2011. Seabird conservation and tidal stream and wave power generation: Information needs for predicting and managing potential impacts. Mar. Policy 35, 623–630. https://doi.org/10.1016/j.marpol.2011.02.002

Lejart, M., Boeuf, M., De Roeck, Y.H., Monbet, P., Barillier, A., Loaec, J.-M., Terme, L., Giry, C., Delpech, J.-P., Carlier, A., Augris, C., Lazure, P., Simplet, L., Prevot, J., Auvray, C., Delafosse, C., Folegot, T., Martinez, L., Cadiou, B., Jalaber, J.-Y., Fortin, M., Alizier, S., Michel, S., Remaud, M., Sterckeman, A., Allo, J.-C., Bonnel, J., Caillet, A., Plassard, V., Debout, G., Galbert, D. de, Dubreuil, J., Fevrier, Y., Gervaise, C., Jourdain, J., Juge, R., Kervella, Y., Le Guennec, S., Lemiere, S., Sabourin, A., Piqueret, M., Prudhomme, J., Provost, P., Robigo, L., Thouzeau, G., 2013. Guide for the assessment of environmental impacts for offshore sea energy technologies: GHYDRO; Guide d’evaluation des impacts environnementaux pour les technologies hydroliennes en mer: GHYDRO.

Lindeboom, H.J., Kouwenhoven, H.J., Bergman, M.J.N., Bouma, S., Brasseur, S., Daan, R., Fijn, R.C., Haan, D. de, Dirksen, S., Hal, R. van, Lambers, R.H.R., Hofstede, R. ter, Krijgsveld, K.L., Leopold, M., Scheidat, M., 2011. Short-term ecological effects of an offshore wind farm in the Dutch coastal zone$\mathsemicolon$ a compilation. Environ. Res. Lett. 6, 035101. https://doi.org/10.1088/1748-9326/6/3/035101

Linley, E., Wilding, T., Black, K., Hawkins, A., Mangi, S., 2007. Review of the Reef Effects of Offshore Wind Farm Structures and their Potential for Enhancement and Mitigation [WWW Document]. URL https://tethys.pnnl.gov/publications/review-reef-effects-offshore-wind-farm-structures-their-potential-enhancement (accessed 4.8.21).

Ma, K.-T., 2013. Historical Review on Integrity Issues of Permanent Mooring Systems.

Maioli, P., 2015. Magnetic field computation of submarine cables. Prysmian Group.

Marques, A.T., Batalha, H., Rodrigues, S., Costa, H., Pereira, M.J.R., Fonseca, C., Mascarenhas, M., Bernardino, J., 2014. Understanding bird collisions at wind farms: An updated review on the causes and possible mitigation strategies. Biol. Conserv. 179, 40–52. https://doi.org/10.1016/j.biocon.2014.08.017

Merck, T., Wasserthal, R., 2009. Assessment of the environmental impacts of cables. OSPAR Biodivers. Ser. 437, 19.

Nedwell, J., Brooker, A., Barham, R., 2012. Assessment of underwater noise during the installation of export power cables at the Beatrice Offshore Wind Farm. Subacoustech Environmental Report.

Nelms, S., Duncan, E., Broderick, A., Galloway, T., Godfrey, M., Hamann, M., Lindeque, P., Godley, B., 2016. Plastic and marine turtles: a review and call for research | ICES Journal of Marine Science | Oxford Academic [WWW Document]. URL https://academic.oup.com/icesjms/article/73/2/165/2614204?login=true (accessed 4.9.21).

OSPAR, 2012. Lignes directrices sur la meilleure pratique environmentale (BEP) pour la pose et l’exploitation de câbles.

OSPAR, 2008. Background Document on potential problems associated with power cables other than those for oil and gas activities.

Pine, M.K., Schmitt, P., Culloch, R.M., Lieber, L., Kregting, L.T., 2019. Providing ecological context to anthropogenic subsea noise: Assessing listening space reductions of marine mammals from tidal energy devices. Renew. Sustain. Energy Rev. 103, 49–57. https://doi.org/10.1016/j.rser.2018.12.024

Polmear, R., Stark, J.S., Roberts, D., McMinn, A., 2015. The effects of oil pollution on Antarctic benthic diatom communities over 5years. Mar. Pollut. Bull. 90, 33–40. https://doi.org/10.1016/j.marpolbul.2014.11.035

Poornima, E.H., Rajadurai, M., Rao, T.S., Anupkumar, B., Rajamohan, R., Narasimhan, S.V., Rao, V.N.R., Venugopalan, V.P., 2005. Impact of thermal discharge from a tropical coastal power plant on phytoplankton. J. Therm. Biol. 30, 307–316. https://doi.org/10.1016/j.jtherbio.2005.01.004

Rainbow, P.S., 2007. Trace metal bioaccumulation: Models, metabolic availability and toxicity - ScienceDirect [WWW Document]. URL https://www.sciencedirect.com/science/article/pii/S0160412006000729?casa_token=JdzgfiaBQtkAAAAA:cecKNbYbtInkJsnrH6s4usIU2-b1V6YY14HzeboluPs8ZR0p2d3GFyZsVMlg6acEcJC-b1BSGw (accessed 4.6.21).

Reach, I., Cooper, W., Jones, D.L., Langman, R., 2014. A Review of Selected Marine Environmental Considerations Associated With Concrete Gravity Base Foundations in Offshore Wind Developments, in: From Sea to Shore ? Meeting the Challenges of the Sea, Conference Proceedings. ICE Publishing, pp. 326–335. https://doi.org/10.1680/fsts.59757.035

Reeves, R.R., McClellan, K., Werner, T.B., 2013. Marine mammal bycatch in gillnet and other entangling net fisheries, 1990 to 2011. Endanger. Species Res. 20, 71–97. https://doi.org/10.3354/esr00481

Robins, P.E., Neill, S.P., Lewis, M.J., 2014. Impact of tidal-stream arrays in relation to the natural variability of sedimentary processes. Renew. Energy 72, 311–321. https://doi.org/10.1016/j.renene.2014.07.037

Robinson, S., Lepper, P., 2013. Scoping Study: Review of Current Knowledge of Underwater Noise Emissions from Wave and Tidal Stream Energy Devices 75.

Ruso, Y. del P., Bayle-Sempere, J.T., 2006. Diel and vertical movements of preflexion fish larvae assemblage associated with Posidonia oceanica beds. Sci. Mar. 70, 399–406. https://doi.org/10.3989/scimar.2006.70n3399

Schwemmer, P., Mendel, B., Dierschke, V., Garthe, S., 2011. Effects of ship traffic on seabirds in offshore waters: implications for marine conservation and spatial planning [WWW Document]. Ecol. Appl. - Wiley Online Libr. URL https://esajournals.onlinelibrary.wiley.com/doi/full/10.1890/10-0615.1?casa_token=7qIUAPtU47kAAAAA%3Au7SIabyUW27GVyY3jnb2y-zBrfbNGbYmy4k-DJrMA-gj15IZkbIaLndZDKHKZRn3QhNT3mYTgVtAmXE (accessed 4.6.21).
Seret, B., 1990. Les requins : questions et réponses.

Shahidul Islam, Md., Tanaka, M., 2004. Impacts of pollution on coastal and marine ecosystems including coastal and marine fisheries and approach for management: a review and synthesis. Mar. Pollut. Bull. 48, 624–649. https://doi.org/10.1016/j.marpolbul.2003.12.004

Sheehy, D.J., Vik, S.F., 2010. The role of constructed reefs in non-indigenous species introductions and range expansions. Ecol. Eng. 36, 1–11. https://doi.org/10.1016/j.ecoleng.2009.09.012

Shields, M.A., Payne, A.I.L. (Eds.), 2014. Marine Renewable Energy Technology and Environmental Interactions, Humanity and the Sea. Springer Netherlands, Dordrecht. https://doi.org/10.1007/978-94-017-8002-5

Shields, M.A., Woolf, D.K., Grist, E.P.M., Kerr, S.A., Jackson, A.C., Harris, R.E., Bell, M.C., Beharie, R., Want, A., Osalusi, E., Gibb, S.W., Side, J., 2011. Marine renewable energy: The ecological implications of altering the hydrodynamics of the marine environment. Ocean Coast. Manag. 54, 2–9. https://doi.org/10.1016/j.ocecoaman.2010.10.036

Slabbekoorn, H., Bouton, N., van Opzeeland, I., Coers, A., ten Cate, C., Popper, A.N., 2010. A noisy spring: the impact of globally rising underwater sound levels on fish. Trends Ecol. Evol. 25, 419–427. https://doi.org/10.1016/j.tree.2010.04.005

Southall, B.L., Finneran, J.J., Reichmuth, C., Nachtigall, P.E., Ketten, D.R., Bowles, A.E., Ellison, W.T., Nowacek, D.P., Tyack, P.L., 2019. Marine Mammal Noise Exposure Criteria: Updated Scientific Recommendations for Residual Hearing Effects. Aquat. Mamm. 45, 125–232. https://doi.org/10.1578/AM.45.2.2019.125

Sparling, C., Grellier, K., Philpott, E., Macleod, K., Wilson, J., 2011. Guidance on survey and monitoring in relation to marine renewables deployments in Scotland - Vol 3 Seals.

Sparling, C.E., Booth, C.G., Hastie, G.D., Gillespie, D., MacAulay, J., 2014. MARINE MAMMALS AND TIDAL TURBINES: UNDERSTANDING TRUE COLLISION RISK 3.

Strod, T., Arad, Z., Izhaki, I., Katzir, G., 2004. Cormorants keep their power: visual resolution in a pursuit-diving bird under amphibious and turbid conditions: Current Biology [WWW Document]. URL https://www.cell.com/current-biology/fulltext/S0960-9822(04)00331-8 (accessed 4.8.21).

Taormina, B., 2019. Impacts potentiels des câbles électriques sous-marins des projets d’énergies marines renouvelables sur les écosystèmes benthiques, Potential impacts of submarine power cables from marine renewable energy projects on benthic communities. Unversité de Bretagne Occidentale.

Taormina, B., Bald, J., Want, A., Thouzeau, G., Lejart, M., Desroy, N., Carlier, A., 2018. A review of potential impacts of submarine power cables on the marine environment: Knowledge gaps, recommendations and future directions. Renew. Sustain. Energy Rev. 96, 380–391. https://doi.org/10.1016/j.rser.2018.07.026

Thanner, S., McIntosh, T., Blair, S., 2006. Development of benthic and fish assemblages on artificial reef materials compared to adjacent natural reef assemblages in Miami-Dade County, Florida. Bull. Mar. Sci. 78, 57–70.
Thomsen, F., Lüdemann, K., Kafemann, R., Piper, W., 2006. Effects of offshore wind farm noise on marine mammals and fish. COWRIE Ltd, Hamburg, Germany.

Thums, M., Whiting, S., Reisser, J., Pendoley, K., Pattiaratchia, C., Proietti, M., Hetzel, Y., Fisher, R., Meekan, M., 2016. Artificial light on water attracts turtle hatchlings during their near shore transit | Royal Society Open Science [WWW Document]. URL https://royalsocietypublishing.org/doi/full/10.1098/rsos.160142 (accessed 4.9.21).

Tillin, H., Hiddink, J., Jennings, S., Kaiser, M., 2006. Chronic bottom trawling alters the functional composition of benthic invertebrate communities on a sea-basin scale. Mar. Ecol. Prog. Ser. 318, 31–45. https://doi.org/10.3354/meps318031

Todd, V.L.G., Todd, I.B., Gardiner, J.C., Morrin, E.C.N., MacPherson, N.A., DiMarzio, N.A., Thomsen, F., 2015. A review of impacts of marine dredging activities on marine mammals. ICES J. Mar. Sci. 72, 328–340. https://doi.org/10.1093/icesjms/fsu187

Uihlein, A., Magagna, D., 2016. Wave and tidal current energy – A review of the current state of research beyond technology. Renew. Sustain. Energy Rev. 58, 1070–1081. https://doi.org/10.1016/j.rser.2015.12.284
US Department of Energy, 2009. Report to Congress on the potential environmental effects of marine and hydrokinetic energy technologies.

Van de Eynde, D., Brabant, R., Fettweis, M., Francken, F., Van Lancker, V., 2010. Monitoring of hydrodynamic and morphological changes at the C‐Power and the Belwind offshore windfarm sites – A synthesis. In: Offshore windfarms in the Belgian part of the North Sea. Early environmental impact assessment and spatio‐temproal variability.

Van der Hoop, J.M., Moore, M.J., Barco, S.G., Cole, T.V.N., Daoust, P.-Y., Henry, A.G., Mcalpine, D.F., Mclellan, W.A., Wimmer, T., Solow, A.R., 2013. Assessment of Management to Mitigate Anthropogenic Effects on Large Whales. Conserv. Biol. 27, 121–133. https://doi.org/10.1111/j.1523-1739.2012.01934.x

Wellsbury, P., Goodman, K., Barth, T., Cragg, B.A., Barnes, S.P., Parkes, R.J., 1997. Deep marine biosphere fuelled by increasing organic matter availability during burial and heating. Nature 388, 573–576. https://doi.org/10.1038/41544

Whitehouse, R.J.S., Harris, J.M., Sutherland, J., Rees, J., 2011. The nature of scour development and scour protection at offshore windfarm foundations. Mar. Pollut. Bull. 62, 73–88. https://doi.org/10.1016/j.marpolbul.2010.09.007

Widdows, J., Brinsley, M., 2002. Impact of biotic and abiotic processes on sediment dynamics and the consequences to the structure and functioning of the intertidal zone. J. Sea Res., Structuring Factors of Shallow Marine Coastal Communities, Part I 48, 143–156. https://doi.org/10.1016/S1385-1101(02)00148-X

Wilhelmsson, D. (Ed.), 2010. Greening blue energy: identifying and managing the biodiversity risks and opportunities of offshore renewable energy. IUCN, Gland, Switzerland.

Wilson, B., 2006. STRATEGIC ENVIRONMENTAL ASSESSMENT OF MARINE RENEWABLE ENGERY DEVELOPMENT IN SCOTLAND. Rep. Scott. Exec. 105.


.. toctree::
   :hidden:
   :maxdepth: 4
