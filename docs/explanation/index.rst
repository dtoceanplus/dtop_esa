.. _esa-explanation:

*********************
Background and theory
*********************

This section describes the background and theory behind the module. 
It provides usefull information to fully understand scientific concepts behind the module. To this end, background of environmental and social impact assessment is detailed to better apprehend the link between the different ocean energies and potential associated risks and opportunities. This note describe the process from general concept to structuring functions in ESA and details the implementation in the tool. 

Background of environmental and social impact assessment of ocean energies
==========================================================================

#. :ref:`ocean-energy_risks_opportunities`
#. :ref:`general-concept_to_structuring_functions`

 - :ref:`endangered-species`
 - :ref:`environmental-impact_assessment`
 - :ref:`life_cycle_assessment`
 - :ref:`social-acceptance`

#. :ref:`references`

.. toctree::
   :maxdepth: 1
   :hidden:

   ocean_energy_risks_opportunities
   general_concept_to_structuring_functions
   references

ESA module Calculation methods
==============================

#. :ref:`es_dto`
#. :ref:`eia_dto`
#. :ref:`cfp_dto`
#. :ref:`sa_dto`

.. toctree::
   :maxdepth: 1
   :hidden:

   es_dto
   eia_dto
   cfp_dto
   sa_dto

