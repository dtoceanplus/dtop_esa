.. _life_cycle_assessment:

Life Cycle Assessment 
=====================

The environmental impact of human activities can take different generic forms such as (i) chronic, local or global pollution; (ii) accidental pollution or (iii) depletion of natural resources. Faced with these impacts, the historical approach (regulatory or incentive) which has consisted of working at the scale of an industrial site or a process has shown its limits. An industrial site can indeed present very limited environmental impacts, while providing products which, through their use or end-life treatment, will have significant environmental impacts. 

Life cycle assessment (LCA) provides an answer to this problem. Indeed, LCA is a tool for assessing the environmental impact of a given product or human industrial activity by considering all the stages of its life cycle, from manufacturing up to its potential recycling. At each of these stages, pollutant emissions and resource consumption are quantified 

.. image:: ../figures/life_cycle_ocean_energy.png 

LCA can be applied to simple consumer goods as well as to complex industrial installations or equipement. LCA application can have several purposes that can be illustrated with the example of electric power generation systems for instance. On one hand, electric power generation systems can be assessed by LCA, and their environmental impacts can be compared to other electric power generators. On the other hand, the LCA measures can also allow a comparision between different systems of ocean energy devices in order to make the designs converge towards the best, least impacting solution. Together with the assessment of technical and economic efficiency, it is therefore possible to access a form of environmental efficiency by using LCA measures.
Performing an LCA is structured by two main standards which are ISO 14040 and ISO 14044. It has four steps as shown in the figure below and which are detailed in the following sections:

.. contents::

.. image:: ../figures/step_ISO.png 

Objective and scope
--------------------

Whatever the motivation for the study, LCA is always a comparison: 

 - comparison between the different stages of the life cycle of the same system in order to identify pollution transfers, 
 - or comparison of the life cycles of several competing systems in order to identify the one with the lowest impacts. 

This comparison is based on a functional unit (FU). For example, in the case where energy production systems are compared, the commonly accepted functional unit is the kWh of energy produced by the system and supplied to the grid.
The scope of the study determines the depth of the investigation. Indeed, all the processes involved in the life stages of a system cannot reasonably be taken into account. For example, consider two different packages of the same food product, one in cardboard, the other in plastic. The manufacturing phase of the packaging takes place in both cases in comparable industrial buildings. It is therefore not useful to consider the construction, maintenance and heating processes of these buildings when carrying out the LCA of the packaging. Here we can see the notion of prioritization of processes which will be necessary to limit the scope of the study. Foreground processes (specific to the considered function) are distinguished from background processes. Different methods are then used to decide whether or not to keep a process within the scope of the study. These methods are based on quantitative criteria (cut-off criteria expressed as a percentage of mass or energy) or qualitative (recognized absence or low environmental impact of certain processes).

Life cycle inventory
--------------------

Life cycle inventory is the Bills of Materials (BoM): the flows entering and leaving the system are characterized (in kind and in quantity) for each stage of the life cycle.
The nature of the flows varies considerably (Figure xx3): for example, the extraction of sand, aluminium, copper or gravel can be considered. For processing, transport, installation and operations and maintenance, electricity use or diesel use can be considered too. For each material used or for each process, a green house gas emission (e.G. CO2 or CH4) is associated.

.. image:: ../figures/inventory.png 

Life cycle inventory extract, main consumption associated with an offshore substation (Elginoz and Bas, 2017)

This variety of flows make it practical to use a vector representation i.e. using a column vector where each row is assigned to a substance. The flows are expressed there by masses or volumes, related or not to the functional unit (i.e. KWh); this quantification can come from a calculation, a measurement or a literature search.
Finally, it can be noted that recycling processes can provide "credits" due to the savings in raw materials that they generate. These credits play a very significant role in the inventory when materials with a high recycling rate are massively used in the system studied. This is particularly the case when considering the use of steel, aluminium or copper for instance that 90% of it can be recycled (Elginoz and Bas, 2017).

Life cycle impact assessment
----------------------------

At the end of the inventory phase, a table containing sometimes several hundred elementary flows is available. The purpose of the life cycle impact assessment is to translate these flows into potential environmental impacts, which are intermediate impact category called midpoint categories (Figure xx4). These midpoint categories, by aggregating the effects of different substances, aim to quantify changes in the target system without actually assessing the final damage caused by these changes. The damage is expressed by the endpoint categories which relate to three different categories, i.e. human health (e.g. morbidity and mortality), impact on the natural environment (e.g. loss of biodiversity) and natural resources (i.e. depletion of resources for future generations).

.. image:: ../figures/ILCD.png 

Framework of the ILCD characterization linking elementary flows from the inventory results to indicator results at midpoint level and endpoint level for 15 midpoint impact categories and 3 areas of protection (Figure from Hauschild et al. 2013)

The midpoint categories are translated into different midpoint indicators that are calculated as a final result from an LCA (Figure xx5). The Global Warming Potential (GWP) is for instance one of these midpoint indicators which is well known as it reflects the contribution of different gases to the greenhouse effect that induces global warming of the planet.

.. image:: ../figures/assessment.png 

Life cycle impact assessment: impact categories and associated midpoint indicators (Table adapted from Uihlein 2016

The first step in LCA assessment will be thus to aggregate the elementary flows having a common impact. Example: certain gases are known to contribute to the greenhouse effect. This is the case for CO2, CH4, N2O, freons etc. All these gases will therefore participate in determining a common environmental indicator of global warming. They will participate in it in proportion to their individual global warming power, which are called the impact factors.

Example of a Global Warming Potential (GWP) impact assessment using different quantities of elementary flows (i.e. different gases release) and their associated impact factor. FU:  functional unit (e.g. KWh)

.. image:: ../figures/gases.png 

This table illustrates this principle. The second column shows the elementary flows (i.e. quantities emitted) of gas emitted by a given process. The third column shows the impact factor associated with these gases and the fourth column being the product of the previous two, and it allows the calculation of the full indicator (in this example, the GWP).
Finally, the determination of midpoint indicators calls for fewer and less complex mechanisms than those involved in the calculation of endpoint indicators. Midpoint indicators are therefore scientifically more robust and commonly used for life cycle impact assessments.

Life cycle interpretation
-------------------------

The interpretation of LCA must generate clear information that can be used for decision support. It seeks to: 

- Identify the sensitive points of the life cycle (those on which an intervention will allow an appreciable reduction of the environmental impacts) 
- Evaluate the quality and robustness of the results obtained. 

The identification of sensitive points is first of all based on multiple and methodical comparisons: for a given impact, it is a question of comparing the contributions of each stage of the life cycle, those of different components, those of different pollutants or substances extracted etc. 
The sensitivity analysis then complements this information: variations are applied to certain input data and their effects on flows and impacts are observed. In the case of LCA of MRE systems, the parameters generally considered for the sensitivity analysis are for example: lifetime, nominal power, load factor, distance from the coast, recycling rate of metallic materials. To assess the robustness of the results, the output data from the sensitivity analysis will again be used and supplemented by an analysis of uncertainties and their propagation. It should be noted that the validation and communication of results are the subject of certain requirements in ISO 14044. When the results of an LCA are to be used in a public way, in particular to support a “comparative claim”, it is necessary that this LCA is validated by an appropriate committee as part of a “critical review”.

LCA limits
----------

Before undertaking an LCA process, the main limitations of the tool must be known. The first limit concerns the scope of use of the tool: LCA is not suitable for determining the local impacts of a system. For a local and precise assessment of the impacts of a phase of the life cycle, an impact study will be used. Moreover, the most emblematic indicators of the LCA approach are indicators operating on a regional or global scale (increase in the greenhouse effect, depletion of resources, etc.). 

Another important limitation is that related to the quality and availability of inventory data. The quality of a data is assessed according to several criteria: its precision, its age, its method of acquisition, its geographical validity etc. Poor quality data will not lead to a reliable LCA. As for the availability of data, it may be limited by the confidentiality that applies to it, by the absence of work or measurement carried out on the subject in question, or by an excessively strong aggregation of existing data which does not allow to get access to sufficiently detailed information. 

Finally, from a methodological point of view, the interest in quantifying the impacts is accompanied by an imperfection in the indicators used. Midpoint indicators are relatively robust, but often difficult to work with collectively, while endpoint indicators gain in conciseness what they lose in reliability. 

In summary, we can say that LCA is a wide-mesh environmental assessment tool, which does not take into account real local impacts. However, it has the indisputable advantage of addressing the entire product life cycle and seeking to produce quantified impacts. It will therefore be particularly suited to technical arbitration during the upstream phases of system design, and use a posteriori, when it comes to knowing whether existing systems have significantly different environmental performance.



See how Life Cycle Assessment is considered in DTOceanPLUS in :ref:`cfp_dto`.
