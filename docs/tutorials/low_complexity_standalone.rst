.. _low-complexity-standalone:

Using ESA at low complexity in standalone mode for RM3 wave example
====================================================================================================

To get information on the  potential presence of endangered species and get recommendations on mitigation measures to lower the main risks associated with the implementation. Use the low complexity (level 1) version of the Environmental and Social Acceptance module. This assumes the user has information only on the coordinates of implementation. 

For this tutorial, the RM3 wave example array of 10 devices is used.

1. Create/open study
---------------------

#. If required, create a new complexity level 1 study, as described in :ref:`new-study-standalone` 

#.  From the list of ESA studies, click ``Modify inputs`` to start 
    working on the complexity level 1 study.

2. Enter inputs
----------------

#.  The inputs at low complexity are grouped into one section: 
    Farm info, there are no Device, Electrical or Logistics inputs.

#. Fill the *Farm general info* section:

   - Enter the *longitude (Decimal degre)* as ``40.77``
   - Enter the *latitude (Decimal degre)* as ``-124.26``

#. Fill the *protected species* section:

   - Click ``Add species``
   - Enter ``Tursiops truncatus`` in the *Name* column
   - Select ``Mammals`` in the *Class* column
   - Click ``Next page``

#. If successful, **inputs summary** page will appear and inform you if farm info is complete
   
   - if not, go back to fill the **Farm info** input page


3. Run the assessment and view results
--------------------------------------

#. Click ``Run module``
#. If the run calculation is successful, results page of Endangered species will appear and detail by the five classes of species:

   - Taxonomic information
   - Main associated risks
   - Recommendations on mitigation measures and surveys


