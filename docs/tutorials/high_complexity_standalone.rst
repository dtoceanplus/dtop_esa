.. _high-complexity-standalone:

Using ESA at medium/high complexity in standalone mode
======================================================

To perform a more detailed assessment of a project environmental impacts use the full complexity (level 2 or 3) version of the Environmental and Social Acceptance module. Difference between level 2 and  3,  is  the  measurements of environmental parameters  before and after implementation of the farm. If  the  user has no information or if  the project is  not implemented yet, it is suggested to use complexity level 2.

For this tutorial, the RM1 tidal example array of 10 devices is used, 
As this does not include environmental parameters, complexity 2 will be used.

1. Create/open study
---------------------

#. If required, create a new complexity level 2 study, as described in :ref:`new-study-standalone` 

#.  From the list of ESA studies, click ``Modify inputs`` to start 
    working on the complexity level 2 study.

2. Enter inputs
----------------

#.  The inputs at low complexity are grouped into four sections: 
    Farm, Device, Electrical and Logistics info.

#. Enter *Farm info* inputs:

	  a. In the ``farm general info`` section:

	    - Enter the *longitude (Decimal degre)* as ``47.28``
   	    - Enter the *latitude (Decimal degre)* as ``-122.55``
	    - Enter the *project lifetime (years)* as ``20``,
	    - Enter the *Levelized cost of Energy (€/kWh)* as ``56.0``

	  b. In the ``Area description`` section:

	    - Select *Zone type* as ``Sea loch entrance``,
	    - Enter *Water depth (m)* as ``40``,
	    - Enter *Current main direction (degree)* as ``45``,
	    - Select *Soil type* as ``Medium dense sand``

	  c. In the ``Initial state`` section:

	    - Enter *Turbidity (mg/L)* as ``10.0``
	    - Enter *Noise (dB re 1 u Pa)* as ``124.0``
	    - Enter *Electrical field (V/m)* as``0.0000001``
	    - Enter *Magnetic field (uT)* as ``0.005``
	    - Enter *Temperature (°C)* as ``14``

	  d. Select the *Fishing regulation* applied in the farm as ``Complete prohibition``

	  e. In the *protected species* section:
	 	
	    - Click ``Add species``
	    - Enter ``Tursiops truncatus`` in the *Name* column
	    - Select ``Mammals`` in the *Class* column

	  f. In the *Receptors* section:

	    - Select ``hard substrate benthic habitat``, ``Medium diving birds``, ``Odontoncete dolphinds``, ``Magnetosensitive species``

	  g. Click ``Next page``

#. Enter *device info* inputs:

	  a. In the ``device general`` info section:

	    - Select the *Type of device* as ``Tidal``, 
	    - Precise if the device(s) are floating or not,
	    - Enter the *Number of devices* as ``10``,
	    - Enter device locations (UTM)

	  b. In ``Device dimensions`` section:

	    - Enter *Height (m)* as ``40``,
	    - Enter *Width (m)* as ``30``, 
	    - Enter *Length (m)* as ``30``,
	    - Enter *Wet area surface (m²)* as ``3048``
	    - Enter *dry area surface (m²)* as ``561``

	  c. In the ``Resources`` section:

	    - Enter *Resource reduction (%)* as ``0.97``,
	    - Select *Unalloyed steel* in the *Used materials for the project* and precise *Quantity of used materials for the project* as ``503600.0`` and *Quantity of used materials to recycle* as ``503600.0``

	  d. In the ``Environmental measurements`` section (*Only for complexity 3*):

	    - Enter measured noise of the device (dB re 1 u Pa)
	    - Enter measured turbidity due to device installation (mg/L)

	  e. In the ``Fishing restriction`` section, enter *Total surface of fishing restriction around devices (m²)* as ``282743.0``

	  
	  f. In ``Foundation`` section:

	    - Select *Unalloyed steel* in the *Used materials for the project* and precise *Quantity of used materials for the project* as ``1497953.2`` and *Quantity of used materials to recycle* as ``1497953.2``
	    - Enter *Footprint* surface of foundation as ``322.7``
	    - Enter *Colonisable surface* of foundation as ``4688``
	    - Enter *Measured noise* of foundation as ``129.0``

	  g. Click ``Next page``

#. Enter *Electrical info* inputs:

	  a. In the ``electrical general info``:

	    - Enter *Annual energy produced (kWh)* as ``300000000``
	    - Enter *Colonisable surface of electrical components (m²)* as ``0``
	    - Enter *Footprint of electrical installation (m²)* as ``0``

	  b. In the ``Installation info`` section:

	    - Precise the presence of *Collection point* and *Substation* as ``No``
	    - Precise if cables are buried as ``Yes``

	  c. In the ``Fishing restriction`` section

	    - Enter *Total surface of fishing restriction around cables (m²)* as ``0``

	  d. In the ``Resources`` section:

	    -  Select *Copper* in the *Used materials for the project* and precise *Quantity of used materials for the project* as ``102000`` and *Quantity of used materials to recycle* as ``0``

	  e. In the ``Environmental measurements`` section (*Only for complexity 3*):

	    - Enter *measured noise of the device (dB re 1 u Pa)* as ``126``
	    - Enter *measured electrical field (V/m)* as``0.08``
	    - Enter *measured magnetic field (uT)* as ``0.038``
	    - Enter *measured temperature around cables (°C)* as ``14.2``

	  f. Click ``Next page``

#. Enter *Logistic info* inputs:

	  a. In the ``Installation info``:

	    - Enter *Number of vessels* as ``3``
	    - Enter *Mean size of vessels (mean lao) (m)* as ``100.0``
	    - Enter *Number of passengers on boats* as ``43``
	    - Enter *Measured noise of the vessels (dB re 1 u Pa)* as 150
	    - Enter *Measured turbidity due to marine operations (mg/L)* as 20.0 
	    - Select *Type of chemical pollutant* if any during marine operation as ``None``
	    - Enter total *Fuel consumption during the phase (kg)* as ``500000.0``

	  b. In the ``Exploitation info``:

	    - Enter *Number of vessels* as ``2``
	    - Enter *Mean size of vessels (mean lao) (m)* as ``35.0``
	    - Enter *Number of passengers on boats* as ``20``
	    - Enter *Measured noise of the vessels (dB re 1 u Pa)* as ``126``
	    - Enter *Measured turbidity due to marine operations (mg/L)* as ``10.0``
	    - Select *Type of chemical pollutant* if any during marine operation as ``None``
	    - Enter total *Fuel consumption during the phase (kg)* as ``1000000.0``

	  c. In the ``Decommissioning info``:

	    - Enter *Number of vessels* as ``3``
	    - Enter *Mean size of vessels (mean lao) (m)* as ``100.0``
	    - Enter *Number of passengers on boats* as ``26``
	    - Enter *Measured noise of the vessels (dB re 1 u Pa)* as ``126``
	    - Enter *Measured turbidity due to marine operations (mg/L)* as ``13.0``
	    - Select *Type of chemical pollutant* if any during marine operation as ``None``
	    - Enter total *Fuel consumption during the phase (kg)* as ``200000.0``

	  d. Click ``Next page``

3. Run the assessment and view results
--------------------------------------

#. Click ``Run module``
#. Click on the ``Endangered species`` section to visualize the taxonomics information of potentialy present endangered species and recommandations on surveys and mitigation measure

	    - Click on ``Mammals`` section 
	    - Click on Tursiops truncatus to visualize taxonomic information and recommandations for this species
	    
	    .. image:: ../figures/tursiops.png

	    .. image:: ../figures/tursiops_recom.png


#. Click on the ``Environmental Impact Assessment`` to visualize impact results in terms of pressure/receptors:
	    
	    - Visualize *Global results* of the project in the ``global environmental impact assessment`` section

	    .. image:: ../figures/eia_global.png


	    - Visualize results at the *technology group* level in the ``Assessement of the technology group`` section
	    - Click on ``Detailed results`` to have access to the results of each pressure associated with the technology group and recommandation to enhance the score
	    - Visualize results at *Pressure level* in the ``Assessment of the pressure`` section

	    .. image:: ../figures/eia_pressure.png
	   	    :height: 3in


#. Click on the ``Carbon Footprint`` to visualize the results of the life cycle assessment

	    - Click on ``Compare to other technologies/energy sources`` and choose between *other energy sources* or *other technologies* and select the indicator you want to compare
	    - Click on ``See references`` to have access to the different LCA
	    

	    .. image:: ../figures/cfp_tech.png


	    - In the *Results per phase* section, Click on ``Compare to other DTOceanPLUS project LCA``


	    .. image:: ../figures/cfp_gwp.png
	   	    :height: 3in

#. Click on the ``Social Acceptance`` to visualize relevant information for the social acceptance of the project


