.. _esa-tutorials:

*********
Tutorials
*********

Below is a set of tutorials that guide the user through each of the main functionalities of the Environmental and Social Acceptance tool.
They are intended for those who are new to the tool.

.. It is recommended to follow the tutorials in the suggested order listed below, as certain tutorials are dependent on others. 

**Using the Environmental and Social Acceptance tool in integrated mode**

These show how to use Environmental and Social Acceptance in integrated mode along with the other
DTOceanPlus tools. Most of the input parameters come from other modules. 

#. :ref:`Using ESA at low complexity for RM3 wave example <low-complexity-tutorial>`
#. :ref:`Using ESA at medium complexity for RM1 tidal example <full-complexity-tutorial>`


**Using the Environmental and Social Acceptance tool in standalone mode**

These show how to use Environmental and Social Acceptance in standalone mode where the user needs to enter
all input parameters for the design process. 

#. :ref:`Creating a new standalone Environmental and Social Acceptance study <new-study-standalone>`
#. :ref:`Using ESA at low complexity standalone for RM3 wave example <low-complexity-standalone>`
#. :ref:`Using ESA at medium complexity standalone for RM1 tidal example <high-complexity-standalone>`



.. toctree::
   :maxdepth: 1
   :hidden:

   low_complexity_tutorial
   full_complexity_tutorial 
   new_study_standalone
   low_complexity_standalone
   high_complexity_standalone



