.. _new-study-standalone:

Creating a new Environmental and Social Acceptance study in standalone mode
==============================================================================

Once  logged  into  the  server, the next  step is  to create a  new  study 
within  the  Environmental and Social Acceptance module. 

.. tip::
   Since multiple users across multiple  organisations may be 
   simultaneously accessing the tool on the server,
   **please add your organisation’s name in the name of the study you
   create**. This is to ensure that all users work on independent studies
   and are not editing the same study at the same time.

#. In the home page, select ``Environmental and Social Acceptance Studies`` and click ``Create new project``

#. Fill in an appropriate name and description to identify your study

#. Choose ``Standalone`` running mode, then select the appropriate complexity level (Click on the ``help`` button for more guidance on complexity level)

#. Click ``confirm`` to enter list of inputs required in the chosen complexity level

#. From any page of inputs, click on "save" or “save as” to name and save the project

.. note::
   This tutorial will be updated once studies are centrally
   managed, but this reflects the current version of the tool.


.. toctree::
   :maxdepth: 10
   :hidden:
   

