.. _how-to-choose-complexity:

How to decide between complexity levels in Environmental and Social Acceptance
==============================================================================

ESA tool  has  different  complexity  levels  that  reflect  the  level  of  information  needed  for  the assessment. 
The  ESA assessments  evolves  according  to  the  level  of  complexity  in  relation  to  the  level  of information that is available from other modules or the user as shown in the table below. This is not a different process of data but an addition of functionalities depending on the stage of development the user is in.


**Low complexity level**

The  early  complexity  level refers  to  early  stages  of  conception  of  an  ocean  energy  project.  This  level  includes  the  concept creation and concept development stages, i.e.the project is still in the early immature concept phase.
The  level  of  information  is  not  enough  developed  in  the  various DTOceanPlus  modules  to  be  able  to  achieve   a  full   ESA  assessment.  

At  this  stage,  only  the “Endangered species” main function can be run as this function requires only the site location in order to run and to inform about the presence or not of endangered species. 

**Mid complexity level**

The mid complexity level refers to stages where the ocean energy project is at the design optimisation and feasibility stage or even at the manufacturing and operability demonstration in representative environment stage, i.e. the project is at mid-mature testing phase.

At this complexity level, the four main functions (i.e. Endangered species; Environmental impacts; Carbon  FootPrint  and  Social  Acceptance)  can  be  partially  run  to  produce  an  incomplete  ESA assessment. 

**High complexity level**

The late complexity level refers to commercial array demonstration scale. The project is considered completely mature at this stage.

At this complexity level, the  ESA assessment is complete and includes all  functions developed under the four main functions.

.. image:: ../figures/cmp.png

.. toctree::
   :maxdepth: 1
   :hidden: