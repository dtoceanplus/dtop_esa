.. _how-to-full-complexity:

How to use Environmental and Social Acceptance at full complexity
=================================================================

To perform a more detailed assessment of a project environmental impacts use the full complexity (level 2 or 3) version of the Environmental and Social Acceptance module. Difference between level 2 and  3,  is  the  measurements of environmental parameters  before and after implementation of the farm. If  the  user has no information or if the project is  not implemented yet, it is suggested to use complexity level 2.

1. Enter inputs
----------------

The inputs at low complexity are grouped into four sections: 
Farm, Device, Electrical and Logistics info.

In integrated mode, most of the required parameters come from other tools, as shown in the middle column in the tables below. In standalone mode the user must enter all parameters, as shown in the right column. Where the source is the same in both modes, this is indicated by a merged cell.

**Farm info :**

+--------------------------------+------------------------+-----------------------------+
| Input parameter                | Integrated mode source | Standalone mode             |
+================================+========================+=============================+
| Coordinates of the farm        | SC tool                | Enter value [required]      |
|                                |                        |                             |
| (Decimal degre)                |                        |                             |
|                                |                        |                             |
+--------------------------------+------------------------+-----------------------------+
| Project life time (years)      | LMO tool               | Enter value [Required]      |
+--------------------------------+------------------------+-----------------------------+
| Levelized cost of energy       | Enter value [Required]                               |
|                                |                                                      |
| (€/kWh)                        |                                                      |
+--------------------------------+------------------------------------------------------+
| Zone type                      | Select zone type [Required]                          |
+--------------------------------+------------------------+-----------------------------+
| Water depth (m)                | SC tool                | Enter value [Required]      |
+--------------------------------+------------------------+-----------------------------+
| Current main direction         | SC tool                | Enter value [Required]      |
|                                |                        |                             |
| (degree)                       |                        |                             |
+--------------------------------+------------------------+-----------------------------+
| Soil type                      | SC tool                | Select from list [Required] |
+--------------------------------+------------------------+-----------------------------+
| Initial Turbidity              | Enter value [Required]                               |
| (mg/L)                         |                                                      |
+--------------------------------+------------------------------------------------------+
| Initial noise (dB re 1 u Pa)   | Enter value [Required]                               |
+--------------------------------+------------------------------------------------------+
| Initial electrical field (V/m) | Enter value [Required]                               |
+--------------------------------+------------------------------------------------------+
| Initial magnetic field (uT)    | Enter value [Required]                               |
+--------------------------------+------------------------------------------------------+
| Initial temperature (°C)       | Enter value [Required]                               |
+--------------------------------+------------------------------------------------------+
| Fishing regulation             | Select from list [Required]                          |
+--------------------------------+------------------------------------------------------+
| Name of protected species      | Enter latin name and Class of the species [Optional] |
|                                |                                                      |
| identified in the area         |                                                      |
+--------------------------------+------------------------------------------------------+
| Receptors                      | Select all receptors that have been identified       |
|                                |                                                      |
|                                | in the area of implementation                        |
+--------------------------------+------------------------------------------------------+
| Seasonal presence of Receptors | Select the months of presence of receptors           |
+--------------------------------+------------------------------------------------------+

**Device info :**


+-------------------------------------+------------------------+-------------------------+
| Input parameter                     | Integrated mode source | Standalone mode         |
+=====================================+========================+=========================+
| Device type                         | EC tool                | Select value [required] |
+-------------------------------------+------------------------+-------------------------+
| Floating                            | EC tool                | Select value [Required] |
+-------------------------------------+------------------------+-------------------------+
| Number of devices                   | EC tool                | Enter value [Required]  |
+-------------------------------------+------------------------+-------------------------+
| Device location (UTM)               | EC tool                | Enter value [Required]  |
+-------------------------------------+------------------------+-------------------------+
| Device height (m)                   | MC tool                | Enter value [Required]  |
+-------------------------------------+------------------------+-------------------------+
| Device width (m)                    | MC tool                | Enter value [Required]  |
+-------------------------------------+------------------------+-------------------------+
| Device length (m)                   | MC tool                | Enter value [Required]  |
+-------------------------------------+------------------------+-------------------------+
| Device wet area surface (m²)        | MC tool                | Enter value [Required]  |
+-------------------------------------+------------------------+-------------------------+
| Device dry area surface (m²)        | MC tool                | Enter value [Required]  |
+-------------------------------------+------------------------+-------------------------+
| Ressource reduction                 | EC tool                | Enter value [Required]  |
+-------------------------------------+------------------------+-------------------------+
| Device used materials(kg)           | MC tool                | Enter value [Required]  |
+-------------------------------------+------------------------+-------------------------+
| Device recycled materials (kg)      | Enter value [Required]                           |
+-------------------------------------+--------------------------------------------------+
| Measured noise around device        | Enter value [Required]                           |
|                                     |                                                  |
| (dB re 1 u Pa)                      |                                                  |
+-------------------------------------+--------------------------------------------------+
| Measured turbidity due to device    | Enter value [Required]                           |
|                                     |                                                  |
| installation (mg/L)                 |                                                  |
+-------------------------------------+------------------------+-------------------------+
| Foundation used materials(kg)       | SK tool                | Enter value [Required]  |
+-------------------------------------+------------------------+-------------------------+
| Foundation recycled materials (kg)  | Enter value [Required]                           |
+-------------------------------------+------------------------+-------------------------+
| Foundation footprint (m²)           | SK tool                | Enter value [Required]  |
+-------------------------------------+------------------------+-------------------------+
| Foundation colonisable surface (m²) | SK tool                | Enter value [Required]  |
+-------------------------------------+------------------------+-------------------------+
| Measured noise around foundation    | Enter value [Required]                           |
|                                     |                                                  |
| (dB re 1 u Pa)                      |                                                  |
+-------------------------------------+--------------------------------------------------+

**Electrical info :**

+-----------------------------------------+------------------------+-------------------------+
|             Input parameter             | Integrated mode source |     Standalone mode     |
+=========================================+========================+=========================+
| Annual energy produced (kWh)            | ED tool                | Enter value [Required]  |
+-----------------------------------------+------------------------+-------------------------+
| Colonisable surface of electrical       | ED tool                | Enter value [Required]  |
|                                         |                        |                         |
| parts (m²)                              |                        |                         |
+-----------------------------------------+------------------------+-------------------------+
| Footprint of electrical parts (m²)      | ED tool                | Enter value [Required]  |
+-----------------------------------------+------------------------+-------------------------+
| Substation height (m)                   | ED tool                | Enter value [Required]  |
+-----------------------------------------+------------------------+-------------------------+
| Substation width (m)                    | ED tool                | Enter value [Required]  |
+-----------------------------------------+------------------------+-------------------------+
| Substation length (m)                   | ED tool                | Enter value [Required]  |
+-----------------------------------------+------------------------+-------------------------+
| Coordinates of substation (UTM)         | ED tool                | Enter value [Required]  |
+-----------------------------------------+------------------------+-------------------------+
| Cable buried                            | ED tool                | Select value [Required] |
+-----------------------------------------+------------------------+-------------------------+
| Fishing restriction area (m²)           | Enter value [Required]                           |
+-----------------------------------------+------------------------+-------------------------+
| Electrical parts used materials (kg)    | ED tool                | Enter value [Required]  |
+-----------------------------------------+------------------------+-------------------------+
| Electrical  parts recycled              | Enter value [Required]                           |
|                                         |                                                  |
| materials (kg)                          |                                                  |
+-----------------------------------------+--------------------------------------------------+
| Measured noise around cables            | Enter value [Required]                           |
|                                         |                                                  |
| (dB re 1 u Pa)                          |                                                  |
+-----------------------------------------+--------------------------------------------------+
| Measured electrical fields(V/m)         | Enter value [Required]                           |
+-----------------------------------------+--------------------------------------------------+
| Measured magnetic fields(µT)            | Enter value [Required]                           |
+-----------------------------------------+--------------------------------------------------+
| Measured temperature around cables (°C) | Enter value [Required]                           |
+-----------------------------------------+--------------------------------------------------+

**Logistic info :**

+------------------------------------------+------------------------+------------------------+
| Input parameter                          | Integrated mode source | Standalone mode        |
+==========================================+========================+========================+
| Installation phase number of vessels     | LMO tool               | Enter value [Required] |
+------------------------------------------+------------------------+------------------------+
| Installation phase mean size             | LMO tool               | Enter value [Required] |
|                                          |                        |                        |
| of vessels (loa) (m)                     |                        |                        |
+------------------------------------------+------------------------+------------------------+
| Installation phase number of passenger   | LMO tool               | Enter value [Required] |
|                                          |                        |                        |
| on all vessels                           |                        |                        |
+------------------------------------------+------------------------+------------------------+
| Installation phase measured noise        | Enter value [Required]                          |
| (dB re 1 u Pa)                           |                                                 |
+------------------------------------------+-------------------------------------------------+
| Installation phase measured turbidity    | Enter value [Required]                          |
| (mg/L)                                   |                                                 |
+------------------------------------------+-------------------------------------------------+
| Installation phase chemical pollution    | Select from list [Required]                     |
+------------------------------------------+------------------------+------------------------+
| Installation phase total fuel            | LMO tool               | Enter value [Required] |
|                                          |                        |                        |
| consumption (kg)                         |                        |                        |
+------------------------------------------+------------------------+------------------------+
| Exploitation phase number of vessels     | LMO tool               | Enter value [Required] |
+------------------------------------------+------------------------+------------------------+
| Exploitation phase mean size             | LMO tool               | Enter value [Required] |
|                                          |                        |                        |
| of vessels (loa) (m)                     |                        |                        |
+------------------------------------------+------------------------+------------------------+
| Exploitation phase number of passenger   | LMO tool               | Enter value [Required] |
|                                          |                        |                        |
| on all vessels                           |                        |                        |
+------------------------------------------+------------------------+------------------------+
| Exploitation phase measured noise        | Enter value [Required]                          |
| (dB re 1 u Pa)                           |                                                 |
+------------------------------------------+-------------------------------------------------+
| Exploitation phase measured turbidity    | Enter value [Required]                          |
| (mg/L)                                   |                                                 |
+------------------------------------------+-------------------------------------------------+
| Exploitation phase chemical pollution    | Select from list [Required]                     |
+------------------------------------------+------------------------+------------------------+
| Exploitation phase total fuel            | LMO tool               | Enter value [Required] |
|                                          |                        |                        |
| consumption (kg)                         |                        |                        |
+------------------------------------------+------------------------+------------------------+
| Decommissioning phase number of vessels  | LMO tool               | Enter value [Required] |
+------------------------------------------+------------------------+------------------------+
| Decommissioning phase mean size          | LMO tool               | Enter value [Required] |
|                                          |                        |                        |
| of vessels (loa) (m)                     |                        |                        |
+------------------------------------------+------------------------+------------------------+
| Decommissioning phase number of          | LMO tool               | Enter value [Required] |
|                                          |                        |                        |
| passenger on all vessels                 |                        |                        |
+------------------------------------------+------------------------+------------------------+
| Decommissioning phase measured noise     | Enter value [Required]                          |
| (dB re 1 u Pa)                           |                                                 |
+------------------------------------------+-------------------------------------------------+
| Decommissining phase measured            | Enter value [Required]                          |
|                                          |                                                 |
| turbidity mg/L)                          |                                                 |
+------------------------------------------+-------------------------------------------------+
| Decommissioning phase chemical pollution | Select from list [Required]                     |
+------------------------------------------+------------------------+------------------------+
| Decommissioning phase total fuel         | LMO tool               | Enter value [Required] |
|                                          |                        |                        |
| consumption (kg)                         |                        |                        |
+------------------------------------------+------------------------+------------------------+

2. Run the assessment and view results
--------------------------------------

- Run the assessment by clicking on ``Run module``

- if the run calculation is successful, results page of Endangered species will appear and details, by the five classes of species, the taxonomics information of potentialy present endangered species and recommandations on surveys and mitigation measures
- Click on the ``Environmental Impact Assessment`` to visualize impact results in terms of pressure/receptors:
- Click on the ``Carbon Footprint`` to visualize the results of the life cycle assessment
- Click on the ``Social Acceptance`` to visualize relevant information for the social acceptance of the project
