.. _esa-how-to:

*************
How-to Guides
*************

Each *how-to guide* listed below contains step-by-step instructions on how to achieve specific outcomes using the Environmental and Social Acceptance module. 
These guides are intended for users who have previously completed all the :ref:`Environmental and Social Acceptance tutorials <esa-tutorials>` and have a good knowledge of the features and workings of the Environmental and Social Acceptance module. 
While the tutorials give an introduction to the basic usage of the module, these *how-to guides* tackle slightly more advanced topics.

#. :ref:`How to choose between the complexity level in ESA <how-to-choose-complexity>`
#. :ref:`How to use ESA  at low complexity <how-to-low-complexity>`
#. :ref:`How to use ESA  at full complexity <how-to-full-complexity>`


.. toctree::
   :maxdepth: 1
   :hidden:

   how_to_choose_complexity
   how-to-low-complexity
   how-to-full-complexity
   


