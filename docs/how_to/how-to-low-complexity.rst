.. _how-to-low-complexity:

How to use Environmental and Social Acceptance at low complexity
================================================================


To get information on the  potential presence of endangered species and get  recommendations on mitigation  measures  to  lower  the  main  risks  associated  with  the  implementation use  the  low complexity (level 1)  version of the Environmental and Social Acceptance module. This assumes the user has information only on the coordinates of implementation.

1. Enter inputs
----------------------

The inputs at low complexity are grouped into one section: 
Farm info, there are no Device, Electrical or Logistics inputs.

**Farm Info :**

+----------------------------+------------------------+------------------------------+
| Input parameter            | Integrated mode source | Standalone mode              |
+============================+========================+==============================+
| Coordinates of the farm    | SC tool                | Enter value **[Required]**   |
|                            |                        |                              |
| (Decimal degre)            |                        |                              |
|                            |                        |                              |
+----------------------------+------------------------+------------------------------+
| Name of protected species  | --                     | Enter latin name and Class   |
|                            |                        |                              |
| identified in the area     |                        | of the species **[Optional]**|
+----------------------------+------------------------+------------------------------+

- Click ``Next page`` 
- If successful, **inputs summary** page will appear and inform you if farm info is complete
- if not, go back to fill the **Farm info** input page

2. Run the assessment and view results
--------------------------------------

- Click ``Run module``
- If the run calculation is successful, results page of Endangered species will appear and detail by the five classes of species:

   - Taxonomic information
   - Main associated risks
   - Recommendations on mitigation measures and surveys

.. toctree::
   :maxdepth: 1
   :hidden: